#include "tester/load_data.h"

#include "util/stl_algorithms.hpp"
#include "util/contains.hpp"
#include "util/exception.h"
#include "util/type_name.hpp"
#include "util/macro.h"
#include "util/string.hpp"
#include "util/miscellany.hpp"
#include "util/poor_mans_reflection.h"
#include "util/endianness.h"
#include "util/builtins.hpp"

#include <ios>
#include <iostream>
#include <array>
#include <iterator>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <string>
#include <algorithm>

// Google the following define, it's necessary
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/utility/string_ref.hpp> // we don't have the GSL string_view, and we need to iterate over a char* buffer

// See http://stackoverflow.com/a/35304501/1593077 ;
// required so that we may be able to use the FileFormat in unordered maps
namespace std {
  template <> struct hash<kernel_tests::data_loading::FileFormat> {
	size_t operator() (const kernel_tests::data_loading::FileFormat& t) const noexcept { return size_t(t); }
  };
}

namespace fs = boost::filesystem;

namespace kernel_tests {
namespace data_loading {


bool format_is_supported(FileFormat format)
{
	static const std::vector<FileFormat> unsupported_formats{ FileFormat::json };
	return !util::contains(unsupported_formats, FileFormat(format));
}

bool can_be_multirecord(FileFormat format)
{
	static const std::vector<FileFormat> potentially_multirecord =
		{ FileFormat::json, FileFormat::separator_delimited, FileFormat::csv };
	return util::contains(potentially_multirecord, format);
}

bool can_be_multirecord(const std::string& file_format_name) {
	try {
		auto format = FileFormat::_from_string(file_format_name.c_str());
		return can_be_multirecord(format);
	} catch(std::exception& e) {
		throw util::invalid_argument(
			std::string("No file format named \"") + file_format_name + "\"");
	}
}

// TODO: Can the tokenizer be const?
template<typename T, typename token_iterator_generator>
size_t load_from_boost_tokenized_line(
	boost::tokenizer<token_iterator_generator>&
		                 line_tokenizer,
	T* __restrict__      destination,
	size_t               allocated_length,
	const Configuration  config,
	size_t               line_number)
{
	try {
		if (config.elements_are_within_records) {

			// a "line" in this case is one record, and we only read one element of it;
			// we will not apply ay line-skipping/element-skipping logic in this case

			auto it = line_tokenizer.begin();
			// it = util::safe_advance(line_tokenizer.begin(), line_tokenizer.end(), config.column_index);
			std::advance(it, config.column_index);

			// We can't use:
			// destination[0] = boost::lexical_cast<T>(*it);
			// here, since this triggers a faux GCC warning, see:
			// https://svn.boost.org/trac/boost/ticket/4946
			// so we copy the appropriate (I hope) implementation of lexical_cast here
			// and specifically default-initialize
			T result {};
	        if (!boost::conversion::detail::try_lexical_convert(*it, result)) {
	            boost::conversion::detail::throw_bad_cast<decltype(*it), T>();
	        }
			destination[0] = result;
			return 1;
		}

		// Input is single-line

		size_t pos = 0;
		auto it = line_tokenizer.begin();
		std::advance(it, config.num_elements_to_skip);
		do {
			// std::cout << "At pos " << pos << " we have token \"" << *it << "\"\n" << std::flush;

			// We can't use:
			// destination[pos++] = boost::lexical_cast<T>(*it);
			// here, since this triggers a faux GCC warning, see:
			// https://svn.boost.org/trac/boost/ticket/4946
			// so we copy the appropriate (I hope) implementation of lexical_cast here
			// and specifically default-initialize
			T result {};
	        if (!boost::conversion::detail::try_lexical_convert(*it, result)) {
	            boost::conversion::detail::throw_bad_cast<decltype(*it), T>();
	        }
			destination[pos++] = result;
		} while (
			it++ != line_tokenizer.end() and
			pos < config.desired_num_elements and
			pos < allocated_length);
		size_t num_read = pos;
		return num_read;
	} catch(std::exception& e) {
		throw util::runtime_error(
			util::concat_string(
				"Failed reading from boost-tokenized line #", line_number, " : ", e.what()));
	}
}

template<typename T>
size_t load_from_single_line_buffer(
	const char* __restrict__  line,
	size_t                    line_length,
	T* __restrict__           destination,
	size_t                    allocated_length,
	const Configuration       config,
	size_t                    line_number)
{
	using namespace boost;
	static const char* WhiteSpaceCharacters = " \t\n\r\f\v";
	const boost::string_ref line_ref(line, line_length); // ugh.

	// TODO: Move this code outside and pass the tokenizer in, to avoid
	// generating it repeatedly
	switch(config.format) {
	case FileFormat::csv: {
		tokenizer<escaped_list_separator<char> > line_tokenizer(
			line_ref, escaped_list_separator<char>('\\', ',', '\"'));
		return load_from_boost_tokenized_line(
			line_tokenizer, destination, allocated_length, config, line_number);
	}
	case FileFormat::whitespace_delimited: {
		// std::cout << "whitespace-delimited!\n" << std::flush;
		tokenizer<char_separator<char> > tk(line_ref, char_separator<char>(WhiteSpaceCharacters));
		return load_from_boost_tokenized_line(tk, destination, allocated_length, config, line_number);
	}
	case FileFormat::separator_delimited: {
		std::string dropped_tokens; dropped_tokens += config.element_delimiter;
		// std::cout << "dropped tokens:" << dropped_tokens << '\n';
		tokenizer<char_separator<char> > tk(
			line_ref, char_separator<char>(dropped_tokens.c_str(), nullptr, keep_empty_tokens));
		return load_from_boost_tokenized_line(tk, destination, allocated_length, config, line_number);
	}
	default:
		throw util::logic_error("Unsupported data file format for a line buffer");
	}

}

template<typename T>
void byte_swap_or_fail(T&) {
	throw util::logic_error(
		std::string("Cannot perform an endianness-changing byte swap on elements of type ") + util::type_name<T>());
}

template<> void byte_swap_or_fail<uint8_t >(uint8_t&  t) { t = util::builtins::byte_swap<uint8_t >(t); }
template<> void byte_swap_or_fail<uint16_t>(uint16_t& t) { t = util::builtins::byte_swap<uint16_t>(t); }
template<> void byte_swap_or_fail<uint32_t>(uint32_t& t) { t = util::builtins::byte_swap<uint32_t>(t); }
template<> void byte_swap_or_fail<uint64_t>(uint64_t& t) { t = util::builtins::byte_swap<uint64_t>(t); }

template<typename T>
size_t binary_load_from_ifstream(
	std::ifstream&       stream,
	size_t               stream_data_length,
	T* __restrict__      destination,
	size_t               allocated_length,
	const Configuration  config)
{
	size_t octets_to_read =
		std::min(sizeof(T) * config.desired_num_elements, stream_data_length);
	size_t elements_to_read = octets_to_read / sizeof(T);
	util::enforce(octets_to_read % sizeof(T) == 0,
		util::concat_string(
			"File size is not a multiple of sizeof(",util::type_name<T>(),
			") = ", sizeof(T), "."));
	util::enforce(config.desired_num_elements <= allocated_length,
		"Not enough space allocated for the desired number of elements from the file");
	stream.read(reinterpret_cast<char*>(destination), octets_to_read);
	// TODO: Don't I need to check here that the correct number of elements
	// has been read?

	if ((config.format == FileFormat::binary_little_endian
		 && util::compilation_target_endianness() == util::endianness_t::big) ||
		(config.format == FileFormat::binary_big_endian
		 && util::compilation_target_endianness() == util::endianness_t::little)
		) {
//			// TODO: Reverse byte order for all elements
//			throw util::logic_error("Not yet supporting switching the endianness");
		for(T* it = destination; it < destination + elements_to_read; it++) {
			byte_swap_or_fail(*it);
		}
	}

	return elements_to_read;
}

template<typename T>
size_t load_from_ifstream(
	std::ifstream&       stream,
	size_t               stream_data_length,
	T* __restrict__      destination,
	size_t               allocated_length,
	const Configuration  config)
{
	util::enforce(format_is_supported(config.format), "Unsupported load configuration");

	if (config.format == FileFormat::binary
		|| (config.format == FileFormat::binary_little_endian
		    && util::compilation_target_endianness() == util::endianness_t::little)) {
		binary_load_from_ifstream(stream, stream_data_length, destination, allocated_length, config);
	}

	// We now think of the (textual) file as being in one of two formats:
	//
	// * Only values we are interested in, separated by a (single kind of) delimiter
	// * Multiple records (=lines), each one having multiple fields (i.e. multi-column line),
	//   with a distinct field separator and record separator
	//
	// As we only support getting a single column from a file, we do not need to consider
	// the possibility of reading multiple fields from each record

	if (!config.elements_are_within_records) {
		std::vector<char> buffer(stream_data_length);
		stream.read((char *)&(buffer[0]), stream_data_length);
		return load_from_single_line_buffer(
			&buffer[0], stream_data_length, destination, allocated_length, config);
	}

	std::string line;
	size_t total_read = 0;
	size_t __attribute((unused)) line_number = 1;
	auto residual_config = config;
	// We should not need this, but careless and unchecked load_config construction makes it necessary:
	char record_delimiter = (config.format == FileFormat::csv) ? '\n' : config.record_delimiter;
	// also note that this is not the right way to break up CSVs into lines - since they may
	// have newlines within double-quoted fields, plus, the record separator is supposed to
	// be CR LF, not just LF. Oh well.
	while (
		total_read < config.desired_num_elements &&
		getline(stream, line, record_delimiter))
	{
		size_t num_read_from_line =
			load_from_single_line_buffer(
				line.c_str(), line.length(),
				destination, allocated_length, residual_config, line_number);
		residual_config.desired_num_elements -= num_read_from_line;
		destination += num_read_from_line;
		allocated_length -= num_read_from_line;
		total_read += num_read_from_line;
		line_number++;
	}
	return total_read;
}


template <typename T>
size_t load_from_file(
	const std::string&   source_filename,
	T* __restrict__      destination,
	size_t               allocated_length,
	const Configuration  config)
{
	static std::unordered_map<FileFormat, std::ios_base::openmode> file_open_modes = {
		{ FileFormat::csv,                  std::ios::in     },
		{ FileFormat::json,                 std::ios::in     },
		{ FileFormat::whitespace_delimited, std::ios::in     },
		{ FileFormat::separator_delimited,  std::ios::in     },
		{ FileFormat::binary,               std::ios::binary },
		{ FileFormat::binary_big_endian,    std::ios::binary },
		{ FileFormat::binary_little_endian, std::ios::binary },
	};

	util::enforce(format_is_supported(config.format), "Unsupported load configuration");
	fs::path p(source_filename);
	util::enforce(fs::exists(p), "File " + source_filename + " not found.");
	// std::cout << "File " << source_filename  << " found.\n";
	size_t size_of_file = fs::file_size(p);
	// std::cout << "Size of " << source_filename << " is " << size_of_file << '\n';

	std::ifstream file(source_filename, file_open_modes.at(config.format));
	return load_from_ifstream(file, size_of_file, destination, allocated_length, config);
}

// Since this file is not a header, we need to explicitly
// instantiate the load_data functions for all relevant types.

#define INSTANTIATE_LOAD_FROM_FILE(...) INSTANTIATE_WITH_SPECIFIC_TEMPLATE_ARGUMENTS(load_from_file, __VA_ARGS__)

MAP(INSTANTIATE_LOAD_FROM_FILE, ALL_NUMERIC_TYPES_NO_DUPES)

} // namespace data_loading
} // namespace kernel_tests

std::ostream& operator<< (std::ostream& os, const ::kernel_tests::data_loading::Configuration& c)
{
	using namespace kernel_tests::data_loading;

	// TODO: Only print fields which have effective meaning with the current format

	os	<< "Configuration["
		<<   "format"                      << " = " << FileFormat(c.format)
		<< ", column_index (0-based)"      << " = " << c.column_index
		<< ", element_delimiter"           << " = " << '\'' << util::sanitize(c.element_delimiter,0) << '\''
		<< ", record_delimiter"            << " = " << '\'' << util::sanitize(c.record_delimiter,0) << '\''
		<< ", elements_are_within_records" << " = " << c.elements_are_within_records
		<< ", num_elements_to_skip"        << " = " << c.num_elements_to_skip
		<< ", desired_num_elements"        << " = " << c.desired_num_elements
		<< "]";
	return os;
}

