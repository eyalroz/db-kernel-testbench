#pragma once
#ifndef KERNEL_TESTS_BUFFERDESCRIPTOR_H_
#define KERNEL_TESTS_BUFFERDESCRIPTOR_H_

#include "DualAspectBuffer.h"

#include "util/miscellany.hpp"
#include "util/memory_region_extra.h"
#include "util/type_name.hpp"

#include "util/optional.hpp"

#include <string>
#include <unordered_map>

namespace kernel_tests {

using cuda::Aspect;

template <typename T>
using optional = util::optional<T>;

using util::nullopt;

enum class BufferDirection: unsigned {
	Input, Output, InOut
};

inline bool is_input (BufferDirection d) { return d == BufferDirection::Input || d == BufferDirection::InOut;  }
inline bool is_output(BufferDirection d) { return d == BufferDirection::InOut || d == BufferDirection::Output; }

using util::memory_region;

template <Aspect>  struct SingleAspectBufferTrait;

template <>   struct SingleAspectBufferTrait<Aspect::HostSide> {
	using type  = memory_region;
};
template <>   struct SingleAspectBufferTrait<Aspect::DeviceSide> {
	using type  = memory_region;
};

//template <> using SingleAspectBuffer<Aspect::DeviceSide>  = memory_region;
// using SingleAspectBuffer = memory_region;
template <Aspect WhichAspect> using Buffers = std::unordered_map<std::string, typename SingleAspectBufferTrait<WhichAspect>::type>;
using HostBuffers   = Buffers<Aspect::HostSide>;
using DeviceBuffers = Buffers<Aspect::DeviceSide>;

class TestConfiguration;

class BufferDescriptor {
public:
	/**
	 * This is the default option for checking whether an expected and an actual
	 * output buffer match. It will simply compare their bytes in memory, in order.
	 * Sometimes it's useful to specify something more involved (e.g. when element
	 * order in the buffer does not matter).
	 *
	 * @param config Configuration of the kernel test used to generate the actual output buffer
	 * @param descriptor information about both the actual and expected buffers
	 * @param expected the expected output buffer
	 * @param actual the actual output buffer produced by a test run of a kernel
	 * @return true if the buffers match, i.e. if the actual output buffer is a valid alternative
	 * to the exact expected output buffer; false otherwise
	 */
	static bool compareMemoryRepresentations(
		const TestConfiguration&  config,
		const HostBuffers&        inputs               __attribute((unused)),
		const HostBuffers&        all_actual_outputs   __attribute((unused)),
		const HostBuffers&        all_expected_outputs __attribute((unused)),
		const BufferDescriptor&   descriptor,
		const memory_region       actual,
		const memory_region       expected);

	template <typename T>
	static bool trivial_comparator(
		const TestConfiguration&  config,
		const HostBuffers&        all_inputs           __attribute((unused)),
		const HostBuffers&        all_actual_outputs   __attribute((unused)),
		const HostBuffers&        all_expected_outputs __attribute((unused)),
		const BufferDescriptor&   descriptor,
		const memory_region       actual,
		const memory_region       expected);

	static void trivialMismatchPrinter(
		std::ostream&             os,
		const TestConfiguration&  config,
		const std::string&        buffer_name,
		const HostBuffers&        inputs               __attribute((unused)),
		const HostBuffers&        all_actual_outputs   __attribute((unused)),
		const HostBuffers&        all_expected_outputs __attribute((unused)),
		const BufferDescriptor&   descriptor,
		const memory_region       actual,
		const memory_region       expected);

	using MatchChecker = decltype(&trivial_comparator<int>);
	using MismatchPrinter = decltype(&trivialMismatchPrinter);

	static decltype(&trivial_comparator<int>) get_trivial_comparator(const char* type_name);


	// constructors

	BufferDescriptor(size_t size_, BufferDirection direction_, const std::string& data_type_,
		optional<MatchChecker> match_checker_ = nullopt,
		optional<MismatchPrinter> mismatch_printer_ = nullopt) :
		capacity(size_), direction(direction_), data_type(data_type_),
		match_checker(match_checker_.value_or(get_trivial_comparator(data_type.c_str()))),
		mismatch_printer(mismatch_printer_.value_or(trivialMismatchPrinter)) { }

	BufferDescriptor(const BufferDescriptor& other) = default;

	// named constructor idioms

	template<typename Datum>
	static BufferDescriptor make(size_t capacity_in_elements, BufferDirection direction,
		optional<MatchChecker> match_checker_ = nullopt,
		optional<MismatchPrinter> mismatch_printer_ = nullopt)
	{
		return BufferDescriptor(
			capacity_in_elements * sizeof(Datum), direction,
			util::type_name<Datum>(), match_checker_, mismatch_printer_);
	}

	// operators and other methods

	BufferDescriptor& operator=(const BufferDescriptor& other) = default;

	bool is_input()  const { return ::kernel_tests::is_input (direction); }
	bool is_output() const { return ::kernel_tests::is_output(direction); }

	// Some builder-pattern methods

	BufferDescriptor& bit_container(bool preference = true)
	{
		dump_bits = preference;
		return *this;
	}
	BufferDescriptor& print_as_hexadecimal(bool preference = true)
	{
		prefer_hexadecimal_printing = preference;
		return *this;
	}
	BufferDescriptor& difference_value_is_meaningful(bool preference = true)
	{
		difference_value_is_meaningful_ = preference;
		return *this;
	}
	BufferDescriptor& difference_value_is_meaningless(bool preference = true)
	{
		difference_value_is_meaningful_ = !preference;
		return *this;
	}
	BufferDescriptor& ignore_difference_value(bool preference = true)
	{
		difference_value_is_meaningful_ = !preference;
		return *this;
	}

	// data members

	size_t           capacity; // in bytes
	// Note: the buffer's actual size is _not_ part of the descriptor.
	BufferDirection  direction;
	std::string      data_type;
	MatchChecker     match_checker;
	MismatchPrinter  mismatch_printer;
	bool             prefer_hexadecimal_printing { false };
	bool             dump_bits { false };
	bool             difference_value_is_meaningful_ { false };
	// TODO: Add some more flags/options, e.g. printing as a sequence of bits
};

} // namespace kernel_tests


inline std::ostream& operator<<(std::ostream& os, const kernel_tests::BufferDirection& dir)
{
	switch(dir) {
	case kernel_tests::BufferDirection::InOut:  os << "InOut";  break;
	case kernel_tests::BufferDirection::Input:  os << "Input";  break;
	case kernel_tests::BufferDirection::Output: os << "Output"; break;
	}
	return os;
}

inline std::ostream& operator<<(std::ostream& os, const kernel_tests::BufferDescriptor& d)
{
	os
		<< "BufferDescriptor[capacity: " << d.capacity << "; direction: " << d.direction << "; data_type: " << d.data_type
		<< "; (match_checker); (mismatch_printer); " << "prefer_hexadecimal_printing: " << d.prefer_hexadecimal_printing << "; dump_bits: "
		<< d.dump_bits << "difference_value_is_meaningful_:" << d.difference_value_is_meaningful_ << "]";
	return os;
}


#endif /* KERNEL_TESTS_BUFFERDESCRIPTOR_H_ */
