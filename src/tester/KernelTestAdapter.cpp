#include "tester/KernelTestAdapter.h"
#include "util/contains.hpp"
#include "tester/load_data.h"
#include "util/miscellany.hpp"
#include "util/exception.h"
#ifdef DEBUG
#include "util/string.hpp"
#endif
#include "util/poor_mans_reflection.h"

#include <cuda/api/miscellany.hpp>

#include <boost/iterator.hpp>

#include <ctime>
#include <cstring>
#include <type_traits>
#include <vector>
// We can't use the standard C++ library's <regex>, since
// CUDA constrains us to GCC 4.x , and even 4.9.3 doesn't
// have a properly-working regex implementation - specifically,
// supporting back-references. So, Boost it is.
#include <boost/regex.hpp>

using std::string;

namespace kernel_tests {

KernelTestAdapter::KernelTestAdapter(const TestConfiguration& config)
	: config(config), random_device(), random_engine(random_device()) {
	random_engine.seed(config.seed_by_time ? std::time(0) : config.random_seed);
}

std::unique_ptr<KernelTestAdapter> KernelTestAdapter::produceAdapter(const TestConfiguration& config)
{
	try {
		return produceSubclass(config);
	}
	catch(std::runtime_error& e) {
		util::die(e, "Test adapter factory failed to produce an adapter using the specified configuration.");
	}
	catch(std::invalid_argument& e) {
		util::die(util::concat_string("No test adapter named ", config.test_adapter_name,
			" is registered which can be produced with the specified configuration"));
	}
	// Can't get here, but let's please IDE parsers anyway
	return nullptr;
}


std::string KernelTestAdapter::massage_type_name(const std::string& s)
{
	// Remove kernel_tests prefix

#ifdef DEBUG
	util::enforce(util::begins_with(s, "kernel_tests::"), "Suspicious kernel test adapter class name \"" + s + "\"");
#endif
	std::string result(util::substr_after(s, "kernel_tests::"));

	// For functors with the same argument and return types, just list
	// one of the types - the same as how they can be instantiated
	boost::regex default_final_template_param("cuda::functors::([a-z_0-9]+)<([^>]+), \\2, \\2>");
	result = boost::regex_replace(result, default_final_template_param, "cuda::functors::\\1<\\2>");

	util::replace_all(result, "cuda::", "");
	util::replace_all(result, "functors::", "");

	boost::regex unsigned_number_suffix("([0-9]+)u(,|>)");
	result = boost::regex_replace(result, unsigned_number_suffix, "\\1\\2");

	return result;
}

KernelTestAdapter::~KernelTestAdapter() { }

template<class K, class V>
static void print_sorted_keys(std::unordered_map<K,V> const& map, std::ostream& os, const std::string& separator){
  auto keys = util::get_keys(map);
  std::sort(keys.begin(), keys.end());
  util::print_separated(os, separator, keys.cbegin(), keys.cend());
}

void KernelTestAdapter::printMismatches(
	const HostBuffers & inputs,
	const HostBuffers & actual_outputs,
	const HostBuffers & expected_outputs) const
{
	UNUSED(inputs);
	UNUSED(actual_outputs);
	UNUSED(expected_outputs);
	throw util::logic_error(
		"Kernel test adapter for " + getName() +
		" does not support printing mismatches.");
}

LaunchConfigurationSequence KernelTestAdapter::resolveLaunchConfigurations() const
{
	throw util::logic_error(
		"Kernel test adapter for " + getName() +
		" cannot report/resolve launch configuration(s) a-priori.");
}

template <typename T>
void KernelTestAdapter::readBuffer(
	const std::string& buffer_name, T* buffer, size_t length,
	util::optional<unsigned> column_index) const
{
	util::enforce(has_value(config.input_file),
		"Attempt to read file data when no filename specified");
	util::enforce(has_value(config.input_file_format),
		"Attempt to read file data when no file format specified");
	util::enforce(data_loading::FileFormat::_is_valid(config.input_file_format.value().c_str()),
		"Invalid input format \"", config.input_file_format.value(), "\"");

	data_loading::Configuration load_config;
	load_config.elements_are_within_records = column_index.operator bool();
	if (column_index) { load_config.column_index = column_index.value(); }
	load_config.format =
		data_loading::FileFormat::_from_string(config.input_file_format.value().c_str());
	if (load_config.format == data_loading::FileFormat::separator_delimited) {
		// Some defaults. TODO: Get these from config itself
		util::enforce(has_value(config.input_file_element_delimiter),
			"Attempt to read from a separator-delimited input file with no element delimiter specified");
		util::enforce(has_value(config.input_file_record_delimiter),
			"Attempt to read from a separator-delimited input file with no record delimiter specified");
		load_config.element_delimiter = config.input_file_element_delimiter.value();
		load_config.record_delimiter = config.input_file_record_delimiter.value();
	}
	load_config.num_elements_to_skip = 0; // TODO: Support this not being 0
	load_config.desired_num_elements = length;

	if (config.be_verbose) {
		std::cout << "Will now read " << length << " elements of type "
			<< util::type_name<T>() << " into buffer \"" << buffer_name
			<< "\" from file " << config.input_file << '\n'
			<< "Loading configuration: " << load_config << '\n';
	}

	auto num_read = data_loading::load_from_file(
		config.input_file.value(), buffer, length, load_config);

	if (config.be_verbose) {
		std::cout << "Have read " << num_read << " elements of type "
			<< util::type_name<T>() << " into buffer \"" << buffer_name
			<< "\" from file " << config.input_file.value() << " (with file format: "
			<< config.input_file_format.value() << ").\n";
	}

	util::enforce(num_read == length,
		util::concat_string("Could only read ", num_read, " elements (of type ",
			util::type_name<T>(), ") from file ", config.input_file,
			" - but ", length, " elements were required"));
}

bool KernelTestAdapter::need_to_generate(const std::string& buffer_name) const
{
	return util::contains(forced_generation_buffers, buffer_name) or
		not util::contains(config.db_column_mappings, buffer_name);
}


#define INSTANTIATE_READ_BUFFER(_type) \
template void KernelTestAdapter::readBuffer< _type >( \
	const std::string& buffer_name, _type * buffer, size_t length, \
	util::optional<unsigned> column_index) const; \

MAP(INSTANTIATE_READ_BUFFER, ALL_NUMERIC_TYPES_NO_DUPES)

} // namespace kernel_tests

namespace util {
namespace mixins {
// TODO:
// - Support having multiple types specified in the configuration file - or no types
// - Use some kind of canonicalization of the string or whatever...
template<>
string factory_producible_t<string, kernel_tests::KernelTestAdapter, const kernel_tests::TestConfiguration&>
	::resolveSubclassKey(const kernel_tests::TestConfiguration& config)
{
	// Note this must be compatible with the buildRegistrationKey()
	return config.test_adapter_name;
}
} // namespace mixins
} // namespace util
