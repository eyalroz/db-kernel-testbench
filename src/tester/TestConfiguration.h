// FIXME: Ignoring custom command-line option when we see a config file
#pragma once
#ifndef _TEST_CONFIGURATION_H
#define _TEST_CONFIGURATION_H

#include "util/better_enums.hpp"

#include "util/random.h"
#include "util/optional.hpp"
#include <unordered_map>

namespace kernel_tests {

using util::optional;
using util::nullopt;

BETTER_ENUM(tester_action_t, char,
	conduct_a_test,
	list_platforms_and_devices,
	list_device_properties,
	list_test_adapters,
	list_kernels,
	describe_a_kernel_test,
	print_number_of_kernels_in_a_test,
	print_test_configuration
)

class TestConfiguration {

public:
	enum { pretty_printing_name_column_width = 50 };

	// A kludge, due to our currently not being able to pass additional
	// options for more parsing by boost program_options (as CUDA doesn't like
	// it - perhaps due to GCC 4-v-5 issues, who knows)
	using ExtraOptions = std::unordered_map<std::string, std::string>;
	using DBInputMappings = std::unordered_map<std::string, std::string>;

	tester_action_t   tester_action;
	std::string       config_file; // kept here in case we want to print it out
	std::string       backend; /// @ref backends::backend_t

	optional<unsigned> execution_platform; /// @ref backends::platform_id_t
	unsigned          test_device; // not using the CUDA/backends type to avoid dependency...
	optional<unsigned> max_threads_per_block; // not using the CUDA type to avoid dependency...
	optional<unsigned short> thread_serialization_factor;
	bool              use_mapped_region_pairs; // use mapped host-and-device region pair allocation
	size_t            test_length; // Can actually mean different things for different kernels,
	                               // but typically this is the input of the longest buffer, input or
	                               // output or both
	size_t            num_test_runs;    // Number of test runs (with the same parameters) to perform; repeating
	                               // kernel tests is sometimes important for gather statistics
	                               // (and even for smoothing out low-probability events)
	bool              use_absolute_difference_for_floating_point;
	double            floating_point_equality_threshold;
	optional<unsigned>  floating_point_printing_precision;
	size_t            num_elements_to_print; // use 0 for no maximum; or switch this to std::optional
	size_t            start_printing_from;   // first element index in each array from which to print
	std::string       test_adapter_name;     // The actual name might differ, see next field...
	mutable // because it might be set using the time value, later on
	unsigned long long random_seed;
	optional<std::string>       monet_db_directory;
	optional<std::string>       input_file; // TODO: Maybe make this optional?
	optional<std::string>       input_file_format; // TODO: Maybe make this optional?
	optional<char>              input_file_element_delimiter;
	optional<char>              input_file_record_delimiter;

	bool              seed_by_time; // Seed the pseudo-random number generation using execution time
	                                // (which means you'll get different data with every run
	bool              zero_output_and_scratch_buffers;
	bool              assume_results_correct;

	bool              print_test_config;
	bool              print_inputs;
	bool              print_launch_details;
	bool              print_device_function_attributes;
	bool              print_outputs;
	bool              print_expected_outputs;
	bool              print_mismatches;
	bool              print_buffer_listing;
	bool              print_result;
	bool              be_verbose;
	mutable // ... because of later-phase parsing
	ExtraOptions      extra_options;
	DBInputMappings   db_column_mappings;


	TestConfiguration() :
		tester_action                     {+tester_action_t::conduct_a_test},
		config_file                       {""},
		backend                           { "CUDA" },
		execution_platform                {},
		test_device                       {0}, // again, not using the CUDA default, to avoid dependencies
		max_threads_per_block             {}, // should probably make this an std::optional, or have 0 mean no limit
		thread_serialization_factor       {},
		use_mapped_region_pairs           {false},
		test_length                       {111111},
		num_test_runs                     {1},
		use_absolute_difference_for_floating_point
		                                  {false},
		floating_point_equality_threshold {1e-5},
		floating_point_printing_precision {10},
		num_elements_to_print             {32},
		start_printing_from               {0},
		test_adapter_name                 {""},
		random_seed                       {0},
		monet_db_directory                {},
		input_file                        {},
		input_file_format                 {},
		input_file_element_delimiter      {},
		input_file_record_delimiter       {},
		seed_by_time                      {false},
		zero_output_and_scratch_buffers   {true},
		assume_results_correct            {false},
		print_test_config                 {true},
		print_inputs                      {false},
		print_launch_details              {false},
		print_device_function_attributes  {false},
		print_outputs                     {false},
		print_expected_outputs            {false},
		print_mismatches                  {false},
		print_buffer_listing              {false},
		print_result                      {true},
		be_verbose                        {false},
		extra_options                     {},
		db_column_mappings                 {}
	{ }

	static TestConfiguration getFromProgramArguments(int argc, char** argv);
};

std::string pretty_print(const TestConfiguration& config);

} // namespace kernel_tests

#endif /* _TEST_CONFIGURATION_H */
