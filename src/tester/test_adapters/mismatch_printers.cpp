#include "tester/test_adapters/mismatch_printers.h"
#include "tester/parallel_cpu_sort.hpp"

#include "util/macro.h"
#include "util/stl_algorithms.hpp"
#include "util/contains.hpp"
#include "util/miscellany.hpp"
#include "util/string.hpp"
#include "util/dump.h"
#include "util/poor_mans_reflection.h"
#include "util/exception.h"

#include "kernel_wrappers/common.h"

#include <vector>
#include <unordered_set>
#include <algorithm>

#include <gsl/gsl-lite.hpp>

namespace kernel_tests {
namespace mismatch_printers {

using util::uint_t;

// This is _almost_ a generic set match checker, except for obtaining
// the lengths.
template <unsigned IndexSize>
void unsorted_sparse_output(
	std::ostream&             os,
	const TestConfiguration&  config,
	const std::string&        buffer_name,
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	using std::string;
	using index_type = uint_t<IndexSize>;
	using size_type = cuda::size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array

	util::ios_flags_saver ifs(os);
	auto num_actual_elements   = all_actual_outputs.at("num_selected").as_span<size_type>().data();
	auto num_expected_elements = all_expected_outputs.at("num_selected").as_span<size_type>().data();
	util::enforceNonNull(num_actual_elements);
	util::enforceNonNull(num_expected_elements);
	auto set_size = *num_expected_elements;
	auto typed_actual = util::as_span<const index_type>(actual);
	auto typed_expected = util::as_span<const index_type>(expected);

	std::unordered_set<index_type> expected_set(typed_expected.cbegin(), typed_expected.cbegin() + set_size);
	util::enforce(expected_set.size() == set_size,
		"Found repeated elements in expected outputs! "
		"Num expected elements is", set_size, " and yet the set of expected elements only has ",
		expected_set.size(), " elements. That's an error in test _adapter_, not the kernel.");
	std::unordered_set<index_type> actual_set(typed_actual.cbegin(), typed_actual.cbegin() + set_size);
	if (actual_set.size() != set_size) {
		os << "Expected " << set_size << " elements, but got " << actual_set.size();
	}

	// TODO: Get dump parameters passed to this function
	util::dump_parameters_t p;
	auto title = buffer_name + " (mismatches)";
	if (p.extra_info.title) {
		os << title << '\n' << string(title.length(), '-') << '\n';
	}

	size_type domain_size = config.test_length;
	util::enforce(domain_size == config.test_length, "Cannot represent the test length with size type ",
		util::type_name<size_type>(), " (with size ", sizeof(size_type), " bytes); we should not be "
		"able to get here.");
	if	(domain_size > config.num_elements_to_print || config.start_printing_from != 0) {
		os << '\n' << "Printing (mis)matches for "
			<< config.num_elements_to_print << " domain elements - [ " << config.start_printing_from << ".."
			<< config.start_printing_from + config.num_elements_to_print - 1 << " ] :"
			<< '\n' << '\n';
	}

	auto decimal_index_width = 1 + util::log_constexpr<size_type, 10>(domain_size);
	auto hexa_index_width = 1 + util::log_constexpr<size_type, 16>(domain_size);
	os
		<< std::right
		<< " " << string(decimal_index_width, ' ') <<  "     " << string(hexa_index_width, ' ')
		<< " " << "  Should be  Is actually\n"
		<< " " << string(decimal_index_width, ' ') <<  "     " << string(hexa_index_width, ' ')
		<< " " << "   in set?     in set?\n"
		<< string(decimal_index_width + hexa_index_width + 40, '-') << '\n';
	size_type num_missing = 0;
	size_type num_unexpected = 0;
	size_type num_matching_members = 0;
	size_type num_matching_non_members = 0;
	size_type num_printed = 0;
	for(size_type i = 0; i < domain_size; i++) {
		auto in_expected = util::contains(expected_set, i);
		auto in_actual = util::contains(actual_set, i);
		auto is_mismatch = (in_expected != in_actual);
		if (in_expected  && !in_actual) { num_missing++; }
		if (!in_expected &&  in_actual) { num_unexpected++; }
		if (in_expected  &&  in_actual) { num_matching_members++; }
		if (!in_expected && !in_actual) { num_matching_non_members++; }
		if ((num_printed >= config.num_elements_to_print) ||
			(i < config.start_printing_from) ||
			((in_expected == in_actual) && !config.be_verbose)) { continue; }
		os
			<< std::setfill(' ') << std::setw(decimal_index_width) << std::dec << i << "  (0x"
			<< std::setfill('0') << std::setw(hexa_index_width) << std::hex << std::uppercase << i
			<< "):    [ "
			<< (in_expected ? '*' : ' ')
			<< " ]       [ "
			<< (in_actual   ? '*' : ' ')
			<< " ] "
			<< (is_mismatch ? "   ERROR" : "") << '\n';
		num_printed++;
	}
	os
		<< std::dec << '\n'
		<< "Domain size:                                             " << domain_size              << '\n'
		<< "Expected set elements missing from the output:           " << num_missing              << '\n'
		<< "Non-set-member domain elements appearing the output:     " << num_unexpected           << '\n'
		<< "Expected set elements appearing in the output (matches): " << num_matching_members     << '\n'
		<< "Expected set elements appearing in the output (matches): " << num_matching_non_members << '\n'
		;

	std::vector<index_type> expected_elements_v(typed_expected.cbegin(), typed_expected.cbegin() + set_size);
	kernel_tests::sort(expected_elements_v);
	std::vector<index_type> actual_elements_v(typed_actual.cbegin(), typed_actual.cbegin() + set_size);
	kernel_tests::sort(actual_elements_v);
	util::print_mismatches(os,
		&actual_elements_v[0], &expected_elements_v[0], descriptor.data_type,
		set_size, set_size, "sparse as sorted vector");
}



// TODO: What about when Datum elements are not easily comparable? e.g. floats?
// Should we use a byte-array of the same length instead?
template <unsigned IndexSize, typename Datum>
void unsorted_selected_elements(
	std::ostream&             os,
	const TestConfiguration&  config,
	const std::string&        buffer_name,
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	using std::string;
	using size_type = cuda::size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array

	UNUSED(descriptor);
	util::ios_flags_saver ifs(os);
	auto num_actual_elements   = all_actual_outputs.at("num_selected").as_span<size_type>().data();
	auto num_expected_elements = all_expected_outputs.at("num_selected").as_span<size_type>().data();
	const Datum* input_data = inputs.at("input_data").as_span<Datum>().data();
	util::enforceNonNull(num_actual_elements);
	util::enforceNonNull(num_expected_elements);
	auto set_size = *num_expected_elements;

	gsl::span<const Datum> typed_actual = util::as_span<const Datum>(actual);
	gsl::span<const Datum> typed_expected = util::as_span<const Datum>(expected);

	std::unordered_multiset<Datum> expected_multiset(typed_expected.cbegin(), typed_expected.cbegin() + set_size);
	util::enforce(expected_multiset.size() == set_size,
		"Found repeated elements in expected outputs! "
		"Num expected elements is", set_size, " and yet the set of expected elements only has ",
		expected_multiset.size(), "elements. That's an error in test _adapter_, not the kernel.");
	std::unordered_multiset<Datum> actual_multiset(typed_actual.cbegin(), typed_actual.cbegin() + set_size);
	if (actual_multiset.size() != set_size) {
		os << "Expected " << set_size << " elements, but got " << actual_multiset.size();
	}

	// TODO: Get dump parameters passed to this function
	util::dump_parameters_t p;
	auto title = buffer_name + " (mismatches)";
	if (p.extra_info.title) {
		os << title << '\n' << string(title.length(), '-') << '\n';
	}

	size_t domain_size = config.test_length;
	if	(domain_size > config.num_elements_to_print || config.start_printing_from != 0) {
		os << '\n' << "Printing (mis)matches for "
			<< config.num_elements_to_print << " domain elements - [ " << config.start_printing_from << ".."
			<< config.start_printing_from + config.num_elements_to_print - 1 << " ] :"
			<< '\n' << '\n';
	}

	auto decimal_index_width = 1 + util::log_constexpr<size_type, 10>(domain_size);
	auto hexa_index_width = 1 + util::log_constexpr<size_type, 16>(domain_size);
	os
		<< std::right
		<< " " << string(decimal_index_width, ' ') <<  "     " << string(hexa_index_width, ' ')
		<< " " << "  Should be  Is actually\n"
		<< " " << string(decimal_index_width, ' ') <<  "     " << string(hexa_index_width, ' ')
		<< " " << "   in set?     in set?\n"
		<< string(decimal_index_width + hexa_index_width + 40, '-') << '\n';
//	auto mismatch_printing_sentinal =
//		std::min(domain_size, (size_t) config.start_printing_from + config.num_elements_to_print);
	size_type num_missing = 0;
	size_type num_unexpected = 0;
	size_type num_matching_members = 0;
	size_type num_matching_non_members = 0;
	size_type num_printed = 0;
	for(size_type i = 0; i < domain_size; i++) {
		bool in_expected, in_actual;
		if (input_data != nullptr) {
			in_expected = util::contains(expected_multiset, input_data[i]);
			in_actual = util::contains(actual_multiset, input_data[i]);
		}
		else {
			in_expected = util::contains(expected_multiset, i);
			in_actual = util::contains(actual_multiset, i);
		}
		auto is_mismatch = (in_expected != in_actual);
		if (in_expected  && !in_actual) { num_missing++; }
		if (!in_expected &&  in_actual) { num_unexpected++; }
		if (in_expected  &&  in_actual) { num_matching_members++; }
		if (!in_expected && !in_actual) { num_matching_non_members++; }
		if ((num_printed > config.num_elements_to_print) ||
			(i < config.start_printing_from) ||
			((in_expected == in_actual) && !config.be_verbose)) { continue; }
		os
			<< std::setfill(' ') << std::setw(decimal_index_width) << std::dec << i << "  (0x"
			<< std::setfill('0') << std::setw(hexa_index_width) << std::hex << std::uppercase << i
			<< "):    [ "
			<< (in_expected ? '*' : ' ')
			<< " ]       [ "
			<< (in_actual   ? '*' : ' ')
			<< " ] "
			<< (is_mismatch ? "   ERROR" : "") << '\n';
		num_printed++;
	}
	os
		<< std::dec << '\n'
		<< "Domain size:                                             " << domain_size              << '\n'
		<< "Expected set elements missing from the output:           " << num_missing              << '\n'
		<< "Non-set-member domain elements appearing the output:     " << num_unexpected           << '\n'
		<< "Expected set elements appearing in the output (matches): " << num_matching_members     << '\n'
		<< "Expected set elements appearing in the output (matches): " << num_matching_non_members << '\n'
		<< '\n';


	std::vector<Datum> expected_elements_v(typed_expected.cbegin(), typed_expected.cbegin() + set_size);
	kernel_tests::sort(expected_elements_v);
	std::vector<Datum> actual_elements_v(typed_actual.cbegin(), typed_actual.cbegin() + set_size);
	kernel_tests::sort(actual_elements_v);
	util::print_mismatches(os,
		&actual_elements_v[0], &expected_elements_v[0], descriptor.data_type,
		set_size, set_size, "selected as sorted vector");

}

MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, unsorted_sparse_output, 1, 2, 4, 8)

MAP_TRINARY(INSTANTIATE_FREESTANDING_FUNCTION, unsorted_selected_elements, 4, // the fixed argument
	char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long, long long, unsigned long long)
MAP_TRINARY(INSTANTIATE_FREESTANDING_FUNCTION, unsorted_selected_elements, 8, // the fixed argument
	char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long, long long, unsigned long long)


} // namespace mismatch_printers
} // namespace kernel_tests



