#pragma once
#ifndef MAKE_KERNEL_WRAPPER_ARGUMENTS_H_
#define MAKE_KERNEL_WRAPPER_ARGUMENTS_H_

#include "util/macro.h" // for MAP()

#include <string>
#include <tuple>
#include <unordered_map>
#include <exception>

namespace cuda {

#define MAKE_KERNEL_ARGUMENTS_ELEMENT(_kernel_arg_identifier) \
	{ QUOTE(_kernel_arg_identifier), _kernel_arg_identifier },

/**
 * This convenience macro will save you the trouble of writing:
 *
 *   {
 *     { "argument1", argument1 },
 *     { "argument2", argument2 }
 *   }
 *
 * for initializing a kernel_t::arguments_type you're passing to one
 * of the methods.
 *
 * @note --== !!REALLY IMPORTANT!! ==--  since we're using boost::any,
 * the data type has to be _exactly_ the type expected by the
 * kernel wrapper, otherwise you'll get a Boost error. Also, for
 * this macro to work, your variable names must be _exactly_ the same
 * as the key names the wrapper is looking for (so, config.test_length
 * will definitely not do, and you will need to define some temporaries
 * of the right types and names; watch out for 'auto's, they might
 * bite you if you're not careful to coerce their type).
 */
#define MAKE_KERNEL_ARGUMENTS(...) \
{ \
	MAP(MAKE_KERNEL_ARGUMENTS_ELEMENT, __VA_ARGS__) \
}

} // namespace cuda

#endif /* MAKE_KERNEL_WRAPPER_ARGUMENTS_H_ */
