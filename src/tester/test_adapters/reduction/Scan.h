#ifndef TEST_ADAPTERS_REDUCE_CUH_
#define TEST_ADAPTERS_REDUCE_CUH_

#include "tester/KernelTestAdapter.h"
#include <cuda/functors.hpp>

namespace kernel_tests {
namespace reduction {

#ifdef CUDA_ON_DEVICE_PRIMITIVES_COMMON_CUH_
using kat::collaborative::inclusivity_t;
#else
enum inclusivity_t : bool {
	Exclusive = false,
	Inclusive = true
};
#endif

using cuda::functors::enumerated::functor_is_enumerated_t;

template <
	unsigned IndexSize, typename ReductionOp, typename InputDatum,
	inclusivity_t Inclusivity = inclusivity_t::Inclusive,
	typename PretransformOp = cuda::functors::identity<InputDatum>
> class Scan :
	public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<
		Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>>
{
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array
	using result_type = typename ReductionOp::result_type;
	using pretransformed_type = typename PretransformOp::result_type;
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Scan);

	virtual size_t getRequiredScratchSize() const override;

protected:
	void generate_data(
		span<InputDatum> buffer_to_fill);
	// This is actually just a scalar, but we don't support those at the moment
	void generate_expected_result(
		span<result_type> buffer_to_fill,
		span<const InputDatum> data);

	bool use_identity_as_data = false;
	bool force_two_phase      = false;
	distribution_t<InputDatum> data_distribution    =
		distribution_t<InputDatum>::uniform(
			(std::is_unsigned<InputDatum>::value ?   0 : -10), // minimum
			(std::is_unsigned<InputDatum>::value ? 100 :  90)  // maximum
		);
		// Note that if you make the distribution symmetric around zero,
		// then for floating-point types you risk cancellation effects
		// which mean large relative errors - as with a large number
		// of elements, the actual sum will be very close to the mean,
		// that is to 0.
};


template <
	unsigned CountSize,
	inclusivity_t Inclusivity = inclusivity_t::Inclusive,
	typename BitContainer = unsigned int>
class PrefixCountSetBits :
	public mixins::TestAdapterNameBuilder<PrefixCountSetBits<CountSize, Inclusivity, BitContainer>>,
	public Scan<
		CountSize, cuda::functors::plus<uint_t<CountSize>>, BitContainer, Inclusivity,
		cuda::functors::population_count<uint_t<CountSize>, BitContainer> >
{
public:
	using this_class = PrefixCountSetBits<CountSize, Inclusivity, BitContainer>;
	using parent = Scan<
		CountSize, cuda::functors::plus<uint_t<CountSize>>, BitContainer, Inclusivity,
		cuda::functors::population_count<uint_t<CountSize>, BitContainer> >;
	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);
};

} // namespace reduction
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_REDUCE_CUH_ */
