#ifndef TEST_ADAPTERS_REDUCE_CUH_
#define TEST_ADAPTERS_REDUCE_CUH_

#include "tester/KernelTestAdapter.h"
#include <cuda/functors.hpp>
#include <type_traits>

namespace kernel_tests {
namespace reduction {

enum { DoConsiderInputAsBits = true, DoNotConsiderInputAsBits = false };

template <
	unsigned IndexSize,
	typename ReductionOp,
	typename InputDatum, // we don't really need this one, it's redundant with the input arg type of the pretransformer - but I feel it makes things a bit clearer
	typename PretransformOp = cuda::functors::identity<InputDatum>,
	bool     ConsiderInputAsBits = DoNotConsiderInputAsBits
> class Reduce :
	public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<
		Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>>
{
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using result_type = typename ReductionOp::result_type;
	using pretransformed_type = typename PretransformOp::result_type;
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Reduce);

protected:

	void generate_data(
		span<InputDatum>        data
	);
	// This is actually just a scalar, but we don't support those at the moment
	void generate_result(
		span<result_type>       initial_result_value
	);
	void generate_expected_result(
		span<result_type>       result,
		span<const InputDatum>  data
	);

	bool                use_identity_as_data = false;
	distribution_t<InputDatum> data_distribution    =
		ConsiderInputAsBits ?
			distribution_t<InputDatum>::full_range_uniform() :
			distribution_t<InputDatum>::uniform(
				(std::is_unsigned<InputDatum>::value ?   0 : -10), // minimum
				(std::is_unsigned<InputDatum>::value ? 100 :  90)  // maximum
			);
		// Note that if you make the distribution symmetric around zero,
		// then for floating-point types you risk cancellation effects
		// which mean large relative errors - as with a large number
		// of elements, the actual sum will be very close to the mean,
		// that is to 0.

	result_type         initial_result_value { ReductionOp().neutral_value() };
	util::optional<unsigned>  input_file_column_index; // 0-based

};

template <unsigned CountSize, typename BitContainer = unsigned int>
class CountSetBits : public Reduce<
	CountSize,
	cuda::functors::plus<uint_t<CountSize>>,
	BitContainer,
	cuda::functors::population_count<uint_t<CountSize>, BitContainer> >
{
public:
	using this_class = CountSetBits<CountSize, BitContainer>;
	using parent = Reduce<CountSize, cuda::functors::plus<uint_t<CountSize>>, BitContainer,
		cuda::functors::population_count<uint_t<CountSize>, BitContainer>>;
	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);
};


// TODO: Maybe use "unsigned DataSize", and define an sized_unsigned_int<unsigned Size> type?
template <unsigned IndexSize, typename BitContainer = unsigned>
class DenseSetMinimum : public
	Reduce< IndexSize, cuda::functors::minimum<uint_t<IndexSize> >,
	BitContainer,
	cuda::functors::global_index_of_first_set_bit<IndexSize, BitContainer>, DoConsiderInputAsBits>
{
public:
	using this_class = DenseSetMinimum<IndexSize, BitContainer>;
	using parent = Reduce<IndexSize, cuda::functors::minimum<uint_t<IndexSize>>, BitContainer,
		cuda::functors::global_index_of_first_set_bit<IndexSize, BitContainer>, DoConsiderInputAsBits>;
	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);
};

// TODO: Maybe use "unsigned DataSize", and define an sized_unsigned_int<unsigned Size> type?
template <unsigned IndexSize, typename BitContainer = unsigned>
class DenseSetMaximum : public
	Reduce<IndexSize, cuda::functors::maximum<uint_t<IndexSize>>, BitContainer,
	cuda::functors::global_index_of_last_set_bit<IndexSize, BitContainer>, DoConsiderInputAsBits >
{
public:
	using this_class = DenseSetMaximum<IndexSize, BitContainer>;
	using parent = Reduce<IndexSize, cuda::functors::maximum<uint_t<IndexSize>>, BitContainer,
		cuda::functors::global_index_of_last_set_bit<IndexSize, BitContainer>, DoConsiderInputAsBits  >;
	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);
};


} // namespace reduction
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_REDUCE_CUH_ */
