#ifndef KERNEL_TEST_ADAPTER_HISTOGRAM_CUH_
#define KERNEL_TEST_ADAPTER_HISTOGRAM_CUH_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace reduction {

template<unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize = sizeof(size_type_by_index_size<IndexSize>)>
class Histogram : public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<Histogram<IndexSize, BinIndexSize, CountSize>> {
public:
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Histogram);

public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array
	using histogram_size_type = size_type_by_index_size<BinIndexSize>;
	using bin_index_type = uint_t<BinIndexSize>;
	using count_type           = uint_t<CountSize>;
	using bin_type             = count_type;

protected:

	// Too bad we can't put namespaces within class
	void generate_indices(
		span<bin_index_type>        buffer_to_fill
	);
	void generate_histogram(
		span<bin_type>              buffer_to_fill
	);
	void generate_expected_histogram(
		span<bin_type>              histogram,
		span<const bin_type>        input_histogram,
		span<const bin_index_type>  indices
	);

	histogram_size_type histogram_length        { 10    };
	distribution_t<bin_index_type>
	               bin_index_distribution
	                                         { distribution_t<bin_index_type>::uniform(0, 9) };
//	bin_index_type   min_used_index          { 0     };
//	bin_index_type   max_used_index          { 9     };
	bool             permutation_as_indices  { false };
};

} // namespace reduction
} // namespace kernel_tests

#endif /* KERNEL_TEST_ADAPTER_HISTOGRAM_CUH_ */
