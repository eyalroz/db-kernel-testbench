
// #define IDENTIFY_FUNCTION_ON_CUDA_DEVICE
#include "CountIf.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>
#include <cuda/api/kernel_launch.cuh>


#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"

#include <algorithm>
#include <iostream>

#include "kernel_wrappers/reduction/count_if.cu"

namespace kernel_tests {
namespace reduction {

template <unsigned IndexSize, typename UnaryPredicate>
KernelTestAdapter::BufferDescriptors CountIf <IndexSize, UnaryPredicate>
	::getBufferDescriptors() const
{
	// Not using any scratch for a reduce - although we theoretically could have
	// auto scratch_length = cuda::device::get(config.test_device).properties().maxThreadsPerBlock;
	return {
		{ "result", BufferDescriptor::make<index_type>(1,                  BufferDirection::InOut) },
		{ "data",   BufferDescriptor::make<predicate_input_type> (config.test_length, BufferDirection::Input)  },
	};
}

template <unsigned IndexSize, typename UnaryPredicate>
void CountIf <IndexSize, UnaryPredicate>::generate_data(span<predicate_input_type> buffer_to_fill)
{
	auto random_engine = this->random_engine;

	if (use_identity_as_data) {
		iota(buffer_to_fill, 0);
	}
	else {
		util::random::fill(buffer_to_fill, data_distribution, random_engine);
	}
}

template <unsigned IndexSize, typename UnaryPredicate>
void CountIf <IndexSize, UnaryPredicate>::generate_result(span<index_type> result)
{
	result[0] = 0;
}

template <unsigned IndexSize, typename UnaryPredicate>
void CountIf <IndexSize, UnaryPredicate>::generate_expected_result(
	span<index_type>                        result,
	span<const predicate_input_type>  data)
{
	result[0] = count_if(data, [] (const predicate_input_type& x) { return UnaryPredicate()(x); });
}


// TODO: Have either Buffer.h or some other .h/.hpp have named constructor idioms -
// for constants, arithmetic progressions, geometric progressions, and
// any lambda
template <unsigned IndexSize, typename UnaryPredicate>
KernelTestAdapter::BufferSizes CountIf <IndexSize, UnaryPredicate>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("data")) {
		auto  data   = buffers.at("data").as_span<predicate_input_type>();
		generate_data(data);
	}
	if (need_to_generate("result")) {
		auto result = buffers.at("result").as_span<index_type>();
		generate_result(result);
	}

	return NoBuffersToResize;
}

template <unsigned IndexSize, typename UnaryPredicate>
KernelTestAdapter::BufferSizes CountIf <IndexSize, UnaryPredicate>
	::generateExpectedOutputs(const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("result")) {
		auto expected_result    = expected_buffers.at("result").as_span<index_type>();
		auto data  = buffers.at("data").as_span<const predicate_input_type>();

		generate_expected_result(expected_result, data);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename UnaryPredicate>
LaunchConfigurationSequence CountIf <IndexSize, UnaryPredicate>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };

	::cuda::kernels::reduction::count_if::kernel_t<IndexSize, UnaryPredicate> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("reduction::count_if");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, typename UnaryPredicate>
void CountIf <IndexSize, UnaryPredicate>
	::launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	index_type*                 result = buffers.at("result").as_span<index_type>().data();
	const predicate_input_type* data   = buffers.at("data"  ).as_span<const predicate_input_type>().data();
	index_type length = config.test_length;

	// TODO: Other adapters need this too...
	util::enforce(length == config.test_length, "specified test length too long to fit in ", IndexSize, " bytes.");

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::reduction::count_if::kernel_t<IndexSize, UnaryPredicate>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(result, data, length)
	);
}

template <unsigned IndexSize, typename UnaryPredicate>
std::string CountIf <IndexSize, UnaryPredicate>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss	<< "A count of data elements of type" << util::type_name<predicate_input_type>()
		<< " which satisfy the unary predicate "
		<< util::discard_template_parameters(util::type_name<UnaryPredicate>())
		<< "with the result having size " << IndexSize << '.';
	return oss.str();
}

template <unsigned IndexSize, typename UnaryPredicate>
std::string CountIf <IndexSize, UnaryPredicate>::getDescription() const {
	return
		"WRITEME";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<unsigned IndexSize, typename UnaryPredicate>
CountIf <IndexSize, UnaryPredicate>::CountIf(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	bool use_all_ones_as_data  = resolveOption(ConfigurationKeySet {
		"use-all-ones-as-input", "trivial-data", "trivial", "data-all-ones"}, false);
	resolveOption(use_identity_as_data, ConfigurationKeySet {
		"use-identity-as-input", "identity-data", "identity-input"
	});

	if (use_all_ones_as_data) {
		data_distribution = distribution_t<predicate_input_type>::single_value(1);
	}

	// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Set data[i] to 1", use_all_ones_as_data),
		make_pair("Set data[i] to i", use_identity_as_data)
	);
}

static_block {

	namespace functors = cuda::functors;

	CountIf < 4, functors::about_zero<float,  bool, 20> >::registerInSubclassFactory();
	CountIf < 4, functors::about_zero<double, bool, 20> >::registerInSubclassFactory();
	CountIf < 4, functors::is_zero<int32_t>             >::registerInSubclassFactory();
	CountIf < 8, functors::about_zero<float,  bool, 20> >::registerInSubclassFactory();
	CountIf < 8, functors::about_zero<double, bool, 20> >::registerInSubclassFactory();
	CountIf < 8, functors::is_zero<int32_t>             >::registerInSubclassFactory();

	CountNonZeros<4, uint32_t >::registerInSubclassFactory();
	CountNonZeros<4, uint64_t >::registerInSubclassFactory();
	CountNonZeros<8, uint32_t >::registerInSubclassFactory();
	CountNonZeros<8, uint64_t >::registerInSubclassFactory();

}

} // namespace reduction
} // namespace kernel_tests


