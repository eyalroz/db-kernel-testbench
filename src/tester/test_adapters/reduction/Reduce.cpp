
// #define IDENTIFY_FUNCTION_ON_CUDA_DEVICE

#include "Reduce.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "kernels/reduction/common.cuh"  // for the apply_op trick

#include <cuda/functors.hpp>
#include <cuda/api/kernel_launch.cuh>

#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"
#include "tester/load_data.h"

#include <algorithm>
#include <iostream>
#include <regex>

#include "kernel_wrappers/reduction/reduce.cu"


namespace kernel_tests {

namespace mixins {
template<unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
struct TestAdapterNameBuilder<reduction::Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>> {
static std::string  buildName() {
	std::string s = KernelTestAdapter::buildName<
		reduction::Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>>();
	util::replace_all(s, "enumerated::as_enumerated_unary","as_enumerated_unary");
	std::regex default_final_template_param(", identity<[^>]*> ?>$");
	util::replace_all(s, "(kernel_tests::reduction::inclusivity_t)1", "Inclusive");
	util::replace_all(s, "(kernel_tests::reduction::inclusivity_t)0", "Exclusive");
	s = std::regex_replace(s, default_final_template_param, ">");
	return s;
}
};

} // namespace mixins

namespace reduction {

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
KernelTestAdapter::BufferDescriptors Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>
	::getBufferDescriptors() const
{
	// Not using any scratch for a reduce - although we theoretically could have
	// auto scratch_length = cuda::device::get(config.test_device).properties().maxThreadsPerBlock;
	return {
		{ "result", BufferDescriptor::make<result_type>(1,                  BufferDirection::InOut) },
		{ "data",   BufferDescriptor::make<InputDatum> (config.test_length, BufferDirection::Input).bit_container(ConsiderInputAsBits) },
	};
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
void Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>::generate_data(span<InputDatum> data)
{
	auto random_engine = this->random_engine;

	if (use_identity_as_data) {
		iota(data, 0);
	}
	else {
		util::random::fill(data,
			data_distribution, random_engine);
	}
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
void Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>::generate_result(
	span<result_type> result)
{
	result[0] = initial_result_value;
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
void Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>::generate_expected_result(
	span<result_type>       result,
	span<const InputDatum>  data
)
{
	ReductionOp op;
	typename ReductionOp::accumulator acc_op;
	namespace detail = cuda::kernels::reduction::detail;

	// Perform a pairwise-summation, to reduce potential
	// floating-point error. Maybe we should use Kahan summation instead?

	// the first phase involves pre-transforming, so we handle it separately

	index_type next_phase_length = util::div_rounding_up(config.test_length, 2);
	std::vector<result_type> scratch(next_phase_length);

	for(index_type i = 0; i < config.test_length/2; i++) {
		pretransformed_type pretransformed_1 =
			detail::apply_unary_op<index_type, PretransformOp>(2*i, data[2*i]);
		pretransformed_type pretransformed_2 =
			detail::apply_unary_op<index_type, PretransformOp>(2*i+1, data[2*i+1]);
		scratch[i] = op(pretransformed_1, pretransformed_2);
	}
	if (config.test_length % 2 != 0) {
		scratch[next_phase_length - 1] =
			detail::apply_unary_op<index_type, PretransformOp>(
				config.test_length-1, data[config.test_length-1]);
	}

	// ... and all the rest of the phases are the same except for their length

	for(index_type current_length = next_phase_length;
	    current_length > 1;
	    current_length = next_phase_length)
	{
		next_phase_length = util::div_rounding_up(current_length, 2);
		for(index_type i = 0; i < next_phase_length; i++) {
			scratch[i] = op(scratch[2*i], scratch[2*i+1]);
		}
		if (current_length % 2 != 0) {
			scratch[next_phase_length - 1] = scratch[current_length - 1];
		}
	}

	result[0] = acc_op(initial_result_value, scratch[0]);
}


template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
KernelTestAdapter::BufferSizes Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>
	::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("data")) {
		auto data   = buffers.at("data").as_span<InputDatum>();
		generate_data(data);
	}
	if (need_to_generate("result")) {
		auto result = buffers.at("result").as_span<result_type>();
		generate_result(result);
	}
	return NoBuffersToResize;
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
KernelTestAdapter::BufferSizes Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>
	::generateExpectedOutputs(const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("result")) {
		auto expected_result = expected_buffers.at("result").as_span<result_type>();
		auto data = buffers.at("data").as_span<const InputDatum>();

		generate_expected_result(expected_result, data);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
LaunchConfigurationSequence Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };

	::cuda::kernels::reduction::reduce::kernel_t<IndexSize, ReductionOp, InputDatum, PretransformOp> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("reduction::reduce");
	return { { kernel_name, kernel.get_device_function(), launch_config } };

}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
void Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>
	::launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	result_type*      result  = buffers.at("result").as_span<result_type>().data();
	const InputDatum* data    = buffers.at("data"  ).as_span<const InputDatum>().data();
	index_type length = config.test_length;

	// TODO: Other adapters need this too...
	util::enforce(length == config.test_length,
		"specified test length too long for size type", util::type_name<index_type>());

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::reduction::reduce::kernel_t<
		IndexSize, ReductionOp, InputDatum, PretransformOp
	>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(result, data, length)
	);
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
std::string Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss << "Reduction of data of type" + util::type_name<InputDatum>();
	if (!std::is_same<PretransformOp, cuda::functors::identity<InputDatum>>::value) {
		oss << ", after preliminary transformation by " << util::type_name<PretransformOp>() << ", ";
	}
	oss	<< " by reducing operation "
		<< util::discard_template_parameters(util::type_name<ReductionOp>())
		<< " into a single scalar value of type " << util::type_name<result_type>() + ".";
	return oss.str();
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
std::string Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>::getDescription() const {
	return
		"WRITEME.";
}

template<unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp, bool ConsiderInputAsBits>
Reduce<IndexSize, ReductionOp, InputDatum, PretransformOp, ConsiderInputAsBits>::Reduce(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	bool use_all_ones_as_data   = resolveOption(ConfigurationKeySet
		{"use-all-ones-as-input", "data-all-ones", "all-ones-data", "all-ones", "all-ones-input"}, false);
	resolveOption( use_identity_as_data, ConfigurationKeySet
		{"use-identity-as-input", "data-identity", "identity-data", "identity", "iota", "iota-data"}
	);
	resolveOption(initial_result_value   , ConfigurationKeySet
		{"reduce-with", "initial-reduced", "initial-reduced-value", "reduce-starting-with", "reduce_from",
		"non-neutral-initial-result-value"});
	resolveOption(data_distribution, ConfigurationKeySet {
		"data", "input-data", "data-distribution", "input-data-distribution", "input-distribution"}
	);
	input_file_column_index =
		resolveOption<typename decltype(input_file_column_index)::value_type>(ConfigurationKeySet
		{"input-file-column", "input-file-column-index", "input-column", "column" });

	if (use_all_ones_as_data) {
		data_distribution = distribution_t<InputDatum>::single_value(1);
	}
	maybe_print_extra_configuration(
		make_pair("Initial value to reduce with",  initial_result_value),
		make_pair("Set data[i] = 1",               use_all_ones_as_data),
		make_pair("Set data[i] = i",               use_identity_as_data)
	);

}

static_block {
	namespace functors = cuda::functors;

	// no pretransform

	//      IndezSize  RductionOp                InputDatum
	//-----------------------------------------------------------------------
	Reduce <4,         functors::plus<int32_t>,  int32_t  >::registerInSubclassFactory();
	Reduce <4,         functors::plus<int64_t>,  int32_t  >::registerInSubclassFactory();
	Reduce <8,         functors::plus<int64_t>,  int32_t  >::registerInSubclassFactory();
	Reduce <4,         functors::plus<float>,    float    >::registerInSubclassFactory();
	Reduce <4,         functors::plus<double>,   float    >::registerInSubclassFactory();
	Reduce <4,         functors::plus<double>,   double   >::registerInSubclassFactory();

	// unenumerated pretransform

	//      IndexSize  ReductionOp               InputDatum  PretransformOp
	//-----------------------------------------------------------------------------------------------------
	Reduce <4,         functors::plus<uint32_t>, uint32_t,   functors::is_zero<uint32_t> >::registerInSubclassFactory();

	CountSetBits<4>::registerInSubclassFactory();
	CountSetBits<8>::registerInSubclassFactory();

	// enumerated pretransform

	//      IndexSize  ReductionOp               InputDatum  PretransformOp
	//-----------------------------------------------------------------------
	Reduce <4,         functors::plus<uint32_t>, uint32_t,   functors::enumerated::as_enumerated_unary<functors::plus<uint32_t> > >::registerInSubclassFactory();

	DenseSetMinimum<4>::registerInSubclassFactory();
	DenseSetMinimum<8>::registerInSubclassFactory();
}

} // namespace reduction
} // namespace kernel_tests


