#include "Histogram.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"

#include <algorithm>
#include <iostream>

#include "kernel_wrappers/reduction/multi_reduce/histogram.cu"

namespace kernel_tests {
namespace reduction {

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
KernelTestAdapter::BufferDescriptors Histogram<IndexSize, BinIndexSize, CountSize>
	::getBufferDescriptors() const
{
	return {
		{ "histogram",   BufferDescriptor::make<index_type    >(histogram_length,   BufferDirection::InOut) },
		{ "bin_indices", BufferDescriptor::make<bin_index_type>(config.test_length, BufferDirection::Input) },
	};
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
void Histogram<IndexSize, BinIndexSize, CountSize>::generate_indices(span<bin_index_type> buffer_to_fill)
{
	if (permutation_as_indices) {
		iota(buffer_to_fill, 0);
		shuffle(buffer_to_fill, random_device);
		return;
	}
	// For some strange reason, I can't directly capture some varibles of *this. Why??
	auto random_engine = this->random_engine;
	auto bin_index_distribution = this->bin_index_distribution;
	generate(buffer_to_fill,
		[&bin_index_distribution, &random_engine]() {
			return util::random::sample_from(bin_index_distribution, random_engine);
		});
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
void Histogram<IndexSize, BinIndexSize, CountSize>::generate_histogram(span<bin_type> histogram)
{
	fill(histogram, 0);
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
void Histogram<IndexSize, BinIndexSize, CountSize>::generate_expected_histogram(
	span<bin_type>              histogram,
	span<const bin_type>        input_histogram,
	span<const bin_index_type>  indices
)
{
	copy(input_histogram, histogram.begin());

	util::enforce(config.test_length < std::numeric_limits<index_type>::max(),
		"Input length ", config.test_length, " too high for size type ", util::type_name<index_type>());

	for(index_type i = 0; i < config.test_length; i++) {
		histogram[indices[i]]++;
	}
}


// TODO: Have either Buffer.h or some other .h/.hpp have named constructor idioms -
// for constants, arithmetic progressions, geometric progressions, and
// any lambda
template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
KernelTestAdapter::BufferSizes Histogram<IndexSize, BinIndexSize, CountSize>::generateInputs(const HostBuffers& buffers)
{
	// Note: We don't support generation of only the input histogram, or only the expected
	// resulting histogram - only both of them
	if (need_to_generate("histogram")) {
		auto input_histogram  = buffers.at("histogram").as_span<bin_type>();
		generate_histogram(input_histogram);
	}
	if (need_to_generate("bin_indices")) {
		auto bin_indices      = buffers.at("bin_indices").as_span<bin_index_type>();
		generate_indices(bin_indices);
	}

	return NoBuffersToResize;
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
KernelTestAdapter::BufferSizes Histogram<IndexSize, BinIndexSize, CountSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	// Note: We don't support generation of only the input histogram, or only the expected
	// resulting histogram - only both of them
	if (need_to_generate("histogram")) {
		auto expected_histogram  = expected_buffers.at("histogram").as_span<bin_type>();
		auto input_histogram     = buffers.at("histogram").as_span<const bin_type>();
		auto bin_indices         = buffers.at("bin_indices"  ).as_span<const bin_index_type>();

		generate_expected_histogram(expected_histogram, input_histogram, bin_indices);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
LaunchConfigurationSequence Histogram<IndexSize, BinIndexSize, CountSize>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		{ "histogram_length",     (size_t) histogram_length          },
		{ "data_length",          config.test_length                 },
	};
	if (config.thread_serialization_factor) {
		cuda::serialization_factor_t serialization_factor = config.thread_serialization_factor.value();
		extra_args.insert( { "serialization_factor", serialization_factor});
	}

	::cuda::kernels::reduction::histogram::kernel_t<IndexSize, BinIndexSize, CountSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("reduction::histogram");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
void Histogram<IndexSize, BinIndexSize, CountSize>
	::launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto histogram    = buffers.at("histogram"  ).as_span<size_type           >().data();
	auto bin_indices  = buffers.at("bin_indices").as_span<const bin_index_type>().data();
	size_type data_length = config.test_length;

	cuda::kernel_t::arguments_type args =
		MAKE_KERNEL_ARGUMENTS(histogram, bin_indices, data_length, histogram_length);
	if (config.thread_serialization_factor) {
		cuda::serialization_factor_t serialization_factor = config.thread_serialization_factor.value();
		args.insert( { "serialization_factor", (cuda::serialization_factor_t) serialization_factor});
	}
	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::reduction::histogram::kernel_t<IndexSize, BinIndexSize, CountSize>().enqueue_launch(
		stream, launch_config, args
	);
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
std::string Histogram<IndexSize, BinIndexSize, CountSize>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss
		<< "Histogram construction from bin-indices of size " << BinIndexSize <<
		" (with indices into the input of bin-indices having size " << IndexSize << " ).";
	return oss.str();
}

template <unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
std::string Histogram<IndexSize, BinIndexSize, CountSize>::getDescription() const {
	return
		"Histogram construction where the data is the same as the bin "
		"indices (0...histogram length - 1) - which is a degenerate form "
		"of the dense-to-dense multi-reduction"
		"kernel with all data implicitly set to 1, and the + operation used"
		"for reduction.";
}

template<unsigned IndexSize, unsigned BinIndexSize, unsigned CountSize>
Histogram<IndexSize, BinIndexSize, CountSize>::Histogram(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	histogram_length = resolveOption(ConfigurationKeySet
		{"num-indices", "number-of-indices", "num-distinct-indices", "hist-lengh", "histogram-length",
		"number-of-bins"}, histogram_length);

	resolveOption(bin_index_distribution,
		ConfigurationKeySet {"index-distribution", "indices", "used-indices", "indices-distribution"});
	resolveOption(permutation_as_indices,
		ConfigurationKeySet {"permutation-as-indices", "permutation", "permute"   });
	if (permutation_as_indices) {
		histogram_length = config.test_length;
		util::enforce(std::numeric_limits<bin_index_type>::max() >= config.test_length,
			"The index type cannot accommodate the maximum index of an input array element.");
	}

	maybe_print_extra_configuration(
		make_pair("Histogram length",                   histogram_length),
		make_pair("Bin index distribution",             bin_index_distribution),
		make_pair("Use a permutation for the indices",  permutation_as_indices)
	);

}

static_block {
	//          IndexSize  BinIndexSize
	//----------------------------------------------------
	Histogram < 4,         1 >::registerInSubclassFactory();
	Histogram < 4,         2 >::registerInSubclassFactory();
	Histogram < 4,         4 >::registerInSubclassFactory();
	Histogram < 4,         8 >::registerInSubclassFactory();
	Histogram < 8,         4 >::registerInSubclassFactory();
	Histogram < 8,         8 >::registerInSubclassFactory();
}

} // namespace reduction
} // namespace kernel_tests


