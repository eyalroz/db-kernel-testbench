#ifndef TEST_ADAPTERS_REDUCE_CUH_
#define TEST_ADAPTERS_REDUCE_CUH_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace reduction {

template <unsigned IndexSize, typename UnaryPredicate>
class CountIf :
	public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<CountIf<IndexSize, UnaryPredicate>>
{
public:
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(CountIf);

public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array

protected:
	using predicate_input_type = typename UnaryPredicate::argument_type;
	// Too bad we can't put namespaces within class
	void generate_data(
		span<predicate_input_type>        buffer_to_fill);
	// This is actually just a scalar, but we don't support those at the moment
	void generate_result(
		span<index_type>                        buffer_to_fill);
	void generate_expected_result(
		span<index_type>                        result,
		span<const predicate_input_type>  data);

	bool      use_identity_as_data = false;
	distribution_t<predicate_input_type> data_distribution    =
		distribution_t<predicate_input_type>::uniform(
			(std::is_unsigned<predicate_input_type>::value ?   0 : -50), // minimum
			(std::is_unsigned<predicate_input_type>::value ? 100 :  50)  // maximum
		);
};

// TODO: Maybe use "unsigned DataSize", and define an sized_unsigned_int<unsigned Size> type?
template <unsigned CountSize, typename Datum>
class CountNonZeros : public CountIf<CountSize, typename cuda::functors::non_zero<Datum>>
{
public:
	using this_class = CountNonZeros<CountSize, Datum>;
	using parent = CountIf<CountSize, typename cuda::functors::non_zero<Datum>>;
	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);
};

} // namespace reduction
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_REDUCE_CUH_ */
