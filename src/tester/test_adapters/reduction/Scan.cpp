
// #define IDENTIFY_FUNCTION_ON_CUDA_DEVICE

#include "Scan.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "kernels/reduction/common.cuh"

#include <cuda/functors.hpp>
#include <cuda/api/kernel_launch.cuh>

#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"

#include <algorithm>
#include <iostream>
#include <regex>
#include <utility>

#include "kernel_wrappers/reduction/scan.cu"


namespace kernel_tests {

using reduction::inclusivity_t;

namespace mixins {
template<unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
struct TestAdapterNameBuilder<reduction::Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>> {
static std::string  buildName() {
	std::string s = KernelTestAdapter::buildName<
		reduction::Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>>();
	util::erase_all(s, "(enumerated::functor_is_enumerated_t)0, ");
	util::erase_all(s, "(enumerated::functor_is_enumerated_t)1, ");
	util::replace_all(s, "(kernel_tests::reduction::inclusivity_t)1", "Inclusive");
	util::replace_all(s, "(kernel_tests::reduction::inclusivity_t)0", "Exclusive");
	util::replace_all(s, "enumerated::as_enumerated_unary","as_enumerated_unary");

	std::regex default_final_template_param(", identity<[^>]*> ?>$");
	s = std::regex_replace(s, default_final_template_param, ">");
	return s;
}
};

template <unsigned CountSize, inclusivity_t Inclusivity, typename BitContainer>
struct TestAdapterNameBuilder<reduction::PrefixCountSetBits<CountSize, Inclusivity, BitContainer>> {
static std::string  buildName() {
	std::string s = KernelTestAdapter::buildName<
		reduction::PrefixCountSetBits<CountSize, Inclusivity, BitContainer>>();
	util::erase_all(s, "(enumerated::functor_is_enumerated_t)0, ");
	util::erase_all(s, "(enumerated::functor_is_enumerated_t)1, ");
	util::replace_all(s, "(kernel_tests::reduction::inclusivity_t)1", "Inclusive");
	util::replace_all(s, "(kernel_tests::reduction::inclusivity_t)0", "Exclusive");
	util::replace_all(s, "enumerated::as_enumerated_unary","as_enumerated_unary");

	std::regex default_final_template_param(", identity<[^>]*> ?>$");
	s = std::regex_replace(s, default_final_template_param, ">");
	return s;
}
};

} // namespace mixins


namespace reduction {


template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
KernelTestAdapter::BufferDescriptors Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>
	::getBufferDescriptors() const
{
	return {
		{ "result", BufferDescriptor::make<result_type>(config.test_length, BufferDirection::Output )},
		{ "data",   BufferDescriptor::make<InputDatum> (config.test_length, BufferDirection::Input  )},
	};
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
size_t Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>::getRequiredScratchSize() const
{

	//  In the first phase of the scan operation, each block performs a reduction over a contiguous segment of
	//  the input data; and that's what it passes to the second phase via the scratch space in global memory.

	auto launch_configs = resolveLaunchConfigurations();
	if (launch_configs.size() == 1) { return 0; } // let's hope nobody chokes on that
	auto reduction_phase_launch_config = launch_configs[0].launch_config;
	auto num_reduction_segments = reduction_phase_launch_config.grid_dimensions.x;
	return sizeof(result_type) * num_reduction_segments;
}


template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
void Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>::generate_data(span<InputDatum> data)
{
	auto random_engine = this->random_engine;

	if (use_identity_as_data) {
		iota(data, 0);
	}
	else {
		util::random::fill(data,
			data_distribution, random_engine);
	}
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
void Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>::generate_expected_result(
	span<result_type> result,
	span<const InputDatum> data)
{
	ReductionOp op;
	namespace detail = cuda::kernels::reduction::detail;
	auto pretransform = [&data](index_type i)  {
		return detail::apply_unary_op<index_type, PretransformOp>(i, data[i]);
	};

	if (config.test_length == 1) {
		result[0] = (Inclusivity == inclusivity_t::Exclusive) ?
			op.neutral_value() : pretransform(0);
		return;
	}

	unsigned num_phases = util::ceil_log2(config.test_length);
	index_type rounded_length = 1 << num_phases;
	std::vector<result_type> scratch_space(rounded_length);
	std::fill(scratch_space.begin(),scratch_space.begin(),77777);
	auto scratch = [&scratch_space, &rounded_length](unsigned phase, index_type i) -> result_type& {
		return *(scratch_space.end() - (rounded_length >> phase) + i);
		// e.g. for 4 phases the scratch space is divided as follows
		// (phase index indicates cell intended for use by that phase):
		//
		// [ 0 0 0 0 0 0 0 0 0 1 1 1 1 2 2 3 ]
		//
	};

	// the first phase involves pre-transforming, so we handle it separately

	for(index_type i = 0; i < config.test_length / 2; i++) {
		scratch(0,i) = op(pretransform(2*i), pretransform(2*i+1));
	}
	if (config.test_length % 2 != 0) {
		scratch(0,config.test_length / 2) = pretransform(config.test_length-1);
	}
	for(index_type i = (config.test_length + 1) / 2; i < rounded_length / 2; i++) {
		scratch(0,i) = op.neutral_value();
	}

	// Now we have a nice power-of-2-sized input

	for(unsigned phase = 1; phase < num_phases; phase++)
	{
		// pairwise-sum into the next phase's scratch space
		auto phase_length = rounded_length >> (phase + 1);
		for(index_type i = 0; i < phase_length; i++) {
			scratch(phase, i) = op(scratch(phase-1, 2*i), scratch(phase-1, 2*i+1));
		}
	}

	for(unsigned phase = num_phases - 1; phase >= 1; phase--)
	{
		auto phase_length = rounded_length >> (phase + 1);
		scratch(phase-1, 1) = scratch(phase, 0);

		// we assume the subsequent phase's scratch area
		// is an _inclusive_ prefix-sum result over blocks of length 2*phase_length,
		// and we generate the same for blocks of length phase_length
		//
		// The implementation is not incredibly efficient, but it doesn't matter
		// anyway since without parallelization we're going to be slow regardless of
		// everything

		for(index_type i = 1; i < phase_length; i++) {
			scratch(phase-1, 2*i) = op(scratch(phase-1, 2*i), scratch(phase, i - 1));
			scratch(phase-1, 2*i+1) = scratch(phase, i);
		}
	}

	if ((Inclusivity == inclusivity_t::Exclusive)) {
		result[0] = op.neutral_value();
		result[1] = pretransform(0);
		for(index_type i = 1; i < config.test_length / 2; i++) {
			result[2*i] = scratch(0, i - 1);
			result[2*i+1] = op(scratch(0, i - 1), pretransform(2*i));
		}
		if (config.test_length % 2 != 0) {
			result[config.test_length - 1] = scratch(0, (config.test_length - 1)/2 - 1);
		}
	}
	else {
		result[0] = pretransform(0);
		if (config.test_length > 1) result[1] = op(result[0], pretransform(1));
		for(index_type i = 1; i < config.test_length / 2; i++) {
			result[2*i] = op(scratch(0,i - 1), pretransform(2*i));
			result[2*i+1] = scratch(0, i);
		}
		if (config.test_length % 2 != 0) {
			result[config.test_length - 1] = scratch(0, (config.test_length - 1)/2);
		}
	}
}


template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
KernelTestAdapter::BufferSizes Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>
	::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("data")) {
		auto data   = buffers.at("data").as_span<InputDatum>();
		generate_data(data);
	}
	return NoBuffersToResize;
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
KernelTestAdapter::BufferSizes Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>
	::generateExpectedOutputs(const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("result")) {
		auto expected_result  = expected_buffers.at("result").as_span<result_type>();
		auto data        = buffers.at("data").as_span<const InputDatum>();

		generate_expected_result(expected_result, data);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
LaunchConfigurationSequence Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	kernel_tests::LaunchConfigurationSequence configs;
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };

	::cuda::kernels::reduction::scan::reduce_segments::kernel_t
			<IndexSize, ReductionOp, InputDatum, PretransformOp> first_phase_kernel;
	cuda::launch_configuration_limits_t limits =
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block);
	auto first_phase_launch_config = cuda::kernel::resolve_launch_configuration(first_phase_kernel,
		device_properties, extra_args, limits);

	if (first_phase_launch_config.grid_dimensions.x == 1 && !force_two_phase) {
		::cuda::kernels::reduction::scan::scan_single_segment::kernel_t
				<IndexSize, ReductionOp, InputDatum, Inclusivity,
				PretransformOp> single_kernel;
		auto single_phase_launch_config = cuda::kernel::resolve_launch_configuration(single_kernel,
			device_properties, extra_args, limits);

		configs.push_back( {
			"reduction::scan::scan_single_segment",
			single_kernel.get_device_function(),
			single_phase_launch_config
		} );
		return configs;
	}

	configs.push_back({
		"reduction::scan::reduce_segments",
		first_phase_kernel.get_device_function(),
		first_phase_launch_config
	});
	auto num_segments = first_phase_launch_config.grid_dimensions.x;
	auto segment_length = util::div_rounding_up(config.test_length, num_segments);
	extra_args["segment_length"] = segment_length;

	::cuda::kernels::reduction::scan::scan_using_segment_reductions::kernel_t
			 <IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp> second_phase_kernel;
	auto second_phase_launch_config =
		cuda::kernel::resolve_launch_configuration(second_phase_kernel,
			 device_properties, extra_args, limits);

	configs.push_back({
		"reduction::scan::scan_using_segment_reductions",
		first_phase_kernel.get_device_function(),
		second_phase_launch_config
	});

	return configs;

}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
void Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>
	::launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	result_type* result    = buffers.at("result").as_span<result_type>().data();
	const InputDatum* data = buffers.at("data"  ).as_span<const InputDatum>().data();
	index_type length = config.test_length;

	auto launch_configs = resolveLaunchConfigurations();

	if (launch_configs.size() == 1) {
		auto single_phase_launch_config = launch_configs[0].launch_config;
		if (config.be_verbose) {
			std::cout << "Since the amount of data is relatively small, "
				"we will only use a single block for the scan operation, "
				"hence only a single phase, with a single kernel.\n";
		}

		cuda::kernels::reduction::scan::scan_single_segment::kernel_t
		<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>().enqueue_launch(
			stream, single_phase_launch_config,
			MAKE_KERNEL_ARGUMENTS(result, data, length)
		);
		return;
	}
	util::enforce(not device_scratch_area.empty(), "Missing on-device scratch area");

	result_type* __attribute__((unused)) segment_reductions = static_cast<result_type*>(device_scratch_area.data());
		// it's not actually unused, but nvcc (for CUDA 7.5 / 8.0 RC) is being difficult

	auto reduction_phase_config = launch_configs[0].launch_config;

	cuda::kernels::reduction::scan::reduce_segments::kernel_t
	<IndexSize, ReductionOp, InputDatum, PretransformOp>().enqueue_launch(
		stream, reduction_phase_config,
		MAKE_KERNEL_ARGUMENTS(segment_reductions, data, length)
	);

	// No synch needed between these two despite their dependency,
	// since they're scheduled to the same stream

	auto scan_phase_config = launch_configs[1].launch_config;

	{
		// This strange redeclaration is due to the MAKE_KERNEL_ARGUMENTS macro,
		// which only works if you have just the right types and identifiers
		const result_type* __attribute__((unused)) segment_reductions =
			static_cast<const result_type*>(device_scratch_area.data());

		cuda::kernels::reduction::scan::scan_using_segment_reductions::kernel_t
		<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>().enqueue_launch(
			stream, scan_phase_config,
			MAKE_KERNEL_ARGUMENTS(result, segment_reductions, data, length)
		);
	}
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
std::string Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss << "Scan (= Prefix-reduction) of data of type " + util::type_name<InputDatum>();
	if (!std::is_same<PretransformOp, cuda::functors::identity<InputDatum>>::value) {
		oss << ", after preliminary transformation by " << util::type_name<PretransformOp>() << ", ";
	}
	oss	<< " by reducing operation "
		<< util::discard_template_parameters(util::type_name<ReductionOp>())
		<< " with the results having type " << util::type_name<result_type>() + ".";

	return oss.str();
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
std::string Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>::getDescription() const {
	return
		"WRITEME.";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<unsigned IndexSize, typename ReductionOp, typename InputDatum, inclusivity_t Inclusivity, typename PretransformOp>
Scan<IndexSize, ReductionOp, InputDatum, Inclusivity, PretransformOp>::Scan(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;
	bool use_all_ones_as_data = resolveOption(ConfigurationKeySet
		{"use-all-ones-as-input", "data-all-ones", "all-ones-data", "all-ones", "all-ones-input"}, false);
	resolveOption(use_identity_as_data, ConfigurationKeySet
		{"use-identity-as-input", "data-identity", "identity-data", "identity", "iota", "iota-data"});
	resolveOption(force_two_phase, ConfigurationKeySet
		{"always-use-two-phases", "two-phase", "force-two-phase", "two-phases"});
	resolveOption(data_distribution, ConfigurationKeySet {
		"data", "input-data", "data-distribution", "input-data-distribution", "input-distribution"}
	);

	if (use_all_ones_as_data) {
		data_distribution = distribution_t<InputDatum>::single_value(1);
	}

	maybe_print_extra_configuration(
		make_pair("Set data[i] to 1",                     use_all_ones_as_data),
		make_pair("Set data[i] to i",                     use_identity_as_data),
		make_pair("Input data distribution",              data_distribution   ),
		make_pair("Always use two phases (two kernels)",  force_two_phase     )
	);
}



static_block {
	namespace functors = cuda::functors;
	constexpr auto Exclusive = reduction::inclusivity_t::Exclusive;

	// No pretransform, inclusive

	//     IndexSize  ReductionOp                InputDatum  Inclusivity  PretransformOp
	//--------------------------------------------------------------------------------------------------
	Scan < 4,         functors::plus<uint32_t>,  uint32_t    >::registerInSubclassFactory();
	Scan < 4,         functors::plus<int32_t>,   int32_t     >::registerInSubclassFactory();
	Scan < 4,         functors::plus<int64_t>,   int32_t     >::registerInSubclassFactory();
	Scan < 8,         functors::plus<int64_t>,   int32_t     >::registerInSubclassFactory();
	Scan < 4,         functors::plus<float>,     float       >::registerInSubclassFactory();
	Scan < 4,         functors::plus<double>,    float       >::registerInSubclassFactory();
	Scan < 4,         functors::plus<double>,    double      >::registerInSubclassFactory();

	// No pretransform, exclusive

	Scan < 4,         functors::plus<int32_t>,   int32_t,    Exclusive >::registerInSubclassFactory();
	Scan < 4,         functors::plus<double>,    double,     Exclusive >::registerInSubclassFactory();

	// Unenumerated pretransform, inclusive & exclusive

	constexpr auto Inclusive = reduction::inclusivity_t::Inclusive;

	// TODO: Add a non-subclasses instantion here, to really test this option directly
	PrefixCountSetBits<4           >::registerInSubclassFactory();
	PrefixCountSetBits<8           >::registerInSubclassFactory();
	PrefixCountSetBits<4, Exclusive>::registerInSubclassFactory();
	PrefixCountSetBits<8, Exclusive>::registerInSubclassFactory();
	// Enumerated pretransform

	Scan < 4,        functors::plus<uint32_t>, uint32_t, Inclusive,
		functors::enumerated::as_enumerated_unary<functors::plus<uint32_t> >>::registerInSubclassFactory();
	Scan < 4,        functors::plus<uint32_t>, uint32_t, Exclusive,
		functors::enumerated::as_enumerated_unary<functors::plus<uint32_t> >>::registerInSubclassFactory();
}


} // namespace reduction
} // namespace kernel_tests
