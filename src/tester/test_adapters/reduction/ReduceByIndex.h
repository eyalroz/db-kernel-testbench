/**
 * Note: This is _not_ the "reduce by key" as defined e.g. in libcub
 * (http://docs.thrust.googlecode.com/hg/group__reductions.html). There, it is assumed that
 * keys are consecutive (or rather that the reduction is for consecutive elements with the
 * same key), which makes for an entirely different problem.
 *
 * It might be worthwhile to consider implementing that as well as a template variants of
 * this kernel.
 *
 * Anyway, what we have here is what at Hua Wei research I refered to as "group-indexed
 * aggregate sum", i.e. what you need to run when you have the SQL query:
 *
 *   SELECT some_aggregate_function(c1) FROM t1 GROUP BY c2;
 *
 * for scalar c1, and c2 which is not given as-is, but rather as non-negative indices which
 * correspond of some c2 value-to-index injective map (i.e. we get "group indices").
 * The reduction operation is very often sum().
 * The case of count() aggregation is _not_ handled here, because there we don't get any data
 * as input, just the c2 values (i.e. group indices).
 *
 * TODO:
 * - Enforce Index being an unsigned integral type.
 *
 */
#ifndef KERNEL_TEST_ADAPTER_REDUCE_BY_KEY_CUH_
#define KERNEL_TEST_ADAPTER_REDUCE_BY_KEY_CUH_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace reduction {

template<unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
class ReduceByIndex : public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>> {
public:
	using size_type = size_type_by_index_size<IndexSize>;
	using result_type = typename ReductionOp::result_type;
	using reduction_index_type = uint_t<ReductionIndexSize>;
	using reduction_index_size_type = size_type_by_index_size<ReductionIndexSize>;
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(ReduceByIndex);

protected:

	// Too bad we can't put namespaces within class
	void generate_data   (span<InputDatum> buffer_to_fill);
	void generate_indices(span<reduction_index_type> buffer_to_fill);
	void generate_target (span<InputDatum> buffer_to_fill);
	void generate_expected_target(
		span<result_type>                 buffer_to_fill,
		span<const result_type>           input_target,
		span<const InputDatum>            data,
		span<const reduction_index_type>  indices);

	reduction_index_size_type num_distinct_indices         {    10 };
	distribution_t<InputDatum> used_index_distribution {
		distribution_t<InputDatum>::uniform(0, 9)
	};
	bool            indices_are_consecutive_zero_based { false }; // This does not support "true" for now!
	bool            permutation_as_indices             { false };
	distribution_t<InputDatum> data_distribution       {
		distribution_t<InputDatum>::uniform(
			(std::is_unsigned<InputDatum>::value ?   0 : -50), // minimum
			(std::is_unsigned<InputDatum>::value ? 100 :  50)  // maximum
		)
	};
};

//template<typename InputDatum, typename Index>
//using SumByKey = ReduceByIndex<cuda::plus<InputDatum>, InputDatum, Index>;

} // namespace reduction
} // namespace kernel_tests

#endif /* KERNEL_TEST_ADAPTER_REDUCE_BY_KEY_CUH_ */
