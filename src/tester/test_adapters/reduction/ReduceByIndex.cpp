/*
 * Various notes and ruminations:
 *
 *
 * * Block-final results to global results:
 *
 * After we're done work at the block level, we need to synch/combine results across the
 * entire GPU. This  typically requires either atomic operations or having a second kernel
 * execute and wrap things up. While atomics might cost us some, it has been suggested:
 *
 *   https://devblogs.nvidia.com/parallelforall/faster-parallel-reductions-kepler/
 *
 * That the overhead of an extra kernel is not worth it. Of course, that link
 * only regards a single reduction, so who knows.
 *
 */

// #define IDENTIFY_FUNCTION_ON_CUDA_DEVICE

#include "ReduceByIndex.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"

#include <algorithm>
#include <iostream>

#include "kernel_wrappers/reduction/multi_reduce/sparse_to_dense.cu"

namespace kernel_tests {
namespace reduction {

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
KernelTestAdapter::BufferDescriptors ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>
	::getBufferDescriptors() const
{
	return {
		{ "target",  BufferDescriptor::make<result_type          >(num_distinct_indices, BufferDirection::InOut) },
		{ "indices", BufferDescriptor::make<reduction_index_type >(config.test_length,   BufferDirection::Input)  },
		{ "data",    BufferDescriptor::make<InputDatum           >(config.test_length,   BufferDirection::Input)  },
	};
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
void ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::generate_data(
	span<InputDatum> data)
{
	util::random::fill(data, data_distribution, this->random_engine);
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
void ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::generate_indices(
	span<reduction_index_type> indices)
{
	if (permutation_as_indices) {
		iota(indices, 0);
		shuffle(indices, random_device);
	}
	else {
		util::random::fill(indices, used_index_distribution, this->random_engine);
	}
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
void ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::generate_target(
	span<InputDatum> target)
{
	fill(target, ReductionOp::neutral_value());
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
void ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::generate_expected_target(
		span<result_type>                 target,
		span<const result_type>           input_target,
		span<const InputDatum>            data,
		span<const reduction_index_type>  indices)
{
	copy(input_target, target.begin());

	typename ReductionOp::accumulator acc_op;

	for(size_t i = 0; i < config.test_length; i++) {
		acc_op(target[indices[i]], data[i]);
	}
}


// TODO: Have either Buffer.h or some other .h/.hpp have named constructor idioms -
// for constants, arithmetic progressions, geometric progressions, and
// any lambda
template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
KernelTestAdapter::BufferSizes ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>
	::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("target")) {
		auto input_target     = buffers.at("target" ).as_span<result_type          >();
		generate_target(input_target);
	}
	if (need_to_generate("data")) {
		auto data             = buffers.at("data"   ).as_span<InputDatum           >();
		generate_data(data);
	}
	if (need_to_generate("indices")) {
		auto indices          = buffers.at("indices").as_span<reduction_index_type >();
		generate_indices(indices);
	}

	return NoBuffersToResize;
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
KernelTestAdapter::BufferSizes ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>
	::generateExpectedOutputs(const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	// Note: We don't support generation of only the initial target buffer, or only
	// the expected resulting target buffer - only both of them
	if (need_to_generate("target")) {
		auto expected_target  = expected_buffers.at("target").as_span<result_type>();
		auto input_target  = buffers.at("target" ).as_span< const result_type          >();
		auto data          = buffers.at("data"   ).as_span< const InputDatum           >();
		auto indices       = buffers.at("indices").as_span< const reduction_index_type >();

		generate_expected_target(expected_target, input_target, data, indices);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
LaunchConfigurationSequence ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args =
	{
		{ "length",               config.test_length                          },
		{ "num_distinct_indices", (size_t) num_distinct_indices               },
	};
	if (config.thread_serialization_factor) { extra_args.insert(
		{ "serialization_factor", (size_t) config.thread_serialization_factor.value() });
	}

	::cuda::kernels::reduction::multi_reduce::dynamic_num_reductions::sparse_to_dense
	 	 ::kernel_t<IndexSize, ReductionOp, InputDatum, ReductionIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("reduction::multi_reduce::dynamic_num_reductions::sparse_to_dense");
	return { { kernel_name, kernel.get_device_function(), launch_config } };

}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
void ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>
	::launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	result_type*                target     = buffers.at("target" ).as_span<result_type          >().data();
	const InputDatum*           data       = buffers.at("data"   ).as_span<InputDatum           >().data();
	const reduction_index_type* indices    = buffers.at("indices").as_span<reduction_index_type >().data();
	size_type length = config.test_length;

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;

	::cuda::kernels::reduction::multi_reduce::dynamic_num_reductions::sparse_to_dense
	 	 ::kernel_t<IndexSize, ReductionOp, InputDatum, ReductionIndexSize> kernel;

	if (config.thread_serialization_factor) {
		cuda::serialization_factor_t serialization_factor(config.thread_serialization_factor.value());
		kernel.enqueue_launch(stream, launch_config,
			MAKE_KERNEL_ARGUMENTS(target, data, indices, length, num_distinct_indices, serialization_factor)
		);
	}
	else {
		kernel.enqueue_launch(stream, launch_config,
			MAKE_KERNEL_ARGUMENTS(target, data, indices, length, num_distinct_indices)
		);
	}

}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
std::string ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::buildPrettyKernelName() const
{
	return
		"ReduceByIndex of data of type" + util::type_name<InputDatum>() + " and indices of type"
		+ util::type_name<reduction_index_type>() + ", with the reduction result having type "
		+ util::type_name<result_type>()
		+ ".";
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
std::string ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::getDescription() const {
	return
		"An adapter for testing implementations of Reduce-by-Key, a.k.a. "
		"Group-Indexed Aggeregate Sum.\n"
		"\n"
		 "Note: This is _not_ the \"reduce by key\" as defined e.g. in libcub "
		"(http://docs.thrust.googlecode.com/hg/group__reductions.html). There, it is assumed that "
		"keys are consecutive (or rather that the reduction is for consecutive elements with the "
		"same key), which makes for an entirely different problem.\n "
		"It might be worthwhile to consider implementing that as well as a template variants of"
		"this kernel.\n"
		"Anyway, what we have here is \"group-indexed aggregate sum\", i.e. what you need to run "
		"when you have the SQL query:\n"
		 " SELECT some_aggregate_function(c1) FROM t1 GROUP BY c2;\n "
		"for scalar c1, and c2 which is not given as-is, but rather as non-negative indices which "
		"correspond of some c2 value-to-index injective map (i.e. we get \"group indices\"). "
		"The reduction operation is very often sum(). "
		"The case of count() aggregation is _not_ handled here, because there we don't get any data "
		"as input, just the c2 values (i.e. group indices). ";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<unsigned IndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
ReduceByIndex<IndexSize, ReductionOp, InputDatum, ReductionIndexSize>::ReduceByIndex(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	resolveOption(num_distinct_indices, ConfigurationKeySet {
		"num-indices", "number-of-indices", "num-distinct-indices" }
	);
	resolveOption(used_index_distribution, ConfigurationKeySet {
		"used-indices", "actual-indices", "used-index-distribution", "index-distribution",
		"actually-used-indices"});
	resolveOption(data_distribution, ConfigurationKeySet {
		"data", "input-data", "data-distribution", "input-data-distribution", "input-distribution"}
	);
	resolveOption(indices_are_consecutive_zero_based, ConfigurationKeySet {
		"consecutive-zero-baed", "simple-indices"            }
	);
	resolveOption(permutation_as_indices , ConfigurationKeySet {
		"permutation-as-indices", "permutation", "permute"   }
	);
	bool use_all_ones_as_data = resolveOption(ConfigurationKeySet {
		"use-all-ones-as-input", "trivial-data", "trivial", "data-all-ones"},
		false);

	if (use_all_ones_as_data) {
		data_distribution = distribution_t<InputDatum>::single_value(1);
	}
	if (permutation_as_indices) {
		num_distinct_indices = config.test_length;
	}

	util::enforce(!indices_are_consecutive_zero_based,
		"Do net yet support a non-dense index set, sorry.");


	// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Number of (distinct) indices",          num_distinct_indices),
		make_pair("Input data distribution",               data_distribution),
		make_pair("Distribution of indices to use",        used_index_distribution),
		make_pair("Use a permutation for the indices",     permutation_as_indices),
		make_pair("Set all data to a trivial value (1)",   use_all_ones_as_data)
	);

}

static_block {
	namespace functors = cuda::functors;
	//              IndexSize  ReductionOp                   InputDatum   ReductionIndexSize
	//------------------------------------------------------------------------------------------------
	ReduceByIndex < 4,         functors::plus<int32_t>,      int32_t,     4 >::registerInSubclassFactory();
	ReduceByIndex < 4,         functors::plus<uint32_t>,     uint32_t,    4 >::registerInSubclassFactory();
	ReduceByIndex < 4,         functors::maximum<int32_t>,   int32_t,     4 >::registerInSubclassFactory();
	ReduceByIndex < 4,         functors::maximum<uint32_t>,  uint32_t,    4 >::registerInSubclassFactory();
	ReduceByIndex < 8,         functors::plus<uint32_t>,     uint32_t,    4 >::registerInSubclassFactory();

	// TODO: Test instantiation for smaller ReductionIndexSize values
}

} // namespace reduction
} // namespace kernel_tests


