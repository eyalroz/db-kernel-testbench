
#include "UnaryOperation.h"
#include "tester/test_adapters/make_kernel_arguments.h"
#include "tester/load_data.h"

#include <cuda/functors.hpp>

#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"

#include "kernel_wrappers/enumerated_elementwise/unary_operation.cu"

namespace kernel_tests {
namespace elementwise {

template<typename Op>
KernelTestAdapter::BufferDescriptors UnaryOperation<Op>::getBufferDescriptors() const
{
	return {
		{ "output", BufferDescriptor::make<output_type>(config.test_length, BufferDirection::Output) },
		{ "input",  BufferDescriptor::make<input_type> (config.test_length, BufferDirection::Input ) }
	};
}

template<typename Op>
LaunchConfigurationSequence UnaryOperation<Op>::resolveLaunchConfigurations() const
{
	auto device_properties(cuda::device::get(config.test_device).properties());
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };
	::cuda::kernels::enumerated_elementwise::unary::kernel_t<Op> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("enumerated_elementwise::unary_operation");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template<typename Op>
void UnaryOperation<Op>::generate_input(
	span<input_type>        input_buffer
)
{
	input_type value = std::is_unsigned<input_type>::value ? 0 : -10;
	if (std::is_floating_point<input_type>::value) { value += 0.5; }
	generate(input_buffer, [&value]() { return value += 1; });
}

template<typename Op>
KernelTestAdapter::BufferSizes UnaryOperation<Op>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("input")) {
		auto input = buffers.at("input").as_span<input_type>();
		generate_input(input);
	}
	return NoBuffersToResize;
}

template<typename Op>
void UnaryOperation<Op>::generate_expected_output(
	span<output_type>       expected_output,
	span<const input_type>  input
)
{
	Op op;
	// std::transform(input, input+config.test_length, expected_results, op);
	for (size_t i = 0; i < config.test_length; i++) {
		expected_output[i] = op(i, input[i]);
	}
}

template<typename Op>
KernelTestAdapter::BufferSizes UnaryOperation<Op>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("output")) {
		auto input = buffers.at("input").as_span<input_type>();
		auto output = expected_buffers.at("output").as_span<output_type>();

		generate_expected_output(output, input);
	}
	return NoBuffersToResize;
}

template<typename Op>
void UnaryOperation<Op>::launchKernels(
	const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area,
	cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto output       = buffers.at("output").as_span<output_type      >().data();
	auto input        = buffers.at("input" ).as_span<const input_type >().data();
	index_type length = config.test_length;

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::enumerated_elementwise::unary::kernel_t<Op>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(output, input, length)
	);
}

template<typename Op>
std::string UnaryOperation<Op>::buildPrettyKernelName() const
{
	using input_type  = typename Op::argument_type;
	using output_type = typename Op::result_type;
	std::ostringstream ss;
	bool same_type = std::is_same<input_type, output_type>::value;

	ss	<< "Apply the enumerated unary operation "
		<< util::type_name<Op>() << " to each pair (index,element) in the array";
	if (!same_type) {
		ss << " of type " << util::type_name<input_type>() << ", ";
	}
	ss << ", with the results ";
	if (!same_type) {
		ss << ", of type" << util::type_name<output_type>() << ",";
	}
	ss << " placed in another array.";
	return ss.str();
}

template<typename Op>
std::string UnaryOperation<Op>::getDescription() const {
	return
		"A single implementation of the application of any \"enumerated unary\" "
		"function (that is, a function which takes an element index and its value).\n"
		"Does _not_ support (for now) in-place application of the operator.\n"
		"Also doesn't support arbitrary lambdas provided at run time.\n";
}

template<typename Op>
UnaryOperation<Op>::UnaryOperation(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	input_file_column_index =
		resolveOption<typename decltype(input_file_column_index)::value_type>(
			ConfigurationKeySet {"input-file-column", "input-file-column-index", "input-column" });

	maybe_print_extra_configuration(
		make_pair("Column to use in input data file", input_file_column_index)
	);
}

static_block {
	using namespace cuda;
	using namespace ::kernel_tests::elementwise;
	using cuda::functors::enumerated::as_enumerated_unary;
	namespace functors = cuda::functors;

	//               Op
	//                                                           LHS       RHS      Result
	//------------------------------------------------------------------------------------
	UnaryOperation < as_enumerated_unary < functors::plus      < uint32_t, int32_t, int32_t > > >::registerInSubclassFactory();
	UnaryOperation < as_enumerated_unary < functors::multiplies< uint32_t, float,   float   > > >::registerInSubclassFactory();
	UnaryOperation < as_enumerated_unary < functors::plus      < uint32_t, int32_t, int32_t > > >::registerInSubclassFactory();
	UnaryOperation < as_enumerated_unary < functors::multiplies< uint64_t, float,   float   > > >::registerInSubclassFactory();


}

} // namespace elementwise
} // namespace kernel_tests
