#ifndef KERNEL_TEST_ADAPTER_UNARY_OPERATION_CUH_
#define KERNEL_TEST_ADAPTER_UNARY_OPERATION_CUH_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace elementwise {

// Note the Size template parameter here is implicit in Op
template<typename Op>
class UnaryOperation :
	public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<UnaryOperation<Op>>
{
public:
	using index_type   = typename Op::enumerator_type; // also used for the overall length of the indexed array
	using input_type   = typename Op::argument_type;
	using output_type  = typename Op::result_type;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(UnaryOperation);

protected:
	void generate_input(
		span<input_type>        input_buffer
	);
	void generate_expected_output(
		span<output_type>       expected_output,
		span<const input_type>  input
	);

	util::optional<unsigned> input_file_column_index;
};

} // namespace elementwise
} // namespace kernel_tests


#endif /* KERNEL_TEST_ADAPTER_UNARY_OPERATION_CUH_ */
