

#ifndef SRC_KERNEL_SPECIFIC_GATHER_H_
#define SRC_KERNEL_SPECIFIC_GATHER_H_

#include <cuda/api/kernel_launch.cuh>
#include "tester/KernelTestAdapter.h"
#include "util/distribution.h"
#include <cuda/bit_vector.cuh>

namespace kernel_tests {

template <unsigned OutputIndexSize, unsigned InputIndexSize>
class GatherBits: public KernelTestAdapter, public mixins::TestAdapterNameBuilder<GatherBits<OutputIndexSize, InputIndexSize>> {
public:
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(GatherBits);

	using input_index_type = uint_t<InputIndexSize>;
	using output_index_type = uint_t<OutputIndexSize>;
	using bit_container_type = cuda::standard_bit_container_t;
	using input_size_type = size_type_by_index_size<InputIndexSize>;
	using output_size_type = size_type_by_index_size<OutputIndexSize>;

	static_assert(
		std::is_same<
			typename cuda::bit_vector<input_index_type>::container_type,
			typename cuda::bit_vector<output_index_type>::container_type>::value,
		"Unexpected difference in bit container types");

protected:
	// Too bad we can't put namespaces within class
	void generate_input_bits(
		span<bit_container_type>       buffer_to_fill);
	void generate_indices(
		span<input_index_type>         buffer_to_fill);
	void generate_expected_gathered_bits(
		span<bit_container_type>       buffer_to_fill,
	    span<const bit_container_type> input_bits,
		span<const input_index_type>   indices);

protected:
	output_size_type         num_bit_indices               { (output_size_type) config.test_length };
	input_size_type          num_input_bits                {
		std::min<size_t>(
			111111,
			util::fill_decimal_digits<input_index_type>(1)
		)
	};
		// ... which is also the length in bits of the output
	bool                      random_permutation_as_indices { false };
	bool                      use_all_ones_as_data          { false };
	bool                      use_parity_as_data            { false };
	bool                      clear_input_slack_bits        { false };
	double                    input_bit_on_probability      { 0.5   };
	distribution_t<input_index_type> index_distribution            {
		distribution_t<input_index_type>::uniform(0, num_input_bits - 1) };
};


} // namespace kernel_tests


#endif /* SRC_KERNEL_SPECIFIC_GATHER_H_ */
