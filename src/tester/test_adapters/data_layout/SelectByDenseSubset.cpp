
#include "SelectByDenseSubset.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "tester/test_adapters/comparators.h"
#include "tester/test_adapters/mismatch_printers.h"

#include <cuda/api/kernel_launch.cuh>
#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"
#include "util/prettyprint.hpp" // for streaming sets
#include "util/integer.h"

#include <algorithm>
#include <unordered_set>
#include <iostream>
#include <bitset>

#include "kernel_wrappers/data_layout/select_by_dense_subset.cu"

enum { DefaultInverseSelectionProbability = 4 }; // i.e. default selectivity is 1 / this value
constexpr double DefaultAdhesionProbability = 0.0;

using kernel_sortedness_t = ::cuda::kernels::set_representation::sortedness_t;
// Convert the tester's sortedness_t to the kernel's sortedness_t. Yes, it's fugly

constexpr kernel_sortedness_t convert(
	const kernel_tests::sortedness_t& sortedness) {
	return sortedness == kernel_tests::sortedness_t::Sorted ?
		kernel_sortedness_t::Sorted : kernel_sortedness_t::Unsorted;
}

namespace kernel_tests {

namespace mixins {

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
struct TestAdapterNameBuilder<SelectByDenseSubset<Sortedness, IndexSize, ElementSize>> {
static std::string  buildName() {
		std::string s = KernelTestAdapter::buildName<SelectByDenseSubset<Sortedness, IndexSize, ElementSize>>();
		util::replace_all(s, "(kernel_tests::sortedness_t)0", "Unsorted");
		util::replace_all(s, "(kernel_tests::sortedness_t)1", "Sorted");
		return s;
	}
};
} // namespace mixins


template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
KernelTestAdapter::BufferDescriptors SelectByDenseSubset<Sortedness, IndexSize, ElementSize>
	::getBufferDescriptors() const
{
	auto num_dense_elements = bit_vector::num_elements_necessary_for(config.test_length);

	constexpr auto selected_comparator = (Sortedness == sortedness_t::Sorted) ?
			BufferDescriptor::trivial_comparator<index_type> :
			kernel_tests::comparators::unsorted_selected_elements<IndexSize, element_type>;
	constexpr auto selected_mismatch_printer = (Sortedness == sortedness_t::Sorted) ?
			BufferDescriptor::trivialMismatchPrinter :
			kernel_tests::mismatch_printers::unsorted_selected_elements<IndexSize, element_type>;

	return {
		{ "selected",     BufferDescriptor::make<element_type> (config.test_length, BufferDirection::Output,
			selected_comparator, selected_mismatch_printer)},
		{ "num_selected", BufferDescriptor::make<size_type>    (1,                  BufferDirection::InOut)  },
		{ "raw_dense",    BufferDescriptor::make<bit_container_type>
		                                                       (num_dense_elements, BufferDirection::Input).bit_container()  },
		{ "input_data",   BufferDescriptor::make<element_type> (config.test_length, BufferDirection::Input)},
	};
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
void SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::generate_input_data(span<element_type> input_data)
{
	if (use_all_ones_as_data) {
		fill(input_data, 1);
		return;
	}
	if (use_identity_as_data) {
		iota(input_data, 0);
		return;
	}
	auto random_engine = this->random_engine;
	auto data_distribution = this->data_distribution;

	generate(input_data,
		[&data_distribution, &random_engine]() {
			return util::random::sample_from(data_distribution, random_engine);
		}
	);
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
void SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::generate_raw_dense(
	span<bit_container_type> raw_dense)
{
	std::bernoulli_distribution basic(basic_selection_probability);
	std::bernoulli_distribution adhesion(adhesion_probability);
	auto random_engine = this->random_engine;

	bit_vector bv(raw_dense.data(), raw_dense.size());
	bv.unset_all(); // not actually necessary
	auto bit_value = util::random::sample_from(basic, random_engine);
	bv.set_to(0, bit_value);

	bool previous_bit_value = bit_value;

	for(size_type i = 1; i < raw_dense.size(); i++) {
		bool independent_value = util::random::sample_from(basic, random_engine);
		bool adheres_to_previous = util::random::sample_from(adhesion, random_engine);
		bool bit_value = adheres_to_previous ? previous_bit_value : independent_value;
		bv.set_to(i, bit_value);
		previous_bit_value = bit_value;
	}
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
void SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::generate_num_selected(
	span<size_type> num_selected)
{
	num_selected[0] = 0;
}


template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
void SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::generate_expected_selected_and_num_selected(
	span<element_type>               expected_selected,
	span<size_type>                  expected_num_selected,
	span<const bit_container_type>   raw_dense,
	span<const element_type>         input_data)
{
	const bit_vector bv(
		const_cast<bit_container_type*>(raw_dense.begin()),
		static_cast<size_type>(config.test_length));
	size_type output_pos = 0;

	for(size_type i = 0; i < config.test_length; i++) {
		if (bv[i]) { expected_selected[output_pos++] = input_data[i]; }
	}
	expected_num_selected[0] = output_pos;
	// TODO: Should we manually zero the rest of the expected_selected buffer here?
	// probably not, but here's the code:
	//
	//	if (config.zero_output_and_scratch_buffers) {
	//		util::memzero(expected_selected + output_pos,
	//			(config.test_length - output_pos) * ElementSize);
	//	}
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
KernelTestAdapter::BufferSizes SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("input_data")) {
		auto  input_data   = buffers.at("input_data"   ).as_span<element_type>();
		generate_input_data(input_data);
	}
	if (need_to_generate("raw_dense")) {
		auto  raw_dense    = buffers.at("raw_dense"    ).as_span<bit_container_type>();
		generate_raw_dense(raw_dense);
	}
	if (need_to_generate("num_selected")) {
		auto num_selected = buffers.at("num_selected" ).as_span<size_type>();
		generate_num_selected(num_selected);
	}

	return NoBuffersToResize;
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
KernelTestAdapter::BufferSizes SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("selected") and need_to_generate("num_selected") ) {
		auto expected_selected      = expected_buffers.at("selected"    ).as_span<element_type>();
		auto expected_num_selected  = expected_buffers.at("num_selected").as_span<size_type   >();
		auto raw_dense    = buffers.at("raw_dense" ).as_span<const bit_container_type>();
		auto input_data   = buffers.at("input_data").as_span<const element_type>();

		generate_expected_selected_and_num_selected(
			expected_selected, expected_num_selected, raw_dense, input_data);
		return NoBuffersToResize;
	}
	if (need_to_generate("selected") or need_to_generate("num_selected") ) {
		throw util::invalid_argument("Can only generate \"selected\" and \"num_selected\" together or not at all");
	}
	return NoBuffersToResize;
}

template<sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
LaunchConfigurationSequence SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args =
	{
		{ "length",                  config.test_length },
	};

	::cuda::kernels::select_by_dense_subset::kernel_t<IndexSize, ElementSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("data_layout::select_by_dense_subset");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}


template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
void SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::launchKernels(
	const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area,
	cuda::stream_t& stream) const
{
	using integer_type = uint_t<ElementSize>;

	util::enforce(device_scratch_area.empty());
	auto selected     = buffers.at("selected"     ).as_span<integer_type            >().data();
	auto num_selected = buffers.at("num_selected" ).as_span<size_type               >().data();
	auto input_data   = buffers.at("input_data"   ).as_span<const integer_type      >().data();
	auto raw_dense    = buffers.at("raw_dense"    ).as_span<const bit_container_type>().data();
	size_type domain_size = config.test_length;

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;

	auto warps_per_block = util::div_rounding_up(launch_config.block_dimensions.x, cuda::warp_size);
	util::enforce(warps_per_block > 0, "There seem to be 0 warps per block planned.");
	unsigned shared_memory_required_for_one_element_per_warp = warps_per_block * IndexSize;
	cuda::memory::shared::size_t per_warp_shared_mem_elements =
		launch_config.dynamic_shared_memory_size / shared_memory_required_for_one_element_per_warp;

	if (config.be_verbose) {
		std::cout << "Note: Of the total shared memory of "
			<< launch_config.dynamic_shared_memory_size << " bytes, "
			<< per_warp_shared_mem_elements * IndexSize
			<< " bytes will used by each warp for its buffer of selected output, which fit "
			<< per_warp_shared_mem_elements
			<< " selected output elements.\n";
	}

	cuda::kernels::select_by_dense_subset::kernel_t<IndexSize, ElementSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(selected, num_selected, input_data,
			raw_dense, domain_size, per_warp_shared_mem_elements)
	);
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
std::string SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::buildPrettyKernelName() const
{
	return util::concat_string(
		"Select a subset of a data column, using a dense-representation of a subset of "
		"the record indices. The data column width is ", ElementSize, ".");
}

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
std::string SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::getDescription() const {
	return
		"WRITEME";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
SelectByDenseSubset<Sortedness, IndexSize, ElementSize>::SelectByDenseSubset(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;
	static_assert(Sortedness == sortedness_t::Unsorted,
		"Sorted output variant not currently supported.");

	resolveOption(
		use_all_ones_as_data,
		ConfigurationKeySet
		{"use-all-ones-as-input", "data-all-ones", "all-ones-data", "all-ones", "all-ones-input"}
	);
	resolveOption(
		use_identity_as_data,
		ConfigurationKeySet {"use-identity-as-input", "data-identity", "identity-data", "identity", "iota", "iota-data"}
	);
	resolveOption(
		basic_selection_probability,
		ConfigurationKeySet {"basic-selection-probability", "selectivity", "selection-probability" }
	);
	resolveOption(
		adhesion_probability,
		ConfigurationKeySet {"adhesion-probability", "probability-of-adhesion" }
	);
	resolveOption(data_distribution,
		ConfigurationKeySet {"data", "data-distribution", "test-data", "test-data-distribution"});


	// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Basic probability of element selection",      basic_selection_probability),
		make_pair("Probability of adhesion to previous element", adhesion_probability),
		make_pair("Data distribution",                           data_distribution),
		make_pair("Set data[i] to 1",                            use_all_ones_as_data),
		make_pair("Set data[i] to i",                            use_identity_as_data)
	);

	util::enforce(util::between_or_equal(adhesion_probability, 0., 1.),
		"Adhesion probability must be strictly between 0 and 1");
	util::enforce(util::strictly_between(basic_selection_probability, 0., 1.),
		"Basic selectivity must be strictly between 0 and 1");
}

static_block {

	constexpr auto unsorted = sortedness_t::Unsorted;

	//                    Sortedness  IndexSize  ElementSize
	//---------------------------------------------------------------
	SelectByDenseSubset < unsorted,   4,         1 >::registerInSubclassFactory();
	SelectByDenseSubset < unsorted,   4,         2 >::registerInSubclassFactory();
	SelectByDenseSubset < unsorted,   4,         4 >::registerInSubclassFactory();
	SelectByDenseSubset < unsorted,   8,         1 >::registerInSubclassFactory();
	SelectByDenseSubset < unsorted,   8,         2 >::registerInSubclassFactory();
	SelectByDenseSubset < unsorted,   8,         4 >::registerInSubclassFactory();

	// No sorted variant yet
//	SelectByDenseSubset < sorted, 8, 4 >::registerInSubclassFactory();
//	SelectByDenseSubset < sorted, 8, 8>::registerInSubclassFactory();
}

} // namespace kernel_tests


