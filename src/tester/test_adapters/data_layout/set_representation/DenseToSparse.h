#ifndef TEST_ADAPTERS_SET_REPRESENTATION_CUH_
#define TEST_ADAPTERS_SET_REPRESENTATION_CUH_

#include "tester/KernelTestAdapter.h"
#include <cuda/bit_vector.cuh>
#include "kernels/data_layout/set_representation/common.h"

namespace kernel_tests {
namespace set_representation {

// Yes, it duplicates other definitions, need to put this elsewhere
enum class sortedness_t : bool {
	Unsorted = false,
	Sorted   = true,
};

template <sortedness_t Sortedness, unsigned IndexSize>
class DenseToSparse : public KernelTestAdapter, mixins::TestAdapterNameBuilder<DenseToSparse<Sortedness, IndexSize>> {
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using domain_size_type = size_type;
	using bit_container_type = cuda::standard_bit_container_t;
	using bit_vector = cuda::bit_vector<index_type>;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(DenseToSparse);

protected:
	void generate_raw_dense(
		span<bit_container_type>        buffer_to_fill);
	void generate_expected_sparse_and_sparse_length(
		span<index_type>                expected_sparse,
		span<size_type>                 expected_sparse_length,
		span<const bit_container_type>  raw_dense);


	double basic_selection_probability                   { 1.0 / 4 };
		// Ignoring dependence on other values, this is the probability of
		// an element in the set's domain to be marked selected in the
		// generated test input
	double adhesion_probability                          { 0.5 };
		// The probability of the next element's membership in the set
		// being determined by the previous element's membership rather
		// than by a Bernouli experiment with the basic selection
		// probability
};

} // namespace set_representation
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_SET_REPRESENTATION_CUH_ */
