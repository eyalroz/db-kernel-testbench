
#include "DenseToSparse.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "tester/test_adapters/comparators.h"
#include "tester/test_adapters/mismatch_printers.h"

#include "kernels/data_layout/set_representation/common.h"

#include <cuda/api/kernel_launch.cuh>
#include "util/miscellany.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"
#include "util/stl_algorithms.hpp"
#include "util/algorithm_container_adapters.hpp"
#include "util/prettyprint.hpp" // for streaming sets

#include <algorithm>
#include <unordered_set>
#include <iostream>
#include <bitset>

#include "kernel_wrappers/data_layout/set_representation/dense_to_sparse.cu"

using kernel_sortedness_t = ::cuda::kernels::set_representation::sortedness_t;
// Convert the tester's sortedness_t to the kernel's sortedness_t. Yes, it's fugly

constexpr kernel_sortedness_t convert(
	const kernel_tests::set_representation::sortedness_t& sortedness) {
	return sortedness == kernel_tests::set_representation::sortedness_t::Sorted ?
		kernel_sortedness_t::Sorted : kernel_sortedness_t::Unsorted;
}

namespace kernel_tests {

namespace mixins {

template <set_representation::sortedness_t Sortedness, unsigned IndexSize>
struct TestAdapterNameBuilder<set_representation::DenseToSparse<Sortedness, IndexSize>> {
static std::string  buildName() {
		std::string s = KernelTestAdapter::buildName<set_representation::DenseToSparse<Sortedness, IndexSize>>();
		util::replace_all(s, "(kernel_tests::set_representation::sortedness_t)0", "Unsorted");
		util::replace_all(s, "(kernel_tests::set_representation::sortedness_t)1", "Sorted");
		return s;
	}
};
} // namespace mixins

namespace set_representation {

template <sortedness_t Sortedness, unsigned IndexSize>
KernelTestAdapter::BufferDescriptors DenseToSparse<Sortedness, IndexSize>
	::getBufferDescriptors() const
{
	size_t num_dense_elements =
		cuda::bit_vector<size_t>::num_elements_necessary_for(config.test_length);

	constexpr auto sparse_comparator = (Sortedness == sortedness_t::Sorted) ?
			BufferDescriptor::trivial_comparator<index_type> :
			kernel_tests::comparators::unsorted_sparse_output<IndexSize>;
	constexpr auto sparse_mismatch_printer = (Sortedness == sortedness_t::Sorted) ?
			BufferDescriptor::trivialMismatchPrinter :
			kernel_tests::mismatch_printers::unsorted_sparse_output<IndexSize>;

	return {
		{ "sparse",        BufferDescriptor::make<index_type        >( config.test_length, BufferDirection::Output, sparse_comparator, sparse_mismatch_printer)},
		{ "sparse_length", BufferDescriptor::make<size_type         >( 1,                  BufferDirection::InOut                                             )},
		{ "dense_bits",     BufferDescriptor::make<bit_container_type>( num_dense_elements, BufferDirection::Input                                             ).bit_container()  },
	};
}

template <sortedness_t Sortedness, unsigned IndexSize>
void DenseToSparse<Sortedness, IndexSize>::generate_raw_dense(
	span<bit_container_type> buffer_to_fill)
{
	if (basic_selection_probability == 0.0 or basic_selection_probability == 1.0) {
		// mark all unselected or all selected - by making all bits 0
		// or all bits 1
		util::fill_bits(buffer_to_fill, (basic_selection_probability == 0.0) ? 0 : 1);
		return;
	}
	// TODO: Perhaps a short-cut for adhesion 0 and adhesion 1?


	std::bernoulli_distribution basic(basic_selection_probability);
	std::bernoulli_distribution adhesion(adhesion_probability);
	auto random_engine = this->random_engine;

	cuda::bit_vector<index_type> bv(buffer_to_fill.data(), buffer_to_fill.size());
	bv.unset_all(); // not actually necessary
	auto bit_value = util::random::sample_from(basic, random_engine);
	bv.set_to(0, bit_value);
/*	if (config.be_verbose) {
		std::cout << 0 << ":"
			<< " previous_dense_element_value = 0"
			<< " adheres_to_previous = ."
			<< " independent_value = " << bit_value
			<< " bit_value = " << bit_value;
		std::cout << " ... container is now: " << bv.element_for(0) << '\n';
	}
*/
	bool previous_bit_value = bit_value;

	for(index_type i = 1; i < config.test_length; i++) {
		bool independent_value = util::random::sample_from(basic, random_engine);
		bool adheres_to_previous = util::random::sample_from(adhesion, random_engine);
		bool bit_value = adheres_to_previous ? previous_bit_value : independent_value;
		bv.set_to(i, bit_value);
/*
		if (config.be_verbose) {
			std::cout << i << ":"
				<< " previous_dense_element_value = " << previous_bit_value
				<< " adheres_to_previous = " << adheres_to_previous
				<< " independent_value = " << independent_value
				<< " bit_value = " << bit_value;
				<< " ... container is now: " << bv.element_for(i) << '\n';
		}
*/
		previous_bit_value = bit_value;
	}
}

template <sortedness_t Sortedness, unsigned IndexSize>
void DenseToSparse<Sortedness, IndexSize>::generate_expected_sparse_and_sparse_length(
	span<index_type>                expected_sparse,
	span<size_type>                 expected_sparse_length,
	span<const bit_container_type>  dense_bits)
{
	const bit_vector bv(
		const_cast<bit_container_type*>(dense_bits.data()),
		static_cast<index_type>(config.test_length));
	index_type output_pos = 0;

	for(index_type i = 0; i < config.test_length; i++) {
		if (bv[i]) { expected_sparse[output_pos++] = i; }
	}
	expected_sparse_length[0] = output_pos;
	if (config.zero_output_and_scratch_buffers) {
		util::memzero(expected_sparse.begin() + output_pos,
			(config.test_length - output_pos) * IndexSize);
	}
}

template <sortedness_t Sortedness, unsigned IndexSize>
KernelTestAdapter::BufferSizes DenseToSparse<Sortedness, IndexSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("dense_bits")) {
		auto  dense_bits     = buffers.at("dense_bits").as_span<bit_container_type>();
		generate_raw_dense(dense_bits);
	}
	auto sparse_length = buffers.at("sparse_length").as_span<size_type>();
	sparse_length[0] = 0;
	return NoBuffersToResize;
}

template <sortedness_t Sortedness, unsigned IndexSize>
KernelTestAdapter::BufferSizes DenseToSparse<Sortedness, IndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("sparse") and need_to_generate("sparse_length")) {
		auto expected_sparse        =	expected_buffers.at("sparse"       ).as_span<index_type>();
		auto expected_sparse_length =	expected_buffers.at("sparse_length").as_span<size_type>();
		auto dense_bits = buffers.at("dense_bits").as_span<const bit_container_type>();

		generate_expected_sparse_and_sparse_length(
			expected_sparse, expected_sparse_length, dense_bits);
	}
	else if (need_to_generate("sparse") or need_to_generate("sparse_length")) {
		throw util::invalid_argument("Can only generate \"sparse\" and \"sparse_length\" together or not at all");
	}
	return NoBuffersToResize;
}

template<sortedness_t Sortedness, unsigned IndexSize>
LaunchConfigurationSequence DenseToSparse<Sortedness, IndexSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args =
	{
		// no serialization_factor
		{ "domain_size",       config.test_length },
	};

	::cuda::kernels::set_representation::dense_to_sparse::kernel_t<IndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("data_layout::set_representation::dense_to_sparse");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}


template <sortedness_t Sortedness, unsigned IndexSize>
void DenseToSparse<Sortedness, IndexSize>::launchKernels(
	const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area,
	cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto sparse          = buffers.at("sparse"       ).as_span<index_type              >().data();
	auto sparse_length   = buffers.at("sparse_length").as_span<size_type               >().data(); // it's a length-1 buffer
	auto dense_bits      = buffers.at("dense_bits"   ).as_span<const bit_container_type>().data();
	index_type domain_size = config.test_length;

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	static_assert(Sortedness == sortedness_t::Unsorted,
		"Conversion to sorted sparse representation not yet / not ever will be "
		"implemented: If you want that kind of conversion, perform a CountSetBits"
		"first to get a position anchoring vector.");

//	if (config.be_verbose) {
//		std::cout << "Note: Of the total shared memory of "
//			<< launch_config.dynamic_shared_memory_size << " bytes, "
//			<< per_warp_shared_mem_elements * sizeof(index_type)
//			<< " bytes will used by each warp for its buffer of sparse output, which fit "
//			<< per_warp_shared_mem_elements
//			<< " sparse output elements.\n";
//	}
	cuda::kernels::set_representation::dense_to_sparse::kernel_t<IndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(sparse, sparse_length, dense_bits, domain_size)
	);
}

template <sortedness_t Sortedness, unsigned IndexSize>
std::string DenseToSparse<Sortedness, IndexSize>::buildPrettyKernelName() const
{
	return
		util::concat_string(
			"Conversion between a dense representations of a subset of a domain "
			"(using one bit per element, selected or not) to a ",
			(Sortedness == sortedness_t::Sorted ? "Sorted" : "Unsorted"),
			" sparse representation with element indices (and set sizes) of size ",
			IndexSize, " bytes.");

}

template <sortedness_t Sortedness, unsigned IndexSize>
std::string DenseToSparse<Sortedness, IndexSize>::getDescription() const {
	return
		"WRITEME";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<sortedness_t Sortedness, unsigned IndexSize>
DenseToSparse<Sortedness, IndexSize>::DenseToSparse(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	util::enforce(config.test_length <= std::numeric_limits<size_type_by_index_size<IndexSize>>::max(),
		"Specified test length ", config.test_length, " requires indices"
		"up to ", config.test_length - 1, ", which cannot fit in index "
		"variables of the specified IndexSize template parameter (",
		IndexSize, " bytes)");

	resolveOption(
		basic_selection_probability,
		ConfigurationKeySet {"basic-selection-probability", "selectivity", "selection-probability" }
	);
	resolveOption(
		adhesion_probability,
		ConfigurationKeySet {"adhesion-probability", "probability-of-adhesion" }
	);

	// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Set domain size (equal to test length)",      config.test_length),
		make_pair("Basic probability of element selection",      basic_selection_probability),
		make_pair("Probability of adhesion to previous element", adhesion_probability)
	);

	util::enforce(util::between_or_equal(adhesion_probability, 0.0, 1.0),
		"Adhesion probability must be between 0 and 1");
	util::enforce(util::between_or_equal(basic_selection_probability, 0.0, 1.0),
		"Basic selectivity must be between 0 and 1");
}

static_block {
	//              Sortedness              IndexSize
	//-------------------------------------------------------------
	DenseToSparse < sortedness_t::Unsorted, 4 >::registerInSubclassFactory();
	DenseToSparse < sortedness_t::Unsorted, 8 >::registerInSubclassFactory();
//	DenseToSparse < sortedness_t::Sorted,   4 >::registerInSubclassFactory();
//	DenseToSparse < sortedness_t::Sorted,   8 >::registerInSubclassFactory();
}

} // set_representation
} // namespace kernel_tests


