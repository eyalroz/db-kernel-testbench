#ifndef TEST_ADAPTERS_SET_REPRESENTATION_CUH_
#define TEST_ADAPTERS_SET_REPRESENTATION_CUH_

#include "tester/KernelTestAdapter.h"
#include <cuda/bit_vector.cuh>
#include "kernels/data_layout/set_representation/common.h"

namespace kernel_tests {
namespace set_representation {

// Yes, it duplicates the definition in the kernels.cuh file; but -
// I don't want people including that... I wonder where else I could
// put it
enum class sortedness_t : bool {
	Unsorted = false,
	Sorted   = true,
};

template <sortedness_t Sortedness, unsigned IndexSize>
class SparseToDense : public KernelTestAdapter, mixins::TestAdapterNameBuilder<SparseToDense<Sortedness, IndexSize>> {
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using bit_container_type = cuda::standard_bit_container_t;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(SparseToDense);
	//static std::string buildName();

protected:
	void generate_sparse(
		span<index_type>          sparse);
	// Zero it, really
	void generate_raw_dense(
		span<bit_container_type>  raw_dense);
	void generate_expected_raw_dense(
		span<bit_container_type>  buffer_to_fill,
		span<const index_type>    sparse);

	enum { DefaultInverseSelectivityFactor = 10 }; // i.e. aiming for a selectivity of about 1/10 by default

	size_type domain_size;
		// How many elements might possibly be in the set?
		// (this is also equal to the maximum possible value of elements in sparse,
		// plus 1; and it is _not_ the same as config.test_length which is the
		// length of the sparse array). But it is initialized relative to
		// config.test_length, to get a density of about DefaultInverseSelectivityFactor

	distribution_t<index_type> sparse_element_sampling_distribution;
		// used for sampling elements into the sparse subset
};

} // namespace set_representation
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_SET_REPRESENTATION_CUH_ */
