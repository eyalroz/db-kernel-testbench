
#include "SparseToDense.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "kernels/data_layout/set_representation/common.h"

#include <cuda/api/kernel_launch.cuh>
#include "util/miscellany.hpp"
#include "util/boolean.hpp"
#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/type_name.hpp"

#include <algorithm>
#include <unordered_set>
#include <iostream>

#include "kernel_wrappers/data_layout/set_representation/sparse_to_dense.cu"

using kernel_sortedness_t = ::cuda::kernels::set_representation::sortedness_t;
// Convert the tester's sortedness_t to the kernel's sortedness_t. Yes, it's fugly

constexpr kernel_sortedness_t convert(
	const kernel_tests::set_representation::sortedness_t& sortedness) {
	return sortedness == kernel_tests::set_representation::sortedness_t::Sorted ?
		kernel_sortedness_t::Sorted : kernel_sortedness_t::Unsorted;
}

namespace kernel_tests {

namespace mixins {

template <set_representation::sortedness_t Sortedness, unsigned IndexSize>
struct TestAdapterNameBuilder<set_representation::SparseToDense<Sortedness, IndexSize>> {
static std::string  buildName() {
		std::string s = KernelTestAdapter::buildName<set_representation::SparseToDense<Sortedness, IndexSize>>();
		util::replace_all(s, "(kernel_tests::set_representation::sortedness_t)0", "Unsorted");
		util::replace_all(s, "(kernel_tests::set_representation::sortedness_t)1", "Sorted");
		return s;
	}
};
} // namespace mixins


namespace set_representation {


template <sortedness_t Sortedness, unsigned IndexSize>
KernelTestAdapter::BufferDescriptors SparseToDense<Sortedness, IndexSize>
	::getBufferDescriptors() const
{
	return {
		{ "raw_dense", BufferDescriptor::make<bit_container_type>(
			cuda::bit_vector<index_type>::num_elements_necessary_for(domain_size),
			BufferDirection::InOut).bit_container() }, // It's InOut since we want to zero it
		{ "sparse",    BufferDescriptor::make<index_type> (config.test_length, BufferDirection::Input)  },
	};
}

template <sortedness_t Sortedness, unsigned IndexSize>
void SparseToDense<Sortedness, IndexSize>::generate_sparse(
	span<index_type> buffer_to_fill)
{
	auto random_engine = this->random_engine;

	util::enforce(config.test_length <= domain_size);

	// When most elements are supposed to be present, it's easier to sample
	// the missing elements, since a uniform sample has a probability
	// of not having been selected as missing
	//
	// TODO: Don't use this trick when the distribution is not uniform
	//
	bool sample_missing = config.test_length > domain_size / 2;
	index_type num_to_sample =
		sample_missing ? domain_size - config.test_length : config.test_length;

	std::unordered_set<index_type> sampled_elements;

	for(index_type i = 0; i < num_to_sample; i ++) {
		index_type sample;
		do {
			sample = util::random::sample_from(sparse_element_sampling_distribution, random_engine);
		} while(sampled_elements.find(sample) != sampled_elements.end());
		sampled_elements.insert(sample);
	}

	index_type* it = buffer_to_fill.begin();
	if (Sortedness == kernel_tests::set_representation::sortedness_t::Unsorted && !sample_missing) {
		for(const auto& i : sampled_elements) { *(it++) = i; }
	}
	else for(index_type i = 0; i < domain_size; i ++) {
		if (logical_xor(sample_missing, util::contains(sampled_elements, i))) {
			*(it++) = i;
		}
	}
	util::enforce((size_t) (it - buffer_to_fill.begin()) == config.test_length,
		"Was supposed to have inserted ", config.test_length,
		" elements, but somehow have inserted ", it - buffer_to_fill.begin());
	if (Sortedness == ::kernel_tests::set_representation::sortedness_t::Unsorted) {
		// We might actually want a more complex shuffling logical, with
		// some sorted fragments rather than a basically uniformly-distributed
		// permutation, but we don't have that now. Also, need to figure out how
		// to pass the random_engine into here
		shuffle(buffer_to_fill, random_device);
	}
}

template <sortedness_t Sortedness, unsigned IndexSize>
void SparseToDense<Sortedness, IndexSize>::generate_raw_dense(
	span<bit_container_type> buffer_to_fill)
{
	auto dense_size =
		sizeof(bit_container_type) *
		cuda::bit_vector<index_type>::num_elements_necessary_for(domain_size);
	util::enforce(dense_size >= buffer_to_fill.size() * sizeof(bit_container_type));
	util::memzero(buffer_to_fill.data(), dense_size);
}

template <sortedness_t Sortedness, unsigned IndexSize>
void SparseToDense<Sortedness, IndexSize>::generate_expected_raw_dense(
	span<bit_container_type>  buffer_to_fill,
	span<const index_type>          sparse)
{
	generate_raw_dense(buffer_to_fill); // zero it out
	cuda::bit_vector<index_type> bv (buffer_to_fill, domain_size);
	for(size_t i = 0; i < config.test_length; i++) {
		bv.set(sparse[i]);
	}
}

template <sortedness_t Sortedness, unsigned IndexSize>
KernelTestAdapter::BufferSizes SparseToDense<Sortedness, IndexSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("sparse")) {
		auto sparse    = buffers.at("sparse").as_span<index_type>();
		generate_sparse(sparse);
	}
	if (need_to_generate("raw_dense")) {
		auto  raw_dense = buffers.at("raw_dense").as_span<bit_container_type>();
		generate_raw_dense(raw_dense);
	}
	return NoBuffersToResize;
}

template <sortedness_t Sortedness, unsigned IndexSize>
KernelTestAdapter::BufferSizes SparseToDense<Sortedness, IndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("raw_dense")) {
		auto sparse = buffers.at("sparse").as_span<const index_type>();
		auto expected_raw_dense = expected_buffers.at("raw_dense").as_span<bit_container_type>();

		generate_expected_raw_dense(expected_raw_dense, sparse);
	}
	return NoBuffersToResize;
}

template<sortedness_t Sortedness, unsigned IndexSize>
LaunchConfigurationSequence SparseToDense<Sortedness, IndexSize>::resolveLaunchConfigurations() const
{
	auto  device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args =
	{
		{ "sparse_length",    config.test_length },
		// The following is probably unnecessary
		{ "sorted",           convert(Sortedness) },
	};

	::cuda::kernels::set_representation::sparse_to_dense::kernel_t<convert(Sortedness), IndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("data_layout::set_representation::sparse_to_dense");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}


template <sortedness_t Sortedness, unsigned IndexSize>
void SparseToDense<Sortedness, IndexSize>::launchKernels(
	const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area,
	cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto raw_dense = buffers.at("raw_dense").as_span<bit_container_type>().data();
	auto sparse    = buffers.at("sparse").as_span<const index_type>().data();
	index_type sparse_length = config.test_length;

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::set_representation::sparse_to_dense::kernel_t<convert(Sortedness), IndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(raw_dense, sparse, sparse_length, domain_size)
	);
}

template <sortedness_t Sortedness, unsigned IndexSize>
std::string SparseToDense<Sortedness, IndexSize>::buildPrettyKernelName() const
{
	return
		util::concat_string(
			(Sortedness == sortedness_t::Sorted ? "Sorted" : "Unsorted"),
			" sparse to dense set representation conversion (with domain from 0 ",
			"upto a value specified at run time, and using set element indices of size ",
			IndexSize, ".");
}

template <sortedness_t Sortedness, unsigned IndexSize>
std::string SparseToDense<Sortedness, IndexSize>::getDescription() const {
	return
		"WRITEME";
}

template<sortedness_t Sortedness, unsigned IndexSize>
SparseToDense<Sortedness, IndexSize>::SparseToDense(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	util::enforce(config.test_length <= std::numeric_limits<size_type_by_index_size<IndexSize>>::max(),
		"Specified test length ", config.test_length, " requires indices"
		"up to ", config.test_length - 1, "which cannot fit in index "
		"variables of the specified IndexSize template parameter (",
		IndexSize, " bytes)");

	domain_size =
		resolveOption(ConfigurationKeySet {"domain-size" },
			config.test_length * DefaultInverseSelectivityFactor);

	util::enforce(domain_size >= config.test_length,
		"The number of sparse input elements requested (", config.test_length,
		") exceed the size of the domain (", domain_size,
		") - but in a set representation, every sparse element is a "
		"distinct element in the domain.");

	sparse_element_sampling_distribution =
		resolveOption(ConfigurationKeySet {
			"sparse-element-sampling-distribution",
			"element-sampling-distribution",
		},
		distribution_t<index_type>::uniform(0, domain_size - 1));

	maybe_print_extra_configuration(
		make_pair("Domain size for set elements", domain_size),
		make_pair("Element sampling distribution", sparse_element_sampling_distribution)
	);

}

static_block {
	//              Sortedness              IndexSize
	//-------------------------------------------------------------
	SparseToDense < sortedness_t::Sorted,   4 >::registerInSubclassFactory();
	SparseToDense < sortedness_t::Sorted,   8 >::registerInSubclassFactory();
	SparseToDense < sortedness_t::Unsorted, 4 >::registerInSubclassFactory();
	SparseToDense < sortedness_t::Unsorted, 8 >::registerInSubclassFactory();
}

} // set_representation
} // namespace kernel_tests


