#ifndef TEST_ADAPTERS_SET_REPRESENTATION_CUH_
#define TEST_ADAPTERS_SET_REPRESENTATION_CUH_

#include "tester/KernelTestAdapter.h"
#include <cuda/bit_vector.cuh>
#include "kernels/data_layout/set_representation/common.h"
#include "util/integer.h"

namespace kernel_tests {

// Yes, it duplicates other definitions, need to put this elsewhere
enum class sortedness_t : bool {
	Unsorted = false,
	Sorted   = true,
};

template <sortedness_t Sortedness, unsigned IndexSize, unsigned ElementSize>
class SelectByDenseSubset : public KernelTestAdapter, mixins::TestAdapterNameBuilder<SelectByDenseSubset<Sortedness, IndexSize, ElementSize>> {
public:
	using element_type = uint_t<ElementSize>;
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using bit_container_type = cuda::standard_bit_container_t;
	using bit_vector = cuda::bit_vector<index_type>;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(SelectByDenseSubset);

protected:
	void generate_raw_dense(
		span<bit_container_type>         raw_dense);
	void generate_input_data(
		span<element_type>               input_data);
	void generate_num_selected(
		span<size_type>                  num_selected);
	void generate_expected_selected_and_num_selected(
		span<element_type>               expected_selected,
		span<size_type>                  expected_num_selected,
		span<const bit_container_type>   raw_dense,
		span<const element_type>         input_data);


	double basic_selection_probability    { 0.25 };
		// Ignoring dependence on other values, this is the probability of
		// an element in the set's domain to be marked selected in the
		// generated test input
	double adhesion_probability           { 0.50 };
		// The probability of the next element's membership in the set
		// being determined by the previous element's membership rather
		// than by a Bernouli experiment with the basic selection
		// probability
	bool      use_all_ones_as_data = false;
	bool      use_identity_as_data = false;

	enum {default_range_size = 100};
	distribution_t<element_type> data_distribution {
		distribution_t<element_type>::uniform(0, default_range_size - 1)
	};

};

} // namespace kernel_tests

#endif /* TEST_ADAPTERS_SET_REPRESENTATION_CUH_ */
