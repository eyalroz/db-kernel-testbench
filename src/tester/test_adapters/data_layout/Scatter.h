
#ifndef SRC_KERNEL_SPECIFIC_SCATTER_H_
#define SRC_KERNEL_SPECIFIC_SCATTER_H_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {

template <unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
class Scatter: public KernelTestAdapter, public mixins::TestAdapterNameBuilder<Scatter<OutputIndexSize, ElementSize, InputIndexSize>> {
public:
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Scatter);

	using element_type = uint_t<ElementSize>;
	using input_index_type = uint_t<InputIndexSize>;
	using output_index_type = uint_t<OutputIndexSize>;
	using input_size_type = size_type_by_index_size<InputIndexSize>;
	using output_size_type = size_type_by_index_size<OutputIndexSize>;

protected:

	// Too bad we can't put namespaces within class
	void generate_data(
		span<element_type> buffer_to_fill);
	void generate_target(
		span<element_type> buffer_to_fill);
	void generate_indices(
		span<output_index_type> buffer_to_fill);
	void generate_expected_target(
		span<element_type>             buffer_to_fill,
		span<const element_type>       initial_target,
		span<const element_type>       data,
		span<const output_index_type>  indices);

protected:
	output_size_type   target_length; // defaults to 2 * config.test_length + 1
	output_index_type   min_possible_index;
	output_index_type   max_possible_index;
	bool                permutation_as_indices = false;
	bool                use_all_ones_as_data   = false;
	bool                use_identity_as_data   = false;
	element_type        target_fill_value      = 0;

	enum {default_range_size = 100};
	distribution_t<element_type> data_distribution {
		distribution_t<element_type>::uniform(11, 99)
	};
	// Not using an index sampling distribution, as we only
	// support uniform-with-no-repetition sampling for the indices
};

} // namespace kernel_tests


#endif /* SRC_KERNEL_SPECIFIC_SCATTER_H_ */
