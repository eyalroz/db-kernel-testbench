#include "Gather.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/distribution.h"

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/stl_algorithms.hpp"

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>

#include "kernel_wrappers/data_layout/gather.cu"

namespace kernel_tests {

template <unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
std::string Gather<OutputIndexSize, ElementSize, InputIndexSize>::buildPrettyKernelName() const
{
	return
		util::concat_string(
		"A Gather operation - target[i] = input[origins[i]] - with data of ",
		"size ", ElementSize, " and indices of size", InputIndexSize, ".");
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
KernelTestAdapter::BufferDescriptors Gather<OutputIndexSize, ElementSize, InputIndexSize>::getBufferDescriptors() const
{
	return {
		{ "reordered_data", BufferDescriptor::make<element_type    >(num_indices,        BufferDirection::Output) },
		{ "data",           BufferDescriptor::make<element_type    >(config.test_length, BufferDirection::Input)  },
		{ "indices",        BufferDescriptor::make<input_index_type>(num_indices,        BufferDirection::Input)  },
	};
}

template <unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
inline Gather<OutputIndexSize, ElementSize, InputIndexSize>::Gather(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	// TODO: This should be a feature of all kernel testers
	util::enforce(config.test_length <= std::numeric_limits<size_type_by_index_size<OutputIndexSize>>::max(),
		"Specified test length ", config.test_length, " requires indices"
		"up to ", config.test_length - 1, "which cannot fit in index "
		"variables of the specified IndexSize template parameter (",
		InputIndexSize, " bytes)");

	resolveOption(num_indices,
		ConfigurationKeySet {"number-of-indices", "num-indices"} );
	resolveOption(random_permutation_as_indices,
		ConfigurationKeySet {"permutation-as-indices", "permutation", "permute"});
	resolveOption(use_all_ones_as_data,
		ConfigurationKeySet {"trivial-data", "trivial", "data-all-ones"});
	resolveOption(use_identity_as_data,
		ConfigurationKeySet {"identity-as-data", "identity-for-data", "use-identity-for-data"});
	resolveOption(data_distribution,
		ConfigurationKeySet {"data", "data-distribution", "test-data", "test-data-distribution"});
	force_shared_mem_caching = resolveOption<bool>(
		ConfigurationKeySet {"force-shared-mem-caching", "force-shared-memory-caching",
			"shared-mem-caching", "cache-in-shared-mem", "cache-in-shared-memory",
			"cache-input-in-shared-mem", "cache-data-in-shared-mem" });
	resolveOption(index_distribution,
		ConfigurationKeySet {
			"indices", "index-distribution", "index", "distribution-of-indices",
			"keys", "key-distribution", "distribution-of-keys"});

	util::enforce(
		(random_permutation_as_indices == true) +
		(use_all_ones_as_data == true) +
		(use_identity_as_data == true) <= 1,
		"Conflicting configuration settings specified");

	if (random_permutation_as_indices) {
		num_indices = config.test_length;
		util::enforce(std::numeric_limits<input_index_type>::max() >= config.test_length,
			"The index type cannot accommodate the maximum index of an input array element.");
	}

	using std::make_pair;
	maybe_print_extra_configuration(
		make_pair("Number of indices",                        num_indices),
		make_pair("Use a random permutation for the indices", random_permutation_as_indices),
		make_pair("Set all data to a trivial value (to 1)",   use_all_ones_as_data),
		make_pair("Input datum distribution",                 data_distribution),
		make_pair("Gather index distribution",                index_distribution),
		make_pair("Set data[i] to i",                         use_identity_as_data),
		make_pair("Force caching of input in shared mem",     force_shared_mem_caching)
	);
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
std::string Gather<OutputIndexSize, ElementSize, InputIndexSize>::getDescription() const
{
	return util::concat_string(
		"Gather - a data reordering operation of the form "
		"target[] = input[indices[i]] , i.e. the target "
		"is populated contiguously, but the input is used "
		"non-contiguously; it is possible some input data is not "
		"used at all, while some input data is used multiple "
		"time. Also, the input might be shorter or longer than"
		" the Gather target. Indices into the output have size ",
		OutputIndexSize, "; the data elements have size ",
		ElementSize, "; and indices into the input have size", InputIndexSize, ".");
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
LaunchConfigurationSequence Gather<OutputIndexSize, ElementSize, InputIndexSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type args = {
		{ "num_indices",    (size_t) num_indices         },
		{ "data_length",    (size_t) config.test_length  }
	};

	if (force_shared_mem_caching) {	args.insert(
		{ "cache_input_data_in_shared_mem", force_shared_mem_caching.value() });
	}

	if (config.thread_serialization_factor) {
		cuda::serialization_factor_t serialization_factor = config.thread_serialization_factor.value();
		args.insert( { "serialization_factor", serialization_factor});
	}
	::cuda::kernels::gather::kernel_t<OutputIndexSize, ElementSize, InputIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("data_layout::gather");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Gather<OutputIndexSize, ElementSize, InputIndexSize>::generate_data(gsl::span<element_type> buffer_to_fill)
{
	if      (use_all_ones_as_data) { fill(buffer_to_fill, 1); return; }
	else if (use_identity_as_data) { iota(buffer_to_fill, 0);  return ;}

	auto random_engine = this->random_engine;
	auto data_distribution = this->data_distribution;
	generate(
		buffer_to_fill,
		[&]() {	return util::random::sample_from(data_distribution, random_engine);	}
	);
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Gather<OutputIndexSize, ElementSize, InputIndexSize>::generate_indices(gsl::span<input_index_type> buffer_to_fill)
{
	if (random_permutation_as_indices) {
		iota(buffer_to_fill, 0);
		shuffle(buffer_to_fill, random_device);
	}
	else {
		auto random_engine = this->random_engine;
		auto index_distribution = this->index_distribution;
		generate(
			buffer_to_fill,
			[&]() { return util::random::sample_from(index_distribution, random_engine); });
	}
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Gather<OutputIndexSize, ElementSize, InputIndexSize>::generate_expected_reordered_data(
	gsl::span<element_type>            buffer_to_fill,
	gsl::span<const element_type>      data,
	gsl::span<const input_index_type>  indices)
{
	util::enforce(buffer_to_fill.length() == num_indices, "Length mismatch");
	for(output_index_type i = 0; i < num_indices; i++) {
		buffer_to_fill[i] = data[indices[i]];
	}
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
KernelTestAdapter::BufferSizes Gather<OutputIndexSize, ElementSize, InputIndexSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("data")) {
		auto data    = buffers.at("data").as_span<element_type>();
		generate_data(data);
	}
	if (need_to_generate("indices")) {
		auto indices = buffers.at("indices").as_span<input_index_type>();
		generate_indices(indices);
	}

	return NoBuffersToResize;
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
KernelTestAdapter::BufferSizes Gather<OutputIndexSize, ElementSize, InputIndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("reordered_data")) {
		auto data     = buffers.at("data"   ).as_span<const element_type    >();
		auto indices  = buffers.at("indices").as_span<const input_index_type>();
		auto reordered_data = expected_buffers.at("reordered_data").as_span<element_type>();

		generate_expected_reordered_data(reordered_data, data, indices);
	}
	return NoBuffersToResize;
}


template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Gather<OutputIndexSize, ElementSize, InputIndexSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto reordered_data  = buffers.at("reordered_data").as_span<element_type          >().data();
	auto data            = buffers.at("data"          ).as_span<const element_type    >().data();
	auto indices         = buffers.at("indices"       ).as_span<const input_index_type>().data();
	input_size_type data_length   = config.test_length;

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	::cuda::kernels::gather::kernel_t<OutputIndexSize, ElementSize, InputIndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(reordered_data, data, indices, data_length, num_indices)
	);
}

static_block {
	namespace functors = cuda::functors;

	//       OutputSize  ElementSize  InputIndexSize
	//------------------------------------------------------------------
	Gather < 4,          1,           1 >::registerInSubclassFactory();
	Gather < 4,          2,           1 >::registerInSubclassFactory();
	Gather < 4,          4,           1 >::registerInSubclassFactory();
	Gather < 4,          8,           1 >::registerInSubclassFactory();

	Gather < 4,          1,           2 >::registerInSubclassFactory();
	Gather < 4,          2,           2 >::registerInSubclassFactory();
	Gather < 4,          4,           2 >::registerInSubclassFactory();
	Gather < 4,          8,           2 >::registerInSubclassFactory();

	Gather < 4,          1,           4 >::registerInSubclassFactory();
	Gather < 4,          2,           4 >::registerInSubclassFactory();
	Gather < 4,          4,           4 >::registerInSubclassFactory();
	Gather < 4,          8,           4 >::registerInSubclassFactory();

	Gather < 4,          4,           8 >::registerInSubclassFactory();

	Gather < 8,          4,           4 >::registerInSubclassFactory();
	Gather < 8,          8,           4 >::registerInSubclassFactory();
	Gather < 8,          4,           8 >::registerInSubclassFactory();
}

} // namespace kernel_tests
