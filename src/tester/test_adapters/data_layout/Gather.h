

#ifndef SRC_KERNEL_SPECIFIC_GATHER_H_
#define SRC_KERNEL_SPECIFIC_GATHER_H_

#include <cuda/api/kernel_launch.cuh>
#include "tester/KernelTestAdapter.h"
#include "util/distribution.h"

namespace kernel_tests {

template <unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
class Gather: public KernelTestAdapter, public mixins::TestAdapterNameBuilder<Gather<OutputIndexSize, ElementSize, InputIndexSize>> {
public:
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Gather);

	using element_type = uint_t<ElementSize>;
	using input_index_type = uint_t<InputIndexSize>;
	using output_index_type = uint_t<OutputIndexSize>;
	using input_size_type = size_type_by_index_size<InputIndexSize>;
	using output_size_type = size_type_by_index_size<OutputIndexSize>;

protected:
	// Too bad we can't put namespaces within class
	void generate_data(
		gsl::span<element_type>            buffer_to_fill);
	void generate_indices(
		gsl::span<input_index_type>        buffer_to_fill);
	void generate_expected_reordered_data(
		gsl::span<element_type>            buffer_to_fill,
		gsl::span<const element_type>      data,
		gsl::span<const input_index_type>  indices);

protected:
	output_size_type  num_indices                      { 111 };
		// The length of the indices array has nothing to do with the length
		// of the data array; either could be larger, and their types differ too
	bool              random_permutation_as_indices    {false};
	bool              use_all_ones_as_data             {false};
	bool              use_identity_as_data             {false};
	optional<bool>
		              force_shared_mem_caching         {};

	enum {default_range_size = 100};
	distribution_t<element_type>
	                  data_distribution { distribution_t<element_type>::uniform(11, 89)
	};
	distribution_t<input_index_type>
	                  index_distribution {
		distribution_t<input_index_type>::uniform(0, config.test_length - 1)
	};
};


} // namespace kernel_tests


#endif /* SRC_KERNEL_SPECIFIC_GATHER_H_ */
