#include "Scatter.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/static_block.h"
#include "util/math.hpp"

#include <algorithm>
#include <iomanip>
#include <iostream>

#include "kernel_wrappers/data_layout/scatter.cu"

namespace kernel_tests {

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
KernelTestAdapter::BufferDescriptors Scatter<OutputIndexSize, ElementSize, InputIndexSize>::getBufferDescriptors() const
{
	return {
		{ "target",  BufferDescriptor::make<element_type     >(target_length,       BufferDirection::InOut)  },
		{ "data",    BufferDescriptor::make<element_type     >(config.test_length,  BufferDirection::Input)  },
		{ "indices", BufferDescriptor::make<output_index_type>(config.test_length,  BufferDirection::Input)  },
	};
}

template <unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
std::string Scatter<OutputIndexSize, ElementSize, InputIndexSize>::buildPrettyKernelName() const
{
	return
		util::concat_string(
		"A Scatter operation - target[indices[i]] = input[i] - with data of ",
		"size ", ElementSize, " and indices of type ",
		util::type_name<output_index_type>(), " (input index size ", InputIndexSize, ").");
}

template <unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
inline Scatter<OutputIndexSize, ElementSize, InputIndexSize>::Scatter(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	util::enforce(config.test_length <= std::numeric_limits<size_type_by_index_size<OutputIndexSize>>::max(),
		"Specified test length ", config.test_length, " requires indices"
		"up to ", config.test_length - 1, "which cannot fit in index "
		"variables of the specified OutputIndexSize template parameter (",
		OutputIndexSize, " bytes)");

	target_length =
		resolveOption(ConfigurationKeySet {
			"target-length", "target-size"                      }, 2 * config.test_length + 1);
	util::enforce(target_length >=  config.test_length);
	util::enforce(std::numeric_limits<output_index_type>::max() >= target_length,
		"Cannot represent all target buffer positions using output indices of size " + util::type_name<output_index_type>());

	target_fill_value =
		resolveOption(ConfigurationKeySet {
			"initial-target-value", "const-target-value",
			"target-value", "target-fill"                       }, 0);
	permutation_as_indices =
		resolveOption(ConfigurationKeySet {
			"permutation-as-indices", "permutation", "permute"  }, false);
	use_all_ones_as_data   =
		resolveOption(ConfigurationKeySet {
			"trivial-data", "trivial", "data-all-ones"          }, false);
	use_identity_as_data =
		resolveOption(ConfigurationKeySet {
			"identity-as-data", "identity-for-data",
			"use-identity-for-data"                             }, false);
	min_possible_index =
		resolveOption(ConfigurationKeySet {
			"index-min","indices-min", "min-index"              }, 0);
	max_possible_index =
		resolveOption(ConfigurationKeySet {
			"index-max","indices-max", "max-index"              }, target_length - 1);
	util::enforce(max_possible_index - min_possible_index + 1 >= config.test_length);


	util::enforce(!(use_all_ones_as_data && use_identity_as_data),
			"Conflicting requirements regarding data.");

	using std::make_pair;
	maybe_print_extra_configuration(
		make_pair("Target area length",                        target_length),
		make_pair("Initial target value",                      target_fill_value),
		make_pair("Use a permutation for the indices",         permutation_as_indices),
		make_pair("Set data[i] to 1",                          use_all_ones_as_data),
		make_pair("Set data[i] to i",                          use_identity_as_data),
		make_pair("Minimum possible index into target array",  min_possible_index),
		make_pair("Maximum possible index into target array",  max_possible_index)
	);

	return;
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
std::string Scatter<OutputIndexSize, ElementSize, InputIndexSize>::getDescription() const
{
	return util::concat_string(
		"Scatter - a data reordering operation of the form "
		"target[indices[i]] = input[i] , i.e. each input datum"
		"overwrites an element in the target array - not "
		"necessarily contiguously. Some of the target area"
		"may be left as-is. If multiple input elements are"
		"directed to overwrite the same target element, the"
		"Scatter result for that element is undefined, and"
		"not required even to be one of these input data."
		"The data (and destination elements) are of size "
		, ElementSize, "and the input index size "
		, InputIndexSize , ".");
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
LaunchConfigurationSequence Scatter<OutputIndexSize, ElementSize, InputIndexSize>::resolveLaunchConfigurations() const
{

	// Note that, unlike in the Gather operation, the number of indices
	// and the data length are necessarily the same, and it's the target length
	// that differs - but we don't care about it, since we don't traverse the target

	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type args = {
		{ "data_length",                  (size_t) config.test_length },
	};

	::cuda::kernels::scatter::kernel_t<OutputIndexSize, ElementSize, InputIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("data_layout::scatter");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Scatter<OutputIndexSize, ElementSize, InputIndexSize>::generate_data(span<element_type> data)
{
	auto random_engine = this->random_engine;
	auto data_distribution = this->data_distribution;
	util::enforce(data.size() == config.test_length);

	if (use_all_ones_as_data) {
		fill(data, 1);
		return;
	}
	if (use_identity_as_data) {
		iota(data, 0);
		return;
	}
	generate(data,
		[&data_distribution, &random_engine]() {
			return util::random::sample_from(data_distribution, random_engine);
		});
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Scatter<OutputIndexSize, ElementSize, InputIndexSize>::generate_indices(span<output_index_type> indices)
{
	util::enforce(indices.size() == config.test_length);
	if (permutation_as_indices) {
		iota(indices, 0);
		shuffle(indices, random_device);
	}
	else {
		auto num_possible_indices = max_possible_index - min_possible_index;
		// Note the following computation can take up as much as 8 times
		// the space as the the target buffer (e.g. for 64-bitr indices and 8-bit datal
		// so careful with overly large target lengths.
		std::vector<output_index_type> all_possible_target_indices(num_possible_indices);
		iota(all_possible_target_indices, min_possible_index);
		shuffle(all_possible_target_indices, random_device);
		std::copy_n(all_possible_target_indices.begin(), config.test_length, indices.begin());
	}
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Scatter<OutputIndexSize, ElementSize, InputIndexSize>::generate_target(span<element_type> buffer_to_fill)
{
	util::enforce(target_length == buffer_to_fill.size());
	fill(buffer_to_fill, target_fill_value);
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Scatter<OutputIndexSize, ElementSize, InputIndexSize>::generate_expected_target(
	span<element_type>             expected_target,
	span<const element_type>       initial_target,
	span<const element_type>       data,
	span<const output_index_type>  indices)
{
	copy(initial_target, expected_target.begin());
	for(size_t i = 0; i < config.test_length; i++) {
		expected_target[indices[i]] = data[i];
	}
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
KernelTestAdapter::BufferSizes Scatter<OutputIndexSize, ElementSize, InputIndexSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("data")) {
		auto data    = buffers.at("data").as_span<element_type>();
		generate_data(data);
	}
	if (need_to_generate("indices")) {
		auto indices = buffers.at("indices").as_span<output_index_type>();
		generate_indices(indices);
	}
	if (need_to_generate("target")) {
		auto target  = buffers.at("target").as_span<element_type>();
		generate_target(target);
	}

	return NoBuffersToResize;
}

template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
KernelTestAdapter::BufferSizes Scatter<OutputIndexSize, ElementSize, InputIndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("target")) {
		auto expected_target = expected_buffers.at("target").as_span<element_type>();

		auto data            = buffers.at("data"   ).as_span<const element_type      >();
		auto indices         = buffers.at("indices").as_span<const output_index_type >();
		auto target          = buffers.at("target" ).as_span<element_type            >();

		generate_expected_target(expected_target, target, data, indices);
	}
	return NoBuffersToResize;
}


template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize>
void Scatter<OutputIndexSize, ElementSize, InputIndexSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());

	auto target   = buffers.at("target" ).as_span<element_type            >().data();
	auto data     = buffers.at("data"   ).as_span<const element_type      >().data();
	auto indices  = buffers.at("indices").as_span<const output_index_type >().data();
	input_size_type data_length = config.test_length;

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::scatter::kernel_t<OutputIndexSize, ElementSize, InputIndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(target, data, indices, data_length)
	);
}

static_block {
	namespace functors = cuda::functors;

	//        OutputIndexSize   ElementSize  InputIndexSize
	//------------------------------------------------------------------------------
	Scatter < 4,                1,           1        >::registerInSubclassFactory();
	Scatter < 4,                1,           2        >::registerInSubclassFactory();
	Scatter < 4,                1,           4        >::registerInSubclassFactory();
	Scatter < 4,                1,           8        >::registerInSubclassFactory();
	Scatter < 4,                2,           1        >::registerInSubclassFactory();
	Scatter < 4,                2,           2        >::registerInSubclassFactory();
	Scatter < 4,                2,           4        >::registerInSubclassFactory();
	Scatter < 4,                2,           8        >::registerInSubclassFactory();
	Scatter < 4,                4,           1        >::registerInSubclassFactory();
	Scatter < 4,                4,           2        >::registerInSubclassFactory();
	Scatter < 4,                4,           4        >::registerInSubclassFactory();
	Scatter < 4,                4,           8        >::registerInSubclassFactory();
	Scatter < 4,                8,           1        >::registerInSubclassFactory();
	Scatter < 4,                8,           2        >::registerInSubclassFactory();
	Scatter < 4,                8,           4        >::registerInSubclassFactory();
	Scatter < 4,                8,           8        >::registerInSubclassFactory();

	Scatter < 8,                1,           1        >::registerInSubclassFactory();
	Scatter < 8,                1,           2        >::registerInSubclassFactory();
	Scatter < 8,                1,           4        >::registerInSubclassFactory();
	Scatter < 8,                1,           8        >::registerInSubclassFactory();
	Scatter < 8,                2,           1        >::registerInSubclassFactory();
	Scatter < 8,                2,           2        >::registerInSubclassFactory();
	Scatter < 8,                2,           4        >::registerInSubclassFactory();
	Scatter < 8,                2,           8        >::registerInSubclassFactory();
	Scatter < 8,                4,           1        >::registerInSubclassFactory();
	Scatter < 8,                4,           2        >::registerInSubclassFactory();
	Scatter < 8,                4,           4        >::registerInSubclassFactory();
	Scatter < 8,                4,           8        >::registerInSubclassFactory();
	Scatter < 8,                8,           1        >::registerInSubclassFactory();
	Scatter < 8,                8,           2        >::registerInSubclassFactory();
	Scatter < 8,                8,           4        >::registerInSubclassFactory();
	Scatter < 8,                8,           8        >::registerInSubclassFactory();
}

} // namespace kernel_tests
