#include "GatherBits.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/bit_vector.cuh>

#include "util/distribution.h"
#include "tester/test_adapters/comparators.h"
#include "tester/test_adapters/mismatch_printers.h"

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/stl_algorithms.hpp"

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>

#include "kernel_wrappers/data_layout/gather_bits.cu"

namespace kernel_tests {

using cuda::bit_vector;
using cuda::const_bit_vector;

template <unsigned OutputIndexSize, unsigned InputIndexSize>
std::string GatherBits<OutputIndexSize, InputIndexSize>::buildPrettyKernelName() const
{
	return
		util::concat_string(
		"A Gather operation on bits; output indices are of size " , OutputIndexSize,
		" bytes and input indices are of size ", InputIndexSize, " bytes.");
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
KernelTestAdapter::BufferDescriptors GatherBits<OutputIndexSize, InputIndexSize>::getBufferDescriptors() const
{
	auto output_bit_vector_size_in_container_types = cuda::bit_vector<output_index_type>::num_elements_necessary_for(num_bit_indices);
	auto input_bit_vector_size_in_container_types = cuda::bit_vector<input_index_type>::num_elements_necessary_for(num_input_bits);
	return {
		{ "gathered_bits", BufferDescriptor::make<bit_container_type>(
			output_bit_vector_size_in_container_types,
			BufferDirection::Output,
			comparators::bit_vectors_by_test_length<OutputIndexSize>).bit_container()
		},
		{ "input_bits",    BufferDescriptor::make<bit_container_type>(input_bit_vector_size_in_container_types,  BufferDirection::Input).bit_container()  },
		{ "indices",       BufferDescriptor::make<input_index_type>  (num_bit_indices,                               BufferDirection::Input)  },
	};
}

template <unsigned OutputIndexSize, unsigned InputIndexSize>
inline GatherBits<OutputIndexSize, InputIndexSize>::GatherBits(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	util::enforce(config.test_length <= std::numeric_limits<output_index_type>::max(),
		"Specified test length exceeds the domain of type \"", util::type_name<output_index_type>(), "\"");
	resolveOption(num_bit_indices,
		ConfigurationKeySet {"number-of-indices", "num-bit-indices", "num-indices"} );
	// This is a bit fishy, but I'd rather allow num_bit_indices as a separate parameter
	// than only accept it as the test length
	if (config.test_length != num_bit_indices) { config.test_length = num_bit_indices; }
	auto configured_num_input_bits  = resolveOption<size_t>(
		ConfigurationKeySet {"num-input-bits", "input-length", "number-of-input-bits"});
	if (configured_num_input_bits) {
		util::enforce(configured_num_input_bits.value() <= std::numeric_limits<input_index_type>::max(),
			"The input index type (", util::type_name<input_index_type>, ") can only "
			"represent values up to ", std::numeric_limits<input_index_type>::max(), ", so "
			"an input length of ", configured_num_input_bits.value(), " is not supported.");
		num_input_bits = configured_num_input_bits.value();
		index_distribution = distribution_t<input_index_type>::uniform(0, num_input_bits - 1);
	}
	resolveOption(random_permutation_as_indices,
		ConfigurationKeySet {"permutation-as-indices", "permutation", "permute"});

	resolveOption(use_all_ones_as_data,
		ConfigurationKeySet {"trivial-data", "trivial", "data-all-ones"});
	resolveOption(clear_input_slack_bits,
		ConfigurationKeySet {"clear-input-slack-bits", "clear-slack", "clear-slack-bits",
			"clear-input-vector-slack",  "clear-input-bit-vector-slack" });
	resolveOption(use_parity_as_data,
		ConfigurationKeySet {
			"parity-as-data", "zero-one-pattern-as-data", "parity-for-data",
			"use-parity-as-data", "use-zero-one-pattern-as-data", "use-parity-for-data",});
	resolveOption(input_bit_on_probability,
		ConfigurationKeySet {
			"input-bits-probability-of-1", "input-bits-p", "input-bits-on-probability",
			"input-bit-probability-of-1", "input-bit-p", "input-bit-on-probability"
		});
	// Note that we may have "updated the default" at this point based on the
	// value specified as the number of input bits; but this can still be overriden by
	// the user
	resolveOption(index_distribution,
		ConfigurationKeySet {
			"indices", "index-distribution", "index", "distribution-of-indices",
			"keys", "key-distribution", "distribution-of-keys"});

	util::enforce(
		(random_permutation_as_indices == true) +
		(use_all_ones_as_data == true) +
		(use_parity_as_data == true) <= 1,
		"Conflicting configuration settings specified");

	if (random_permutation_as_indices) {
		num_bit_indices = config.test_length;
		num_input_bits = config.test_length;
		util::enforce(
			std::numeric_limits<input_index_type>::max() >= config.test_length &&
			std::numeric_limits<output_index_type>::max() >= config.test_length,
			"The index types cannot accommodate a permutation of the required length.");
	}

	using std::make_pair;
	maybe_print_extra_configuration(
		make_pair("Number of input bits",                   num_input_bits),
		make_pair("Number of bit indices (= output size)",  num_bit_indices),
		make_pair("Use a permutation for the indices",      random_permutation_as_indices),
		make_pair("Set all input_bits to a trivial value (to 1)", use_all_ones_as_data),
		make_pair("Bernoulli parameter for input bit sampling",
			                                                input_bit_on_probability),
		make_pair("Distribution of indices into the input", index_distribution),
		make_pair("Set input_bits[i] to i % 2",             use_parity_as_data),
		make_pair("Clear slack bits in last input bit container", clear_input_slack_bits)
	);
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
std::string GatherBits<OutputIndexSize, InputIndexSize>::getDescription() const
{
	return util::concat_string(
		"GatherBits - a bit reordering operation of the form "
		"target[i] = input[indices[i]] (when considering the inputs and"
		"outputs as arrays of bits), i.e. the target "
		"is populated contiguously, but the input is used "
		"incontiguously; it is possible some input input_bits is not "
		"used at all, while some input input_bits is used multiple "
		"time. Also, the input might be shorter or longer than"
		" the GatherBits target. the number of output bits (and hence of indices) "
		"is of size ", OutputIndexSize, " bytes; the size of input indices is ",
		InputIndexSize, " bytes."
	);
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
LaunchConfigurationSequence GatherBits<OutputIndexSize, InputIndexSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type args = {
		{ "num_input_bits",          (size_t) num_input_bits    },
		{ "num_bit_indices",         (size_t) num_bit_indices   },
	};

	if (config.thread_serialization_factor) {
		cuda::serialization_factor_t serialization_factor = config.thread_serialization_factor.value();
		args.insert( { "serialization_factor", serialization_factor});
	}

	::cuda::kernels::gather_bits::kernel_t<OutputIndexSize, InputIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("data_layout::gather_bits");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
void GatherBits<OutputIndexSize, InputIndexSize>::generate_input_bits(
	span<bit_container_type> buffer_to_fill)
{
	bit_vector<input_index_type> input_bits_vector(buffer_to_fill, num_input_bits);

	if      (use_all_ones_as_data) { input_bits_vector.set_all(); return; }
	else if (use_parity_as_data) {
		bool b = false;
		for(input_index_type bit_index = 0; bit_index < num_input_bits; bit_index++) {
			input_bits_vector.set_to(bit_index, b);
			b = !b;
		}
	}

	// This is a bit ugly, but I'm not sure distributions of bool's work yet
	auto input_bits_distribution = distribution_t<bit_container_type>::bernoulli(input_bit_on_probability);
	if (input_bits_distribution.mean() == 0.5) {
		// this is faster than sampling bits separately
		std::uniform_int_distribution<bit_container_type> distribution;
		generate(buffer_to_fill, [&]() {
			return util::random::sample_from(distribution, this->random_engine);
		});
	}
	for(input_index_type bit_index = 0; bit_index < num_input_bits; bit_index++) {
		auto sample = util::random::sample_from(input_bits_distribution, this->random_engine);
		input_bits_vector.set_to(bit_index, sample);
	}

	if (clear_input_slack_bits) {
		input_bits_vector.clear_slack();
	}

}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
void GatherBits<OutputIndexSize, InputIndexSize>::generate_indices(span<input_index_type> buffer_to_fill)
{
	if (random_permutation_as_indices) {
		iota(buffer_to_fill, 0);
		shuffle(buffer_to_fill, random_device);
	}
	else {
		auto random_engine = this->random_engine;
		auto index_distribution = this->index_distribution;
		generate(
			buffer_to_fill,
			[&]() { return util::random::sample_from(index_distribution, random_engine); });
	}
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
void GatherBits<OutputIndexSize, InputIndexSize>::generate_expected_gathered_bits(
	span<bit_container_type>       buffer_to_fill,
	span<const bit_container_type> input_bits,
	span<const input_index_type>          indices)
{
	util::enforce(
		buffer_to_fill.length() == bit_vector<output_index_type>::num_elements_necessary_for(num_bit_indices),
		"For an output of ", num_bit_indices, " bits, ",
		bit_vector<output_index_type>::num_elements_necessary_for(num_bit_indices), " bit container elements "
		"(of ", util::size_in_bits<bit_container_type>::value, " bits each) are required, but ",
		buffer_to_fill.length(), " have been made available.");
	bit_vector<output_index_type>       output_bits_vector(buffer_to_fill, num_bit_indices);
	const_bit_vector<input_index_type>  input_bits_vector(input_bits, num_input_bits);
	auto num_full_bit_containrs = num_bit_indices / output_bits_vector.bits_per_element;
	for(output_index_type container_index = 0; container_index < num_full_bit_containrs; container_index++) {
		bit_container_type bit_container = 0;
		for(unsigned i = 0; i < output_bits_vector.bits_per_element; i++) {
			auto overall_bit_index = output_bits_vector.bits_per_element * container_index + i;
			bit_vector<output_index_type>::set_to(bit_container, i, input_bits_vector[indices[overall_bit_index]]);
		}
//		output_bits_vector.set_to(i, input_bits_vector[indices[i]]);
		buffer_to_fill[container_index] = bit_container;
	}
	auto bits_in_last_container = num_bit_indices % output_bits_vector.bits_per_element;
	for(output_index_type i = num_bit_indices - bits_in_last_container; i < num_bit_indices; i++) {
		output_bits_vector.set_to(i, input_bits_vector[indices[i]]);
	}
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
KernelTestAdapter::BufferSizes GatherBits<OutputIndexSize, InputIndexSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("input_bits")) {
		auto input_bits = buffers.at("input_bits").as_span<bit_container_type>();
		generate_input_bits(input_bits);
	}

	if (need_to_generate("indices")) {
		auto indices    = buffers.at("indices").as_span<input_index_type>();
		generate_indices(indices);
	}
	return NoBuffersToResize;
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
KernelTestAdapter::BufferSizes GatherBits<OutputIndexSize, InputIndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("gathered_bits")) {
		auto gathered_bits    = expected_buffers.at("gathered_bits").as_span<bit_container_type>();
		auto input_bits = buffers.at("input_bits").as_span<const bit_container_type>();
		auto indices             = buffers.at("indices").as_span<const input_index_type>();

		generate_expected_gathered_bits(gathered_bits, input_bits, indices);
	}
	return NoBuffersToResize;
}

template<unsigned OutputIndexSize, unsigned InputIndexSize>
void GatherBits<OutputIndexSize, InputIndexSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto gathered_bits  = buffers.at("gathered_bits").as_span<bit_container_type       >().data();
	auto input_bits     = buffers.at("input_bits"   ).as_span<const bit_container_type >().data();
	auto indices        = buffers.at("indices"      ).as_span<const input_index_type          >().data();

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	::cuda::kernels::gather_bits::kernel_t<OutputIndexSize, InputIndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(gathered_bits, input_bits, indices, num_input_bits, num_bit_indices)
	);
}

static_block {
	namespace functors = cuda::functors;

	//           OutputIndexSize   InputIndexSize
	//------------------------------------------------------------------
	GatherBits < 4,               1  >::registerInSubclassFactory();
	GatherBits < 4,               2  >::registerInSubclassFactory();
	GatherBits < 4,               4  >::registerInSubclassFactory();
	GatherBits < 4,               8  >::registerInSubclassFactory();
	GatherBits < 8,               1  >::registerInSubclassFactory();
	GatherBits < 8,               2  >::registerInSubclassFactory();
	GatherBits < 8,               4  >::registerInSubclassFactory();
	GatherBits < 8,               8  >::registerInSubclassFactory();
}

} // namespace kernel_tests
