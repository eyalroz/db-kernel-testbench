#pragma once
#ifndef MISMATCH_PRINTERS_H_
#define MISMATCH_PRINTERS_H_

#include "tester/TestConfiguration.h"
#include "tester/BufferDescriptor.h"
#include "util/memory_region_extra.h"
#include "util/integer.h"

namespace kernel_tests {
namespace mismatch_printers {

template <unsigned IndexSize>
void unsorted_sparse_output(
	std::ostream&             os,
	const TestConfiguration&  config,
	const std::string&        buffer_name,
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected);

template <unsigned IndexSize, typename Datum = util::uint_t<IndexSize>>
void unsorted_selected_elements(
	std::ostream&             os,
	const TestConfiguration&  config,
	const std::string&        buffer_name,
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected);

} // namespace mismatch_printers
} // namespace kernel_tests

#endif /* MISMATCH_PRINTERS_H_ */
