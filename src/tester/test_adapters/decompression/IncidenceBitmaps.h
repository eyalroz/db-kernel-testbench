#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_INCIDENCE_BITMAPS_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_INCIDENCE_BITMAPS_H_

#include "tester/test_adapters/decompression/common.h"
#include <cuda/bit_operations.cuh>

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary = true, unsigned BitmapAlignmentInBytes = 8>
class IncidenceBitmaps:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>>
{
	static_assert(WithDictionary or UncompressedSize == 1,
		"To decompress values of size 2 and above, dictionary decompression of the bitmaps is necessary, as no more than "
		"256 bitmaps can be used.");
	static_assert(BitmapAlignmentInBytes >= 8, "Bit mask memory storage alignment must be at least 64 bits");
	static_assert(BitmapAlignmentInBytes % sizeof(int) == 0,
		"Only multiples of sizeof(int) supported for the bit mask memory storage alignment in bytes");

public: // types
	using index_type             = uint_t<IndexSize>;
	using size_type              = size_type_by_index_size<IndexSize>;
	using bitmap_size_type       = size_type;
	using uncompressed_type      = uint_t<UncompressedSize>;
	using bit_container_type     = cuda::standard_bit_container_t;
	enum { bitmap_index_size = 1 };
	using bitmap_index_type      = uint_t<bitmap_index_size>;
	using bitmap_index_size_type = size_type_by_index_size<bitmap_index_size>;
	enum : index_type { bits_per_container = util::size_in_bits<bit_container_type>::value };
	using dictionary_index_type = bitmap_index_type; // not used, just clarifying thing here...

protected:
	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(IncidenceBitmaps)

protected:
	// Only relevant when actually using a dictionary
	void generate_dictionary(
		span<uncompressed_type>                  dictionary);
	void generate_incidence_bitmaps(
		span<bit_container_type>                 incidence_bitmaps);
	void generate_expected_decompressed(
		span<uncompressed_type>                  decompressed,
		optional<span<const uncompressed_type>>  dictionary,
		span<const bit_container_type>           incidence_bitmaps);

public: // data members
	// This should really be protected and let the patched adapters be friends, but - expediency

	// It's never worth (compression-wise) to having more than 8 bitmaps, since with
	// that many bitmaps you have 1 byte of storage for every element in the uncompressed
	// data, reaching parity with the uncompressed size.
	index_type         max_num_bitmaps                                 { util::bits_per_byte };

	// If we're reading the input for some external source, this value is overwritten
	// by whatever's in that
	index_type         num_actual_bitmaps                              {   3    };

	distribution_t<uncompressed_type> dictionary_value_random_part     {
		distribution_t<uncompressed_type>::uniform(10, 99)
	};
	double             dictionary_value_random_contribution            {  1.   };
	double             dictionary_value_linear_contribution            {  0.   };
	uncompressed_type  dictionary_value_linear_scale                   { 10    };
	uncompressed_type  dictionary_value_linear_offset                  {  1    };

	bool               use_all_ones_as_compressed_data                 { false };
	distribution_t<bitmap_index_type>
	                   compressed_input_random_part                    {
		distribution_t<bitmap_index_type>::uniform(0, 2)
	};
		// This will be get its default value after we have a final value of num_actual_bitmaps
	double             compressed_input_random_contribution            {  1    };
	double             compressed_input_linear_contribution            {  0    };
	unsigned char      compressed_input_linear_scale                   {  1    };
	unsigned char      compressed_input_linear_offset                  {  0    };

	bool&              fail_when_bitmaps_cant_cover_all_data = fail_on_invalid_uncompressed_data;

	// derived data

	index_type         single_bitmap_length;
	index_type         aligned_single_bitmap_length;

};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_INCIDENCE_BITMAPS_H_ */
