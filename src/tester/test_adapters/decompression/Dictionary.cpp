
#include "Dictionary.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/poor_mans_reflection.h"

#include <algorithm>
#include <iomanip>
#include <iostream>

#include "kernel_wrappers/decompression/dictionary.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
std::string Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::buildPrettyKernelName() const
{
	return
		util::concat_string(
		"A Dictionary lookup operation: decompressed[i] = dictionary_entries[compressed[i]] - where the ",
		"dictionary indices (the compressed data) are ", DictionaryIndexSize, " bytes, while the ",
		"decompressed data has size ", UncompressedSize, ". (Data indices/lengths are of size",
		IndexSize, " bytes.)");
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
KernelTestAdapter::BufferDescriptors Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::getBufferDescriptors() const
{
	return {
		{ "decompressed",        BufferDescriptor::make<uncompressed_type   >(config.test_length,     BufferDirection::Output) },
		{ "dictionary_entries",  BufferDescriptor::make<uncompressed_type   >(dictionary_capacity,    BufferDirection::Input)  },
		{ "compressed_input",    BufferDescriptor::make<dictionary_index_type>(config.test_length,    BufferDirection::Input)  },
	};
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
inline Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::Dictionary(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;

	auto have_capacity_override = resolveOption(
		dictionary_capacity,
		ConfigurationKeySet {
			"dictionary-size", "dictionary-length", "dict-size", "dict-length",
			"dict-len", "num-dict-entries", "number-of-dictionary-entries", "num-dictionary-entries"}
	);
	if (have_capacity_override) {
		util::enforce(dictionary_capacity <= util::capped_domain_size<dictionary_index_type>::value,
			"Number of dictionary entries out of range for type ", util::type_name<dictionary_index_type>());
	}
	else {
		if (not need_to_generate("decompressed")) {
			// We will be compressing data we read from the input, so we default to the full potential
			// capacity of the type we use as the dictionary index size
			dictionary_capacity = util::capped_domain_size<dictionary_index_type>::value;
		}
	}
	util::enforce(dictionary_capacity == (index_type) dictionary_capacity ,
		"The index size", IndexSize, " is too small to hold the dictionary capacity ",
		dictionary_capacity);

	auto have_distinct_values_to_generate_override_override = resolveOption(
		distinct_values_to_generate,
		ConfigurationKeySet {
			"dictionary-size", "dictionary-length", "dict-size", "dict-length",
			"dict-len", "num-dict-entries", "number-of-dictionary-entries", "num-dictionary-entries"}
	);
	if (need_to_generate("decompressed")) {
		if (have_capacity_override and not have_distinct_values_to_generate_override_override) {
			distinct_values_to_generate = dictionary_capacity;
		}
		util::enforce(distinct_values_to_generate <= dictionary_capacity,
			"Number of dictionary entries out of range for type ", util::type_name<dictionary_index_type>());
	}
	// and if we don't need to generate the compressed input, we don't care about
	// distinct_values_to_generate anyway

	resolveOption(
		fail_on_insufficient_dictionary_capacity,
		ConfigurationKeySet {
			"fail-on-insufficient-dictionary-capacity", "no-partial-dictionary",
			"no-partial-dict", "fail-on-insufficient-capacity", "dictionary-must-fit-data",
			"dictionary-must-fit", "data-must-fit-in-dictionary"
		}
	);

	resolveOption(
		dictionary_value_random_part,
		ConfigurationKeySet {
			"dict-value-random",       "dict-value-random-contribution",
			"dictionary-value-random", "dictionary-value-random-contribution"}
	);
	resolveOption(
		dictionary_value_random_contribution_factor,
		ConfigurationKeySet {
			"dictionary-value-random-contribution", "dict-value-random-contribution",
			"dictionary-random-contribution", "dict-random-contribution"}
	);
	resolveOption(
		dictionary_value_linear_scale,
		ConfigurationKeySet {
			"dictionary-value-linear-scale", "dict-value-linear-scale",
			"dictionary-linear-scale", "dict-linear-scale"}
	);
	resolveOption(
		dictionary_value_linear_offset,
		ConfigurationKeySet {
			"dictionary-value-linear-offset", "dict-value-linear-offset",
			"dictionary-linear-offset", "dict-linear-offset"}
	);
	resolveOption(
		dictionary_value_linear_contribution_factor,
		ConfigurationKeySet {
			"dictionary-value-linear-contribution", "dict-value-linear-contribution",
			"dictionary-linear-contribution", "dict-linear-contribution"}
	);


	resolveOption(
		compressed_input_random_contribution_factor,
		ConfigurationKeySet {
			"compressed-input-random", "compressed-input-random-contribution",
			"compressed-random",       "compressed-random-contribution"}
	);

	resolveOption(
		compressed_input_random_contribution_factor,
		ConfigurationKeySet {
			"compressed-input-random-contribution", "data-value-random-contribution",
			"compressed-random-contribution", "data-random-contribution"}
	);
	resolveOption(
		compressed_input_linear_scale,
		ConfigurationKeySet {
			"compressed-input-linear-scale", "data-value-linear-scale",
			"compressed-linear-scale", "data-linear-scale"}
	);
	resolveOption(
		compressed_input_linear_offset,
		ConfigurationKeySet {
			"compressed-input-linear-offset", "data-value-linear-offset",
			"compressed-linear-offset", "data-linear-offset"}
	);
	resolveOption(
		compressed_input_linear_contribution_factor,
		ConfigurationKeySet {
			"compressed-input-linear-contribution", "data-value-linear-contribution",
			"compressed-linear-contribution", "data-linear-contribution"}
	);

	resolveOption(use_all_ones_as_compressed_data,
		ConfigurationKeySet {
			"use-all-ones-as-input", "data-all-ones", "all-ones-data",
			"all-ones", "all-ones-input"});

	force_shared_mem_caching = resolveOption<bool>(
		ConfigurationKeySet {"force-shared-mem-caching", "force-shared-memory-caching",
			"shared-mem-caching", "cache-in-shared-mem", "cache-in-shared-memory",
			"cache-input-in-shared-mem", "cache-data-in-shared-mem" });

		// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Dictionary capacity",                            dictionary_capacity                           ),
		make_pair("Number distinct values to generate",             distinct_values_to_generate                   ),
		make_pair("Set all compressed data to 1",                   use_all_ones_as_compressed_data               ),
        make_pair("Force caching of input in shared mem",           force_shared_mem_caching),
		make_pair("Generate uniformly random compressed data",      compressed_input_uniformly_random             ),
		make_pair("Dictionary value generation - random part",      dictionary_value_random_part                  ),
		make_pair("Dictionary value generation - random part contribution",
		                                                            dictionary_value_random_contribution_factor   ),
		make_pair("Dictionary value generation - linear factor contribution",
		                                                            dictionary_value_linear_contribution_factor   ),
		make_pair("Dictionary value generation - linear scale",     dictionary_value_linear_scale                 ),
		make_pair("Dictionary value generation - linear offset",    dictionary_value_linear_offset                ),
		make_pair("Compressed input generation - random part",      compressed_input_random_part                  ),
		make_pair("Compressed input generation - random factor contribution",
		                                                            compressed_input_random_contribution_factor   ),
		make_pair("Compressed input generation - linear factor contribution",
		                                                            compressed_input_linear_contribution_factor   ),
		make_pair("Compressed input generation - linear scale",     compressed_input_linear_scale                 ),
		make_pair("Compressed input generation - linear offset",    compressed_input_linear_offset                )
	);

	// TODO: Perhaps do this before the printing?
	if (compressed_input_uniformly_random) {
		compressed_input_linear_contribution_factor = 0;
		compressed_input_linear_offset = 0;
		compressed_input_linear_scale = 0;
		compressed_input_random_contribution_factor = 1;
		compressed_input_random_part = distribution_t<dictionary_index_type>::uniform(0, distinct_values_to_generate - 1);
	}

	return;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
std::string Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::getDescription() const
{
	return
		"Decompression kernel for the Dictionary (DICT) compression"
		"scheme: Every element of the input data sequence (or column, "
		"if you will) is replaced with an index into a 'dictionary' ("
		"= a map from integers to elements of the uncompressed type;"
		"instead of passing the single, uncompressed sequence of elements"
		"of the original type, one passes a sequence of dictionary indices,"
		"along with the dictionary itself, a (hopefully) shorter sequence"
		"of elements of the original type. Note that decompression of "
		"a DICT-compressed sequence of elements is the equivalent of "
		"Gather'ing from the dictionary.";
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
LaunchConfigurationSequence Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		{ "data_length",            config.test_length           },
		{ "num_dictionary_entries", (size_t) effective_dictionary_length  },
	};

	if (force_shared_mem_caching) {	extra_args.insert(
		{ "cache_input_data_in_shared_mem", force_shared_mem_caching.value() });
	}

	::cuda::kernels::decompression::dictionary::kernel_t<IndexSize, UncompressedSize, DictionaryIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("decompression::dictionary::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
void Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::generate_dictionary(
	span<uncompressed_type> dictionary)
{
	// Generation not involving compression, i.e. artificial
	// dictionary entries irrespective of the actual data

	auto random_engine = this->random_engine;

	for(dictionary_index_type i = 0; i < distinct_values_to_generate; i++) {
		uncompressed_type linear_component =
			dictionary_value_linear_scale * i + dictionary_value_linear_offset;
		uncompressed_type random_component =
			util::random::sample_from(dictionary_value_random_part, random_engine);
		dictionary[i] =
			linear_component * dictionary_value_linear_contribution_factor +
			random_component * dictionary_value_random_contribution_factor;
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
void Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::generate_compressed_input(
	span<dictionary_index_type> compressed)
{
	// Generation without compression, i.e. of artificial compressed data
	//
	// Note: There may be an unused portion of the dictionary at the 'end',
	// i.e. the higher values - between distinct_values_to_generate and
	// dictionary_capacities - is always unused

	if (use_all_ones_as_compressed_data) {
		fill(compressed, 1);
		return;
	}
	auto random_engine = this->random_engine;

	for(index_type i = 0; i < config.test_length; i++) {
		auto linear_component =
			compressed_input_linear_scale * i + compressed_input_linear_offset;
		auto random_component =
			util::random::sample_from(compressed_input_random_part, random_engine);
		compressed[i] =
			((dictionary_index_type) (linear_component * compressed_input_linear_contribution_factor +
			random_component * compressed_input_random_contribution_factor)) % distinct_values_to_generate;
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
void Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::generate_expected_decompressed(
	span<uncompressed_type>            decompressed,
	span<const uncompressed_type>      dictionary,
	span<const dictionary_index_type>   compressed_input)
{
	for(index_type i = 0; i < compressed_input.size(); i++) {
		decompressed[i] = dictionary[compressed_input[i]];
	}
}

// TODO: Perhaps allow for other kinds of containers?
template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
static void build_naive_dictionary_and_compress(
	span<uint_t<DictionaryIndexSize> >                    naive_compressed,
	span<uint_t<UncompressedSize> >                       naive_dictionary_entries,
	span<const uint_t<UncompressedSize> >                 uncompressed,
	size_type_by_index_size<DictionaryIndexSize>&         num_distinct_uncompressed_values,
		// Note the assumption here regarding the maximum possible value
		// of num_distinct_uncompressed_values
	optional<span<size_type_by_index_size<IndexSize> > >  multiplicities = {}
	// TODO: Perhaps some sort of guesstimate of the number of distinct values?
)
{
	using size_type = size_type_by_index_size<IndexSize>;
	using dictionary_index_type = uint_t<DictionaryIndexSize>;
	using uncompressed_type = uint_t<UncompressedSize>;

	std::unordered_map<uncompressed_type, dictionary_index_type>
		naive_dictionary_as_map(naive_dictionary_entries.size());
	// TODO: maybe set the load factor here, and hence a larger number of buckets? Or
	// assume much fewer distinct elements and use a lower number?

	// initially we make no restriction on the dictionary size, since we might
	// want to to eventually obtain a dictionary covering only some of the uncompressed
	// elements, and use

	// TODO: We're making a very generous guess here; perhaps guess lower?

	for (size_type i = 0; i < uncompressed.size(); i++) {
		auto it = naive_dictionary_as_map.find(uncompressed[i]);
		dictionary_index_type dictionary_index;
		if (it != naive_dictionary_as_map.end()) {
			dictionary_index = it->second;
			if (has_value(multiplicities)) {
				multiplicities.value()[dictionary_index]++;
			}
		} else {
			dictionary_index = naive_dictionary_as_map.size();
			naive_dictionary_as_map.insert( { uncompressed[i], dictionary_index });
			naive_dictionary_entries[dictionary_index] = uncompressed[i];
			if (has_value(multiplicities)) {
				multiplicities.value()[dictionary_index] = 1;
			}
		}
		naive_compressed[i] = dictionary_index;
	}
	num_distinct_uncompressed_values = naive_dictionary_as_map.size();
}

// TODO: May have some code duplication with IncidenceBitmaps
template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
void Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::build_dictionary_and_compress(
	span<dictionary_index_type>    compressed,
	span<uncompressed_type>        dictionary_entries,
	span<const uncompressed_type>  uncompressed,
	dictionary_size_type&          num_distinct_uncompressed_values,
	dictionary_size_type&          effective_dictionary_length,
	dictionary_size_type&          num_uncovered_elements) const
{
	auto dictionary_capacity = dictionary_entries.length();

	// Just some sanity checking
	util::enforce(dictionary_capacity == this->dictionary_capacity,
		util::concat_string("The buffer to populate with the dictionary does not have "
			"the expected number of elements : ", dictionary_entries.length(), " != ",
			dictionary_capacity));
	// We first populate a 'naive dictionary' using the first appearances;
	// but we also record the frequencies, which are then used to reorder
	// the dictionary to make frequent elements come first.

	if (config.be_verbose) {
		std::cout << "Building a dictionary with at most " << dictionary_entries.length() << " entries"
			<< " for " << uncompressed.size() << " input elements.\n";
	}

	if (not generate_dictionary_by_decreasing_frequency and can_assume_dictionary_is_large_enough)
	{
		build_naive_dictionary_and_compress<IndexSize, UncompressedSize, DictionaryIndexSize>(
			compressed,	dictionary_entries, uncompressed, effective_dictionary_length);
		num_uncovered_elements = 0;
		num_distinct_uncompressed_values = effective_dictionary_length;
		return;
	}
	if (config.be_verbose) {
		std::cout << "Building a naive dictionary first and counting the multiplicities of "
			"elements in the uncompressed data.\n";
	}

	std::vector<size_type>              multiplicities(uncompressed.length());
	std::vector<dictionary_index_type>  naive_compressed(uncompressed.length());
	std::vector<uncompressed_type>      naive_dictionary_entries(uncompressed.length());

	build_naive_dictionary_and_compress<IndexSize, UncompressedSize, DictionaryIndexSize>(
		span<dictionary_index_type>(naive_compressed),
		span<uncompressed_type>(naive_dictionary_entries),
		span<const uncompressed_type>(uncompressed),
		num_distinct_uncompressed_values,
		span<size_type>(multiplicities));

	if (config.be_verbose) {
		std::cout
			<< "Naive dictionary built. The compressed input has "
			<< num_distinct_uncompressed_values << " distinct values (so "
			<< "the naive dictionary has " << num_distinct_uncompressed_values
			<< " entries).\n";
	}

	if (num_distinct_uncompressed_values > dictionary_capacity
	    and fail_on_insufficient_dictionary_capacity)
	{
		throw util::invalid_argument("uncompressed_type data has more distinct values "
			"than can fit in a dictionary of size " + std::to_string(dictionary_capacity));
	}

	naive_dictionary_entries.resize(num_distinct_uncompressed_values);
	multiplicities.resize(num_distinct_uncompressed_values);

	effective_dictionary_length  = (num_distinct_uncompressed_values > dictionary_capacity) ?
		dictionary_capacity : num_distinct_uncompressed_values;

	auto decreasing_multiplicity_order = util::partial_sorted_indices(
		multiplicities, effective_dictionary_length, std::greater<index_type>());

	// At this point, sorted_multiplicity_indices starts with the most frequently-used
	// entries in the naive dictionary and ends with the least frequently used;
	// consequently we'll just clip that at the point of filling our actual dictionary
	// to capacity.

	// Note: We will _always_ use 0 as the fallback, even if it's not supposed
	// to be exclusive, so that it's in a 'popular' position in the dictionary
	// which is likely to be in the cache.
	//
	// It is, of course, possible to allow for an arbitrary fallback value
	// (and then perhaps replace the dictionary value it's conflicting
	// with, with the 'natural' element in its position
	dictionary_index_type shift_due_to_fallback = 0;
	if (num_distinct_uncompressed_values > dictionary_capacity) {
		if (reserve_fallback_for_missing_data_only) {
			dictionary_capacity--;
			shift_due_to_fallback = 1;
			dictionary_entries[0] = uncompressed[0]; // the value doesn't really matter - I think
		}
	} else {
	}

	std::vector<index_type> dictionary_translator(naive_dictionary_entries.size());
	fill(dictionary_translator, 0);
	for(index_type entry_index = 0; entry_index < effective_dictionary_length; entry_index++) {
		auto naive_dictionary_index = decreasing_multiplicity_order[entry_index];
		dictionary_translator[naive_dictionary_index] = entry_index;
		dictionary_entries[entry_index + shift_due_to_fallback] = naive_dictionary_entries[naive_dictionary_index];
	}

	// ... and now we actually compress with the final dictionary
	num_uncovered_elements = 0;
	for (index_type element_index = 0; element_index < uncompressed.size(); element_index++) {
		auto naive_dictionary_index = naive_compressed[element_index];
		auto dictionary_index = dictionary_translator[naive_dictionary_index];
		compressed[element_index] = dictionary_index;
		num_uncovered_elements += (dictionary_index == fallback_value ? 1 : 0);
	}
}


template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
void Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::compress_with_dictionary(
       span<dictionary_index_type>       compressed,
       span<const uncompressed_type>     uncompressed,
       span<const uncompressed_type>     dictionary_entries) const
{
	util::enforce(compressed.size() == uncompressed.size());
	index_type num_elements = compressed.size();

	std::unordered_map<uncompressed_type, dictionary_index_type> dictionary_as_map(dictionary_entries.size());
	for (dictionary_index_type i = 0; i < dictionary_entries.size(); i++) {
		dictionary_as_map.insert( { dictionary_entries[i], i });
	}

	// TODO: We could avoid this check if we know from the dictionary
	// generation phase that there are enough entries
	if (fail_on_insufficient_dictionary_capacity) {
		try {
			for (index_type i = 0; i < num_elements; i++) {
				compressed[i] = dictionary_as_map.at(compressed[i]);
			}
		} catch (std::exception& e) {
			throw util::invalid_argument(
				"The dictionary does not cover all uncompressed input values: " + std::string(e.what()));
		}
	} else {
		for (index_type i = 0; i < num_elements; i++) {
			auto it = dictionary_as_map.find(uncompressed[i]);
			compressed[i] = (it != dictionary_as_map.end()) ? it->second : uncompressed_type{fallback_value};
		}
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
KernelTestAdapter::BufferSizes Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::generateInputs(
	const HostBuffers& buffers)
{
   span<uncompressed_type> dictionary;

	if (need_to_generate("decompressed")) {
		if (need_to_generate("dictionary_entries")) {
			auto dictionary = buffers.at("dictionary_entries").as_span<uncompressed_type>();
			generate_dictionary(dictionary);
		}
		if (need_to_generate("compressed_input")) {
			auto compressed_input = buffers.at("compressed_input").as_span<dictionary_index_type>();
			generate_compressed_input(compressed_input);
		}
		return {};
	}
	else {
		// Ok, harder - we'll need to actually compress_with_dictionary

		KernelTestAdapter::BufferSizes new_buffer_sizes;
		util::enforce(need_to_generate("compressed_input"), "Invalid combination of buffers to generate");
		auto decompressed = buffers.at("decompressed").as_span<const uncompressed_type>();
		if (decompressed.size() != config.test_length) {
			throw util::invalid_argument(util::concat_string(
				"The length of the apriori buffer of uncompressed data does not agree with "
				"the configured test length (", decompressed.size()," != ", config.test_length, ")"));
		}
		auto compressed_input = buffers.at("compressed_input").as_span<dictionary_index_type>();
		if (need_to_generate("dictionary_entries")) {
			auto dictionary = buffers.at("dictionary_entries").as_span<uncompressed_type>();
			dictionary_size_type num_uncovered_elements; // unused for now
			dictionary_size_type num_distinct_values; // unused for now
			build_dictionary_and_compress(
				compressed_input, dictionary, decompressed,
				num_distinct_values,
				effective_dictionary_length,
				num_uncovered_elements);
			if (config.be_verbose) {
				std::cout
					<< "Built a dictionary with " << effective_dictionary_length
					<< " entries for data with " << num_distinct_values << " distinct values.\n";
				if (effective_dictionary_length < num_distinct_values) {
					std::cout
						<< num_uncovered_elements << " = "
						<< util::shorten_with_highest_prefix(num_uncovered_elements)
						<< " elements (" << std::setprecision(4)
						<< 100.0 * num_uncovered_elements / decompressed.length()
						<< "% of the uncompressed data) aren't covered by the dictionary "
						<< "(they have one of "
						<< num_distinct_values - effective_dictionary_length
						<< " distinct missing values).\n";
				}
			}
			new_buffer_sizes.insert( { "dictionary_entries", effective_dictionary_length * UncompressedSize });
		} else {
			auto dictionary = buffers.at("dictionary_entries").as_span<const uncompressed_type>();
			compress_with_dictionary(compressed_input, decompressed, dictionary);
		}
		return new_buffer_sizes;
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
KernelTestAdapter::BufferSizes Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto dictionary     = buffers.at("dictionary_entries").as_span<const uncompressed_type>();
		auto indices        = buffers.at("compressed_input").as_span<const dictionary_index_type>();
		auto decompressed   = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		generate_expected_decompressed(decompressed, dictionary, indices);
	}
	return NoBuffersToResize;
}


template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
void Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto   decompressed           = buffers.at("decompressed"      ).as_span<uncompressed_type           >().data();
	auto   compressed_input       = buffers.at("compressed_input"  ).as_span<const dictionary_index_type >().data();
	auto   dictionary_entries     = buffers.at("dictionary_entries").as_span<const uncompressed_type     >().data();

	size_type length = config.test_length; // the number of compressed elements == number of decompressed elements
	dictionary_size_type num_dictionary_entries = buffers.at("dictionary_entries").as_span<const uncompressed_type>().size();

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::decompression::dictionary::kernel_t<IndexSize, UncompressedSize, DictionaryIndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(
			decompressed, compressed_input, dictionary_entries, length, num_dictionary_entries )
	);
}

static_block {
	//           IndexSize   UncompressedSize  DictionaryIndexSize
	//------------------------------------------------------------------------
    Dictionary < 4,          1,                1 >::registerInSubclassFactory();

    Dictionary < 4,          2,                1 >::registerInSubclassFactory();
    Dictionary < 4,          2,                2 >::registerInSubclassFactory();

    Dictionary < 4,          4,                1 >::registerInSubclassFactory();
    Dictionary < 4,          4,                2 >::registerInSubclassFactory();
    Dictionary < 4,          4,                4 >::registerInSubclassFactory();

    Dictionary < 4,          8,                1 >::registerInSubclassFactory();
    Dictionary < 4,          8,                2 >::registerInSubclassFactory();
    Dictionary < 4,          8,                4 >::registerInSubclassFactory();
    Dictionary < 4,          8,                8 >::registerInSubclassFactory();

    Dictionary < 8,          1,                1 >::registerInSubclassFactory();

    Dictionary < 8,          2,                1 >::registerInSubclassFactory();
    Dictionary < 8,          2,                2 >::registerInSubclassFactory();

    Dictionary < 8,          4,                1 >::registerInSubclassFactory();
    Dictionary < 8,          4,                2 >::registerInSubclassFactory();
    Dictionary < 8,          4,                4 >::registerInSubclassFactory();

    Dictionary < 8,          8,                1 >::registerInSubclassFactory();
    Dictionary < 8,          8,                2 >::registerInSubclassFactory();
    Dictionary < 8,          8,                4 >::registerInSubclassFactory();
    Dictionary < 8,          8,                8 >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
