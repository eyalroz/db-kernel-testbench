#include "Naive.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "util/static_block.h"
#include "kernel_wrappers/data_layout/scatter.cu"

// ... and for some specific prior adapters:
//----------------------------------------------
#include "../../Delta.h"
#include "../../Dictionary.h"
#include "../../DiscardZeroBytesFixed.h"
#include "../../DiscardZeroBytesVariable.h"
#include "../../FrameOfReference.h"
#include "../../IncidenceBitmaps.h"
#include "../../RunLengthEncoding.h"

#include <cuda/functors.hpp>       // necessary for some of the instantiations
#include <cuda/model_functors.hpp> // necessary for some of the instantiations

namespace kernel_tests {

namespace mixins {

template<typename PriorAdapter>
struct TestAdapterNameBuilder<kernel_tests::decompression::patched::aposteriori::Naive<PriorAdapter>> {
static std::string  buildName() {
		std::string s = KernelTestAdapter::buildName<kernel_tests::decompression::patched::aposteriori::Naive<PriorAdapter>>();
		util::replace_all(s, "Naive<kernel_tests::decompression::", "Naive<");
		return s;
	}
};
} // namespace mixins

namespace decompression {
namespace patched {
namespace aposteriori {

template <typename PriorAdapter>
std::string Naive<PriorAdapter>::buildPrettyKernelName() const
{
	return
		util::concat_string(
			"Naive patched version of the following decompression kernel:\n",
			PriorAdapter::buildPrettyKernelName());
}

template <typename PriorAdapter>
KernelTestAdapter::BufferDescriptors Naive<PriorAdapter>::getBufferDescriptors() const
{
	auto& config = parent::config;
	KernelTestAdapter::BufferDescriptors parent_descriptors = parent::getBufferDescriptors();
	return util::get_union(
		parent_descriptors, {
		{ "positions_to_patch",  BufferDescriptor::make<index_type>(config.test_length,         BufferDirection::Input) },
		{ "patch_values",        BufferDescriptor::make<uncompressed_type>(config.test_length,  BufferDirection::Input) },
	});
	// Note: We've not made the decompressed buffer into an inout, since for the
	// test adapter, overall, it's out-only - even if internally we're writing into it
}

template <typename PriorAdapter>
Naive<PriorAdapter>::Naive(const TestConfiguration& config) : Naive::parent(config)
{
	// The underlying, unpatched adapter may be required to compress data
	// which cannot be represented by that compression scheme and requires
	// patching; we want the underlying adapter, therefore, not to choke on
	// that and let us handle it.
	//auto p = dynamic_cast<PriorAdapter*>(this)->fail_on_invalid_uncompressed_data = false;
	parent::fail_on_invalid_uncompressed_data = false;

	using std::make_pair;
	using ConfigurationKeySet = KernelTestAdapter::ConfigurationKeySet;

	KernelTestAdapter::resolveOption(
		patch_index_offset, ConfigurationKeySet {
		"patch-index-offset", "patch-index-offset-distribution"
		}
	);

	KernelTestAdapter::resolveOption(
		patch_value, ConfigurationKeySet {
		"patch-value",  "patch-value-distribution",
		"patch-values", "patch-values-distribution" }
	);

	// TODO: Consider setting the distributions to nullopt - and redclaring them -
	// when we're just going to compress

	KernelTestAdapter::maybe_print_extra_configuration(
		make_pair("Distance between generated patch positions", patch_index_offset),
		make_pair("Generated patch value",                      patch_value)
	);

	if (need_to_generate("decompressed")) {
		util::enforce(patch_index_offset.min() >= 1 || patch_index_offset.mean() < 1.0,
			"Patch indices must be strictly-monotone-increasing, "
			"so the patch index offset distribution must not have support below 1");
	}
}

template <typename PriorAdapter>
std::string Naive<PriorAdapter>::getDescription() const
{
	auto parent_description = parent::getDescription();
	return parent_description + "\n"
		"In addition to the above, values which lie outside the range covered by the"
		"basic decompression, and appear in the input, are encoded separately as 'patches', "
		"specifically, pairs of position in the data and the uncompressed value to assign "
		"there. This is very inefficient size-wise, but the assumption is that there are "
		"very few patches to make, and the savings due to a smaller range for the basic "
		"compression scheme are worth it.";
}


template <unsigned IndexSize, unsigned UncompressedSize>
static LaunchConfigurationSequence::value_type resolve_scatter_configuration(const TestConfiguration& config)
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type args = {
		{ "data_length",                  config.test_length },
	};
	cuda::kernels::scatter::kernel_t<IndexSize, UncompressedSize, IndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(
		kernel, device_properties, args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	return {
		"data_layout::scatter",
		kernel.get_device_function(),
		launch_config
	};
}

template <typename PriorAdapter>
LaunchConfigurationSequence Naive<PriorAdapter>::resolveLaunchConfigurations() const
{
	// Naive patching means decompressing with whatever kernels the underlying decompressor
	// uses, then finally making independent local patches to the result
	LaunchConfigurationSequence configs = parent::resolveLaunchConfigurations();
	auto scatter_config = resolve_scatter_configuration<sizeof(index_type), sizeof(uncompressed_type)>(parent::config);
	configs.push_back(scatter_config);
	return configs;
}

template <typename PriorAdapter>
void Naive<PriorAdapter>::generate_patches(
	span<Naive<PriorAdapter>::index_type>                       positions_to_patch,
	span<Naive<PriorAdapter>::uncompressed_type>                patch_values,
	Naive<PriorAdapter>::size_type&               __restrict__  num_patched_elements)
{
	const auto& config = parent::config;
	auto random_engine = this->random_engine;
	auto patch_index_offset = this->patch_index_offset;
	auto patch_value = this->patch_value;

	size_type position_to_patch = 0;
	index_type patch_index = 0;
	do {
		position_to_patch += util::random::sample_from(patch_index_offset, random_engine);
		if (position_to_patch >= config.test_length) { break; }
		positions_to_patch[patch_index] = position_to_patch;
		patch_values[patch_index] = util::random::sample_from(patch_value, random_engine);
		patch_index++;
	} while (true);
	num_patched_elements = patch_index;
}

template <typename PriorAdapter>
typename Naive<PriorAdapter>::index_type Naive<PriorAdapter>::resolvePatches(const HostBuffers& buffers)
{
	parent::forced_generation_buffers.insert("decompressed");

	auto uncompressed = buffers.at("decompressed").as_span<uncompressed_type>();
	HostBuffers buffers_copy(buffers);
		// there's non deep copying here, as HostBuffers is a map of non-owning pointers/
		// pointer-wrappers
	buffers_copy.erase("decompressed");

	std::vector<uncompressed_type> unpatched_scheme_uncompressed(uncompressed.size());
	HostBuffers unpatched_expected_buffers {
		{"decompressed", span<uncompressed_type>(unpatched_scheme_uncompressed)}
	};
	parent::generateExpectedOutputs(unpatched_expected_buffers, buffers_copy); // with this, the unpatched adapter decompresses

	auto positions_to_patch = buffers.at("positions_to_patch").as_span<index_type>();
	auto patch_values       = buffers.at("patch_values"      ).as_span<uncompressed_type>();

	auto positions_it = positions_to_patch.begin();
	auto patch_values_it = patch_values.begin();

	for(size_type pos = 0; pos < uncompressed.size(); pos++) {
		if (UNLIKELY(uncompressed[pos] != unpatched_scheme_uncompressed[pos])) {
			*(positions_it++)    = pos;
			*(patch_values_it++) = uncompressed[pos];

			if (UNLIKELY(positions_it >= positions_to_patch.end())) {
				throw util::logic_error(util::concat_string(
					"The number of possible patch positions was set to ",
					positions_to_patch.size(), " - but it seems we need "
					"more than that number of patches"));
			}
		}
	}
	auto num_elements_patches = positions_it - positions_to_patch.begin();
	return num_elements_patches;
}

template <typename PriorAdapter>
KernelTestAdapter::BufferSizes Naive<PriorAdapter>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		parent::generateInputs(buffers);
		if (need_to_generate("positions_to_patch") and need_to_generate("patch_values"))
		{
			auto positions_to_patch = buffers.at("positions_to_patch").as_span<index_type>();
			auto patch_values       = buffers.at("patch_values"      ).as_span<uncompressed_type>();

			size_type num_patched_elements;
			generate_patches(positions_to_patch, patch_values, num_patched_elements);
			return {
				std::make_pair("positions_to_patch", num_patched_elements * sizeof(index_type)),
				std::make_pair("patch_values", num_patched_elements * sizeof(uncompressed_type)),
			};
		} else if (need_to_generate("positions_to_patch") or need_to_generate("patch_values")) {
			throw util::invalid_argument(
				"Either all patch-related info is generated or none of it can be");
		}
		// Nothing for us to generate, then
		return NoBuffersToResize;
	}
	else {
		// We're compressing the expected output

		// We must first ensure that the unpatched-scheme test adapter is able to
		// compress the data we pass it; that may (or may not) mean replacing data
		// at the positions we later patch
		parent::fail_on_invalid_uncompressed_data = false;

		// ... and now we can let the unpatched compression happen
		auto buffer_resizes = parent::generateInputs(buffers);
		size_type num_patches = resolvePatches(buffers);
		return util::get_union(
			buffer_resizes,
			{
				std::make_pair("positions_to_patch", num_patches * sizeof(index_type)),
				std::make_pair("patch_values",       num_patches * sizeof(uncompressed_type))
			}
		);
	}
}

/*
 * Note that using UncompressedSize risks some type discrepancies
 * in other functions. It's doable, but I'd rather not reinterpret_cast.
 */
template<unsigned IndexSize, typename Uncompressed>
static void patch(
	const TestConfiguration&                config,
	Uncompressed*             __restrict__  buffer_to_patch,
	const uint_t<IndexSize>*  __restrict__  positions_to_patch,
	const Uncompressed*       __restrict__  patch_values,
	size_type_by_index_size<IndexSize>      num_elements_to_patch)
{
	UNUSED(config);
	for(decltype(num_elements_to_patch) i = 0; i < num_elements_to_patch; i++) {
		buffer_to_patch[positions_to_patch[i]] = patch_values[i];
	}
}

template <typename PriorAdapter>
KernelTestAdapter::BufferSizes Naive<PriorAdapter>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		const auto& config = parent::config;
		parent::generateExpectedOutputs(expected_buffers, buffers);
			// this performs the unpatched decompression, resulting in some kind
			// of irrelevant junk in the positions to patch
		auto positions_to_patch = buffers.at("positions_to_patch").as_span<const index_type        >();
		auto patch_values       = buffers.at("patch_values"      ).as_span<const uncompressed_type>();
		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		patch<sizeof(index_type), uncompressed_type>(
			config,
			decompressed.data(),
			positions_to_patch.data(),
			patch_values.data(),
			positions_to_patch.size()
		);
	}
	return NoBuffersToResize;
}

template <typename PriorAdapter>
void Naive<PriorAdapter>::launchKernels(
	const DeviceBuffers&                 buffers,
	typename DeviceBuffers::mapped_type  device_scratch_area,
	cuda::stream_t&                      stream) const
{
	util::enforce(device_scratch_area.empty());
	cuda::device::current::get().synchronize(stream);
	parent::launchKernels(buffers, DeviceBuffers::mapped_type(), stream);


	auto target   = buffers.at("decompressed"        ).as_span<type_erased_element_type      >().data();
	auto data     = buffers.at("patch_values"        ).as_span<const type_erased_element_type>().data();
	auto indices  = buffers.at("positions_to_patch"  ).as_span<const index_type              >().data();

	size_type data_length = buffers.at("patch_values").as_span<const uncompressed_type>().size();

	auto launch_config = resolveLaunchConfigurations()[1].launch_config;

	cuda::device::current::get().synchronize(stream);
	cuda::kernels::scatter::kernel_t<sizeof(index_type), sizeof(uncompressed_type), sizeof(index_type)>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(target, data, indices, data_length)
	);
}

static_block {
	namespace functors = cuda::functors;
	namespace unary_models = ::cuda::functors::unary::parametric_model;

	//                                IndexSize   UncompressedSize  DictionaryIndexSize
	//------------------------------------------------------------------------------------------------------
    Naive<decompression::Dictionary < 4,          1,                1 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 4,          2,                1 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 4,          2,                2 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 4,          4,                1 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 4,          4,                2 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 4,          4,                4 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 4,          8,                1 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 4,          8,                2 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 4,          8,                4 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 4,          8,                8 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 8,          1,                1 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 8,          2,                1 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 8,          2,                2 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 8,          4,                1 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 8,          4,                2 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 8,          4,                4 >>::registerInSubclassFactory();

    Naive<decompression::Dictionary < 8,          8,                1 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 8,          8,                2 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 8,          8,                4 >>::registerInSubclassFactory();
    Naive<decompression::Dictionary < 8,          8,                8 >>::registerInSubclassFactory();

       // Straightforward aposteriori patching of Delta-compressed data does not make much sense,
       // as while the patch may correct a certain element, its subsequents would still be decompressed using
       // the unpatched value; alternatively, one would need to patch entire intervals due to just one delta
       // value exceeding the representable range. Instead, the patching can only be integral to the de/compression
       // of the Delta values themselves.

	//                                IndexSize   UncompressedSize  CompressedSize
	// ----------------------------------------------------------------------------------------
	Naive<decompression::Delta < 4,          uint_t<2>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<4>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<4>,     sint_t<2> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<8>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<8>,     sint_t<2> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<8>,     sint_t<4> > >::registerInSubclassFactory();

	Naive<decompression::Delta < 4,          sint_t<2>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          sint_t<4>,     sint_t<2> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          sint_t<2>,     uint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          sint_t<4>,     uint_t<2> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<2>,     uint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 4,          uint_t<4>,     uint_t<2> > >::registerInSubclassFactory();

	Naive<decompression::Delta < 8,          uint_t<2>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 8,          uint_t<4>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 8,          uint_t<4>,     sint_t<2> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 8,          uint_t<8>,     sint_t<1> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 8,          uint_t<8>,     sint_t<2> > >::registerInSubclassFactory();
	Naive<decompression::Delta < 8,          uint_t<8>,     sint_t<4> > >::registerInSubclassFactory();

	//                                           IndexSize UncompressedSize  CompressedSize
	//----------------------------------------------------------------------------------------------------
	Naive<decompression::DiscardZeroBytesFixed < 4,         2,                1 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 4,         4,                1 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 4,         4,                2 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 4,         8,                1 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 4,         8,                2 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 4,         8,                4 >>::registerInSubclassFactory();

	Naive<decompression::DiscardZeroBytesFixed < 8,         2,                1 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 8,         4,                1 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 8,         4,                2 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 8,         8,                1 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 8,         8,                2 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesFixed < 8,         8,                4 >>::registerInSubclassFactory();

	//                                              IndexSize  UncompressedSize   ElementSizesContainerSize
	//-----------------------------------------------------------------------------------------
	Naive<decompression::DiscardZeroBytesVariable < 4,         2,                 4 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesVariable < 4,         4,                 4 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesVariable < 4,         8,                 4 >>::registerInSubclassFactory();

	Naive<decompression::DiscardZeroBytesVariable < 8,         2,                 4 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesVariable < 8,         4,                 4 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesVariable < 8,         8,                 4 >>::registerInSubclassFactory();

	Naive<decompression::DiscardZeroBytesVariable < 4,         2,                 8 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesVariable < 4,         4,                 8 >>::registerInSubclassFactory();
	Naive<decompression::DiscardZeroBytesVariable < 4,         8,                 8 >>::registerInSubclassFactory();


	//                                      IndexSize  Uncompressed   Compressed   UnaryModelFuction
	//----------------------------------------------------------------------------------------------------------------------
	Naive<decompression::FrameOfReference < 4,         int16_t,       int8_t,      unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	Naive<decompression::FrameOfReference < 4,         int32_t,       int8_t,      unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	Naive<decompression::FrameOfReference < 4,         int32_t,       int16_t,     unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();

	Naive<decompression::FrameOfReference < 8,         int32_t,       int16_t,     unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	Naive<decompression::FrameOfReference < 8,         int32_t,       int16_t,     unary_models::linear  < 8, int32_t >  >>::registerInSubclassFactory();
	Naive<decompression::FrameOfReference < 4,         int16_t,       int8_t,      unary_models::constant< 4, int32_t >  >>::registerInSubclassFactory();
	Naive<decompression::FrameOfReference < 4,         int32_t,       int8_t,      unary_models::constant< 4, int32_t >  >>::registerInSubclassFactory();
	Naive<decompression::FrameOfReference < 4,         int32_t,       int16_t,     unary_models::constant< 4, int32_t >  >>::registerInSubclassFactory();


	//                                      IndexSize UncompressedSize  WithDictionary   BitmapAlignmentInBytes
	//--------------------------------------------------------------------------------------------------------------
	Naive<decompression::IncidenceBitmaps < 4,        1,                true,            8  >>::registerInSubclassFactory();
	Naive<decompression::IncidenceBitmaps < 4,        2,                true,            8  >>::registerInSubclassFactory();
	Naive<decompression::IncidenceBitmaps < 4,        4,                true,            8  >>::registerInSubclassFactory();
	Naive<decompression::IncidenceBitmaps < 4,        8,                true,            8  >>::registerInSubclassFactory();
	Naive<decompression::IncidenceBitmaps < 4,        1,                false,           8  >>::registerInSubclassFactory();
}

} // namespace aposteriori
} // namespace patched
} // namespace decompression
} // namespace kernel_tests
