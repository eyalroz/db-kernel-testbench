#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_PATCHED_NAIVE_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_PATCHED_NAIVE_H_

#include "tester/test_adapters/decompression/common.h"
#include "util/distribution.h"

namespace kernel_tests {
namespace decompression {
namespace patched {
namespace aposteriori {

using util::optional;

template <typename PriorAdapter>
class CompressedIndices : public PriorAdapter
{
public:
	using this_class = CompressedIndices<PriorAdapter>;
	using parent = PriorAdapter;
	using output_index_type = typename parent::index_type;
	using output_size_type = typename parent::size_type;
	using patch_index_type = typename parent::index_type; // Yes, same type
	using patch_index_size_type = size_type_by_index_size<sizeof(patch_index_type)>;
	using uncompressed_type = typename parent::uncompressed_type;
	using type_erased_element_type = uint_t<sizeof(uncompressed_type)>;
		// used for scattering - which doesn't care about the actual type and
		// assumes the data is trivially-copy-constructible

	using KernelTestAdapter::BufferDescriptors;
	using KernelTestAdapter::BufferSizes;

	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);

	CompressedIndices(const TestConfiguration& config);
	std::string buildPrettyKernelName() const override;
	KernelTestAdapter::BufferDescriptors getBufferDescriptors() const override;
	std::string getDescription() const override;
	LaunchConfigurationSequence resolveLaunchConfigurations() const override;
	void generate_patch_values(
		span<typename PriorAdapter::uncompressed_type> patch_values);
	void generate_patch_position_sequences(
		span<patch_index_type>               patch_output_position_run_lengths,
		span<unsigned char>                  patch_output_position_run_individual_offset_sizes, // in bits
		span<output_index_type>              patch_output_position_run_baseline_values,
		span<unsigned char>                  patch_output_position_offset_bytes,
		span<patch_index_type>               patch_output_position_run_offsets_start_pos,
		span<patch_index_type>               patch_output_position_anchors,
		output_size_type&                    total_size_of_all_run_offsets);
	KernelTestAdapter::BufferSizes generateInputs(const HostBuffers& buffers) override;
	KernelTestAdapter::BufferSizes generateExpectedOutputs(
		const HostBuffers&                   expected_buffers,
		const HostBuffers& buffers)          override;
	void launchKernels(
		const DeviceBuffers&                 buffers,
		typename DeviceBuffers::mapped_type  device_scratch_area,
		cuda::stream_t&                      stream) const override;

protected: // configurable
	patch_index_size_type  patch_anchoring_period                 { 1024    };
		// For every this many input elements, we explicitly indicate
		// where the patch information begins.
		//
		// Values which are not a power of 2 are less well tested

	// Not currently supported
	/*
	optional<patch_index_type> fixed_number_of_patch_runs { nullopt };
	optional<patch_index_type> fixed_number_of_total_patches { nullopt };

	// When false, we first sample positions to patch just like in the naive
	// patching test adapter, then try to build patch sequences as best we can (well,
	// not really, but we build them somehow) to capture all these patches; otherwise
	// we build patch sequences first, then decide which positions they path (while
	// ensuring they do not intersect when we don't want them to etc.);
	bool sample_patched_position_before_building_sequences { false };
	*/

	bool run_baseline_pos_is_always_pos_of_first_patch_in_run { false };

	distribution_t<output_index_type> inter_patch_run_distance =
		distribution_t<output_index_type>::geometric( patch_anchoring_period / 3 );
	distribution_t<output_index_type> intra_patch_run_element_distance =
		distribution_t<output_index_type>::geometric(10, 1, patch_anchoring_period);
	distribution_t<patch_index_type> patch_run_length =
		distribution_t<patch_index_type>::uniform(1, patch_anchoring_period);
	distribution_t<uncompressed_type> patch_value =
		distribution_t<uncompressed_type>::uniform();
	// If you change the following distribution, you'll be able to control
	// whether patch sequences have 1, 2, 4 or 8-byte offsets - which also means they can
	// or can't have longer lengths
	distribution_t<unsigned char> index_into_patch_offset_sizes_array;

protected: // set during execution
	patch_index_size_type  num_patch_position_runs {0};
		// ... not to be confused with num_test_runs, which is the number of
		// runs the kernel test will be conducted by the tester binary

	patch_index_size_type  num_patched_elements {0}; // ... in total
};

} // namespace aposteriori
} // namespace patched
} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_PATCHED_NAIVE_H_ */
