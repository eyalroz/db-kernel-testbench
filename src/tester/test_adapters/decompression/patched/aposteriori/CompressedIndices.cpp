#include "CompressedIndices.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "util/static_block.h"
#include "kernel_wrappers/data_layout/compressed_indices_scatter.cu"
#include "util/random.h"
#include "util/math.hpp"

// ... and for some specific prior adapters:
//----------------------------------------------
#include "../../Delta.h"
#include "../../Dictionary.h"
#include "../../DiscardZeroBytesFixed.h"
#include "../../DiscardZeroBytesVariable.h"
#include "../../FrameOfReference.h"
#include "../../IncidenceBitmaps.h"
#include "../../RunLengthEncoding.h"

#include <cuda/functors.hpp>       // necessary for some of the instantiations
#include <cuda/model_functors.hpp> // necessary for some of the instantiations

namespace kernel_tests {

namespace mixins {

template<typename PriorAdapter>
struct TestAdapterNameBuilder<kernel_tests::decompression::patched::aposteriori::CompressedIndices<PriorAdapter>> {
static std::string  buildName() {
		std::string s = KernelTestAdapter::buildName<kernel_tests::decompression::patched::aposteriori::CompressedIndices<PriorAdapter>>();
		util::replace_all(s, "CompressedIndices<kernel_tests::decompression::", "CompressedIndices<");
		return s;
	}
};
} // namespace mixins

namespace decompression {
namespace patched {
namespace aposteriori {

template <typename PriorAdapter>
std::string CompressedIndices<PriorAdapter>::buildPrettyKernelName() const
{
	return
		util::concat_string(
			"CompressedIndices patched version of the following decompression kernel:\n",
			PriorAdapter::buildPrettyKernelName());
}

template <typename PriorAdapter>
KernelTestAdapter::BufferDescriptors CompressedIndices<PriorAdapter>::getBufferDescriptors() const
{
	auto& config = parent::config;
	KernelTestAdapter::BufferDescriptors parent_descriptors = parent::getBufferDescriptors();
	auto num_patch_position_anchors = util::div_rounding_up(config.test_length,patch_anchoring_period);
	size_t offsets_bytes_upper_bound = config.test_length * sizeof(output_index_type);
	return util::get_union(
		parent_descriptors, {
		{ "patch_values",                                      BufferDescriptor::make<uncompressed_type>(config.test_length,        BufferDirection::Input) },
		{ "patch_output_position_run_lengths",                 BufferDescriptor::make<patch_index_type>(config.test_length,               BufferDirection::Input) },
		{ "patch_output_position_run_individual_offset_sizes", BufferDescriptor::make<unsigned char>(config.test_length,            BufferDirection::Input) },
		{ "patch_output_position_run_baseline_values",         BufferDescriptor::make<output_index_type>(config.test_length,               BufferDirection::Input) },
		{ "patch_output_position_run_offsets_start_pos",       BufferDescriptor::make<patch_index_type>(config.test_length,               BufferDirection::Input) },
		{ "patch_output_position_anchors",                     BufferDescriptor::make<patch_index_type>(num_patch_position_anchors,       BufferDirection::Input) },
		{ "patch_output_position_offset_bytes",                BufferDescriptor::make<unsigned char>(offsets_bytes_upper_bound,     BufferDirection::Input) },
	});

	// Note: We've not made the decompressed buffer into an inout, since for the
	// test adapter, overall, it's out-only
}

template <typename PriorAdapter>
CompressedIndices<PriorAdapter>::CompressedIndices(const TestConfiguration& config) : CompressedIndices::parent(config)
{
	using std::make_pair;
	using ConfigurationKeySet = KernelTestAdapter::ConfigurationKeySet;

	KernelTestAdapter::resolveOption(patch_anchoring_period,
		ConfigurationKeySet {
			"patch-anchoring-period", "patch-position-anchoring-period",
			"position-anchoring-period", "anchoring-period",
			"elements-per-anchor", "patch-position-anchoring-interval",
			"anchoring-interval"
		}
	);

	KernelTestAdapter::resolveOption<bool>(
		run_baseline_pos_is_always_pos_of_first_patch_in_run, ConfigurationKeySet {
		"run-baseline-pos-is-always-pos-of-first-patch-in-run",
		"runs-start-at-baseline",
		"runs-start-at-baseline-pos",
		"patch-runs-start-at-baseline-pos",
		"patch-runs-start-at-baseline",
	});

	KernelTestAdapter::resolveOption(
		inter_patch_run_distance, ConfigurationKeySet {
			"inter-patch-run-distance", "distance-between-runs", "distance-between-patch-runs",
			"inter-patch-run-distance-distribution",
			"distance-between-runs-distribution",
			"distance-between-patch-runs-distribution",
		}
	);

	KernelTestAdapter::resolveOption(
		inter_patch_run_distance, ConfigurationKeySet {
			"intra-patch-run-distance",
			"distance-between-patches-within-run",
			"distance-between-patches-inside-run",
			"intra-patch-run-distance-distribution",
			"distribution-of-distance-between-patches-within-run",
			"distribution-of-distance-between-patches-inside-run",
		}
	);

	KernelTestAdapter::resolveOption(
			patch_run_length, ConfigurationKeySet {
			"patch-run-length",
			"run-length",
			"patch-run-length-distribution",
			"run-length-distribution",
			"distribution-of-patch-run-lengths",
			"distribution-of-run-lengths",
		}
	);

	// Super-ugly hack for setting the default;
	// I should really have a distribution_t over
	// the domain of possible patch offset sizes
	unsigned char max_usable_patch_pos_offset_size =
		util::min_size_to_represent(config.test_length);
	index_into_patch_offset_sizes_array =
		distribution_t<unsigned char>::uniform(0,
			util::ilog2<unsigned char>(max_usable_patch_pos_offset_size));

	KernelTestAdapter::resolveOption(
		index_into_patch_offset_sizes_array, ConfigurationKeySet {
			"index-into-patch-offset-sizes-array",
			"run-offset-log-of-size-in-bytes-distribution",
			"log-of-run-offset-size-in-bytes",
		}
	);

	KernelTestAdapter::resolveOption(
		patch_value, ConfigurationKeySet {
			"patch-value", "patch-values", "patch-value-distribution"
		}
	);


	make_pair("Period for anchors into patch runs",     patch_anchoring_period),

	KernelTestAdapter::maybe_print_extra_configuration(
		make_pair("Period for anchors into patch runs",     patch_anchoring_period),
//		make_pair("Fixed number of patch runs",             fixed_number_of_patch_runs),
//		make_pair("Fixed number of total patches",          fixed_number_of_total_patches),
		make_pair("Baseline position for patch runs is the first patch's",
			run_baseline_pos_is_always_pos_of_first_patch_in_run),
		make_pair("Inter-patch-run distance distribution", inter_patch_run_distance),
		make_pair("Intra-patch-run element distance distribution", intra_patch_run_element_distance),
		make_pair("Patch run length distribution", patch_run_length),
		make_pair("Patch value distribution", patch_value),
		make_pair("Single Patch run offset log-of-size distribution", index_into_patch_offset_sizes_array)
	);
}

template <typename PriorAdapter>
std::string CompressedIndices<PriorAdapter>::getDescription() const
{
	auto parent_description = parent::getDescription();
	return parent_description + "\n"
		"In addition to the above, values which lie outside the range covered by the"
		"basic decompression, and appear in the input, are encoded separately as 'patches', "
		"specifically, pairs of position in the data and the uncompressed value to assign "
		"there. This is very inefficient size-wise, but the assumption is that there are "
		"very few patches to make, and the savings due to a smaller range for the basic "
		"compression scheme are worth it.";
}


template <unsigned IndexSize, typename Uncompressed>
static LaunchConfigurationSequence::value_type resolve_compressed_index_scatter_configuration(
	const TestConfiguration& config, size_t num_patched_elements, size_t patch_anchoring_period)
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type args = {
		{ "input_data_length",            num_patched_elements },
		{ "anchoring_period",             patch_anchoring_period },
	};
	cuda::kernels::scatter::compressed_indices::kernel_t<IndexSize, sizeof(Uncompressed), IndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(
		kernel, device_properties, args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	return {
		"data_layout::compressed_indices_scatter",
		kernel.get_device_function(),
		launch_config
	};
}

template <typename PriorAdapter>
LaunchConfigurationSequence CompressedIndices<PriorAdapter>::resolveLaunchConfigurations() const
{
	LaunchConfigurationSequence configs = parent::resolveLaunchConfigurations();
	// Will only launch the second kernel if there's any patching to do
	if (num_patched_elements > 0) {
		configs.push_back(
			resolve_compressed_index_scatter_configuration<sizeof(patch_index_type), uncompressed_type>(
				parent::config, num_patched_elements, patch_anchoring_period) );
	}
	return configs;
}

/**
 * This has been split off from generate_patch_sequences since its templated
 * on the run sequence's offset size/type. We have to pass in a bunch
 * of parameters though...:
 *
 * @param run_offsets
 * @param run_length
 * @param
 * @param
 * @param baseline_pos
 * @param distribution
 * @param overall_data_length
 * @param random_engine
 */
template <unsigned OffsetSize, unsigned OutputIndexSize, typename Engine, unsigned PatchIndexSize>
static void generate_single_run(
	uint_t<OffsetSize>*                          run_offsets,
	uint_t<PatchIndexSize>&                      run_length,
	uint_t<OutputIndexSize>&                     first_output_position_after_all_patches,
	uint_t<OutputIndexSize>                      baseline_output_pos,
	distribution_t<uint_t<OutputIndexSize>>&     distribution,
	size_type_by_index_size<OutputIndexSize>     overall_data_length,
	Engine&                                      random_engine,
	bool                                         run_baseline_pos_is_always_pos_of_first_patch_in_run)
{
	using offset_type       = uint_t<OffsetSize>;
	using output_size_type = size_type_by_index_size<OutputIndexSize>;
	using patch_run_length_type = uint_t<PatchIndexSize>;
		// So, a run of length 1 << (PatchIndexSize * 8) can _not_ be
		// represented; you would need a

	offset_type offset;
	util::enforce(baseline_output_pos < overall_data_length);

	offset = run_baseline_pos_is_always_pos_of_first_patch_in_run ? 0 :
		// Runs must not be empty, so even in the case of allowing
		// non-zero initial offset we have to make sure at least the first
		// patched position fits the target range
		std::min<output_size_type>(overall_data_length - baseline_output_pos - 1,
			util::random::sample_from(distribution, random_engine) & (util::all_one_bits<offset_type>()));
	run_offsets[0] = offset;

	for(patch_run_length_type offset_index = 1; offset_index < run_length; offset_index++) {
		offset_type offset_delta =
			util::random::sample_from(distribution, random_engine) & (util::all_one_bits<offset_type>());
		if (util::addition_will_overflow(offset, offset_delta)){
			run_length = offset_index;
			break;
		}
		// offsets are from the baseline position, not from the last patch index, i.e.
		// it's not like in \\delta
		offset += offset_delta;
		if (baseline_output_pos + offset >= overall_data_length) {
			run_length = offset_index;
			break;
		}
		run_offsets[offset_index] = offset;
	}
	first_output_position_after_all_patches = baseline_output_pos + run_offsets[run_length-1] + 1;
}

template <typename PriorAdapter>
void CompressedIndices<PriorAdapter>::generate_patch_position_sequences(
	span<patch_index_type>        patch_output_position_run_lengths,
	span<unsigned char>           patch_output_position_run_individual_offset_sizes, // in bits
	span<output_index_type>       patch_output_position_run_baseline_values,
	span<unsigned char>           patch_output_position_offset_bytes,
	span<patch_index_type>        patch_output_position_run_offsets_start_pos,
	span<patch_index_type>        patch_output_position_anchors,
	output_size_type&             total_size_of_all_run_offsets)
{
	static_assert(util::is_power_of_2(sizeof(output_index_type)),
		"Not currently supporting non-power-of-2 output size types (although it would not be difficult)");

	// Used to randomly sample a size from among the values here - we'll same
	// the array index
	static constexpr const std::array<unsigned char,4> PatchPositionOffsetSizes { 1, 2, 4, 8 };

	const auto& config = parent::config;
	auto random_engine = this->random_engine;

	// For now, only disjoint-range patch sequence generation is supported

	total_size_of_all_run_offsets = 0;
	patch_index_type run_index = 0;
	num_patched_elements = 0;
	output_size_type first_output_position_after_all_patches = 0;
	auto run_offset_bytes = patch_output_position_offset_bytes.begin();
	while (first_output_position_after_all_patches < config.test_length) {
		auto distance_to_next_run =
			util::random::sample_from(inter_patch_run_distance, random_engine);
		auto run_baseline_pos = first_output_position_after_all_patches + distance_to_next_run;
		if (run_baseline_pos >= config.test_length) { break; }
		auto index_into_offset_sizes =
			util::random::sample_from(index_into_patch_offset_sizes_array, random_engine);
		unsigned char offset_size_in_bytes = PatchPositionOffsetSizes[index_into_offset_sizes];
		// The run length is the number of patches to perform in this run, not the length
		// of the span between the first and last data elements which get patched
		unsigned char offset_size_in_bits = offset_size_in_bytes * CHAR_BIT;
		auto run_length = util::random::sample_from(patch_run_length, random_engine);
		patch_index_type patch_positions_left_in_anchoring_interval =
			patch_anchoring_period - (num_patched_elements % patch_anchoring_period);
		run_length = std::min(run_length, patch_positions_left_in_anchoring_interval);
			// Note that the length is still prospective, and could go down further, even to 0 perhaps
		util::enforce(run_length > 0);
		util::enforce(util::is_aligned(patch_output_position_offset_bytes.begin(), offset_size_in_bytes),
			"The allocated space for the offsets of the various patch sequences is "
			"not well-enough aligned");
		// TODO: Different offset distributions for different sizes
		switch(offset_size_in_bits) {
		case  8: {
			using offset_type = uint_t<8 / util::bits_per_byte>;
			generate_single_run<sizeof(offset_type), sizeof(output_index_type), decltype(random_engine), sizeof(patch_index_type)>(
				reinterpret_cast<offset_type*>(run_offset_bytes), run_length,
				first_output_position_after_all_patches,
				run_baseline_pos, intra_patch_run_element_distance,
				config.test_length, random_engine,
				run_baseline_pos_is_always_pos_of_first_patch_in_run);
			} break;
		case 16: {
			using offset_type = uint_t<16 / util::bits_per_byte>;
			generate_single_run<sizeof(offset_type), sizeof(output_index_type), decltype(random_engine), sizeof(patch_index_type)>(
				reinterpret_cast<offset_type*>(run_offset_bytes), run_length,
				first_output_position_after_all_patches,
				run_baseline_pos, intra_patch_run_element_distance,
				config.test_length, random_engine,
				run_baseline_pos_is_always_pos_of_first_patch_in_run);
			} break;
		case 32: {
			using offset_type = uint_t<32 / util::bits_per_byte>;
			generate_single_run<sizeof(offset_type), sizeof(output_index_type), decltype(random_engine), sizeof(patch_index_type)>(
				reinterpret_cast<offset_type*>(run_offset_bytes), run_length,
				first_output_position_after_all_patches,
				run_baseline_pos, intra_patch_run_element_distance,
				config.test_length, random_engine,
				run_baseline_pos_is_always_pos_of_first_patch_in_run);
			} break;
		case 64: {
			using offset_type = uint_t<64 / util::bits_per_byte>;
			generate_single_run<sizeof(offset_type), sizeof(output_index_type), decltype(random_engine), sizeof(patch_index_type)>(
				reinterpret_cast<offset_type*>(run_offset_bytes), run_length,
				first_output_position_after_all_patches,
				run_baseline_pos, intra_patch_run_element_distance,
				config.test_length, random_engine,
				run_baseline_pos_is_always_pos_of_first_patch_in_run);
			} break;
		}
		if (run_length == 0) { break; }
		// at this point, the run length is final
		if (patch_positions_left_in_anchoring_interval == patch_anchoring_period ) {
			// we've just populated the first run within an anchoring interval -
			// which is where we place the interval's anchor
			auto anchoring_interval_index = num_patched_elements / patch_anchoring_period;
			patch_output_position_anchors[anchoring_interval_index] = run_index;
//			std::cout << "anchoring interval " << anchoring_interval_index
//				<< ": anchoring at run index " << run_index << "\n";
		}
		patch_output_position_run_lengths[run_index] = run_length;
		patch_output_position_run_individual_offset_sizes[run_index] = offset_size_in_bits;
		patch_output_position_run_baseline_values[run_index] = run_baseline_pos;
		auto size_used_by_run_offsets =
			util::round_up(run_length * offset_size_in_bytes, sizeof(output_index_type));
			// ... have to maintain alignment
		patch_output_position_run_offsets_start_pos[run_index] =
			(run_offset_bytes - patch_output_position_offset_bytes.begin()) / sizeof(output_index_type);

/*
  		if (config.be_verbose) {
			using namespace util;
			using std::setw;
			ios_flags_saver(std::cout);
			std::cout << std::right << "Run " << setw(2) << run_index << " (in anchoring interval "
				<< setw(4) << num_patched_elements / patch_anchoring_period << "): Length "
				<< setw(5) << promote_for_streaming(run_length)
				<< " (cumulative " << setw(7) << num_patched_elements + run_length << "); "
				<< " baseline output position " << setw(7) << run_baseline_pos
				<< "; offsets use " << setw(6) << size_used_by_run_offsets
				<< " bytes overall ( " << promote_for_streaming(offset_size_in_bits / CHAR_BIT) << " bytes each), "
				<< "starting at " << (void*) run_offset_bytes << " which is after " << setw(6)
				<< patch_output_position_run_offsets_start_pos[run_index]
				<< " units of size " << promote_for_streaming(sizeof(output_index_type))
				<< " bytes ( = " << sizeof(output_index_type) *
				   patch_output_position_run_offsets_start_pos[run_index]
				<< " bytes).\n";
		}
*/


		run_offset_bytes += size_used_by_run_offsets;

		util::enforce(patch_positions_left_in_anchoring_interval >= run_length,
			"We should never get past the next anchor within a patch run");
		patch_positions_left_in_anchoring_interval -= run_length;
		num_patched_elements += run_length;
		total_size_of_all_run_offsets += size_used_by_run_offsets;
		run_index++;
	}
	num_patch_position_runs = run_index;
}


template <typename PriorAdapter>
void CompressedIndices<PriorAdapter>::generate_patch_values(
	span<typename PriorAdapter::uncompressed_type> patch_values)
{
	util::random::fill(patch_values, patch_value, this->random_engine);
}


template <typename PriorAdapter>
KernelTestAdapter::BufferSizes CompressedIndices<PriorAdapter>::generateInputs(const HostBuffers& buffers)
{
	// TODO: We could read the uncompressed data from the DB
	// and generate everything else; but this is not supported for now

	parent::generateInputs(buffers);
	output_size_type total_size_of_all_run_offsets;
	//output_index_type num_anchors;

	if (need_to_generate("patch_values") and
		need_to_generate("patch_output_position_run_lengths") and
		need_to_generate("patch_output_position_run_individual_offset_sizes") and
		need_to_generate("patch_output_position_run_baseline_values") and
		need_to_generate("patch_output_position_offset_bytes") and
		need_to_generate("patch_output_position_run_offsets_start_pos") and
		need_to_generate("patch_output_position_anchors")
	) {
		auto patch_values                                = buffers.at("patch_values"                             ).as_span<uncompressed_type>();
		auto patch_output_position_run_lengths           = buffers.at("patch_output_position_run_lengths"        ).as_span<patch_index_type>();
		auto patch_output_position_run_individual_offset_sizes      = buffers.at("patch_output_position_run_individual_offset_sizes"   ).as_span<unsigned char>();
		auto patch_output_position_run_baseline_values   = buffers.at("patch_output_position_run_baseline_values").as_span<output_index_type>();
		auto patch_output_position_offset_bytes          = buffers.at("patch_output_position_offset_bytes"       ).as_span<unsigned char>();
		auto patch_output_position_run_offsets_start_pos = buffers.at("patch_output_position_run_offsets_start_pos").as_span<patch_index_type>();
		auto patch_output_position_anchors               = buffers.at("patch_output_position_anchors"            ).as_span<patch_index_type>();

		generate_patch_position_sequences(
			patch_output_position_run_lengths,
			patch_output_position_run_individual_offset_sizes,
			patch_output_position_run_baseline_values,
			patch_output_position_offset_bytes,
			patch_output_position_run_offsets_start_pos,
			patch_output_position_anchors,
			total_size_of_all_run_offsets);

		if (this->config.be_verbose) {
			std::cout << "The compressed input has " << num_patched_elements << " patches, "
				"in " << num_patch_position_runs << " runs.\n";
		}

		if (num_patched_elements > 0) {
			util::enforce(num_patch_position_runs > 0, "Can't have patches with no patch runs");
		}
		else if (this->config.be_verbose) {
			std::cerr << "No patching to perform - only underlying decompression will occur.\n";
		}

		generate_patch_values(patch_values);

		auto num_anchors = util::div_rounding_up(num_patched_elements, patch_anchoring_period);
		return {
				std::make_pair("patch_values", num_patched_elements * sizeof(uncompressed_type)),
				std::make_pair("patch_output_position_run_lengths",         num_patch_position_runs * sizeof(patch_index_type)),
				std::make_pair("patch_output_position_run_individual_offset_sizes",  num_patch_position_runs * sizeof(unsigned char)),
				std::make_pair("patch_output_position_run_baseline_values", num_patch_position_runs * sizeof(output_index_type)),
				std::make_pair("patch_output_position_offset_bytes",        total_size_of_all_run_offsets),
				std::make_pair("patch_output_position_run_offsets_start_pos",
						                                                    num_patch_position_runs * sizeof(patch_index_type)),
				std::make_pair("patch_output_position_anchors",             num_anchors * sizeof(patch_index_type)),
			};
	}
	else if (need_to_generate("patch_values") or
		need_to_generate("patch_output_position_run_lengths") or
		need_to_generate("patch_output_position_run_individual_offset_sizes") or
		need_to_generate("patch_output_position_run_baseline_values") or
		need_to_generate("patch_output_position_offset_bytes") or
		need_to_generate("patch_output_position_run_offsets_start_pos") or
		need_to_generate("patch_output_position_anchors")
	) {
		throw util::invalid_argument("Either all patch-related info is generated or none of it can be");
	}
	// Nothing for us to generate, then
	return { };

}

template<typename PatchIndex, typename OutputSize, typename Uncompressed, typename RunPatchPositionOffset>
static void apply_single_patch_run(
	Uncompressed*                  __restrict__  buffer_to_patch,
	const Uncompressed*            __restrict__  patch_values,
	const PatchIndex                             single_patch_run_length,
	OutputSize                                   baseline_value,
	PatchIndex                                   starting_patch_index,
	const RunPatchPositionOffset*  __restrict__  patch_output_position_offsets)
{
	for(PatchIndex index_within_run = 0; index_within_run < single_patch_run_length; index_within_run++) {
		PatchIndex patch_index = starting_patch_index + index_within_run;
		OutputSize patch_output_position = baseline_value + patch_output_position_offsets[index_within_run];
		buffer_to_patch[patch_output_position] = patch_values[patch_index];
	}
}

/*
 * TODO: Switch to using sizes, and separate size type from index type
 */
template<typename PatchIndex, typename OutputSize, typename Uncompressed>
static void patch(
	Uncompressed*             __restrict__  buffer_to_patch,
	const Uncompressed*       __restrict__  patch_values,
	const PatchIndex*         __restrict__  patch_output_position_run_lengths,
	const unsigned char*      __restrict__  patch_output_position_run_individual_offset_sizes, // in bits
	const OutputSize*         __restrict__  patch_output_position_run_baseline_values,
	const unsigned char*      __restrict__  patch_output_position_offset_bytes,
	const PatchIndex*         __restrict__  patch_output_position_run_offsets_start_pos,
	const PatchIndex*         __restrict__  patch_output_position_anchors,
	PatchIndex                              num_patch_position_runs,
	PatchIndex                              num_patched_elements)
{
	UNUSED(patch_output_position_anchors);  // That could theoretically be used if we were parallelizing here as well. But - we aren't.
	UNUSED(num_patched_elements); // TODO: Perhaps just drop that parameter?
	PatchIndex starting_patch_index_of_run = 0;
	for(PatchIndex run_index = 0; run_index < num_patch_position_runs; run_index++) {
		auto run_length = patch_output_position_run_lengths[run_index];
		auto run_baseline_value = patch_output_position_run_baseline_values[run_index];
		auto run_offsets_start = patch_output_position_run_offsets_start_pos[run_index];
		auto offset_size_in_bits = patch_output_position_run_individual_offset_sizes[run_index];
		switch(offset_size_in_bits) {
		case  8:
			apply_single_patch_run(
				buffer_to_patch, patch_values, run_length, run_baseline_value,
				starting_patch_index_of_run,
				reinterpret_cast<const unsigned char*>(patch_output_position_offset_bytes + run_offsets_start * sizeof(OutputSize)));
			break;
		case 16:
			apply_single_patch_run(
				buffer_to_patch, patch_values, run_length, run_baseline_value,
				starting_patch_index_of_run,
				reinterpret_cast<const unsigned short*>(patch_output_position_offset_bytes + run_offsets_start * sizeof(OutputSize)));
			break;
		case 32:
			apply_single_patch_run(
				buffer_to_patch, patch_values, run_length, run_baseline_value,
				starting_patch_index_of_run,
				reinterpret_cast<const unsigned int*>(patch_output_position_offset_bytes + run_offsets_start * sizeof(OutputSize)));
			break;
		case 64:
			apply_single_patch_run(
				buffer_to_patch, patch_values, run_length, run_baseline_value,
				starting_patch_index_of_run,
				reinterpret_cast<const unsigned long long*>(patch_output_position_offset_bytes + run_offsets_start * sizeof(OutputSize)));
			break;
		}
		starting_patch_index_of_run += run_length;
	}
}


template <typename PriorAdapter>
KernelTestAdapter::BufferSizes CompressedIndices<PriorAdapter>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		parent::generateExpectedOutputs(expected_buffers, buffers);
		auto patch_values                                = buffers.at("patch_values"                             ).as_span<const uncompressed_type>();
		auto patch_output_position_run_lengths           = buffers.at("patch_output_position_run_lengths"        ).as_span<const patch_index_type>();
		auto patch_output_position_run_individual_offset_sizes =
			                                               buffers.at("patch_output_position_run_individual_offset_sizes"
			                                            	                                                     ).as_span<unsigned char>();
		auto patch_output_position_run_baseline_values   = buffers.at("patch_output_position_run_baseline_values").as_span<const output_index_type>();
		auto patch_output_position_offset_bytes          = buffers.at("patch_output_position_offset_bytes"       ).as_span<const unsigned char>();
		auto patch_output_position_run_offsets_start_pos = buffers.at("patch_output_position_run_offsets_start_pos").as_span<const patch_index_type>();
		auto patch_output_position_anchors               = buffers.at("patch_output_position_anchors"            ).as_span<const patch_index_type>();
		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		patch<patch_index_type, output_index_type, uncompressed_type>(
			decompressed.data(),
			patch_values.data(),
			patch_output_position_run_lengths.data(),
			patch_output_position_run_individual_offset_sizes.data(),
			patch_output_position_run_baseline_values.data(),
			patch_output_position_offset_bytes.data(),
			patch_output_position_run_offsets_start_pos.data(),
			patch_output_position_anchors.data(),
			num_patch_position_runs,
			num_patched_elements);
		// patching doesn't change the decompressed buffer's size, only its contents, so we
		// return an empty size-changes map
	}
	return NoBuffersToResize;
}

template <typename PriorAdapter>
void CompressedIndices<PriorAdapter>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	auto& config = parent::config;
	util::enforce(device_scratch_area.empty());
	cuda::device::current::get().synchronize(stream);
	parent::launchKernels(buffers, DeviceBuffers::mapped_type(), stream);

	// The kernel we're applying is compressed-indices scatter, hence the argument names correspond
	// to that and do not regard compression

	auto target                                       = buffers.at("decompressed"                                      ).as_span<type_erased_element_type       >().data();
	auto data_to_scatter                              = buffers.at("patch_values"                                      ).as_span<const type_erased_element_type >().data();
	auto scatter_position_run_lengths                 = buffers.at("patch_output_position_run_lengths"                 ).as_span<const patch_index_type         >().data();
	auto scatter_position_run_individual_offset_sizes = buffers.at("patch_output_position_run_individual_offset_sizes" ).as_span<const unsigned char            >().data();
	auto scatter_position_run_baseline_values         = buffers.at("patch_output_position_run_baseline_values"         ).as_span<const output_index_type        >().data();
	auto scatter_position_offset_bytes                = buffers.at("patch_output_position_offset_bytes"                ).as_span<const unsigned char            >().data();
	auto scatter_position_run_offsets_start_pos       = buffers.at("patch_output_position_run_offsets_start_pos"       ).as_span<const patch_index_type         >().data();
	auto scatter_position_anchors                     = buffers.at("patch_output_position_anchors"                     ).as_span<const patch_index_type         >().data();

	patch_index_size_type input_data_length =
		buffers.at("patch_values").as_span<const uncompressed_type>().size();
		// that's the input to the _scatter_ kernel, which is the number of patches

	patch_index_size_type num_scatter_position_runs =
		buffers.at("patch_output_position_run_lengths").as_span<const patch_index_type>().size();
	auto anchoring_period = patch_anchoring_period;

	if (input_data_length == 0) {
		if (config.be_verbose) {
			std::cout << "There's at least one element to patch, proceeding with patching (scatter) kernel";
		}
		return;
	}
	if (config.be_verbose) {
		std::cout << "There are no elements to patch; skipping patching (scatter) kernel.";
	}
	auto launch_config = resolveLaunchConfigurations()[1].launch_config;

	// template<unsigned OutputIndexSize, unsigned ElementSize, unsigned InputIndexSize, unsigned RunLengthSize = InputIndexSize>
	cuda::device::current::get().synchronize(stream);
	cuda::kernels::scatter::compressed_indices::kernel_t<
		sizeof(output_index_type), sizeof(uncompressed_type), sizeof(patch_index_type), sizeof(patch_index_type)>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(
			target,
			data_to_scatter,
			scatter_position_run_lengths,
			scatter_position_run_individual_offset_sizes,
			scatter_position_run_baseline_values,
			scatter_position_run_offsets_start_pos,
			scatter_position_offset_bytes,
			scatter_position_anchors,
			anchoring_period,
			num_scatter_position_runs,
			input_data_length));
}


static_block {
	namespace functors = cuda::functors;
	namespace unary_models = ::cuda::functors::unary::parametric_model;

	//                                            IndexSize   UncompressedSize  DictionaryIndexSize
	//------------------------------------------------------------------------------------------------------
    CompressedIndices<decompression::Dictionary < 4,          1,                1 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 4,          2,                1 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 4,          2,                2 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 4,          4,                1 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 4,          4,                2 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 4,          4,                4 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 4,          8,                1 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 4,          8,                2 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 4,          8,                4 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 4,          8,                8 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 8,          1,                1 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 8,          2,                1 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 8,          2,                2 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 8,          4,                1 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 8,          4,                2 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 8,          4,                4 >>::registerInSubclassFactory();

    CompressedIndices<decompression::Dictionary < 8,          8,                1 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 8,          8,                2 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 8,          8,                4 >>::registerInSubclassFactory();
    CompressedIndices<decompression::Dictionary < 8,          8,                8 >>::registerInSubclassFactory();

       // Straightforward aposteriori patching of Delta-compressed data does not make much sense,
       // as while the patch may correct a certain element, its subsequents would still be decompressed using
       // the unpatched value; alternatively, one would need to patch entire intervals due to just one delta
       // value exceeding the representable range. Instead, the patching can only be integral to the de/compression
       // of the Delta values themselves.


	//                                       IndexSize   UncompressedSize  CompressedSize
	// ---------------------------------------------------------------------------------
	CompressedIndices<decompression::Delta < 4,          uint_t<2>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<4>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<4>,     sint_t<2> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<8>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<8>,     sint_t<2> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<8>,     sint_t<4> > >::registerInSubclassFactory();

	CompressedIndices<decompression::Delta < 4,          sint_t<2>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          sint_t<4>,     sint_t<2> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          sint_t<2>,     uint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          sint_t<4>,     uint_t<2> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<2>,     uint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 4,          uint_t<4>,     uint_t<2> > >::registerInSubclassFactory();

	CompressedIndices<decompression::Delta < 8,          uint_t<2>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 8,          uint_t<4>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 8,          uint_t<4>,     sint_t<2> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 8,          uint_t<8>,     sint_t<1> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 8,          uint_t<8>,     sint_t<2> > >::registerInSubclassFactory();
	CompressedIndices<decompression::Delta < 8,          uint_t<8>,     sint_t<4> > >::registerInSubclassFactory();


	//                                                       IndexSize UncompressedSize  CompressedSize
	//------------------------------------------------------------------------------------------------------------------------
	CompressedIndices<decompression::DiscardZeroBytesFixed < 4,         2,                1 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 4,         4,                1 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 4,         4,                2 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 4,         8,                1 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 4,         8,                2 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 4,         8,                4 >>::registerInSubclassFactory();

	CompressedIndices<decompression::DiscardZeroBytesFixed < 8,         2,                1 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 8,         4,                1 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 8,         4,                2 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 8,         8,                1 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 8,         8,                2 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesFixed < 8,         8,                4 >>::registerInSubclassFactory();

	//                                                          IndexSize  UncompressedSize   ElementSizesContainerSize
	//---------------------------------------------------------------------------------------------------------------------------
	CompressedIndices<decompression::DiscardZeroBytesVariable < 4,         2,                 4 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesVariable < 4,         4,                 4 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesVariable < 4,         8,                 4 >>::registerInSubclassFactory();

	CompressedIndices<decompression::DiscardZeroBytesVariable < 8,         2,                 4 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesVariable < 8,         4,                 4 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesVariable < 8,         8,                 4 >>::registerInSubclassFactory();

	CompressedIndices<decompression::DiscardZeroBytesVariable < 4,         2,                 8 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesVariable < 4,         4,                 8 >>::registerInSubclassFactory();
	CompressedIndices<decompression::DiscardZeroBytesVariable < 4,         8,                 8 >>::registerInSubclassFactory();


	//                                                  IndexSize  Uncompressed   Compressed   UnaryModelFuction
	//----------------------------------------------------------------------------------------------------------------------
	CompressedIndices<decompression::FrameOfReference < 4,         int16_t,       int8_t,      unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 4,         int32_t,       int8_t,      unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 4,         int32_t,       int16_t,     unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 8,         int32_t,       int16_t,     unary_models::linear  < 4, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 8,         int32_t,       int16_t,     unary_models::linear  < 8, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 4,         int16_t,       int8_t,      unary_models::constant< 4, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 4,         int32_t,       int8_t,      unary_models::constant< 4, int32_t >  >>::registerInSubclassFactory();
	CompressedIndices<decompression::FrameOfReference < 4,         int32_t,       int16_t,     unary_models::constant< 4, int32_t >  >>::registerInSubclassFactory();


	//                                                  IndexSize UncompressedSize  WithDictionary   BitmapAlignmentInBytes
	//----------------------------------------------------------------------------------------------------------------------------
	CompressedIndices<decompression::IncidenceBitmaps < 4,        1,                true,            8  >>::registerInSubclassFactory();
	CompressedIndices<decompression::IncidenceBitmaps < 4,        2,                true,            8  >>::registerInSubclassFactory();
	CompressedIndices<decompression::IncidenceBitmaps < 4,        4,                true,            8  >>::registerInSubclassFactory();
	CompressedIndices<decompression::IncidenceBitmaps < 4,        8,                true,            8  >>::registerInSubclassFactory();
	CompressedIndices<decompression::IncidenceBitmaps < 4,        1,                false,           8  >>::registerInSubclassFactory();
}
} // namespace aposteriori
} // namespace patched
} // namespace decompression
} // namespace kernel_tests
