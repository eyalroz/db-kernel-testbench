#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_PATCHED_NAIVE_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_PATCHED_NAIVE_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {
namespace patched {
namespace aposteriori {

template <typename PriorAdapter>
class Naive : public PriorAdapter
{
public:
	using this_class = Naive<PriorAdapter>;
	using parent = PriorAdapter;
	using index_type = typename parent::index_type;
	using size_type = typename parent::size_type;
	using uncompressed_type = typename parent::uncompressed_type;
	using type_erased_element_type = uint_t<sizeof(uncompressed_type)>;\
		// used for scattering - which doesn't care about the actual type and
		// assumes the data is trivially-copy-constructible

	using KernelTestAdapter::BufferDescriptors;
	using KernelTestAdapter::BufferSizes;

	KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(this_class, parent);

	Naive(const TestConfiguration& config);
	std::string buildPrettyKernelName() const override;
	KernelTestAdapter::BufferDescriptors getBufferDescriptors() const override;
	std::string getDescription() const override;
	LaunchConfigurationSequence resolveLaunchConfigurations() const override;
	void generate_patches(
		span<index_type>                       positions_to_patch,
		span<uncompressed_type>                patch_values,
		size_type&               __restrict__  num_patched_elements);
	Naive<PriorAdapter>::index_type resolvePatches(const HostBuffers& buffers);
	KernelTestAdapter::BufferSizes generateInputs(const HostBuffers& buffers) override;
	KernelTestAdapter::BufferSizes generateExpectedOutputs(
		const HostBuffers& expected_buffers,
		const HostBuffers& buffers) override;
	void launchKernels(
		const DeviceBuffers& buffers,
		typename DeviceBuffers::mapped_type device_scratch_area,
		cuda::stream_t& stream) const override;

protected:
	distribution_t<index_type> patch_index_offset {
		util::random::distribution_family_t::geometric, 16.0, (index_type)1, (index_type) 200
	};
	distribution_t<uncompressed_type> patch_value =
		distribution_t<uncompressed_type>::uniform();
};

} // namespace aposteriori
} // namespace patched
} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_PATCHED_NAIVE_H_ */
