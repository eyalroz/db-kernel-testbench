#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_RUN_POSITION_ENCODING_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_RUN_POSITION_ENCODING_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

template <
	unsigned UncompressedIndexSize,
	unsigned UncompressedSize,
	unsigned PositionOffsetSize,
	bool PositionsAreRelative,
	unsigned RunIndexSize = UncompressedIndexSize>
class RunPositionEncoding:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<RunPositionEncoding<
		UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize> >
{
public:
	using run_index_type                = uint_t<RunIndexSize>;
	using run_index_size_type           = size_type_by_index_size<RunIndexSize>;
	using uncompressed_index_type       = uint_t<UncompressedIndexSize>;
	using position_offset_type          = uint_t<PositionOffsetSize>;
	using position_offset_size_type     = size_type_by_index_size<PositionOffsetSize>;
	using uncompressed_type             = uint_t<UncompressedSize>;

	using index_type                    = uncompressed_index_type;
	using uncompressed_index_size_type  = size_type_by_index_size<UncompressedIndexSize>;
	using size_type                     = uncompressed_index_size_type;
	using run_length_type               = position_offset_type;
		// They're the same type since the length of a run is the difference between
		// between the end and start position offsets

	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(RunPositionEncoding)

	static_assert(util::is_power_of_2(UncompressedIndexSize), "(Uncompressed) index size is not a power of 2");
	static_assert(PositionsAreRelative or PositionOffsetSize == UncompressedIndexSize,
		"If run positions are in absolute values, their type must be able to cover the entire "
		"potential range of data (i.e. their type must be at least as large as the size type");

protected:
	void generate_run_data(
		span<uncompressed_type>           run_data);
	void generate_run_start_positions_and_anchors(
		span<position_offset_type>        run_positions,
		span<run_index_type>              position_anchors
	);
	void generate_expected_decompressed(
		span<uncompressed_type>           decompressed,
		span<const uncompressed_type>     run_data,
		span<const position_offset_type>  run_positions,
		span<const run_index_type>        position_anchors
	) const;

protected:
	run_index_size_type                   num_element_runs;
		// ... not to be confused with num_test_runs, which is the number of
		// runs the kernel test will be conducted by the tester binary;
		// this value is used when generating synthetic compressed data (
		// not when compressing input data read from a DB)

	position_offset_size_type  segment_length       { 1024    };
		// Values which are not a power of 2 are less well tested

	distribution_t<run_length_type>
	                      random_run_length               { distribution_t<run_length_type>::geometric(10.0, 1) };
		// ... Note that the distribution is capped, so it's not really
		// geometric, i.e. the probabilities for 0 and 1 are added up and are
		// the probability of 1.

	// Note: We can't "use all ones" as the run data, but we can do something similar:
	bool                  use_identity_for_run_data       { false   };
	bool                  run_data_uniformly_random       { false   };

	distribution_t<uncompressed_type>
	                      random_datum                    { distribution_t<uncompressed_type>::uniform(11, 99) };
	double                run_data_random_contribution    { 0.9     };
	double                run_data_linear_contribution    { 0.1     };
	double                run_data_linear_scale           { 10.0    };
	double                run_data_linear_offset          { 20.0    };

};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_RUN_POSITION_ENCODING_H_ */
