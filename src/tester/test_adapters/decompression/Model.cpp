#include "Model.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>
#include <cuda/model_functors.hpp>

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/miscellany.hpp"

#include <boost/lexical_cast.hpp>

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <type_traits>

#include "kernel_wrappers/decompression/model.cu"

/*
template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::linear<IndexSize, T>& umf,
	std::string name, std::string parameter, std::ostream& os)
{
	os << name << "(" << parameter << ") = " << umf.model_coefficients[1] << " * x + " << umf.model_coefficients[0];
}

template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::constant<IndexSize, T>& umf,
	std::string name, std::string parameter, std::ostream& os)
{
	os << name << "(" << parameter << ") = " << umf.model_coefficients[0];
}

template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::zero<IndexSize, T>& umf,
	std::string name, std::string parameter, std::ostream& os)
{
	os << name << "(" << parameter << ") = 0";
}


template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::linear<IndexSize, T>& umf,
	std::string parameter, std::ostream& os)
{
	os << umf.model_coefficients[1] << " * x + " << umf.model_coefficients[0];
}

template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::constant<IndexSize, T>& umf,
	std::string parameter, std::ostream& os)
{
	os << umf.model_coefficients[0];
}
*/

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
std::string Model<IndexSize, Uncompressed, UnaryModelFunction>::buildPrettyKernelName() const
{
	return util::concat_string(
		"A Model decompression kernel, with uncompressed data type ", util::type_name<Uncompressed>(),
		"which corresponds exactly to the evaluation of a model function ",
		" of type ", util::type_name<UnaryModelFunction>(), " for every index in the array of data "
		"(with length and indices having size ", IndexSize, ")."
	);
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
KernelTestAdapter::BufferDescriptors Model<IndexSize, Uncompressed, UnaryModelFunction>::getBufferDescriptors() const
{
	return {
		{ "decompressed",               BufferDescriptor::make<Uncompressed>          (config.test_length,           BufferDirection::Output) },
	};
}

template<typename UnaryModelFunction>
static typename UnaryModelFunction::coefficients_type parse_model_coefficients(
	std::string comma_separated_coefficients,
	char coefficient_delimiter)
{
	typename UnaryModelFunction::coefficients_type result;
	using model_coefficient_type  = typename UnaryModelFunction::coefficient_type;
	enum { model_dimension = UnaryModelFunction::model_dimension };

	// TODO: Move defaults to the class definition somehow
	if  (comma_separated_coefficients.empty()) {
		model_coefficient_type default_coefficient_value =
			std::is_floating_point<model_coefficient_type>::value ? 1.789 : 1;
		std::fill(result.begin(), result.end(), default_coefficient_value);
	}
	else {
		auto coefficient_strings = util::explode(comma_separated_coefficients, coefficient_delimiter);
		util::enforce(coefficient_strings.size() == model_dimension,
			comma_separated_coefficients.size(), " coefficients were specified as a "
			"configuration option, but there must be exactly ", model_dimension,
			" coefficients for a model of type ", util::type_name<UnaryModelFunction>());
		std::transform(coefficient_strings.begin(), coefficient_strings.end(),
			result.begin(), boost::lexical_cast<model_coefficient_type, std::string>);
	}
	return result;
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
inline Model<IndexSize, Uncompressed, UnaryModelFunction>
	::Model(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;

	std::string delimited_coefficients;
	bool have_coefficients_option = resolveOption(delimited_coefficients,
		ConfigurationKeySet {
			"single-reference-model-coefficients", "single-reference-model",
			"model", "model-coefficients", "coefficients", "coeffs"
		}
	);
	if (have_coefficients_option) {
		model_function_coefficients =
			parse_model_coefficients<UnaryModelFunction>(
				delimited_coefficients, coefficient_delimiter);
		have_model_function_coefficients = true;
	}
	else {
		for(auto& coefficient : model_function_coefficients) {
			coefficient = util::random::sample_from(model_coefficient_random_part, random_engine);
		}
	}

	// Note: We may be printing junk for the model coefficients here, if we calculate
	// them later based on the decompressed data
	maybe_print_extra_configuration(
		make_pair("single all-data model coefficients",             model_function_coefficients)
	);

	return;
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
std::string Model<IndexSize, Uncompressed, UnaryModelFunction>::getDescription() const
{
	return
		"Decompression kernel for the Model compression scheme. It is intended for "
		"synthetic data, which corresponds perfectly to a certain (single) model "
		"function: A constant, a line, a fixed-degree polynomial, a sinusoid etc. "
		"Since the correspondence is perfect, no actual compressed data is kept, and "
		"the choice of model function and coefficients are the complete description of "
		"the data.";
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
LaunchConfigurationSequence Model<IndexSize, Uncompressed, UnaryModelFunction>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		// not specifying the serialization factor
		{ "length",          config.test_length }
	};
	::cuda::kernels::decompression::model::kernel_t<IndexSize, Uncompressed, UnaryModelFunction> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("decompression::model::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
void Model<IndexSize, Uncompressed, UnaryModelFunction>::generate_expected_decompressed(
	span<Uncompressed>                   decompressed)
{
	UnaryModelFunction model_function(model_function_coefficients);
	for(index_type pos = 0; pos < decompressed.length(); pos++) {
		decompressed[pos] = static_cast<Uncompressed>(model_function(pos));
	}
}

namespace detail {

//template <unsigned IndexSize, typename Datum, UnaryModelFunction>
//static optional<UnaryModelFunction> fit(span<Datum> data, tag<UnaryModelFunction> a_tag);

template <unsigned IndexSize, typename Datum>
static optional<::cuda::functors::unary::parametric_model::linear<IndexSize, Datum>> fit_impl(
	span<const Datum> data, tag<::cuda::functors::unary::parametric_model::linear<IndexSize, Datum>>)
{
	using size_type = size_type_by_index_size<IndexSize>;
	if (data.empty()) { return nullopt; }
	if (data.size() == 1) {
		// We don't need the linear component, let's zero it out
		return ::cuda::functors::unary::parametric_model::linear<IndexSize, Datum>( { data[0], 0 });
	};
	typename ::cuda::functors::unary::parametric_model::linear<IndexSize, Datum>::coefficients_type coefficients;
	coefficients[0] = data[0];
	coefficients[1] = data[1] - data[0];
	auto model = ::cuda::functors::unary::parametric_model::linear<IndexSize, Datum>(coefficients);

	for(size_type i = 0; i < data.size(); i++) {
		if (model(i) != data[i]) { return nullopt; }
	}
	return model;
}

template <unsigned IndexSize, typename Datum>
static optional<::cuda::functors::unary::parametric_model::constant<IndexSize, Datum>> fit_impl(
	span<const Datum> data, tag<::cuda::functors::unary::parametric_model::constant<IndexSize, Datum>>)
{
	using size_type = size_type_by_index_size<IndexSize>;
	if (data.empty()) { return nullopt; }
	typename ::cuda::functors::unary::parametric_model::constant<IndexSize, Datum>::coefficients_type coefficients;
	coefficients[0] = data[0];
	auto model = ::cuda::functors::unary::parametric_model::constant<IndexSize, Datum>(coefficients);

	for(size_type  i = 0; i < data.size(); i++) {
		if (model(i) != data[i]) { return nullopt; }
	}
	return model;
}

} // namespace detail

/**
 * Fits a model function of type UnaryModelFunction to a sequence
 * of data (which is interpreted as a function from an index into the
 * data, i.e. the function we fit with a model is f(i) = data[i] for
 * non-negative i)
 */
template <unsigned IndexSize, typename Datum, typename UnaryModelFunction>
static optional<UnaryModelFunction> fit(span<const Datum> data)
{
	return detail::fit_impl<IndexSize, Datum>(data, tag<UnaryModelFunction>());
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
void Model<IndexSize, Uncompressed, UnaryModelFunction>::determine_model_coefficients(span<const Uncompressed> uncompressed)
{
	if (config.be_verbose) {
		std::cout << "fitting a " << util::type_name<UnaryModelFunction>() <<
			" model function to the uncompressed data.";
	}
	auto fit_result = fit<IndexSize, Uncompressed, UnaryModelFunction>(uncompressed);
	if (not has_value(fit_result)) {
		throw util::invalid_argument(util::concat_string(
			"Failed fitting a ", util::type_name<UnaryModelFunction>(), " model function "
			"to the uncompressed data.\n"));
	}
	model_function_coefficients = fit_result.value().model_coefficients;
	have_model_function_coefficients = true;
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
KernelTestAdapter::BufferSizes Model<IndexSize, Uncompressed, UnaryModelFunction>::generateInputs(const HostBuffers& buffers)
{
	if (not need_to_generate("decompressed")) {
		// We'll need to compress and determine the model parameters...
		auto decompressed   = buffers.at("decompressed").as_span<Uncompressed>();
		if (!have_model_function_coefficients) {
			determine_model_coefficients(decompressed);
		}
	}
	return NoBuffersToResize;
}

template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
KernelTestAdapter::BufferSizes Model<IndexSize, Uncompressed, UnaryModelFunction>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	UNUSED(buffers);
	if (need_to_generate("decompressed")) {
		auto decompressed   = expected_buffers.at("decompressed").as_span<Uncompressed>();

		generate_expected_decompressed(decompressed);
	}
	return NoBuffersToResize;
}


template <unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
void Model<IndexSize, Uncompressed, UnaryModelFunction>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto decompressed = buffers.at("decompressed").as_span<Uncompressed>().data();
	auto model_coefficients = model_function_coefficients;
	auto launch_config = resolveLaunchConfigurations()[0].launch_config;
	index_type length = config.test_length;
	cuda::kernels::decompression::model
		::kernel_t<IndexSize, Uncompressed, UnaryModelFunction>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(decompressed, model_coefficients, length)
	);

}

static_block {
	namespace functors = cuda::functors;
	namespace unary_models = ::cuda::functors::unary::parametric_model;

	/*
	 * Note: to enable model functions with floating-point coefficients you must make sure
	 * the device-side (and host-side, for the test adapter) floating-point calculations
	 * are consistent. And NB that it's the _consistency_ rather than the _accuracy_ that matters,
	 * because all we need is for the compressor and the decompressor to having the same model-predicted
	 * value, regardless of what it is exactly.
	 *
	 * To do so, read about the IEEE rounding options for floating-point operation result rounding
	 * (nearest even, nearest odd, towards zero, towards infinity, down, up) and, on the device, use the
	 * __fmul_[rn,rz,ru,rd](x,y),   __fadd_[rn,rz,ru,rd](x,y) etc intrinsics
	 */

	//      IndexSize  Uncompressed   UnaryModelFuction
	//----------------------------------------------------------------------------------------------------------------------
	Model < 4,         int16_t,       unary_models::linear  < 4, int16_t >  >::registerInSubclassFactory();
	Model < 4,         int32_t,       unary_models::linear  < 4, int32_t >  >::registerInSubclassFactory();
	Model < 8,         int32_t,       unary_models::linear  < 8, int32_t >  >::registerInSubclassFactory();
	Model < 4,         int16_t,       unary_models::constant< 4, int16_t >  >::registerInSubclassFactory();
	Model < 4,         int32_t,       unary_models::constant< 4, int32_t >  >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
