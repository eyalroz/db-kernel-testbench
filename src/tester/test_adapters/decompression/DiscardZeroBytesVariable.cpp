
#include "DiscardZeroBytesVariable.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>


#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/boolean.hpp"

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>

#include "kernel_wrappers/decompression/discard_zero_bytes_variable.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
std::string DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss <<
		"Decompression kernel for the variable discard zero-bytes / variable null suppression"
		"compression scheme (DZBV / NSV) with uncompressed data of size "
		<< UncompressedSize << "(with element sizes containers holding "
		<< ElementSizesContainerSize << "bits each, input indices and input length have size" << IndexSize << " bytes)";
	return oss.str();
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
KernelTestAdapter::BufferDescriptors DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>
::getBufferDescriptors() const
{
	// Note: The number of element size containers may well be smaller than the estimate
	// here, which assumes all possible sizes are represented

	auto num_anchors = util::div_rounding_up(config.test_length, segment_length);
	auto min_possible_element_sizes_per_container =	element_sizes_per_container(0, UncompressedSize);
	auto max_num_sizes_containers = util::div_rounding_up(config.test_length, min_possible_element_sizes_per_container);

	return {
		{ "decompressed", BufferDescriptor::make<uncompressed_type>(
			config.test_length, BufferDirection::Output ).print_as_hexadecimal() },
		{ "compressed_data",
			BufferDescriptor::make<unsigned char>(
				UncompressedSize * config.test_length, BufferDirection::Input).print_as_hexadecimal() },
		{ "packed_element_sizes",
			BufferDescriptor::make<element_sizes_container_type>(
				max_num_sizes_containers, BufferDirection::Input).print_as_hexadecimal() },
		{ "position_anchors",
			BufferDescriptor::make<index_type>(num_anchors, BufferDirection::Input) },
	};
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
inline DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>
::DiscardZeroBytesVariable(const TestConfiguration& config_)
	: KernelTestAdapter(config_)
{
	using std::make_pair;

	resolveOption(segment_length,
		ConfigurationKeySet {
			"position-anchoring-period", "anchoring-period",
			"elements-per-anchor", "position-anchoring-interval"}
	);

	have_size_range_overrides.for_min = resolveOption(represented_sizes.min,
		ConfigurationKeySet {
			"min-represented-element-size", "min-represented-element-size", "min-represented-size"}
	);

	have_size_range_overrides.for_max = resolveOption(represented_sizes.max,
		ConfigurationKeySet {
			"max-represented-element-size", "max-represented-element-size", "max-represented-size"}
	);

	if (not have_size_range_overrides.for_min) {
		represented_sizes.min = 1; // What about size 0?
	}
	if (not have_size_range_overrides.for_max) {
		represented_sizes.max = UncompressedSize;
	}
	if (have_size_range_overrides.for_min or have_size_range_overrides.for_max) {
		generated_element_size_distribution =
			distribution_t<element_size_type>::uniform(
				represented_sizes.min, represented_sizes.max);
	}

	resolveOption(generated_element_size_distribution,
		ConfigurationKeySet {
			"generated-element-size", "generated-element-size-distribution",
			"generated-size", "generated-size-distribution"}
	);

	resolveOption(element_value_byte_distribution,
		ConfigurationKeySet {
			"element-value-byte", "element-value-bytes", "element-value-byte-distribution"
			"element-byte", "element-byte-distribution"
		}
	);

	bool use_all_ones_for_nonzero_bytes = resolveOption(ConfigurationKeySet {
		"use-one-for-every-nonzero-byte", "use-all-ones-for-data",
		"all-ones-data", "data-all-ones"}, false);

	resolveOption(use_modular_identity_for_nonzero_bytes,
		ConfigurationKeySet {
			"use-iota-for-every-nonzero-byte", "use-modular-iota-for-every-nonzero-byte",
			"use-iota-for-data", "use-modular-iota-for-data",
			"identity-data", "modular-identity-data",
			"iota-data", "modular-iota-data",
	});


	resolveOption(fail_on_invalid_uncompressed_data,
		ConfigurationKeySet {
			"fail-on-invalid-uncompressed-data",
			"fail-on-invalid-data",
			"dont-compress-invalid-uncompressed-data",
			"dont-compress-invalid-data",
	});

	util::enforce(not (use_modular_identity_for_nonzero_bytes && use_all_ones_for_nonzero_bytes));
	util::enforce(
		represented_sizes.min >= 0 &&
		represented_sizes.min <= UncompressedSize,
		"Invalid minimum represented element size");
	util::enforce(
		represented_sizes.max >= represented_sizes.min &&
		represented_sizes.min <= UncompressedSize,
		"Invalid maximum represented element size");
	util::enforce(bits_per_element_size() <= util::size_in_bits<element_sizes_container_type>::value,
		"specified represented element size range would require ",
		bits_per_element_size(), " bits, while the container for uncompressed element sizes "
		"has only ", ElementSizesContainerSize * util::bits_per_byte, " bits." );
	util::enforce(generated_element_size_distribution.min() >= represented_sizes.min,
		"Element size distribution must not be able to generate elements too small to represent");
	util::enforce(generated_element_size_distribution.max() <= UncompressedSize,
		"Element size distribution must not be able to generate elements too large to represent");
	util::enforce(element_sizes_per_container() >= 1,
		"Cannot fit a single element's size into a lengths container of size ", ElementSizesContainerSize);

	if (use_all_ones_for_nonzero_bytes) {
		element_value_byte_distribution = distribution_t<unsigned char>::single_value(1);
	}

	maybe_print_extra_configuration(
		make_pair("Period between successive anchors into the output",
			                                                         segment_length              ),
		make_pair("Represented element minimum size",                represented_sizes.min                  ),
		make_pair("Represented element maximum size",                represented_sizes.max                  ),
		make_pair("Generated element size distribution",             generated_element_size_distribution    ),
		make_pair("Number of bits for representing element size",    bits_per_element_size()                ),
		make_pair("Number of element sizes per sizes container",     element_sizes_per_container()          ),
		make_pair("Set the i'th compressed byte to i (modulo 0xFF)", use_modular_identity_for_nonzero_bytes ),
		make_pair("Uncompressed data bytes distribution",            element_value_byte_distribution        )
	);
	return;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
std::string DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::getDescription() const
{
	return
		"Decompression kernel for the 'discard zero bytes - variable length' or "
		"'variable null suppression' compression scheme (DZBV / NS): With this scheme, it "
		"is assumed that most compressed data elements fit into the lower bytes of the memory "
		"representation - some into less bytes, some into more. We thus discard those 'empty' "
		"high bytes for each element; and since we discard a variable number of bytes per element, "
		"we keep an indication of how many bytes the element now takes up in the compressed"
		"sequence byte sequence; these length values are uniform in the length they themselves "
		"take, which is typically under 1 byte. Note endianness is an issue with this compression "
		"scheme.";
}

// Note: This will return _incorrect results_ if you invoke it before generateInputs() !
template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
LaunchConfigurationSequence DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		{ "length",                 (size_t) config.test_length        },
		{ "segment_length",         (size_t) segment_length },
		{ "bits_per_element_size",  (unsigned) bits_per_element_size() },
	};
	::cuda::kernels::decompression::discard_zero_bytes::variable_width::kernel_t<IndexSize, UncompressedSize, ElementSizesContainerSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("decompression::discard_zero_bytes::variable_width::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
void DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::generate_compressed_data(
	span<unsigned char>                       compressed,
	span<const element_sizes_container_type>  element_sizes,
	size_type                                 total_compressed_size
)
{
	UNUSED(element_sizes);
	// If you're wondering about the + - it's a better_enums quirk;
	// just pretend it isn't there
	if (element_value_byte_distribution.family() ==
		+util::random::distribution_family_t::single_value)
	{
		fill_n(compressed, total_compressed_size, element_value_byte_distribution.mean());
		return;
	}

	auto random_engine = this->random_engine;

	if (use_modular_identity_for_nonzero_bytes) {
		for(size_type i = 0; i < total_compressed_size; i++) {
			compressed[i] = i & 0xFF;
		}
	}
	else {
		util::random::fill_n(compressed.begin(), total_compressed_size, element_value_byte_distribution, random_engine);
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
void DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::generate_element_sizes_and_anchors(
	span<element_sizes_container_type>  packed_element_sizes,
	span<index_type>            position_anchors,
	size_type&                  total_compressed_size
)
{
	auto random_engine = this->random_engine;

	unsigned sizes_per_container = element_sizes_per_container();
	index_type anchor_index = 0;
	element_sizes_container_type sizes_container = 0;
	index_type sizes_pos = 0;
	index_type compressed_size_thus_far = 0;

	for(size_t pos = 0; pos < config.test_length; pos++) {

		if (pos == anchor_index * segment_length) {
			position_anchors[anchor_index++] = compressed_size_thus_far;
				// total_compressed_size is also the offset within the
				// compressed data array at which the element at position
				// pos is stored
		}

		element_size_type element_size =
			util::random::sample_from(generated_element_size_distribution, random_engine);

		element_size_type element_size_representation =
			element_size - represented_sizes.min;

		auto element_size_offset_in_container = bits_per_element_size() * (pos % sizes_per_container);
		sizes_container |=
			((static_cast<element_sizes_container_type>(element_size_representation)) << element_size_offset_in_container);
		if ((pos+1) % sizes_per_container == 0) {
			packed_element_sizes[sizes_pos++] = sizes_container;
			sizes_container = 0;
		}

		compressed_size_thus_far += element_size;
	}
	if (config.test_length % sizes_per_container != 0) {
		packed_element_sizes[sizes_pos] = sizes_container;
	}
	total_compressed_size = compressed_size_thus_far;

	if (config.be_verbose) {
		std::cout << "Generated " << config.test_length << " compressed elements, "
			<< " of total size " << total_compressed_size << " (average element size " <<
			total_compressed_size / (double)config.test_length << ", element sizes distributed "
			<< generated_element_size_distribution << "\n";
	}
	util::enforce(total_compressed_size <= UncompressedSize * config.test_length);
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
void DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::generate_expected_decompressed(
	span<uncompressed_type>                   decompressed,
	span<const unsigned char>                 compressed_data,
	span<const element_sizes_container_type>  packed_element_sizes,
	span<const index_type>                    position_anchors
)
{
	const unsigned char* compressed_data_iterator = compressed_data.begin() + position_anchors[0];
		// Note: we support the compressed data not actually starting where it's supposed to -
		// due to the semantics of the position anchoring mechanism; typically we
		// would expect position_anchors[0] to be 0
	auto num_bits_for_element_size = bits_per_element_size();
	auto sizes_per_container = element_sizes_per_container();

	for(size_type pos = 0; pos < config.test_length; pos++) {
		auto element_size_offset_in_container =
			(pos % sizes_per_container) * num_bits_for_element_size;
		auto element_size = util::bit_subsequence(
			packed_element_sizes [pos / sizes_per_container],
			element_size_offset_in_container,
			num_bits_for_element_size) + represented_sizes.min;
		Uncompressed padded_element = 0;
		// This assumes little-endianness
		std::copy_n(compressed_data_iterator, element_size, (char*) &padded_element);
		decompressed[pos] = padded_element;
		compressed_data_iterator += element_size;
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
static void compress(
	span<unsigned char>                      compressed_data,
	span<uint_t<ElementSizesContainerSize>>  packed_element_sizes,
	span<uint_t<IndexSize>>                  position_anchors,
	span<const uint_t<UncompressedSize>>     uncompressed,
	uint_t<IndexSize>                        segment_length,
	uint_t<IndexSize>&                       total_compressed_size,
	typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::size_range_type
	                                         represented_sizes,
	bool                                     fail_on_invalid_sizes = true)
{
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using element_size_type = typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::element_size_type;
	using uncompressed_type = uint_t<UncompressedSize>;
	using element_sizes_container_type = uint_t<ElementSizesContainerSize>;

	auto compressed_data_iterator = compressed_data.begin();
	auto packed_element_sizes_iterator = packed_element_sizes.begin();
	position_anchors[0] = 0;
	auto num_bits_per_element_size =
		DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::bits_per_element_size(
			represented_sizes.min, represented_sizes.max);
	auto sizes_per_container =
		DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::element_sizes_per_container(
			represented_sizes.min, represented_sizes.max);
	index_type offset_in_period = 0;
	index_type period_index = 0;
	unsigned element_index_within_size_container = 0;
	packed_element_sizes[0] = 0;

	for(size_type pos = 0;
	    pos < uncompressed.length();
	    pos++, offset_in_period++, element_index_within_size_container++)
	{
		if (offset_in_period == segment_length) {
			position_anchors[++period_index] = std::distance(compressed_data.begin(), compressed_data_iterator);
			offset_in_period = 0;
		}
		element_size_type element_size = util::num_bytes_to_fit<uncompressed_type>(uncompressed[pos]);
		if UNLIKELY(element_size > represented_sizes.max) {
			if (fail_on_invalid_sizes) {
				throw util::invalid_argument(util::concat_string(
					"Uncompressed data has elements requiring representation larger than the maximum "
					"represented size: uncompressed[", pos, "] = ", uncompressed[pos], " requires size ",
					element_size, " > ", represented_sizes.max ));
			}
			element_size = represented_sizes.min;
				// we're going to get this element wrong anyway, so let's at
				// least not waste too much space on it
		}
		element_size = std::max(element_size, represented_sizes.min);
		auto element_size_representation = element_size - represented_sizes.min;

		if (element_index_within_size_container == sizes_per_container) {
			*(++packed_element_sizes_iterator) = 0;
			element_index_within_size_container = 0;
		}
		auto element_size_offset_in_container =
			element_index_within_size_container * num_bits_per_element_size;

		*packed_element_sizes_iterator |=
			(((element_sizes_container_type) element_size_representation) << element_size_offset_in_container);

		// Notes:
		// - This assumes little-endianness!
		// - May be sped up by using a switch on the element size
		// - If some element has a size beyond the represented range,
		//   this is a "best effort" compression - keeping as many lower
		//   bytes as possible; and patching might be needed to take
		//   care of the rest
		std::memcpy(compressed_data_iterator, &uncompressed[pos], element_size);
		/*std::cout << "*compressed_data_iterator = "
			<< std::hex << *compressed_data_iterator << std::dec
			<< "while *(reinterpret_cast<char*>(&uncompressed[i]) = " // << *(reinterpret_cast<char*>(&uncompressed[i]))
			<< "\n"; */
		compressed_data_iterator += element_size;
	}
	total_compressed_size = std::distance(std::begin(compressed_data), compressed_data_iterator);
}

// Note: This result array is 0-based, not 1-based; that almost certainly
// means that counts[0] == 0
// TODO: Maybe use a vector, just to be on the safe side?
template <unsigned IndexSize, typename Container>
static std::vector<uint_t<IndexSize>> effective_size_counts(const Container& container)
{
	using size_type = size_type_by_index_size<IndexSize>;

	static_assert(
		std::is_unsigned<typename Container::value_type>::value,
		"Only unsigned integers protected for now");
		// ... which is too bad, actually :-(  we can certainly think of
		// having arbitrary-size data for DZBV

	std::vector<size_type> result(sizeof(typename Container::value_type) + 1);
		// this is initialized to 0!

	for(const auto& e : container) {
		result[util::min_size_to_represent(e)]++;
	}
	return result;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
static typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::element_size_type
compute_compressed_size(
	uint_t<IndexSize>              data_length,
	span<const uint_t<IndexSize>>  counts_by_element_size,
	typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::size_range_type
	                                     represented_size_range)
{
	constexpr const auto size_of_a_patch = UncompressedSize + IndexSize;
	auto num_represented_element_sizes = represented_size_range.min + 1 - represented_size_range.max;
	auto size_representation_bits_per_element = util::ceil_log2(num_represented_element_sizes);
	auto size_representations_per_sizes_container = ElementSizesContainerSize / size_representation_bits_per_element;
	auto num_size_representations = util::div_rounding_up(data_length, size_representations_per_sizes_container);
	auto total_size_of_size_representations = num_size_representations * ElementSizesContainerSize;

	size_t size_of_patches = 0;
	size_t size_of_compressed_bytes = 0;
	for(typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::element_size_type s = 0; s < counts_by_element_size.size(); s++) {
		auto elements_of_size_s_require_patches = (s <= represented_size_range.max);
		if (elements_of_size_s_require_patches) {
			size_of_patches  += counts_by_element_size[s] * size_of_a_patch;
			size_of_compressed_bytes += counts_by_element_size[s] * represented_size_range.min;
				// we would have liked to not list anything here, but we can't be sure
				// that the 0 value will be represented
		}
		else {
			// if s is smaller than the min represented size, we don't need to patch - we simply
			// have it represented with more zeros than is necessary
			auto size_actually_used_instead_of_s = std::max<decltype(s)>(s, represented_size_range.min);
			size_of_compressed_bytes += counts_by_element_size[s] * size_actually_used_instead_of_s;
		}
	}
	return size_of_patches + total_size_of_size_representations + size_of_compressed_bytes;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
static optional<typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::element_size_type>
determine_min_size_which_must_be_represented(
	span<const uint_t<UncompressedSize>>  uncompressed,
	span<const uint_t<IndexSize>>         counts)
{
	using element_size_type = typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::element_size_type;

	// Assume for size s bytes we have n_s elements in our buffer.
	// If element size s is not represented, either it's too small or too
	// large.
	//
	// If it's too small, it will be 'represented' by the minimum
	// represented size, i.e. the cost of not representing it is:
	//
	// (min element representation size - s)    * n_s    gratuitous zeros in the representation
	// >=
	// 1                                        * n_s    gratuitous zeros in the representation
	//
	// while the benefit is the possibility of sparing a bit.
	//
	//
	// If s is too large, we would need patches for it. That means
	//
	//   (IndexSize + UncompressedSize)  * n_s    for the patches
	// + (min element representation size)      * n_s    of junk data for these elements
	//
	// the second addend is non-negative, but its exact value is unknown to us;
	// but it's non-negative, so let's be conservative here and set it to 0.
	// If s _is_ represented we need to most
	//
	//   ceil(log2(UncompressedSize))  * n      for the bit length
	//   s                                 * n_s    for the representations
	//
	// for it in the unpatched compressed data. The benefit of representation
	// is therefore:
	//
	//   n_s * (IndexSize + UncompressedSize - s - ceil(log2(UncompressedSize)) * (n/n_s))
	//
	// with the sign determined by the right multiplicand.
	//
	// We're mostly going to brute-force determine the representation range based
	// on the above; but we can start by being a bit greedy with the largest values of s,
	// while we have a sequence of must-represent sizes at the top of the range:

	for(auto s = UncompressedSize; s > 0; s--) {
		// at this point we're certain that all sizes above s must be represented, i.e.
		// the maximum represented size is at most s, i.e. we can use the "s above
		// represented range" calculation.

		auto size_multiplicity = counts[s];
		auto inverse_frequency = uncompressed.size() / size_multiplicity;
		auto benefit_for_representing_size_s =
			(IndexSize + UncompressedSize - s - util::ceil_log2_constexpr(UncompressedSize) * inverse_frequency);
		if (benefit_for_representing_size_s <= 0) { return optional<element_size_type>(s+1); }
	}
	return nullopt;
}


template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
typename DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::size_range_type
DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::determine_optimal_size_range(
	span<const uncompressed_type> uncompressed)
{
	enum : element_size_type {
		bits_per_size_container = ElementSizesContainerSize * util::bits_per_byte,
	};

	// Need to set the minimum and maximum represented lengths

	auto counts = effective_size_counts<IndexSize, decltype(uncompressed)>(uncompressed);

	// We need to decide on a range of sizes to represent. We assume the data is
	// small enough for its size representation to always fit within one
	// element_sizes_container_type element.
	//
	// First we note that some sizes of elements may be worth representing regardless
	// of how many bits are used to represent the element size (that is, even
	// if the maximum number of bits is used, which is no more than
	// log_2(UncompressedSize). So let's determine those:



	auto min_size_which_must_be_represented =
		determine_min_size_which_must_be_represented<IndexSize, UncompressedSize, ElementSizesContainerSize>(uncompressed, counts);
	size_range_type present_in_uncompressed = {
		(element_size_type) std::distance(counts.begin(), util::find_first_unequal(counts, 0)),
		(element_size_type) std::distance(counts.begin(), util::find_last_unequal (counts, 0))
	};

	// Note: We are talking about...
	//
	// * uncompressed elements
	// * the (single) original size of uncompressed elements - UncompressedSize - in bytes
	// * the (variable) effective sizes of uncompressed elements, depending on how many leading
	//   zeros they have
	// * the range of effective sizes we want to represent
	// * the representation of that range, which is 0..range_size-1 (even though the actual sizes
	//   might be, say, 7..range_size+6 )
	// * the number of bits we use to encode the representation, which is ceil(log_2(range_size)
	// * the size and the number of bits in each element_sizes_container_type value
	//
	// Try to not get confused by all of these! :-(
	//

	// Ok, at this point we know we'll represent at least a certain range of sizes; now let's
	// scan the entire solution space, i.e. all size ranges which contain that core range

	auto min_size_bits_considered = min_size_which_must_be_represented ?
		util::ceil_log2(
			present_in_uncompressed.max -
			min_size_which_must_be_represented.value() + 1)
		: 0;
	auto max_size_bits_considered = util::ceil_log2(
		present_in_uncompressed.max - present_in_uncompressed.min + 1);

	struct representation_range_t {
		element_size_type first_size_;
		element_size_type num_bits_;
		element_size_type& first_size() { return first_size_; }
		const element_size_type& first_size() const { return first_size_; }
		element_size_type last_size() const { return first_size_ + (1 << num_bits_) - 1; }
		operator size_range_type() const { return { first_size(), last_size() }; }
		static representation_range_t first_valid_range(
			element_size_type  fixed_num_bits,
			size_range_type    present_in_uncompressed,
			optional<element_size_type>  min_size_which_must_be_represented)
		{
			if (not has_value(min_size_which_must_be_represented)) {
				return { 0, fixed_num_bits };
			}
			element_size_type must_represent = min_size_which_must_be_represented.value();
			element_size_type length = 1 << fixed_num_bits;
			if (must_represent  > present_in_uncompressed.min and
				must_represent - present_in_uncompressed.min < length)
			{
				return { present_in_uncompressed.min, fixed_num_bits };
			}
			if (must_represent  <= length) { return { 0, fixed_num_bits }; }
			return { must_represent - length, fixed_num_bits };
		}
	};

	struct representation_choice_t {
		representation_range_t  representation_range;
		size_type               overall_size_of_compressed_data;
	};
	optional<representation_choice_t> representation_choice { };

	for (auto candidate_num_bits = min_size_bits_considered;
		 candidate_num_bits <= max_size_bits_considered;
		 candidate_num_bits++)
	{
		auto candidate_range = representation_range_t::first_valid_range(
			candidate_num_bits, present_in_uncompressed, min_size_which_must_be_represented);

		do {
			auto overall_compressed_size =
				compute_compressed_size<IndexSize, UncompressedSize, ElementSizesContainerSize>(
					uncompressed.size(), counts, candidate_range);

			if (not representation_choice or
				representation_choice.value().overall_size_of_compressed_data > overall_compressed_size)
			{
				representation_choice = representation_choice_t { candidate_range, overall_compressed_size };
			}
			candidate_range.first_size()++;
		} while (candidate_range.last_size() <= present_in_uncompressed.max);
	}

	if (config.be_verbose) {
		std::cout
			<< "Have determined that the best compression will be achieved by representing "
			<< "element sizes " <<  representation_choice.value().representation_range.first_size() << ".."
			<< representation_choice.value().representation_range.last_size() << " (using "
			<< representation_choice.value().representation_range.num_bits_ << " bits for the element sizes, i.e. "
			<< element_sizes_per_container(
				representation_choice.value().representation_range.first_size(),
				representation_choice.value().representation_range.last_size())
			<< " element sizes per size container (whose size is " << ElementSizesContainerSize << " bytes)\n";
	}

	return representation_choice.value().representation_range;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
KernelTestAdapter::BufferSizes
DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::generateInputs(const HostBuffers& buffers)
{
	index_type total_compressed_size;
	// TODO: Support having the expected output generated rather than the inputs
	if (need_to_generate("decompressed")) {
		if(need_to_generate("compressed_data")
		   and need_to_generate("packed_element_sizes")
		   and need_to_generate("position_anchors"))
		{
			return { };
		}
		if (not need_to_generate("compressed_data")
		   or not need_to_generate("packed_element_sizes")
		   or not need_to_generate("position_anchors"))
		{
			throw util::invalid_argument("Invalid combination of buffers to generate");
		}

		auto element_sizes     = buffers.at("packed_element_sizes"   ).as_span< element_sizes_container_type >();
		auto compressed_data   = buffers.at("compressed_data"        ).as_span< unsigned char  >();
		auto position_anchors  = buffers.at("position_anchors"       ).as_span< index_type           >();

		// The order of calls here is significant
		generate_element_sizes_and_anchors(element_sizes, position_anchors, total_compressed_size);
		generate_compressed_data(compressed_data, element_sizes, total_compressed_size);
	}
	else {
		// We'll be compressing data.

		if (not need_to_generate("compressed_data")
		   or not need_to_generate("packed_element_sizes")
		   or not need_to_generate("position_anchors"))
		{
			throw util::invalid_argument("Invalid combination of buffers to generate");
		}

		util::enforce(not logical_xor(have_size_range_overrides.for_min,have_size_range_overrides.for_max),
			"Calculation of just-the-minimum or just-the-maximum represented size not supported; you must "
			"either allow both to be calculated or specify them both explicitly");

		auto element_sizes     = buffers.at("packed_element_sizes"   ).as_span< element_sizes_container_type >();
		auto compressed_data   = buffers.at("compressed_data"        ).as_span< unsigned char                >();
		auto position_anchors  = buffers.at("position_anchors"       ).as_span< index_type                   >();
		auto uncompressed      = buffers.at("decompressed"           ).as_span< const uncompressed_type      >();

		bool need_to_determine_optimal_size_range =
			(not have_size_range_overrides.for_min and not have_size_range_overrides.for_max);
		if (need_to_determine_optimal_size_range) {
			represented_sizes = determine_optimal_size_range(uncompressed);
		}

		compress<IndexSize, UncompressedSize, ElementSizesContainerSize>(
			compressed_data, element_sizes, position_anchors, uncompressed,
			segment_length, total_compressed_size,
			represented_sizes, fail_on_invalid_uncompressed_data);
	}
	return { std::make_pair("compressed_data", total_compressed_size) };
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
KernelTestAdapter::BufferSizes DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto element_sizes     = buffers.at("packed_element_sizes"   ).as_span<element_sizes_container_type >();
		auto compressed_data   = buffers.at("compressed_data"        ).as_span<unsigned char                >();
		auto position_anchors  = buffers.at("position_anchors"       ).as_span<index_type                   >();

		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		generate_expected_decompressed(decompressed, compressed_data, element_sizes, position_anchors);
	}
	return NoBuffersToResize;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
void DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto decompressed          = buffers.at("decompressed"        ).as_span<uncompressed_type                  >().data();
	auto packed_element_sizes  = buffers.at("packed_element_sizes").as_span<const element_sizes_container_type >().data();
	auto compressed_data       = buffers.at("compressed_data"     ).as_span<const unsigned char                >().data();
	auto position_anchors      = buffers.at("position_anchors"    ).as_span<const index_type                   >().data();
	auto bits_per_element_size = this->bits_per_element_size();
	size_type length = config.test_length;
	auto min_represented_element_size = represented_sizes.min;

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::decompression::discard_zero_bytes::variable_width::kernel_t
	<IndexSize, UncompressedSize, ElementSizesContainerSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(
			decompressed, compressed_data, packed_element_sizes, position_anchors,
			segment_length, length, min_represented_element_size,
			bits_per_element_size)
	);
}

static_block {
	//                         IndexSize  UncompressedSize   ElementSizesContainerSize
	//-----------------------------------------------------------------------------------------
	DiscardZeroBytesVariable < 4,         2,                 4 >::registerInSubclassFactory();
	DiscardZeroBytesVariable < 4,         4,                 4 >::registerInSubclassFactory();
	DiscardZeroBytesVariable < 4,         8,                 4 >::registerInSubclassFactory();

	DiscardZeroBytesVariable < 8,         2,                 4 >::registerInSubclassFactory();
	DiscardZeroBytesVariable < 8,         4,                 4 >::registerInSubclassFactory();
	DiscardZeroBytesVariable < 8,         8,                 4 >::registerInSubclassFactory();

	DiscardZeroBytesVariable < 4,         2,                 8 >::registerInSubclassFactory();
	DiscardZeroBytesVariable < 4,         4,                 8 >::registerInSubclassFactory();
	DiscardZeroBytesVariable < 4,         8,                 8 >::registerInSubclassFactory();

}

} // namespace decompression
} // namespace kernel_tests
