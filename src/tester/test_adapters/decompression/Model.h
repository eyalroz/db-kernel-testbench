#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_FRAME_OF_REFERENCE_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_FRAME_OF_REFERENCE_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

/**
 * @note It's the unpatched, exceptions-not-allowed compression scheme; see also the
 * @ref PatchedFrameOfReference test adapter
 */
template<unsigned IndexSize, typename Uncompressed, typename UnaryModelFunction>
class Model:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<Model<IndexSize, Uncompressed, UnaryModelFunction>>
{
public:
	using index_type                 = uint_t<IndexSize>;
	using size_type                  = size_type_by_index_size<IndexSize>;
	using uncompressed_type          = Uncompressed;
	using model_coefficient_type     = typename UnaryModelFunction::coefficient_type;
	using model_coefficients_type    = typename UnaryModelFunction::coefficients_type;
	enum { model_dimension = UnaryModelFunction::model_dimension };
	using model_dimensions_size_type = typename UnaryModelFunction::model_dimensions_size_type;

	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Model)

protected:
	void determine_model_coefficients(
		span<const uncompressed_type>   data);
	void generate_model_function_parameters(
		span<model_coefficients_type>   buffer_to_fill);
	void generate_expected_decompressed(
		span<uncompressed_type>         decompressed);

protected:
	double      interval_model_coefficient_random_contribution  {   0.9 };
	double      interval_model_coefficient_model_contribution   {   0.1 };
	distribution_t<double> model_coefficient_random_part {
		std::uniform_real_distribution<double>(-100, 100)
	};
	char        coefficient_delimiter                           {   ',' };

	model_coefficients_type
	            model_function_coefficients                     {       };
	bool        have_model_function_coefficients                { false };

};

} // namespace decompression
} // namespace kernel_tests


#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_FRAME_OF_REFERENCE_H_ */
