#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_DICTIONARY_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_DICTIONARY_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

/**
 * @note It's the unpatched, exceptions-not-allowed compression scheme; see also the
 * @ref PatchedDictionary test adapter
 */
template <unsigned IndexSize, unsigned UncompressedSize, unsigned DictionaryIndexSize>
class Dictionary:
	public KernelTestAdapter,
	public ::kernel_tests::mixins::TestAdapterNameBuilder<Dictionary<IndexSize, UncompressedSize, DictionaryIndexSize>>
{
public: // types
	using index_type            = uint_t<IndexSize>;
	using size_type             = size_type_by_index_size<IndexSize>;
	using dictionary_index_type = uint_t<DictionaryIndexSize>;
	using uncompressed_type     = uint_t<UncompressedSize>;
	using dictionary_size_type  = size_type_by_index_size<DictionaryIndexSize>;

protected:
	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Dictionary)

protected:
	void generate_dictionary(
		span<uncompressed_type>           dictionary);

   // Called when we have both an apriori uncompressed buffer and an apriori dictionary
   void compress_with_dictionary(
	   span<dictionary_index_type>        compressed,
	   span<const uncompressed_type>      uncompressed,
	   span<const uncompressed_type>      dictionary) const;

   // Called when we have no apriori buffers
	void generate_compressed_input(
	   span<dictionary_index_type>        compressed_input);

   // Called when we have an apriori uncompressed buffer, but no apriori dictionary
   void build_dictionary_and_compress(
	   span<dictionary_index_type>        compressed,
	   span<uncompressed_type>            dictionary,
	   span<const uncompressed_type>      uncompressed,
	   dictionary_size_type&              effective_dictionary_length,
	   dictionary_size_type&              num_uncovered_elements,
	   dictionary_size_type&              num_distinct_uncovered_values) const;

   // Called when we have no apriori buffers
	void generate_expected_decompressed(
	   span<uncompressed_type>            decompressed,
	   span<const uncompressed_type>      dictionary,
	   span<const dictionary_index_type>  compressed_input);


public:
	// If we need to compress data which is not found in the dictionary,
	// this fixed value will be used as a fallback; it may or may not als
	// be a valid dictionary index corresponding to an actual uncompressed
	// value
	//
	// Note: The code relies on this value being 0; if you wish to make it
	// variable for some reason, some (light) refactoring would be necessary
	enum : dictionary_index_type { fallback_value = 0 };

public: // data members
	// This should really be protected and let the patched adapters be friends, but - expediency

	dictionary_size_type dictionary_capacity { 180   };
		// The default value here will be ignored if decompressed values are read
		// in rather than generated; but we will respect an override. Note that this
		// is distinct from distinct_values_to_use, since one can have a larger
		// dictionary but not use it
		//
		// TODO: This and the next parameter are together kind of confusing; need
		// either a better explanation or some way to only use one of them

	dictionary_size_type effective_dictionary_length { dictionary_capacity };
		// If we're not compressing data, this will always be equal to dictionary_capacity;
		// otherwise it might be lower - a result of our having detected fewer
		// values in the input

	dictionary_size_type distinct_values_to_generate { 150   };
		// When generating both compressed and uncompressed data, we'll simply
		// restrict ourselves to this many distinct values. When compressing
		// data we've obtained from elsewhere, we might need to assign one of
		// these as a fallback values for all those elements whose values we
		// can't fit inside the dictionary

	optional<bool>   force_shared_mem_caching                           {};

	distribution_t<uncompressed_type> dictionary_value_random_part =
		distribution_t<uncompressed_type>::uniform(0, 99);
	double           dictionary_value_random_contribution_factor        { 1.    };
	double           dictionary_value_linear_contribution_factor        { 0.    };
	uncompressed_type dictionary_value_linear_scale                     { 10    };
	uncompressed_type dictionary_value_linear_offset                    { 1     };

	// All values here will be modulated by the number of entries

	bool             compressed_input_uniformly_random                  { true  };
	bool             use_all_ones_as_compressed_data                    { false };
	distribution_t<dictionary_index_type> compressed_input_random_part =
		distribution_t<dictionary_index_type>::uniform(0,9);
	double           compressed_input_random_contribution_factor        { 0.9   };
	double           compressed_input_linear_contribution_factor        { 0.1   };
	dictionary_index_type  compressed_input_linear_scale                { 10    };
	dictionary_index_type  compressed_input_linear_offset               { 20    };

   // Compression

	bool&            fail_on_insufficient_dictionary_capacity = fail_on_invalid_uncompressed_data;
	bool             reserve_fallback_for_missing_data_only             { false };
	bool             generate_dictionary_by_decreasing_frequency        { true  };
	bool             can_assume_dictionary_is_large_enough              { false };

};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_DICTIONARY_H_ */
