#include "RunLengthEncoding.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"

#include <iomanip>
#include <iostream>

#include "kernel_wrappers/decompression/run_length_encoding.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
std::string RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::buildPrettyKernelName() const
{
	return util::concat_string(
		"Decompression kernel for the run-length encoding compression scheme (RLE), "
		"with data of size ", UncompressedSize, " and run lengths of size ",
		RunLengthSize, ", run indices of size ", RunIndexSize, " and uncompressed "
		"data index size ", UncompressedIndexSize, "."
	);
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
KernelTestAdapter::BufferDescriptors RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::getBufferDescriptors() const
{
	// Note: The number of runs in practice will probably be (much) lower than the capacity, which
	// for purposes of testing must be the maximum possible

	auto num_segments = util::div_rounding_up(config.test_length, segment_length);

	return {
		{ "decompressed",     BufferDescriptor::make<uncompressed_type> (config.test_length, BufferDirection::Output )},
		{ "run_data",         BufferDescriptor::make<uncompressed_type> (config.test_length, BufferDirection::Input  )},
		{ "run_lengths",      BufferDescriptor::make<run_length_type>   (config.test_length, BufferDirection::Input  )},
		{ "position_anchors", BufferDescriptor::make<run_index_type>    (num_segments,       BufferDirection::Input  )},
		{ "intra_run_anchor_offsets",
			                  BufferDescriptor::make<run_length_type>   (num_segments,       BufferDirection::Input  )},
	};
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
inline RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::RunLengthEncoding(const TestConfiguration& config_)
	: KernelTestAdapter(config_), num_element_runs()
{
	using std::make_pair;

	resolveOption(segment_length,
		ConfigurationKeySet {
			"segment-length", "position-anchoring-period", "anchoring-period",
			"elements-per-anchor", "position-anchoring-interval",
			"segment-length", "anchored-segment-length" }
	);

	resolveOption(random_run_length,
		ConfigurationKeySet {
			"run-length-distribution", "run-length", "random-run-length"}
	);

	resolveOption(
		random_datum,
		ConfigurationKeySet {
			"run-data-distribution", "run-datum-distribution", "run-data", "run-datum"}
	);

	resolveOption(
		run_data_random_contribution,
		ConfigurationKeySet {
			"run-data-value-random-contribution", "run-data-random-contribution",}
	);
	resolveOption(
		run_data_linear_scale,
		ConfigurationKeySet {
			"run-data-value-linear-scale", "run-data-linear-scale"}
	);
	resolveOption(
		run_data_linear_offset,
		ConfigurationKeySet {
			"run-data-value-linear-offset", "run-data-linear-offset"}
	);
	resolveOption(
		run_data_linear_contribution,
		ConfigurationKeySet {
			"run-data-value-linear-contribution",
			"run-data-linear-contribution"}
	);

	resolveOption(
		run_data_random_contribution,
		ConfigurationKeySet {
			"run-data-random-contribution", "data-value-random-contribution",
			"compressed-random-contribution", "data-random-contribution"}
	);
	resolveOption(
		run_data_linear_scale,
		ConfigurationKeySet {
			"run-data-linear-scale", "data-value-linear-scale",
			"compressed-linear-scale", "data-linear-scale"}
	);
	resolveOption(
		run_data_linear_offset,
		ConfigurationKeySet {
			"run-data-linear-offset", "data-value-linear-offset",
			"compressed-linear-offset", "data-linear-offset"}
	);
	resolveOption(
		run_data_linear_contribution,
		ConfigurationKeySet {
			"run-data-linear-contribution", "data-value-linear-contribution",
			"compressed-linear-contribution", "data-linear-contribution"}
	);

	resolveOption(use_identity_for_run_data,
		ConfigurationKeySet {
			"use-identity-for-run-data","use-identity-for-data",
			"identity-for-data", "identity-data", "identity-run-data" }
	);

	run_data_uniformly_random = resolveOption(
		ConfigurationKeySet {
			"run-data-uniformly-random", "data-uniformly-random",
			"compressed-uniformly-random"}, false);

	if (run_data_uniformly_random) {
		run_data_linear_contribution = 0;
		run_data_linear_offset       = 0;
		run_data_linear_scale        = 0;
		run_data_random_contribution = 1;
		random_datum                 = distribution_t<uncompressed_type>::full_range_uniform();
	}

	util::enforce(!(use_identity_for_run_data && run_data_uniformly_random),
		"Cannot force compressed input to be both identity and uniformly-random");

	maybe_print_extra_configuration(
		make_pair("Segment length (= position anchoring period)",
			                                                        segment_length),
		make_pair("Run length distribution",                        random_run_length                 ),
		make_pair("Number of runs (length-value pairs)",            num_element_runs                  ),
		make_pair("Set run_data[i] = i",                            use_identity_for_run_data         ),
		make_pair("Generate uniformly random run data",             run_data_uniformly_random         ),
		make_pair("run data generation - random part distribution", random_datum                      ),
		make_pair("run data generation - random part contribution",
			                                                        run_data_random_contribution      ),
		make_pair("run data generation - linear part contribution",
			                                                        run_data_linear_contribution      ),
		make_pair("run data generation - linear scale",             run_data_linear_scale             ),
		make_pair("run data generation - linear offset",            run_data_linear_offset            )
	);

	return;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
std::string RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::getDescription() const
{
	return
		"Decompression kernel for the Run-Length Encoding (RLE) compression"
		"scheme: Assuming uncompressed data is characterized by many 'runs' "
		"of identical elements, the original data is replaced by a sequence "
		"of such 'runs'. Each run consists of an element of the original "
		"uncompressed type, and a run length (constrained at least by its "
		"own type. These are stored in SoA rather than AoS form (structure "
		"of arrays form) - that is, the kernel receives an array of run lengths "
		"and an array of run data elements, both with lengths equal to the "
		"overall number of runs. The sum of all run lengths is the length of the "
		"original input array/column.";
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
LaunchConfigurationSequence RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		{ "length",              config.test_length      },
		{ "segment_length",      (size_t) segment_length },
	};
	::cuda::kernels::decompression::run_length_encoding	::kernel_t<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("decompression::run_length_encoding::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
void RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::generate_run_data(
	span<uncompressed_type> run_data)
{
	if (use_identity_for_run_data) {
		iota(run_data, 0);
		return;
	}

	auto random_engine = this->random_engine;
	for(run_index_size_type i = 0; i < num_element_runs; i++) {
		uncompressed_type linear_component =
			run_data_linear_scale * i + run_data_linear_offset;
		uncompressed_type random_component =
			util::random::sample_from(random_datum, random_engine);
		run_data[i] =
			linear_component * run_data_linear_contribution +
			random_component * run_data_random_contribution;
	}
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
void RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::generate_run_lengths_and_anchors(
	span<run_length_type>           run_lengths,
	span<run_index_type>             position_anchors,
	span<run_length_type>           intra_run_anchor_offsets
)
{
	auto random_engine = this->random_engine;

	uncompressed_index_size_type input_pos = 0;
	index_type anchor_index = 0;
	run_index_size_type run_index = 0;
	run_length_type shortest_run_length = random_run_length.max();
	run_length_type longest_run_length = random_run_length.min();
	while(input_pos < config.test_length) {
		run_length_type run_length = util::random::sample_from(random_run_length, random_engine);
		run_length = std::max(run_length, (run_length_type) 1);
		if (input_pos + run_length > config.test_length) {
			run_length = std::min(run_length, (run_length_type) (config.test_length - input_pos));
		}
		run_lengths[run_index] = run_length;
		util::update_min(shortest_run_length, run_length);
		util::update_max(longest_run_length, run_length);
		index_type new_input_pos = input_pos + run_length;
		if (new_input_pos > anchor_index * segment_length) {
			position_anchors[anchor_index] = run_index;
			intra_run_anchor_offsets[anchor_index] =
				anchor_index * segment_length - input_pos;
			anchor_index++;
		}
		input_pos = new_input_pos;
		run_index++;
	}
	num_element_runs = run_index;

	if (config.be_verbose) {
		std::cout << "Generated input data consists of " << num_element_runs <<
			" runs, with lengths ranging between " <<
			util::promote_for_streaming(shortest_run_length) <<
			" and " << util::promote_for_streaming(longest_run_length) <<
			", and averaging " <<
			config.test_length / (double) num_element_runs <<
			" elements per run.\n";
	}
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
void RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::generate_expected_decompressed(
	span<uncompressed_type>        decompressed,
	span<const uncompressed_type>  run_data,
	span<const run_length_type>    run_lengths,
	span<const run_index_type>     position_anchors,
	span<const run_length_type>    intra_run_anchor_offset
) const
{
	UNUSED(position_anchors);
	UNUSED(intra_run_anchor_offset);
	auto out_iterator = decompressed.begin();
	for(run_index_size_type run_index = 0; run_index < num_element_runs; run_index++) {
		std::fill_n(out_iterator, run_lengths[run_index], run_data[run_index]);
		out_iterator += run_lengths[run_index];
	}
}

template <unsigned UncompressedIndexSize, typename Uncompressed, typename RunLength, unsigned RunIndexSize>
static void compress(
	span<Uncompressed>                     run_data,
	span<RunLength>                        run_lengths,
	span<uint_t<UncompressedIndexSize> >   position_anchors,
	span<RunLength>                        intra_run_anchor_offset,
	span<const Uncompressed>               decompressed,
	uint_t<UncompressedIndexSize>          segment_length,
	uint_t<UncompressedIndexSize>&         max_unclipped_run_length,
	uint_t<UncompressedIndexSize>&         number_of_runs)
{
	using index_type = uint_t<UncompressedIndexSize>;
	using uncompressed_index_size_type = size_type_by_index_size<UncompressedIndexSize>;
	using run_index_size_type =  size_type_by_index_size<RunIndexSize>;
	util::enforce(decompressed.size() > 0);
	auto max_representable_run_length = std::numeric_limits<RunLength>::max();
	run_index_size_type run_index = 0;
	index_type current_segment_index = 0;
	index_type current_run_start_pos = 0;
	index_type last_anchor_pos = 0;
	position_anchors[0] = 0;
	intra_run_anchor_offset[0] = 0;
	max_unclipped_run_length = 0;
	// The runs actually represented in the compressed data are clipped
	// at max_representable_run_length; but we want to report back what
	// was the actual longest run
	index_type current_unclipped_run_start_pos = 0;
	Uncompressed current_run_value = decompressed[current_run_start_pos];
		// This is a bit dodgy in case uncompressed_type is very large
	auto mark_run = [&](uncompressed_index_size_type end_pos) {
		run_lengths[run_index] = end_pos - current_run_start_pos;
			// and we know, at this point, that the difference _will_ fit
			// in a run_length_type value
		run_data[run_index] = current_run_value;
		run_index++;
		current_run_start_pos = end_pos;
	};
	auto mark_unclipped_run = [&](uncompressed_index_size_type end_pos) {
		util::update_max(max_unclipped_run_length,end_pos - current_unclipped_run_start_pos);
		current_unclipped_run_start_pos = end_pos;
	};

	// Note that while runs are marked right after they _end_, segments
	// are marked as they _begin_
	auto mark_new_anchored_segment = [&](index_type pos) {
		position_anchors[++current_segment_index] = run_index;
		intra_run_anchor_offset[current_segment_index] = pos - current_run_start_pos;
		last_anchor_pos = pos;
	};

	for(uncompressed_index_size_type pos = 1; pos < decompressed.size(); pos++) {
		if (pos - last_anchor_pos == segment_length) {
			mark_new_anchored_segment(pos);
		}
		if (decompressed[pos] == current_run_value) {
			if (pos - current_run_start_pos == max_representable_run_length) {
				// If we have runs too long to be captured
				mark_run(pos);
			}
			if (pos - last_anchor_pos == segment_length) {
				mark_new_anchored_segment(pos);
			}
			continue;
		}
		mark_run(pos);
		mark_unclipped_run(pos);
		current_run_value = decompressed[pos];
	}
	mark_run(decompressed.size());
	mark_unclipped_run(decompressed.size());
	number_of_runs = run_index;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
KernelTestAdapter::BufferSizes RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("run_data") and
		need_to_generate("run_lengths") and
		need_to_generate("position_anchors") and
		need_to_generate("intra_run_anchor_offsets"))
	{
		auto run_data                  = buffers.at("run_data"                ).as_span< uncompressed_type       >();
		auto run_lengths               = buffers.at("run_lengths"             ).as_span< run_length_type         >();
		auto position_anchors          = buffers.at("position_anchors"        ).as_span< run_index_type          >();
		auto intra_run_anchor_offsets  = buffers.at("intra_run_anchor_offsets").as_span< run_length_type         >();
		if (not need_to_generate("decompressed")) {
			auto decompressed          = buffers.at("decompressed"            ).as_span< const uncompressed_type >();
			uncompressed_index_size_type max_unclipped_run_length = 0;
			compress<UncompressedIndexSize, uncompressed_type, run_length_type, RunIndexSize>(
				run_data, run_lengths, position_anchors, intra_run_anchor_offsets,
				decompressed, segment_length, max_unclipped_run_length, num_element_runs);
		}
		else {
			// The order of calls here is significant
			generate_run_lengths_and_anchors(run_lengths, position_anchors, intra_run_anchor_offsets);
			generate_run_data(run_data);
		}
		return {
			std::make_pair("run_lengths", num_element_runs * RunLengthSize),
			std::make_pair("run_data",    num_element_runs * UncompressedSize),
		};
	}
	if (need_to_generate("run_data") or
		need_to_generate("run_lengths") or
		need_to_generate("position_anchors") or
		need_to_generate("intra_run_anchor_offsets")) {
		throw util::invalid_argument("Can either generate all input buffers or none of them");
	}
	if (not need_to_generate("decompressed_data")) {
		throw util::invalid_argument("Invalid combinations of buffers to generate.");
	}
	return NoBuffersToResize;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
KernelTestAdapter::BufferSizes RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto run_lengths               = buffers.at("run_lengths"             ).as_span< run_length_type   >();
		auto run_data                  = buffers.at("run_data"                ).as_span< uncompressed_type >();
		auto position_anchors          = buffers.at("position_anchors"        ).as_span< run_index_type    >();
		auto intra_run_anchor_offsets  = buffers.at("intra_run_anchor_offsets").as_span< run_length_type   >();

		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		generate_expected_decompressed(
			decompressed, run_data, run_lengths, position_anchors, intra_run_anchor_offsets);
	}
	return NoBuffersToResize;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize>
void RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto decompressed              = buffers.at("decompressed"            ).as_span< uncompressed_type       >().data();
	auto run_lengths               = buffers.at("run_lengths"             ).as_span< const run_length_type   >().data();
	auto run_data                  = buffers.at("run_data"                ).as_span< const uncompressed_type >().data();
	auto position_anchors          = buffers.at("position_anchors"        ).as_span< const run_index_type    >().data();
	auto intra_run_anchor_offsets  = buffers.at("intra_run_anchor_offsets").as_span< const run_length_type   >().data();
	size_type num_segments = util::div_rounding_up(config.test_length, segment_length);
	size_type length = config.test_length; // that's the _uncompressed_ length

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::decompression::run_length_encoding
		::kernel_t<UncompressedIndexSize, UncompressedSize, RunLengthSize, RunIndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(
			decompressed, run_data, run_lengths, position_anchors, intra_run_anchor_offsets,
			segment_length, num_segments, num_element_runs, length)
	);
}

static_block {
	// TODO: Only use sizes, not types, instead of Datum

	//                  UncompressedIndexSize   UncompressedSize  RunLengthSize
	//----------------------------------------------------------------------
	RunLengthEncoding < 4,                      1,                1 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      2,                1 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      4,                1 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      8,                1 >::registerInSubclassFactory();

	RunLengthEncoding < 4,                      1,                2 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      2,                2 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      4,                2 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      8,                2 >::registerInSubclassFactory();

	RunLengthEncoding < 4,                      1,                4 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      2,                4 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      4,                4 >::registerInSubclassFactory();
	RunLengthEncoding < 4,                      8,                4 >::registerInSubclassFactory();

	RunLengthEncoding < 8,                      4,                1 >::registerInSubclassFactory();
	RunLengthEncoding < 8,                      4,                2 >::registerInSubclassFactory();
	RunLengthEncoding < 8,                      4,                4 >::registerInSubclassFactory();

	RunLengthEncoding < 8,                      1,                8 >::registerInSubclassFactory();
	RunLengthEncoding < 8,                      4,                8 >::registerInSubclassFactory();
	RunLengthEncoding < 8,                      8,                8 >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
