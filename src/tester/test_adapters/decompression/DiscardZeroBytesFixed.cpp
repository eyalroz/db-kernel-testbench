
#include "DiscardZeroBytesFixed.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>


#include "util/type_name.hpp"
#include "util/boolean.hpp"
#include "util/endianness.h"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/poor_mans_reflection.h"

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "kernel_wrappers/decompression/discard_zero_bytes_fixed.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
std::string DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss <<
		"A DiscardZeroBytesFixed decompression kernel, with elements of size " << UncompressedSize
		<< " padded by zeros into elements of size" << CompressedSize << ".";
	return oss.str();
}


template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
std::string DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::getDescription() const
{
	return
		"Decompression kernel for the 'discard zero bytes' or 'null suppression' compression"
		"schemes (DZBF / NS): With this scheme, it is assumed the compressed data actually "
		"fits into the lower-significance bytes of the uncompressed elements, hence one can "
		"discard the higher bytes and store only the lower ones. Decompression is simply "
		"those high bytes with zeros. Note endianness is an issue here.";
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
KernelTestAdapter::BufferDescriptors DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::getBufferDescriptors() const
{
	return {
		{ "decompressed",     BufferDescriptor::make<uncompressed_type>(config.test_length,     BufferDirection::Output) },
		{ "compressed_input", BufferDescriptor::make<compressed_type>  (config.test_length,     BufferDirection::Input)  },
	};
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
inline DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::DiscardZeroBytesFixed(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;

	resolveOption(
		compressed_input_random_part,
		ConfigurationKeySet {
			"compressed-input", "compressed-input-distribution",
			"compressed-distribution", "compressed-data-distribution"}
	);
	resolveOption(
		compressed_input_random_contribution,
		ConfigurationKeySet {
			"compressed-input-random-contribution", "data-value-random-contribution",
			"compressed-random-contribution", "data-random-contribution",
			"random-contribution"}
	);
	resolveOption(
		compressed_input_linear_scale,
		ConfigurationKeySet {
			"compressed-input-linear-scale", "data-value-linear-scale",
			"compressed-linear-scale", "data-linear-scale", "linear-scale"}
	);
	resolveOption(
		compressed_input_linear_offset,
		ConfigurationKeySet {
			"compressed-input-linear-offset", "data-value-linear-offset",
			"compressed-linear-offset", "data-linear-offset", "linear-offset"}
	);
	resolveOption(
		compressed_input_linear_contribution,
		ConfigurationKeySet {
			"compressed-input-linear-contribution", "data-value-linear-contribution",
			"compressed-linear-contribution", "data-linear-contribution",
			"linear-contribution"}
	);

	resolveOption(
		fail_on_invalid_uncompressed_data,
		ConfigurationKeySet {
			"fail-on-invalid-uncompressed-data", "fail-on-invalid-data",
			"only-compress-valid-data" }
	);

	resolveOption(use_all_ones_as_compressed_data,
		ConfigurationKeySet {
			"use-all-ones-as-input", "data-all-ones", "all-ones-data",
			"all-ones", "all-ones-input"});

	resolveOption(compressed_input_uniformly_random,
		ConfigurationKeySet {
			"compressed-input-uniformly-random", "data-uniformly-random",
			"compressed-uniformly-random"});

	resolveOption(compressed_input_modular_iota,
		ConfigurationKeySet {
			"compressed-input-modular-iota", "data-modular-iota",
			"compressed-modular-iota", "compressed-input-iota", "data-iota",
			"compressed-iota", "iota", "use-iota", "modular-iota", "use-modular-iota"
	});


	if (compressed_input_uniformly_random) {
		compressed_input_linear_contribution = 0;
		compressed_input_linear_offset = 0;
		compressed_input_linear_scale = 0;
		compressed_input_random_contribution = 1;
		compressed_input_random_part = distribution_t<compressed_type>::full_range_uniform();
			// What about the actual maximum value?
	}

	if (compressed_input_modular_iota) {
		compressed_input_linear_contribution = 1;
		compressed_input_linear_offset = 0;
		compressed_input_linear_scale = 1;
		compressed_input_random_contribution = 0;
	}

	maybe_print_extra_configuration(
		// ... something re coefficients
		make_pair("Set all compressed data to 1",                    use_all_ones_as_compressed_data   ),
		make_pair("Generate uniformly random compressed data",       compressed_input_uniformly_random ),
		make_pair("Set  compressed data element i to i (modularly)", compressed_input_modular_iota     ),

		make_pair("Compressed input generation - random factor distribution", compressed_input_random_part ),
		make_pair("Compressed input generation - random factor contribution",
			compressed_input_random_contribution                                                       ),
		make_pair("Compressed input generation - linear factor contribution",
			compressed_input_linear_contribution                                                       ),
		make_pair("Compressed input generation - linear scale",      compressed_input_linear_scale     ),
		make_pair("Compressed input generation - linear offset",     compressed_input_linear_offset    )
	);

	util::enforce(
		util::at_most_one_of(
			compressed_input_uniformly_random,
			compressed_input_modular_iota,
			use_all_ones_as_compressed_data));

	return;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
LaunchConfigurationSequence DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		// not specifying the serialization factor
		{ "length", config.test_length }
	};
	::cuda::kernels::decompression::discard_zero_bytes::fixed_width::kernel_t<IndexSize, UncompressedSize, CompressedSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("decompression::discard_zero_bytes::fixed_width::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
void DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::generate_compressed_input(
	span<compressed_type> compressed)
{
	if (use_all_ones_as_compressed_data) {
		fill(compressed, 1);
		return;
	}

	auto random_engine = this->random_engine;

	for(index_type i = 0; i < config.test_length; i++) {
		auto linear_component =
			compressed_input_linear_scale * i + compressed_input_linear_offset;
		auto random_component =
			util::random::sample_from(compressed_input_random_part, random_engine);
		compressed[i] =
			linear_component * compressed_input_linear_contribution +
			random_component * compressed_input_random_contribution;
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
void DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::generate_expected_decompressed(
	span<uncompressed_type>      decompressed,
	span<const compressed_type>  compressed)
{
	util::enforce(decompressed.size() == config.test_length);
	util::enforce(compressed.size() == config.test_length);
	for(index_type i = 0; i < decompressed.size(); i++) {
		decompressed[i] = 0;
		// TODO: Perhaps don't assume little-endianness here?
		reinterpret_cast<compressed_type&>(decompressed[i]) = compressed[i];
	}
}

template <unsigned UncompressedSize, unsigned CompressedSize>
static void discard_zero_bytes(
	span<uint_t<CompressedSize>>          compressed,
	span<const uint_t<UncompressedSize>>  uncompressed,
	bool                                        fail_on_invalid_uncompressed_data = false)
{
	using uncompressed_type = uint_t<UncompressedSize>;
	using compressed_type = uint_t<CompressedSize>;

	// Note: current implementation does not support types which cannot be
	// bitwise-operated on, i.e. basically only supports numeric types

	util::enforce(uncompressed.size() == compressed.size());

	uncompressed_type bitmask = ((uncompressed_type )1 << util::size_in_bits<compressed_type>::value) - 1;
	if (fail_on_invalid_uncompressed_data) {
		for(size_t i = 0; i < uncompressed.size(); i++) {
			if ((uncompressed[i] & bitmask) != uncompressed[i]) {
				throw util::invalid_argument(util::concat_string("Input element ", i,
					"has invalid value " , util::promote_for_streaming(uncompressed[i]),
					" (not enough leading zero bytes)"));
			}
			compressed[i] = static_cast<compressed_type>(uncompressed[i] & bitmask);
		}
	}
	else {
		for(size_t i = 0; i < uncompressed.size(); i++) {
			compressed[i] = static_cast<compressed_type>(uncompressed[i] & bitmask);
		}
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
void DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::compress(
	span<compressed_type>          compressed,
	span<const uncompressed_type>  uncompressed)
{
	return discard_zero_bytes<UncompressedSize, CompressedSize>(
		compressed, uncompressed, fail_on_invalid_uncompressed_data);
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
KernelTestAdapter::BufferSizes DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		// No need to actually compress anything
		if (need_to_generate("compressed_input")) {
			auto compressed_input = buffers.at("compressed_input").as_span<compressed_type>();
			generate_compressed_input(compressed_input);
		}
	}
	else {
		util::enforce(need_to_generate("compressed_input"), "Invalid combination of buffers to generate");
		auto compressed = buffers.at("compressed_input").as_span<compressed_type>();
		auto decompressed = buffers.at("decompressed").as_span<const uncompressed_type>();
		this->compress(compressed, decompressed);
	}
	return NoBuffersToResize;
}

template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
KernelTestAdapter::BufferSizes DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto indices        = buffers.at("compressed_input").as_span<const compressed_type>();
		auto decompressed   = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		generate_expected_decompressed(decompressed, indices);
	}
	return NoBuffersToResize;
}


template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
void DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto decompressed     = buffers.at("decompressed"    ).as_span<uncompressed_type    >().data();
	auto compressed_input = buffers.at("compressed_input").as_span<const compressed_type>().data();
	index_type length = config.test_length;

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::decompression::discard_zero_bytes::fixed_width::kernel_t<IndexSize, UncompressedSize, CompressedSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(decompressed, compressed_input, length)
	);

}

static_block {
	namespace functors = cuda::functors;

	//                      IndexSize UncompressedSize  CompressedSize
	//------------------------------------------------------------------------------------
	DiscardZeroBytesFixed < 4,         2,                1 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 4,         4,                1 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 4,         4,                2 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 4,         8,                1 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 4,         8,                2 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 4,         8,                4 >::registerInSubclassFactory();

	DiscardZeroBytesFixed < 8,         2,                1 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 8,         4,                1 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 8,         4,                2 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 8,         8,                1 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 8,         8,                2 >::registerInSubclassFactory();
	DiscardZeroBytesFixed < 8,         8,                4 >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
