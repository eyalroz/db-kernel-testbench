#include "RunPositionEncoding.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"

#include <iomanip>
#include <iostream>

#include "kernel_wrappers/decompression/run_position_encoding.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
std::string RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss <<
		"Decompression kernel for the run position encoding compression scheme (RPE). " <<
		"The uncompressed data has size " << UncompressedSize <<
		" and indices into it have size " << UncompressedIndexSize << " bytes, the run start position/" <<
		"position offset has size " << PositionOffsetSize << " and the run indices have size " << RunIndexSize <<
		" bytes.";
	return oss.str();
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
KernelTestAdapter::BufferDescriptors RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::getBufferDescriptors() const
{
	// Note: The number of runs in practice will probably be (much) lower than the capacity, which
	// for purposes of testing must be the maximum possible

	auto num_anchors = util::div_rounding_up(config.test_length, segment_length);

	return {
		{ "decompressed",        BufferDescriptor::make<uncompressed_type       >(config.test_length, BufferDirection::Output )},
		{ "run_data",            BufferDescriptor::make<uncompressed_type       >(config.test_length, BufferDirection::Input  )},
		{ "run_start_positions", BufferDescriptor::make<position_offset_type    >(config.test_length, BufferDirection::Input  )},
		{ "position_anchors",    BufferDescriptor::make<uncompressed_index_type >(num_anchors,        BufferDirection::Input  )},
	};
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
inline RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::RunPositionEncoding(const TestConfiguration& config_)
	: KernelTestAdapter(config_), num_element_runs()
{
	using std::make_pair;

	resolveOption(segment_length,
		ConfigurationKeySet {
			"segment-length", "position-anchoring-period", "anchoring-period",
			"elements-per-anchor", "position-anchoring-interval",
			"segment-length", "anchored-segment-length" }
	);

	resolveOption(random_run_length,
		ConfigurationKeySet {
			"run-length-distribution", "run-length", "random-run-length"}
	);

	resolveOption(
		random_datum,
		ConfigurationKeySet {
			"run-data-distribution", "run-datum-distribution", "run-data", "run-datum"}
	);

	resolveOption(
		run_data_random_contribution,
		ConfigurationKeySet {
			"run-data-value-random-contribution", "run-data-random-contribution",}
	);
	resolveOption(
		run_data_linear_scale,
		ConfigurationKeySet {
			"run-data-value-linear-scale", "run-data-linear-scale"}
	);
	resolveOption(
		run_data_linear_offset,
		ConfigurationKeySet {
			"run-data-value-linear-offset", "run-data-linear-offset"}
	);
	resolveOption(
		run_data_linear_contribution,
		ConfigurationKeySet {
			"run-data-value-linear-contribution",
			"run-data-linear-contribution"}
	);

	resolveOption(
		run_data_random_contribution,
		ConfigurationKeySet {
			"run-data-random-contribution", "data-value-random-contribution",
			"compressed-random-contribution", "data-random-contribution"}
	);
	resolveOption(
		run_data_linear_scale,
		ConfigurationKeySet {
			"run-data-linear-scale", "data-value-linear-scale",
			"compressed-linear-scale", "data-linear-scale"}
	);
	resolveOption(
		run_data_linear_offset,
		ConfigurationKeySet {
			"run-data-linear-offset", "data-value-linear-offset",
			"compressed-linear-offset", "data-linear-offset"}
	);
	resolveOption(
		run_data_linear_contribution,
		ConfigurationKeySet {
			"run-data-linear-contribution", "data-value-linear-contribution",
			"compressed-linear-contribution", "data-linear-contribution"}
	);

	resolveOption(use_identity_for_run_data,
		ConfigurationKeySet {
			"use-identity-for-run-data","use-identity-for-data",
			"identity-for-data", "identity-data", "identity-run-data" }
	);

	run_data_uniformly_random = resolveOption(
		ConfigurationKeySet {
			"run-data-uniformly-random", "data-uniformly-random",
			"compressed-uniformly-random"}, false);

	if (run_data_uniformly_random) {
		run_data_linear_contribution = 0;
		run_data_linear_offset       = 0;
		run_data_linear_scale        = 0;
		run_data_random_contribution = 1;
		random_datum                 = distribution_t<uncompressed_type>::full_range_uniform();
	}

	util::enforce(not (use_identity_for_run_data and run_data_uniformly_random),
		"Cannot force compressed input to be both identity and uniformly-random");

	maybe_print_extra_configuration(
		make_pair("Segment length (= position anchoring period)",
			                                                        segment_length),
		make_pair("Run length distribution",                        random_run_length                 ),
		make_pair("Number of runs (length-value pairs)",            num_element_runs                  ),
		make_pair("Set run_data[i] = i",                            use_identity_for_run_data         ),
		make_pair("Generate uniformly random run data",             run_data_uniformly_random         ),
		make_pair("run data generation - random part distribution", random_datum                      ),
		make_pair("run data generation - random part contribution",
			                                                        run_data_random_contribution      ),
		make_pair("run data generation - linear part contribution",
			                                                        run_data_linear_contribution      ),
		make_pair("run data generation - linear scale",             run_data_linear_scale             ),
		make_pair("run data generation - linear offset",            run_data_linear_offset            )
	);

	return;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
std::string RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::getDescription() const
{
	return
		"Decompression kernel for the Run-Position Encoding (RPE) compression"
		"scheme: Assuming uncompressed data is characterized by many 'runs' "
		"of identical elements, the original data is replaced by a sequence "
		"of such 'runs'. Each run consists of an element of the original "
		"uncompressed type, and the position at which the run begins. These "
		"positions are either absolute, within the entire uncompressed data, "
		"or relative within some fixed-length segment (in which "
		"case they can be encoded with a smaller size than would be used for "
		"an absolute position). The scheme is similar to RLE, Run-Length "
		"Encoding, in which the length of each (run rather than starting "
		"position) is encoded. All data is stored in SoA rather than AoS form "
		"(structure of arrays form) - that is, the kernel receives an array of "
		"run positions and an array of run data elements, both with lengths "
		"equal to the overall number of runs.";
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
LaunchConfigurationSequence RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		{ "length",                     config.test_length                 },
		{ "position_offset_relativity", PositionsAreRelative               },
		{ "segment_length",             (size_t) segment_length },
	};
	::cuda::kernels::decompression::run_position_encoding::kernel_t<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("decompression::run_position_encoding::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
void RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::generate_run_data(
	span<uncompressed_type> run_data)
{
	if (use_identity_for_run_data) {
		iota(run_data, 0);
		return;
	}

	auto random_engine = this->random_engine;
	for(decltype(num_element_runs) i = 0; i < num_element_runs; i++) {
		uncompressed_type linear_component =
			run_data_linear_scale * i + run_data_linear_offset;
		uncompressed_type random_component =
			util::random::sample_from(random_datum, random_engine);
		run_data[i] =
			linear_component * run_data_linear_contribution +
			random_component * run_data_random_contribution;
	}
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
void RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::generate_run_start_positions_and_anchors(
	span<position_offset_type>  run_start_positions,
	span<run_index_type>        position_anchors
)
{
	auto random_engine = this->random_engine;

//	std::cout << "Positions are " << (PositionsAreRelative ? "" : "NOT ") << "relative.\n"
//		<< "We can have " << run_start_positions.size() << " runs and " << position_anchors.size() << " anchors\n";

	size_type input_pos = 0;
	auto position_anchors_it = position_anchors.begin();
	*(position_anchors_it++) = 0;
	uncompressed_index_size_type anchored_position = 0;
	run_index_size_type run_index; // TODO: Make this size_type perhaps?
	auto shortest_run_length = random_run_length.max();
	auto longest_run_length = random_run_length.min();
	for(run_index = 0; input_pos < config.test_length; run_index++) {
//		std::cout << "input_pos - anchored_position = " << input_pos << " - " << anchored_position << " = " << input_pos - anchored_position << "\n";
		run_start_positions[run_index] = PositionsAreRelative ?
			(input_pos - anchored_position) : input_pos;
//		std::cout << "run_start_positions[run_index] = " << "run_start_positions[" << run_index << "] = " << run_start_positions[run_index] << "\n";
		auto sampled_run_length = util::random::sample_from(random_run_length, random_engine);
//		std::cout << "sampled run length is " << std::setw(3) << sampled_run_length << "; ";
		auto run_length = util::clip(sampled_run_length, (index_type)1, (index_type) (config.test_length - input_pos));
//		std::cout << "run length is " << std::setw(3) << run_length << "; ";
		auto input_pos_after_run = input_pos + run_length; // this can be at most config.test length!
//		std::cout << "input_pos_after_run  = " << std::setw(3) << input_pos_after_run << "\n" << std::flush;
		util::update_min(shortest_run_length, run_length);
		util::update_max(longest_run_length, run_length);
		while (input_pos_after_run >= anchored_position + segment_length) {
			*(position_anchors_it++) = run_index;
			anchored_position += segment_length;
		}
		input_pos = input_pos_after_run;
	}
	num_element_runs = run_index;

	if (config.be_verbose) {
		std::cout << "Generated input data consists of " << num_element_runs <<
			" runs, with lengths ranging between " <<
			util::promote_for_streaming(shortest_run_length) <<
			" and " << util::promote_for_streaming(longest_run_length) <<
			", and averaging " <<
			config.test_length / (double) num_element_runs <<
			" elements per run.\n" << std::flush;
	}
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
void RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::generate_expected_decompressed(
	span<uncompressed_type>          decompressed,
	span<const uncompressed_type>    run_data,
	span<const position_offset_type> run_start_positions,
	span<const run_index_type>       position_anchors) const
{
	if (PositionsAreRelative) {
		auto out_iterator = decompressed.begin();
		auto num_segments = position_anchors.size();

		for (size_type segment_index = 0; segment_index < num_segments; segment_index++) {
			auto in_last_segment = (segment_index + 1 == num_segments);
			auto this_segment_s_length = in_last_segment ?
				decompressed.size() - segment_length * segment_index :
				segment_length;

			// The runs to which the the beginning and end of the segment are anchored to
			struct { size_type start, end; } segment_anchors_run_indices =
				{ position_anchors[segment_index], in_last_segment ?
					num_element_runs: // conceptually the "end" of the anchors is the start of a run after the last run
					position_anchors[segment_index + 1]
				};
			if (segment_anchors_run_indices.end == segment_anchors_run_indices.start or
				segment_anchors_run_indices.start + 1 == num_element_runs)
			{
				std::fill_n(out_iterator, this_segment_s_length, run_data[segment_anchors_run_indices.start]);
				continue;
			}
			// Ok, we have more than one run intersecting this segment. That means that
			// the run to which the next segment is anchored either starts within this segment
			// but not at its beginning (in which case its relative position is not 0),
			// or it is aligned with the next segment start (in which case its relative position
			// _is_ 0). We'll use that fact obviously...
			auto effective_end_of_runs_for_segment =
				(in_last_segment or run_start_positions[segment_anchors_run_indices.end] == 0) ?
				segment_anchors_run_indices.end : segment_anchors_run_indices.end + 1;

			auto run_index = segment_anchors_run_indices.start;
			position_offset_size_type pos_within_segment = 0;
			// auto start_of_decompressed_segment = out_iterator;
			// fill all but the last runs of the segment
			for(; run_index < effective_end_of_runs_for_segment - 1; run_index++)
			{
				auto length_to_fill =
					run_start_positions[run_index + 1] - pos_within_segment;
				std::fill_n(out_iterator, length_to_fill, run_data[run_index]);
				out_iterator += length_to_fill;
				pos_within_segment += length_to_fill;
			}
			// ... and fill in the last run
			auto length_to_fill = this_segment_s_length - pos_within_segment;
			std::fill_n(out_iterator, length_to_fill, run_data[run_index]);
			out_iterator += length_to_fill;
		}
	}
	else {
		auto out_iterator = decompressed.begin();
		// not PositionsAreRelative
		for(size_type run_index = 0; run_index < num_element_runs - 1; run_index++) {
			auto run_length = run_start_positions[run_index + 1] - run_start_positions[run_index];
			std::fill_n(out_iterator, run_length, run_data[run_index]);
			out_iterator += run_length;
		}
		std::fill_n(out_iterator,
			decompressed.size() - run_start_positions[num_element_runs - 1],
			run_data[num_element_runs - 1]);
	}
}

template <
	unsigned UncompressedIndexSize,
	typename Uncompressed,
	unsigned PositionOffsetSize,
	bool PositionsAreRelative,
	unsigned RunIndexSize>
static void compress(
	span<Uncompressed>                               run_data,
	span<uint_t<PositionOffsetSize>>                 run_start_positions,
	span<uint_t<RunIndexSize>>                       position_anchors,
	span<const Uncompressed>                         decompressed,
	size_type_by_index_size<UncompressedIndexSize>   segment_length,
	size_type_by_index_size<PositionOffsetSize>&     max_unclipped_run_length,
	size_type_by_index_size<RunIndexSize>&           number_of_runs)
{
	using index_type = uint_t<UncompressedIndexSize>;
	using position_offset_size_type = size_type_by_index_size<PositionOffsetSize>;
	util::enforce(decompressed.size() > 0);
	auto max_representable_run_length =
		(position_offset_size_type) 1 << PositionOffsetSize;
	index_type run_index = 0;
	index_type current_interval_index = 0;
	index_type current_run_start_pos = 0;
	index_type last_anchor_pos = 0;
	position_anchors[0] = 0;
	max_unclipped_run_length = 0;
	// The runs actually represented in the compressed data are clipped
	// at max_representable_run_length; but we want to report back what
	// was the actual longest run
	index_type current_unclipped_run_start_pos = 0;
	Uncompressed current_run_value = decompressed[current_run_start_pos];
		// This is a bit dodgy in case Uncompressed is very large
	auto mark_run = [&](index_type end_pos) {
		run_start_positions[run_index] =
			PositionsAreRelative ?
			current_run_start_pos - last_anchor_pos :
			current_run_start_pos;
		run_data[run_index] = current_run_value;
		run_index++;
		current_run_start_pos = end_pos;
	};
	auto mark_unclipped_run = [&](index_type end_pos) {
		util::update_max(max_unclipped_run_length,end_pos - current_unclipped_run_start_pos);
		current_unclipped_run_start_pos = end_pos;
	};

	// Note that while runs are marked right after they _end_, anchoring
	// periods are marked as they _begin_
	auto mark_new_anchored_interval = [&](index_type pos) {
		position_anchors[++current_interval_index] = run_index;
		last_anchor_pos = pos;
	};

	for(index_type pos = 1; pos < decompressed.size(); pos++) {
		if (pos - last_anchor_pos == segment_length) {
			mark_new_anchored_interval(pos);
		}
		if (decompressed[pos] == current_run_value) {
			if (pos - current_run_start_pos == max_representable_run_length) {
				// If we have runs too long to be captured
				mark_run(pos);
			}
			if (pos - last_anchor_pos == segment_length) {
				mark_new_anchored_interval(pos);
			}
			continue;
		}
		mark_run(pos);
		mark_unclipped_run(pos);
		current_run_value = decompressed[pos];
	}
	mark_run(decompressed.size());
	mark_unclipped_run(decompressed.size());
	number_of_runs = run_index;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
KernelTestAdapter::BufferSizes RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>
::generateInputs(const HostBuffers& buffers)
{
	if (    need_to_generate("run_data")
		and need_to_generate("run_start_positions")
		and need_to_generate("position_anchors"))
	{
		auto run_data                  = buffers.at("run_data"                ).as_span< uncompressed_type       >();
		auto run_start_positions       = buffers.at("run_start_positions"     ).as_span< position_offset_type    >();
		auto position_anchors          = buffers.at("position_anchors"        ).as_span< run_index_type          >();
		if (not need_to_generate("decompressed")) {
			auto decompressed          = buffers.at("decompressed"            ).as_span< const uncompressed_type >();
			position_offset_size_type max_unclipped_run_length = 0;
			compress<UncompressedIndexSize, uncompressed_type, PositionOffsetSize, PositionsAreRelative, RunIndexSize>(
				run_data, run_start_positions, position_anchors,
				decompressed, segment_length,
				max_unclipped_run_length, num_element_runs);
		}
		else {
			// The order of calls here is significant
			generate_run_start_positions_and_anchors(run_start_positions, position_anchors);
			generate_run_data(run_data);
		}
		return {
			std::make_pair("run_start_positions", num_element_runs * PositionOffsetSize),
			std::make_pair("run_data",            num_element_runs * UncompressedSize ),
		};
	}
	if (   need_to_generate("run_data")
		or need_to_generate("run_start_positions")
		or need_to_generate("position_anchors"))
	{
		throw util::invalid_argument("Can either generate all input buffers or none of them");
	}
	if (not need_to_generate("decompressed_data")) {
		throw util::invalid_argument("Invalid combinations of buffers to generate.");
	}
	return NoBuffersToResize;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
KernelTestAdapter::BufferSizes RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto run_start_positions  = buffers.at("run_start_positions").as_span< position_offset_type >();
		auto run_data             = buffers.at("run_data"           ).as_span< uncompressed_type    >();
		auto position_anchors     = buffers.at("position_anchors"   ).as_span< run_index_type       >();

		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		generate_expected_decompressed(decompressed, run_data, run_start_positions, position_anchors);
	}
	return NoBuffersToResize;
}

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned PositionOffsetSize, bool PositionsAreRelative, unsigned RunIndexSize>
void RunPositionEncoding<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());

	auto decompressed              = buffers.at("decompressed"            ).as_span< uncompressed_type          >().data();
	auto run_start_positions       = buffers.at("run_start_positions"     ).as_span< const position_offset_type >().data();
	auto run_data                  = buffers.at("run_data"                ).as_span< const uncompressed_type    >().data();
	auto position_anchors          = buffers.at("position_anchors"        ).as_span< const run_index_type       >().data();
	uncompressed_index_size_type num_anchors = util::div_rounding_up(config.test_length, segment_length);
	uncompressed_index_size_type length = config.test_length;

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::decompression::run_position_encoding
		::kernel_t<UncompressedIndexSize, UncompressedSize, PositionOffsetSize, PositionsAreRelative, RunIndexSize>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(
			decompressed, run_data, run_start_positions, position_anchors,
			segment_length, num_anchors, num_element_runs, length)
	);
}

static_block {
	constexpr const bool relative = true;
	constexpr const bool absolute = false;

	//                    UncompressedIndexSize   UncompressedSize  RunPositionSize  PositionsAreRelative
	//----------------------------------------------------------------------------------------

	RunPositionEncoding < 4,                      1,                2,               relative >::registerInSubclassFactory();
	RunPositionEncoding < 4,                      2,                2,               relative >::registerInSubclassFactory();
	RunPositionEncoding < 4,                      4,                2,               relative >::registerInSubclassFactory();
	RunPositionEncoding < 4,                      8,                2,               relative >::registerInSubclassFactory();
	RunPositionEncoding < 8,                      4,                2,               relative >::registerInSubclassFactory();

	// and now the absolutely-positioned

	RunPositionEncoding < 4,                      1,                4,               absolute >::registerInSubclassFactory();
	RunPositionEncoding < 4,                      2,                4,               absolute >::registerInSubclassFactory();
	RunPositionEncoding < 4,                      4,                4,               absolute >::registerInSubclassFactory();
	RunPositionEncoding < 4,                      8,                4,               absolute >::registerInSubclassFactory();
	RunPositionEncoding < 8,                      4,                8,               absolute >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
