#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_DELTA_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_DELTA_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

/**
 * @note It's the unpatched, exceptions-not-allowed compression scheme; see also the
 * @ref PatchedDelta test adapter
 */
template <unsigned IndexSize, typename Uncompressed, typename Difference>
class Delta:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<Delta<IndexSize, Uncompressed, Difference> >
{
public: // types
	using index_type            = uint_t<IndexSize>;
	using size_type             = size_type_by_index_size<IndexSize>;
	using compressed_type       = Difference;
	using uncompressed_type     = Uncompressed;

protected:
	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Delta)

protected:
	void generate_compressed_input(
		span<compressed_type>          compressed_input);
	void generate_anchor_values(
		span<uncompressed_type>        anchor_values,
		span<const compressed_type>    compressed_input);
	void generate_expected_decompressed(
		span<uncompressed_type>        decompressed,
		span<const compressed_type>    compressed_input,
		span<const uncompressed_type>  anchor_values);

public: // data members
	// This should really be protected and let the patched adapters be friends, but - expediency

	uncompressed_type  initial_anchor_value                            { 100   };

	// All values here will be modulated by the number of entries

	bool               compressed_input_uniformly_random               { false };
	bool               use_all_ones_as_compressed_data                 { false };
	distribution_t<compressed_type> compressed_input_random_contribution =
		distribution_t<compressed_type>::uniform(-6, 3);
	double             compressed_input_random_contribution_factor     { 0.5   };
	double             compressed_input_linear_contribution_factor     { 1.0   };
	uncompressed_type  compressed_input_linear_scale                   { 0     };
	uncompressed_type  compressed_input_linear_offset                  { 1     };
	size_type          segment_length                                  { 2048  };
};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_DELTA_H_ */
