
#include "IncidenceBitmaps.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>


#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/builtins.hpp"
#include "util/miscellany.hpp"

#include <iostream>

#include "kernel_wrappers/decompression/incidence_bitmaps.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned BitmapAlignmentInBytes, bool Aligned = true, typename BitContainer = cuda::standard_bit_container_t>
static size_t full_bitmap_length(size_t length_in_bits)
{
	auto unaligned_length = util::div_rounding_up(length_in_bits, util::size_in_bits<BitContainer>::value);
	auto alignment_in_ints = util::div_rounding_up(BitmapAlignmentInBytes, sizeof(BitContainer));
	return Aligned ? util::round_up(unaligned_length, alignment_in_ints) : unaligned_length;
}


template <unsigned IndexSize, unsigned BitmapAlignmentInBytes, typename BitContainer = cuda::standard_bit_container_t>
static void set_in_bitmap(void* bitmap, uint_t<IndexSize> index)
{
	uint_t<IndexSize> bit_index = index % util::size_in_bits<BitContainer>::value;
	uint_t<IndexSize> container_element_index = index / util::size_in_bits<BitContainer>::value;
	((BitContainer*) bitmap)[container_element_index] |= (1 << bit_index);
}

// Note: May not currently be in use
template <unsigned IndexSize, unsigned BitmapAlignmentInBytes, typename BitContainer = cuda::standard_bit_container_t>
static void unset_in_bitmap(BitContainer* bitmap, uint_t<IndexSize> index)
{
	auto bit_index = index % util::size_in_bits<BitContainer >();
	uint_t<IndexSize> container_element_index = index / util::size_in_bits<BitContainer>();
	bitmap[container_element_index] &= (1 << bit_index);
}

template <unsigned IndexSize, unsigned BitmapAlignmentInBytes, typename BitContainer = cuda::standard_bit_container_t>
static BitContainer* get_bitmap(BitContainer* all_bitmaps, uint_t<IndexSize> bitmap_length, unsigned char value)
{
	constexpr auto aligned = true;
	return all_bitmaps + full_bitmap_length<BitmapAlignmentInBytes, aligned, BitContainer>(bitmap_length) * value;
}

template <unsigned IndexSize, unsigned BitmapAlignmentInBytes, typename BitContainer = cuda::standard_bit_container_t>
static void set_in_bitmaps(BitContainer* all_bitmaps, uint_t<IndexSize> bitmap_length, uint_t<IndexSize> index, unsigned char value)
{
	auto relevant_bitmap = get_bitmap<IndexSize, BitmapAlignmentInBytes, BitContainer>(all_bitmaps, bitmap_length, value);
	set_in_bitmap<IndexSize, BitmapAlignmentInBytes>(relevant_bitmap, index);
}

// Note: May not currently be in use
template <unsigned IndexSize, unsigned BitmapAlignmentInBytes, typename BitContainer = cuda::standard_bit_container_t>
static void unset_in_bitmaps(BitContainer* all_bitmaps, uint_t<IndexSize> bitmap_length, uint_t<IndexSize> index, unsigned char value)
{
	auto relevant_bitmap = get_bitmap<IndexSize, BitmapAlignmentInBytes>(all_bitmaps, bitmap_length, value);
	unset_in_bitmap(relevant_bitmap, index);
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
std::string IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::buildPrettyKernelName() const
{
	std::ostringstream oss;
	oss <<
		"Decompression kernel for the per-value incidence bitmaps scheme (BITMAP), with uncompressed "
		"elements of size " << UncompressedSize << ", "
		<< (WithDictionary ?
			"using a dictionary to translate bitmap indices into uncompressed data" :
			"with the bitmap index for each element being the uncompressed uncompressed_type")
		<< ".";
	return oss.str();
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
KernelTestAdapter::BufferDescriptors IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::getBufferDescriptors() const
{
	auto overall_length_of_bitmaps = max_num_bitmaps  * aligned_single_bitmap_length;
	if (WithDictionary) { return
	{
		{ "decompressed",       BufferDescriptor::make<uncompressed_type>(config.test_length,        BufferDirection::Output) },
		{ "dictionary_entries", BufferDescriptor::make<uncompressed_type>(max_num_bitmaps,       BufferDirection::Input)  },
		{ "incidence_bitmaps",  BufferDescriptor::make<bit_container_type>(overall_length_of_bitmaps, BufferDirection::Input).bit_container()  },
	}; }
	else return
	{
		{ "decompressed",       BufferDescriptor::make<uncompressed_type>(config.test_length,        BufferDirection::Output) },
		{ "incidence_bitmaps",  BufferDescriptor::make<bit_container_type>(overall_length_of_bitmaps, BufferDirection::Input).bit_container()  },
	} ;
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
inline IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>
	::IncidenceBitmaps(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;

	auto got_max_num_bitmaps_override = resolveOption(
		max_num_bitmaps,
		ConfigurationKeySet {
			"num-distict-values", "distinct-values", "num-values", "num-bitmaps",
			"number-of-bitmaps", "max-num-bitmaps", "maximum-number-of-bitmaps",
			"max-num-distinict-values", "maximum-number-of-distinct-values"}
	);

	if (not need_to_generate("decompressed")) {
		// In this case we don't need a distinction between the maximum and actual
		// number of bitmaps - we can set them to be the same and generate decompressed
		// data accordingly

		if (got_max_num_bitmaps_override) {
			num_actual_bitmaps = max_num_bitmaps;
		}
		else {
			max_num_bitmaps = num_actual_bitmaps;
		}
		compressed_input_random_part =
			distribution_t<dictionary_index_type>::uniform(0, num_actual_bitmaps);
	}
	// ... and otherwise we'll determine the actual value later; but we do need
	// the maximum value defined - that's whiy it not optional - to allocate memory
	// for the dictionary and bitmap buffers

	// TODO: We actually don't need to parse these if we're not generating data
	resolveOption(
		compressed_input_random_part,
		ConfigurationKeySet {
			"dictionary-value", "dictionary-value-distribution", "dictionary-value-random-part",
			"dictionary-distribution", "dictionarty-random-part"}
	);
	resolveOption(
		dictionary_value_random_contribution,
		ConfigurationKeySet {
			"dictionary-value-random-contribution", "dict-value-random-contribution",
			"dictionary-random-contribution", "dict-random-contribution"}
	);
	resolveOption(
		dictionary_value_linear_scale,
		ConfigurationKeySet {
			"dictionary-value-linear-scale", "dict-value-linear-scale",
			"dictionary-linear-scale", "dict-linear-scale"}
	);
	resolveOption(
		dictionary_value_linear_offset,
		ConfigurationKeySet {
			"dictionary-value-linear-offset", "dict-value-linear-offset",
			"dictionary-linear-offset", "dict-linear-offset"}
	);
	resolveOption(
		dictionary_value_linear_contribution,
		ConfigurationKeySet {
			"dictionary-value-linear-contribution", "dict-value-linear-contribution",
			"dictionary-linear-contribution", "dict-linear-contribution"}
	);


	resolveOption(
		compressed_input_random_part,
		ConfigurationKeySet {
			"compressed-input-random-part", "data-value-random-part",
			"compressed-random", "data-random",
			"compressed-distribution", "data-distribution"}
	);
	resolveOption(
		compressed_input_random_contribution,
		ConfigurationKeySet {
			"compressed-input-random-contribution", "data-value-random-contribution",
			"compressed-random-contribution", "data-random-contribution"}
	);
	resolveOption(
		compressed_input_linear_scale,
		ConfigurationKeySet {
			"compressed-input-linear-scale", "data-value-linear-scale",
			"compressed-linear-scale", "data-linear-scale"}
	);
	resolveOption(
		compressed_input_linear_offset,
		ConfigurationKeySet {
			"compressed-input-linear-offset", "data-value-linear-offset",
			"compressed-linear-offset", "data-linear-offset"}
	);
	resolveOption(
		compressed_input_linear_contribution,
		ConfigurationKeySet {
			"compressed-input-linear-contribution", "data-value-linear-contribution",
			"compressed-linear-contribution", "data-linear-contribution"}
	);

	resolveOption(use_all_ones_as_compressed_data,
		ConfigurationKeySet {
			"use-all-ones-as-input", "data-all-ones", "all-ones-data",
			"all-ones", "all-ones-input"
		}
	);

	bool compressed_input_uniformly_random = resolveOption(ConfigurationKeySet {
		"compressed-input-uniformly-random", "data-uniformly-random",
		"compressed-uniformly-random"}, false);

	util::enforce(!use_all_ones_as_compressed_data || compressed_input_uniformly_random,
		"Incompatible configuration options");

	if (compressed_input_uniformly_random) {
		compressed_input_linear_contribution = 0;
		compressed_input_linear_offset = 0;
		compressed_input_linear_scale = 0;
		compressed_input_random_contribution = 1;
		compressed_input_random_part = distribution_t<dictionary_index_type>::uniform(0, max_num_bitmaps);
	}

	// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Maximum number of bitmaps / entries in dictionary",              max_num_bitmaps                       ),
		make_pair("Set all compressed data to 1",                                   use_all_ones_as_compressed_data       ),
		make_pair("Dictionary value generation - random distribution",              dictionary_value_random_part          ),
		make_pair("Dictionary value generation - random factor contribution",       dictionary_value_random_contribution  ),
		make_pair("Dictionary value generation - linear factor contribution",       dictionary_value_linear_contribution  ),
		make_pair("Dictionary value generation - linear scale",                     dictionary_value_linear_scale         ),
		make_pair("Dictionary value generation - linear offset",                    dictionary_value_linear_offset        ),
		make_pair("Compressed input generation - random part",                      compressed_input_random_part          ),
		make_pair("Compressed input generation - random factor contribution",       compressed_input_random_contribution  ),
		make_pair("Compressed input generation - linear factor contribution",       compressed_input_linear_contribution  ),
		make_pair("Compressed input generation - linear scale",                     compressed_input_linear_scale         ),
		make_pair("Compressed input generation - linear offset",                    compressed_input_linear_offset        )
	);

	util::enforce(max_num_bitmaps <= util::capped_domain_size<dictionary_index_type>::value,
		"Maximum number of bitmaps ", max_num_bitmaps, " is too high - the maximum supported is ",
		util::capped_domain_size<dictionary_index_type>::value, " (when the dictionary index type is ",
		util::type_name<dictionary_index_type>(), ")");

	single_bitmap_length = full_bitmap_length<BitmapAlignmentInBytes, false>(config.test_length);
	aligned_single_bitmap_length = full_bitmap_length<BitmapAlignmentInBytes, true>(config.test_length);

	return;
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
std::string IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::getDescription() const
{
	return
		"Decompression kernel for the Incidence Bitmaps (BITMAP) compression"
		"scheme: For every element of the uncompressed element's domain, we "
		"store a bitmap for the entire compressed array/column - where a "
		"value of 1 at index i in bitmap k means that the array element of "
		"index i has value k. bitmap indices may or may not require "
		"translation using a dictionary into other, arbitrary, values.";
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
LaunchConfigurationSequence IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>
::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };

	::cuda::kernels::decompression::incidence_bitmaps::kernel_t<IndexSize, UncompressedSize, BitmapAlignmentInBytes / sizeof(int), WithDictionary> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("decompression::incidence_bitmaps::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

// This is only relevant for synthetic uncompressed data
template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
void IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>
	::generate_dictionary(span<uncompressed_type> dictionary)
{
	auto random_engine = this->random_engine;

	for(unsigned char i = 0; i < num_actual_bitmaps; i++) {
		uncompressed_type linear_component =
			dictionary_value_linear_scale * i + dictionary_value_linear_offset;
		uncompressed_type random_component =
			util::random::sample_from(dictionary_value_random_part, random_engine);
		dictionary[i] =
			linear_component * dictionary_value_linear_contribution +
			random_component * dictionary_value_random_contribution;
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
void IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::generate_incidence_bitmaps(
	span<
		typename IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::bit_container_type
	> incidence_bitmaps)
{
	util::enforce(incidence_bitmaps.size() * sizeof(bit_container_type) >= aligned_single_bitmap_length * num_actual_bitmaps);
	fill_n(incidence_bitmaps, aligned_single_bitmap_length * num_actual_bitmaps, 0);
	if (use_all_ones_as_compressed_data) {
		auto one_bitmap = get_bitmap<IndexSize, BitmapAlignmentInBytes>(incidence_bitmaps.data(), config.test_length, 1);
		// This is actually not quite correct, since we might be filling beyond
		// the last bit, for bitmaps which represent a non-rounded number of bits
		std::fill(one_bitmap, one_bitmap + single_bitmap_length - 1, util::all_one_bits<bit_container_type>());
		auto num_set_bits_in_last_container = config.test_length % util::size_in_bits<bit_container_type>::value;
		// the last container might not be entirely full of ones, so:
		one_bitmap[single_bitmap_length - 1] |= ((1 << num_set_bits_in_last_container) - 1);
		return;
	}

	auto random_engine = this->random_engine;

	for(index_type i = 0; i < config.test_length; i++) {
		if (use_all_ones_as_compressed_data) {
			set_in_bitmaps<IndexSize, BitmapAlignmentInBytes>(
				incidence_bitmaps.data(), config.test_length, i, 1);
			continue;
		}
		auto linear_component =
			compressed_input_linear_scale * i + compressed_input_linear_offset;
		auto random_component =
			util::random::sample_from(compressed_input_random_part, random_engine);
		dictionary_index_type dictionary_index_or_value =
			(int) (linear_component * compressed_input_linear_contribution +
			random_component * compressed_input_random_contribution) % num_actual_bitmaps;
		set_in_bitmaps<IndexSize, BitmapAlignmentInBytes>(
			incidence_bitmaps.data(), config.test_length, i, dictionary_index_or_value);
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
void IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::generate_expected_decompressed(
	span<uncompressed_type>                  decompressed,
	optional<span<const uncompressed_type>>  dictionary,
	span<const bit_container_type>      incidence_bitmaps)
{
//	using IncidenceBitmaps::bitmap_index_type;
	for(bitmap_index_type bitmap_index = 0; bitmap_index < num_actual_bitmaps; bitmap_index++) {
		auto bitmap = get_bitmap<IndexSize, BitmapAlignmentInBytes>(
			const_cast<bit_container_type*>(incidence_bitmaps.data()), config.test_length, bitmap_index);
		uncompressed_type value_correspondig_to_bitmap = WithDictionary ?
			dictionary.value()[bitmap_index] : (uncompressed_type) bitmap_index;
		for(unsigned container = 0; container < single_bitmap_length; container++) {
			auto x = bitmap[container];
			while (x != 0) {
				auto first_set_bit_index = util::builtins::count_trailing_zeros(x);
				auto output_index = container * util::size_in_bits<bit_container_type>::value + first_set_bit_index;
				if (output_index >= config.test_length) { break; }
				decompressed[output_index] = value_correspondig_to_bitmap;
				x &= ~(1 << first_set_bit_index);
			}
		}
	}
}

template<typename Uncompressed, typename DictionaryIndex>
static std::vector<DictionaryIndex> apply_dictionary(
	span<const Uncompressed>                                  data,
	const std::unordered_map<Uncompressed, DictionaryIndex>&  dictionary)
{
	std::vector<DictionaryIndex> result(data.size());

	// can't just use the lookup primitive, since we need to
	// allow for the case of failure. TODO: Make the lookup primitive
	// be willing to handle failures
	for(decltype(data.size()) i = 0; i < data.size(); i++) {
		auto it = dictionary.find(data[i]);
		if (UNLIKELY(it == dictionary.end())) { continue; }
		result[i] = it->second;
	}
	return result;
}

template <
	unsigned IndexSize,
	unsigned UncompressedSize,
	typename UncompressedIndicesContainer,
	unsigned BitmapAlignmentInBytes,
	typename BitContainer = cuda::standard_bit_container_t>
static void compress(
	span<BitContainer>                   incidence_bitmaps,
	const UncompressedIndicesContainer&  uncompressed_indices,
	uint_t<IndexSize>                    num_bitmaps)
{
	using size_type = size_type_by_index_size<IndexSize>;
	size_type single_bitmap_length = uncompressed_indices.size();
	for(size_type i = 0; i < uncompressed_indices.size(); i++) {
		auto index = uncompressed_indices[i];
		if (UNLIKELY(index >= num_bitmaps)) { continue; }
		set_in_bitmaps<IndexSize, BitmapAlignmentInBytes>(
			incidence_bitmaps.data(), single_bitmap_length, i, index );
		// TODO: Prepare container elements with multiple bits, then write them
		// at once - instead of writing each bit
	}
}

template <unsigned IndexSize, typename Uncompressed, typename DictionaryIndex = unsigned>
static std::unordered_map<Uncompressed, DictionaryIndex> build_dictionary(
	span<const Uncompressed>  uncompressed,
	span<Uncompressed>        dictionary_entries)
{
	using size_type = size_type_by_index_size<IndexSize>;
	std::unordered_map<Uncompressed, DictionaryIndex> dictionary;
	// TODO: maybe set the load factor here, and hence a larger number of buckets?

	auto max_dictionary_size = dictionary_entries.size();
	for (size_type i = 0; i < uncompressed.size(); i++) {
		auto it = dictionary.find(uncompressed[i]);
		if (it != dictionary.end()) continue;
		if (dictionary.size() >= max_dictionary_size) {
			throw util::invalid_argument("Input has more distinct elements than are allowed in the dictionary");
		}
		auto new_index = dictionary.size();
		dictionary.emplace_hint(it, uncompressed[i], new_index);
		dictionary_entries[new_index] = uncompressed[i];
	}
	return dictionary;
}


template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
KernelTestAdapter::BufferSizes IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>
	::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		if (need_to_generate("incidence_bitmaps")) {
			auto incidence_bitmaps = buffers.at("incidence_bitmaps").as_span<bit_container_type>();
			generate_incidence_bitmaps(incidence_bitmaps);
		}

		if (WithDictionary) {
			if (need_to_generate("dictionary_entries")) {
				auto dictionary       = buffers.at("dictionary_entries").as_span<uncompressed_type>();
				generate_dictionary(dictionary);
			}
		}
		return NoBuffersToResize; // ... as the maximum and the actual number of bits are the same
	}
	else {
		// Ok, we actually need to compress some data
		if (not need_to_generate("incidence_bitmaps"))	{
			throw util::invalid_argument("Invalid combination of buffers to generate");
		}
		auto incidence_bitmaps  = buffers.at("incidence_bitmaps").as_span<bit_container_type>();
		if (WithDictionary) {
			auto uncompressed       = buffers.at("decompressed").as_span<const uncompressed_type>();
			auto dictionary_entries = buffers.at("dictionary_entries").as_span<uncompressed_type>();

			if (need_to_generate("dictionary_entries")) {
				auto multiplicities = util::count_multiplicities(uncompressed);
				bool bitmaps_can_cover_all_data = multiplicities.size() <= max_num_bitmaps;
				if (not bitmaps_can_cover_all_data and fail_when_bitmaps_cant_cover_all_data) {
					throw util::invalid_argument(util::concat_string(
						"Uncompressed input data has ", multiplicities.size(), " distinct values, "
						"while only as many as ", max_num_bitmaps, " may be used (and all data must "
						"be covered by the bitmaps)."));
				}
				// We want the high-frequency elements to appear first in sorted_multiplicities
				auto comparator =
					[](
						const std::pair<uncompressed_type, size_t>& x,
						const std::pair<uncompressed_type, size_t>& y)
					{
						return x.second > y.second;
					};
				std::vector<std::pair<uncompressed_type, size_t>> sorted_higher_multiplicities(max_num_bitmaps);
				auto it = partial_sort_copy(multiplicities, sorted_higher_multiplicities, comparator);
				num_actual_bitmaps = it - sorted_higher_multiplicities.begin();

				// This extracts just the values of the most common elements in uncompressed
				std::transform(sorted_higher_multiplicities.begin(), sorted_higher_multiplicities.end(),
					dictionary_entries.begin(),
					[](const std::pair<uncompressed_type, size_t>& p) { return p.first; });

			} else {
				// TODO: Perhaps set this elsewhere?
				num_actual_bitmaps = dictionary_entries.size();
			}
			std::unordered_map<uncompressed_type, dictionary_index_type> dictionary_as_map(num_actual_bitmaps);
			for(index_type i=0; i < num_actual_bitmaps; i++) {
				dictionary_as_map.insert( {dictionary_entries[i], i} );
			}

			auto uncompressed_indices = apply_dictionary(uncompressed, dictionary_as_map);
			compress<IndexSize, UncompressedSize, decltype(uncompressed_indices), BitmapAlignmentInBytes>(
				incidence_bitmaps, uncompressed_indices, num_actual_bitmaps);

			auto single_bitmap_size = aligned_single_bitmap_length * sizeof(bit_container_type);
			return	{
				{ "dictionary_entries", num_actual_bitmaps * UncompressedSize},
				{ "incidence_bitmaps",  num_actual_bitmaps * single_bitmap_size }
			};
		}
		else {
			// Note that we use a supposedly-different type for this span than the way
			// it's defined in getBufferDescriptors: a buffer of uncompressed_type. Fortunately,
			// when we're not using the dictionary, uncompressed_type _is_ the same type
			// as dictionary_index_type .
			//
			// Why do we need this contortion? Because the if (WithDictionary) {} else {} block
			// we're in is not if-constexpr, like in C++17, so both branches of it get compiled
			// in both cases; but for non-unsigned-integral uncompressed_type type we can't call
			// the compress function...

			auto uncompressed       = buffers.at("decompressed").as_span<dictionary_index_type>();
			auto max_value = *max_element(uncompressed);
			index_type num_actual_bitmaps = std::min<index_type>( max_value + 1,  max_num_bitmaps);

			compress<IndexSize, UncompressedSize, decltype(uncompressed), BitmapAlignmentInBytes>(
				incidence_bitmaps, uncompressed, num_actual_bitmaps);

			return	{
				{ "incidence_bitmaps",  num_actual_bitmaps * aligned_single_bitmap_length }
			};

		}
	}
}

template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
KernelTestAdapter::BufferSizes IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();
		auto incidence_bitmaps = buffers.at("incidence_bitmaps").as_span<const bit_container_type>();
		optional<span<const uncompressed_type>> dictionary;
		if (WithDictionary) {
			dictionary = buffers.at("dictionary_entries").as_span<const uncompressed_type>();
		}
		generate_expected_decompressed(decompressed, dictionary, incidence_bitmaps);
	}
	return NoBuffersToResize;
}


template <unsigned IndexSize, unsigned UncompressedSize, bool WithDictionary, unsigned BitmapAlignmentInBytes>
void IncidenceBitmaps<IndexSize, UncompressedSize, WithDictionary, BitmapAlignmentInBytes>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	using uncompressed_type = uint_t<UncompressedSize>;

	util::enforce(device_scratch_area.empty());
	auto decompressed        = buffers.at("decompressed"      ).as_span<uncompressed_type        >().data();
	auto incidence_bitmaps   = buffers.at("incidence_bitmaps" ).as_span<const bit_container_type >().data();
	auto dictionary_entries  = WithDictionary ?
		                       buffers.at("dictionary_entries").as_span<const uncompressed_type  >().data() : nullptr;
	index_type bitmap_length = single_bitmap_length;
	bitmap_index_size_type num_bitmaps  = num_actual_bitmaps;
		// it is actually used, but nvcc (for CUDA 7.5/8.0( RC is being difficult

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::decompression::incidence_bitmaps::kernel_t
		<IndexSize, UncompressedSize, BitmapAlignmentInBytes / sizeof(bit_container_type), WithDictionary>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(decompressed, incidence_bitmaps, dictionary_entries, bitmap_length, num_bitmaps)
		);
}

static_block {
	//                 IndexSize UncompressedSize  WithDictionary   BitmapAlignmentInBytes
	//---------------------------------------------------------------------------------------
	IncidenceBitmaps < 4,        1,                true,            8  >::registerInSubclassFactory();
	IncidenceBitmaps < 4,        2,                true,            8  >::registerInSubclassFactory();
	IncidenceBitmaps < 4,        4,                true,            8  >::registerInSubclassFactory();
	IncidenceBitmaps < 4,        8,                true,            8  >::registerInSubclassFactory();
	IncidenceBitmaps < 4,        1,                false,           8  >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
