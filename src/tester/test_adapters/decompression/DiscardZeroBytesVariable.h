#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_DISCARD_ZERO_BYTES_VARIABLE_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_DISCARD_ZERO_BYTES_VARIABLE_H_

#include "util/math.hpp"
#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, unsigned UncompressedSize, unsigned ElementSizesContainerSize>
class DiscardZeroBytesVariable:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<DiscardZeroBytesVariable<IndexSize, UncompressedSize, ElementSizesContainerSize>>
{
	static_assert(UncompressedSize > 1, "There can be no variation in size if the uncompressed size is 1 byte");
	static_assert(util::divides(UncompressedSize, ElementSizesContainerSize * util::bits_per_byte),
		"At the moment, only supporting the case of element sizes completely filling container elements, with no slack bits");

public: // types
	using index_type            = uint_t<IndexSize>;
	using size_type             = size_type_by_index_size<IndexSize>;
	using uncompressed_type     = uint_t<UncompressedSize>;
	using element_size_type     = cuda::native_word_t;
	using size_range_type       = struct { element_size_type min; element_size_type max; };

protected: // types
	// this type definition is required for using the boilerplate macro below, which assumes
	// it is defined
	using Uncompressed = uint_t<UncompressedSize>;

	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(DiscardZeroBytesVariable)

public:
	using element_sizes_container_type = uint_t<ElementSizesContainerSize>;

protected:
	void generate_element_sizes_and_anchors(
		span<element_sizes_container_type>        packed_element_sizes,
		span<index_type>                          position_anchors,
		size_type&                                total_compressed_size
	);
	void generate_compressed_data(
		span<unsigned char>                       buffer_to_fill,
		span<const element_sizes_container_type>  element_sizes,
		size_type                                 total_compressed_size
	);
	void generate_expected_decompressed(
		span<uncompressed_type>                   decompressed,
		span<const unsigned char>                 compressed_data,
		span<const element_sizes_container_type>  packed_element_sizes,
		span<const index_type>                    position_anchors
	);
	size_range_type determine_optimal_size_range(
		span<const uncompressed_type>             decompressed);

public: // utility methods
	static unsigned bits_per_element_size(unsigned min_represented_size, unsigned max_represented_size)
	{
		return util::ceil_log_constexpr<unsigned, 2>(
			max_represented_size - min_represented_size + 1);
	}

	static unsigned element_sizes_per_container(unsigned min_represented_size, unsigned max_represented_size)
	{
		return ElementSizesContainerSize * util::bits_per_byte /
			bits_per_element_size(min_represented_size, max_represented_size);
	}

protected: // utility methods

	unsigned bits_per_element_size() const
	{
		return bits_per_element_size(represented_sizes.min, represented_sizes.max);
	}

	unsigned element_sizes_per_container() const
	{
		return element_sizes_per_container(represented_sizes.min, represented_sizes.max);
	}

public: // data members
	size_type segment_length               { 1024 };
		// Values which are not a power of 2 are less well tested
		// ... otherwise it's uniform
	distribution_t<uncompressed_type> uncompressed_element_distribution {
		distribution_t<uncompressed_type>::uniform(
			11, std::numeric_limits<uncompressed_type>::max()
		)
	};

	// The compressed per-element representation of the element size
	// is a number ranging from 0 to 1 << the number of bits used
	// in the representation. But it's likely to be the case that we want
	// to represent a different range of sizes; after all, we don't
	// have elements with size 0; and it's possibly we only have
	// elements of sizes larger than one.
	size_range_type represented_sizes;

	const size_range_type default_generated_size_range = { 1, UncompressedSize };


	// Note: The following options are only relevant when we generate
	// decompressed data, rather than compressing data we were provided.
	// The min_ and represented_sizes.max options
	// are relevant in both cases, but in the latter case - the defaults
	// here will be overridden by dynamically-calculated values unless
	// overridden already from the command-line or a configuration file

	distribution_t<element_size_type> generated_element_size_distribution {
		distribution_t<element_size_type>::uniform(
			default_generated_size_range.min,
			default_generated_size_range.max)
	};
	distribution_t<unsigned char> element_value_byte_distribution  {
		distribution_t<unsigned char>::uniform(11,99)
	};
	bool           use_modular_identity_for_nonzero_bytes  { false };

protected:
	struct { bool for_min, for_max; } have_size_range_overrides = { false, false };

};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_DISCARD_ZERO_BYTES_VARIABLE_H_ */
