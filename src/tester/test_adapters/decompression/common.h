#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_COMMON_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_COMMON_H_

#include <cuda/api/kernel_launch.cuh>
#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace decompression {

// This is kind of fugly, but unforunately we have some reliance
// on the inheritance structure in our reflection(ish) code,
// and this avoids an extra node in the inheritance tree.
//
// NOTE:
// Every decompression adapter must support, at least as an option,
// receiving invalid uncompressed data as input - that is, data that
// is not the possible result of a decompression - and carry out a
// best-effort to compress it if so requested, so as to allow for
// its use with patching schemes
//
#define 	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(subclass_name) \
public: \
	bool  fail_on_invalid_uncompressed_data { true }; \
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(subclass_name);


} // namespace decompression
} // namespace kernel_tests



#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_COMMON_H_ */
