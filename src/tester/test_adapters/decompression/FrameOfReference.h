#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_FRAME_OF_REFERENCE_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_FRAME_OF_REFERENCE_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

/**
 * @note It's the unpatched, exceptions-not-allowed compression scheme; see also the
 * @ref PatchedFrameOfReference test adapter
 */
template<
	unsigned IndexSize, typename Uncompressed,
	typename Compressed, typename UnaryModelFunction>
class FrameOfReference:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>>
{
public: // types
	using index_type                 = uint_t<IndexSize>;
	using size_type                  = size_type_by_index_size<IndexSize>;
	using uncompressed_type          = Uncompressed;
	using model_coefficient_type     = typename UnaryModelFunction::coefficient_type;
	using model_coefficients_type    = typename UnaryModelFunction::coefficients_type;
	enum { model_dimension = UnaryModelFunction::model_dimension };
	using model_dimensions_size_type = typename UnaryModelFunction::model_dimensions_size_type;
	using model_index_type           = typename UnaryModelFunction::index_type;
	enum { model_index_size = UnaryModelFunction::index_size };


	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(FrameOfReference)

protected:
	void generate_compressed_input(
		span<Compressed> buffer_to_fill);
	void generate_model_function_parameters(
		span<model_coefficients_type> buffer_to_fill);
	void generate_expected_decompressed(
		span<Uncompressed>                   decompressed,
		span<const Compressed>               compressed_input,
		span<const model_coefficients_type>  segment_model_coefficients);

public: // data members
	// This should really be protected and let the patched adapters be friends, but - expediency

	index_type  segment_length                                 { 1024 };
	bool        compressed_input_uniformly_random              { sizeof(Compressed) > 2 ? false : true  };
	bool        use_same_model_for_all_segments                { false };
	bool        use_continued_model_for_all_segments           { false };
	distribution_t<Compressed> compressed_input_random_part {
		distribution_t<Compressed>::uniform(
			std::is_signed<Compressed>::value ? -5 : 0,
			std::is_signed<Compressed>::value ?  5 : 9
		)
	};
	double      compressed_input_random_contribution            { 0.9 };
	double      compressed_input_model_contribution             { 0.1 };
	double      segment_model_coefficient_random_contribution  { 0.9 };
	double      segment_model_coefficient_model_contribution   { 0.1 };
	distribution_t<double> model_coefficient_random_part {
		std::uniform_real_distribution<double>(-100, 100)
	};
	char        coefficient_delimiter                           { ',' };
	struct {
		model_coefficients_type intra_segment;
		std::array<model_coefficients_type, model_dimension> per_parameter;
	} model_coefficient_sets;
	model_coefficients_type single_reference_model_coefficients;
};

} // namespace decompression
} // namespace kernel_tests


#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_FRAME_OF_REFERENCE_H_ */
