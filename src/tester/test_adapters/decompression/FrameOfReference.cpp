#include "FrameOfReference.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>
#include <cuda/model_functors.hpp>


#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"

#include <boost/lexical_cast.hpp>

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <type_traits>

#include "kernel_wrappers/decompression/frame_of_reference.cu"

/**
 * @brief Perform a translation of a model function along
 * its parameter's axis
 *
 * @todo fugly code for now.
 *
 * @param umf the unary model function to translate
 * @param t the translation distance
 * @return  Give @p umf and @t , returns the function
 * x |-> umf(x+t) - that is, a model function for
 * inputs after a translation of t "backwards".
 */
template <unsigned IndexSize, typename T>
static cuda::functors::unary::parametric_model::linear<IndexSize, T> translate(
	const cuda::functors::unary::parametric_model::linear<IndexSize, T>& umf,
	T translation_amount)
{
	return cuda::functors::unary::parametric_model::linear<IndexSize, T>(
		{ umf(translation_amount), umf.model_coefficients[1] }
	);
}

template <unsigned IndexSize, typename T>
static cuda::functors::unary::parametric_model::constant<IndexSize, T> translate(
	const cuda::functors::unary::parametric_model::constant<IndexSize, T>& umf,
	T translation_amount)
{
	UNUSED(translation_amount);
	return umf;
}

template <unsigned IndexSize, typename T>
static cuda::functors::unary::parametric_model::zero<IndexSize, T> translate(
	const cuda::functors::unary::parametric_model::zero<IndexSize, T>& umf,
	T translation_amount)
{
	UNUSED(translation_amount);
	return umf;
}
/*
template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::linear<IndexSize, T>& umf,
	std::string name, std::string parameter, std::ostream& os)
{
	os << name << "(" << parameter << ") = " << umf.model_coefficients[1] << " * x + " << umf.model_coefficients[0];
}

template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::constant<IndexSize, T>& umf,
	std::string name, std::string parameter, std::ostream& os)
{
	os << name << "(" << parameter << ") = " << umf.model_coefficients[0];
}

template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::zero<IndexSize, T>& umf,
	std::string name, std::string parameter, std::ostream& os)
{
	os << name << "(" << parameter << ") = 0";
}


template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::linear<IndexSize, T>& umf,
	std::string parameter, std::ostream& os)
{
	os << umf.model_coefficients[1] << " * x + " << umf.model_coefficients[0];
}

template <unsigned IndexSize, typename T>
static void print_formula(
	const cuda::functors::unary::parametric_model::constant<IndexSize, T>& umf,
	std::string parameter, std::ostream& os)
{
	os << umf.model_coefficients[0];
}
*/


namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
std::string FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::buildPrettyKernelName() const
{
	return util::concat_string(
		"A FrameOfReference decompression kernel, with elements of type ",
		util::type_name<Uncompressed>(), " decompressed to elements of type ",
		util::type_name<Compressed>(), " using reference model functions "
		" of type ", util::type_name<UnaryModelFunction>(), ", constructed for each "
		"consecutive segment of fixed length, with indices having size ", IndexSize, " bytes)."
	);
}

template <
	unsigned IndexSize, typename Uncompressed,
	typename Compressed, typename UnaryModelFunction>
KernelTestAdapter::BufferDescriptors FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::getBufferDescriptors() const
{
	size_t num_segments =
		util::div_rounding_up(config.test_length, segment_length);
	auto total_num_model_coefficients = num_segments * model_dimension;
	return {
		{ "decompressed",                BufferDescriptor::make<Uncompressed>          (config.test_length,           BufferDirection::Output) },
		{ "compressed_input",            BufferDescriptor::make<Compressed>            (config.test_length,           BufferDirection::Input)  },
		{ "segment_model_coefficients",  BufferDescriptor::make<model_coefficient_type>(total_num_model_coefficients, BufferDirection::Input)  },
			// Note that we're "going down the levels of abstraction" here, since this buffer "actually" holds kat::array's of
			// model coefficients; but we don't have "reflection" support for that kind of type, so let's just go with the constituent
			// type, which is luckily uniform
	};
}

template<typename UnaryModelFunction>
static typename UnaryModelFunction::coefficients_type parse_model_coefficients(
	std::string comma_separated_coefficients,
	char coefficient_delimiter)
{
	typename UnaryModelFunction::coefficients_type result;
	using model_coefficient_type  = typename UnaryModelFunction::coefficient_type;
	enum { model_dimension = UnaryModelFunction::model_dimension };

	// TODO: Move defaults to the class definition somehow
	if  (comma_separated_coefficients.empty()) {
		model_coefficient_type default_coefficient_value =
			std::is_floating_point<model_coefficient_type>::value ? 1.789 : 1;
		std::fill(result.begin(), result.end(), default_coefficient_value);
	}
	else {
		auto coefficient_strings = util::explode(comma_separated_coefficients, coefficient_delimiter);
		util::enforce(coefficient_strings.size() == model_dimension,
			comma_separated_coefficients.size(), " coefficients were specified as a "
			"configuration option, but there must be exactly ", model_dimension,
			" coefficients for a model of type ", util::type_name<UnaryModelFunction>());
		std::transform(coefficient_strings.begin(), coefficient_strings.end(),
			result.begin(), boost::lexical_cast<model_coefficient_type, std::string>);
	}
	return result;
}

template <
unsigned IndexSize, typename Uncompressed,
typename Compressed, typename UnaryModelFunction>
inline FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>
	::FrameOfReference(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;

	size_t configured_segment_length = segment_length;
		// have to have this to avoid reading in a single character's ASCII
	resolveOption(
		configured_segment_length,
		ConfigurationKeySet {
			"segment-length", "period", "model-period",
			"model-segment-length", "single-model-segment-length"
			"modeling-period", "period-for-modeling"}
	);
	segment_length = configured_segment_length;

	resolveOption(compressed_input_uniformly_random,
		ConfigurationKeySet {
			"compressed-input-uniformly-random", "data-uniformly-random",
			"compressed-uniformly-random"});

	resolveOption(use_same_model_for_all_segments,
			ConfigurationKeySet {
				"use-same-model-for-all-segments", "same-model-for-all-segments",
				"same-model", "single-model", "repeating-model", "use-repeating-model",
				"use-single-model", "use-same-model"});

	bool use_all_ones_as_compressed_data = resolveOption(ConfigurationKeySet {
			"use-all-ones-as-input", "data-all-ones", "all-ones-data",
			"all-ones", "all-ones-input"}, false
	);

	resolveOption(use_continued_model_for_all_segments,
			ConfigurationKeySet {
				"use-continued-model-for-all-segments", "continued-model-for-all-segments",
				"continued-model", "long-model", "one-long-model", "use-continued-model"});

	bool compressed_input_uniformly_random_override = resolveOption(
		compressed_input_random_part,
		ConfigurationKeySet {
			"compressed-input-random-part",
			"compressed-input-random-part-distribution",
			"data-value-random-part",
			"data-value-random-part-distribution",
			"data-value-distribution",
			"compressed-random-part",
			"compressed-random-part-distribution",
			"compressed-data-distribution"}
	);

	if (compressed_input_uniformly_random_override) {
		compressed_input_uniformly_random = false;
	}

	resolveOption(
		compressed_input_random_contribution,
		ConfigurationKeySet {
		"compressed-input-random-contribution", "compressed-input-random-weight"}
	);

	resolveOption(
		compressed_input_model_contribution,
		ConfigurationKeySet {
		"compressed-input-model-contribution", "compressed-input-model-weight"}
	);

	resolveOption(
		segment_model_coefficient_random_contribution,
		ConfigurationKeySet {
		"segment-model-coefficient-random-contribution", "segment-model-coefficient-random-weight",
		"period-model-coefficient-random-contribution", "period-model-coefficient-random-weight",
	}
	);

	resolveOption(
		segment_model_coefficient_model_contribution,
		ConfigurationKeySet {
		"segment-model-coefficient-model-contribution", "segment-model-coefficient-model-weight",
		"period-model-coefficient-model-contribution", "period-model-coefficient-model-weight",
	}
	);

	resolveOption(
		model_coefficient_random_part, ConfigurationKeySet {
			"model-coefficient-random-part", "model-coefficient-random-part-distribution"
		}
	);

	resolveOption(coefficient_delimiter,
		ConfigurationKeySet { "coefficient_delimiter", "model-coefficient_delimiter"}
	);

	std::string delimited_coefficients;
	resolveOption(delimited_coefficients,
		ConfigurationKeySet { "single-reference-model-coefficients", "single-reference-model"}
	);
	single_reference_model_coefficients =
		parse_model_coefficients<UnaryModelFunction>(delimited_coefficients, coefficient_delimiter);

	resolveOption(delimited_coefficients,
		ConfigurationKeySet { "intra-segment-compressed-input-generation-model", "intra-segment-generation-model"}
	);
	model_coefficient_sets.intra_segment =
		parse_model_coefficients<UnaryModelFunction>(delimited_coefficients, coefficient_delimiter);

	for (model_dimensions_size_type d = 0; d < model_dimension; d++) {
		resolveOption(delimited_coefficients,
			ConfigurationKeySet { "model-parameter-" + std::to_string(d) + "-generation-model"}
		);
		model_coefficient_sets.per_parameter[d] =
			parse_model_coefficients<UnaryModelFunction>(delimited_coefficients, coefficient_delimiter);
	}

	util::enforce(!(use_all_ones_as_compressed_data && compressed_input_uniformly_random),
		"Cannot force compressed input to be both all-ones and uniformly-random");

	if (compressed_input_uniformly_random) {
		compressed_input_random_contribution = 1;
		compressed_input_model_contribution = 0;
		compressed_input_random_part = distribution_t<Compressed>::full_range_uniform();
				// What about the actual maximum value?
	}
	else if (use_all_ones_as_compressed_data) {
		compressed_input_random_contribution = 1;
		compressed_input_model_contribution = 0;
		compressed_input_random_part =
			distribution_t<Compressed>::single_value(1);
	}


		// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		// ... something re coefficients
		make_pair("Segment length (= modeling period)",                segment_length                      ),
		make_pair("Model function type",                               util::type_name<UnaryModelFunction>()),
		make_pair("Use a repeating model for all segments",            use_same_model_for_all_segments       ),
		make_pair("Use a continuing model across all segments",        use_continued_model_for_all_segments  ),
		make_pair("Generate uniformly random compressed data",         compressed_input_uniformly_random    ),
		make_pair("Compressed input generation - random distribution", compressed_input_random_part      ),
		make_pair("Compressed input generation - random factor contribution",
			compressed_input_random_contribution                                                         ),
		make_pair("Compressed input generation - model contribution",  compressed_input_model_contribution                                                          ),
		make_pair("Per-segment models coefficient generation - random factor contribution",
			segment_model_coefficient_random_contribution),
		make_pair("Per-segment models coefficient generation - model contribution",
			segment_model_coefficient_model_contribution),
		make_pair("Model coefficients' random part distribution",      model_coefficient_random_part),
		make_pair("Delimiter used to parse coefficient sequence",      util::concat_string('\'', coefficient_delimiter, '\'')),
		make_pair("Single all-data model coefficients",                single_reference_model_coefficients),
		make_pair("Single intra-segment model coefficients",           single_reference_model_coefficients),
		make_pair("Data generation intra-segment model coefficients",  single_reference_model_coefficients),
		make_pair("Segment model coefficients - model coefficients",  model_coefficient_sets.per_parameter)
	);

	return;
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
std::string FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::getDescription() const
{
	return
		"Decompression kernel for the Frame-of-Reference compression"
		"schemes (FOR, FORLIN, FORFUN): With this scheme, the uncompressed "
		"data is assumed to correspond relatively well to a certain model function: "
		"A constant value (in the case of simple FOR), a linear function (FORLIN), "
		"or a linear combination of several base functions (FORFUN; specifically, this "
		"covers polynomial models). The compressed value is the divergence between the "
		"value predicted or expected by the model, and the actual value; additionally, .";
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
LaunchConfigurationSequence FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		// not specifying the serialization factor
		{ "length",          config.test_length },
		{ "segment_length", (size_t) segment_length }
	};
	::cuda::kernels::decompression::frame_of_reference::kernel_t<
	 	 IndexSize, Uncompressed, Compressed, UnaryModelFunction> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("decompression::frame_of_reference::decompress");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
void FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::generate_compressed_input(
	span<Compressed> compressed)
{
	auto random_engine = this->random_engine;

	for(size_type i = 0; i < config.test_length; i++) {
		auto model_component = UnaryModelFunction(model_coefficient_sets.intra_segment)(i);
		auto random_component =
			util::random::sample_from(compressed_input_random_part, random_engine);
		auto combined =
			model_component * compressed_input_model_contribution +
			random_component * compressed_input_random_contribution;
		if (std::is_floating_point<Compressed>::value) {
			if (combined > std::numeric_limits<Compressed>::max()) {
				compressed[i] = std::fmod(combined , std::numeric_limits<Compressed>::max());
			}
			else if (combined < std::numeric_limits<Compressed>::min()) {
				// This should be ok semantically, perhaps re-check the reference guides
				compressed[i] = std::fmod(combined , std::numeric_limits<Compressed>::max());
			}
		}
		else {
			compressed[i] = combined; // % std::numeric_limits<Compressed>::max();
		}
	}
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
void FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::generate_model_function_parameters(
	span<model_coefficients_type> model_function_parameters)
{
	size_type num_segments = util::div_rounding_up(config.test_length, segment_length);

	auto random_engine = this->random_engine;

	// fill single model if it hasn't been filled

	if (use_same_model_for_all_segments) {
		fill(model_function_parameters, single_reference_model_coefficients);
		return;
	}
	for(size_type segment_index = 0; segment_index < num_segments; segment_index++) {
		if (use_continued_model_for_all_segments) {
			auto translated_model = translate<model_index_size, model_coefficient_type>(
				UnaryModelFunction(single_reference_model_coefficients), segment_index);
			model_function_parameters[segment_index] = translated_model.model_coefficients;
			continue;
		}
		model_coefficients_type segment_model_coefficients;
		for (model_dimensions_size_type d = 0; d < model_dimension; d++) {
			double modeled_component = UnaryModelFunction(model_coefficient_sets.per_parameter[d])(segment_index);
			double random_component =
				util::random::sample_from(model_coefficient_random_part, random_engine);
			// it might be a good idea to do:
			// random_component  = std::pow(random_component, 1.0 / d);
			// for parameters which are straight up polynomial coefficients,
			// or maybe even better,
			// random_component  = std::exp(std::log(std::pow(random_component) / d));
			segment_model_coefficients[d] =
				segment_model_coefficient_model_contribution * random_component +
				segment_model_coefficient_model_contribution * modeled_component;
		}
		model_function_parameters[segment_index] = segment_model_coefficients;
	}
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
void FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::generate_expected_decompressed(
	span<Uncompressed>                   decompressed,
	span<const Compressed>               compressed_input,
	span<const model_coefficients_type>  segment_model_coefficients)
{

	size_t num_segments = util::div_rounding_up(config.test_length, segment_length);
	for(size_type segment_index = 0; segment_index < num_segments; segment_index++) {
		UnaryModelFunction segment_model_function(segment_model_coefficients[segment_index]);
		index_type segment_start_position = ((index_type) segment_index) * segment_length;
		size_type this_segment_s_length =
			std::min((size_type) segment_length, (size_type) config.test_length - segment_start_position);
		for(size_type i = 0; i < this_segment_s_length; i++) {
			decompressed[segment_start_position + i] =
				static_cast<Uncompressed>(segment_model_function(i))
				+ compressed_input[i + segment_start_position];
		}
	}
}

template <typename T, typename Size = size_t>
struct histogram_entry { T value; Size count; };

template <typename T, typename Size = size_t>
static std::vector<struct histogram_entry<T, Size>> get_segment_histogram(span<const T> sorted_segment_data)
{
	std::vector<histogram_entry<T, Size>> result;
	result.reserve(sorted_segment_data.length());
	for(auto it = sorted_segment_data.begin(); it < sorted_segment_data.end(); ) {
		auto value = *it;
		auto next_value_it = util::find_next_value(it, sorted_segment_data.end());
		Size multiplicity = std::distance(it, next_value_it);
		result.push_back( histogram_entry<T, Size>{ value, multiplicity } );
		it = next_value_it;
	}
	return result;
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
static UnaryModelFunction determine_segment_model_function(
	span<const Uncompressed>             uncompressed_segment,
	bool                                 fail_on_invalid_uncompressed_data)
{
	using size_type = size_type_by_index_size<IndexSize>;
	if (not std::is_same<UnaryModelFunction, cuda::functors::unary::parametric_model::constant<IndexSize, Uncompressed>>::value) {
		util::invalid_argument("Compression not supported for non-constant models (Need an LP solver for polynomials");
	}

	typename UnaryModelFunction::coefficients_type segment_model_coefficients;
	if (fail_on_invalid_uncompressed_data) {
		// Since we've assumed a constant reference value, and since we can also assume
		// that the offset to any actual value will fit within a Compressed value,
		// we can act naively:
		Uncompressed min_value, max_value;
		std::tie(min_value, max_value) = util::extremal_values(uncompressed_segment);
		segment_model_coefficients[0] = std::is_unsigned<Compressed>::value ?
			min_value : (min_value + max_value) / 2 ;
		return UnaryModelFunction{segment_model_coefficients};
	}

	// Within the domain of Uncompressed, the domain of a Compressed offset value plus a FOR
	// value of our choosing is a 'window', described by the following:
	struct window_t {
		Uncompressed min, max;
		size_type num_elements_captured;
	};
	// Our best-effort at compression is to use the window which contains as many of the values
	// in the segment as is possible

	 // TODO: This gets repeatedly allocated and de-allocated... :-(
	std::vector<Uncompressed> segment_data_copy(uncompressed_segment.begin(), uncompressed_segment.end());
	sort(segment_data_copy);
	auto segment_histogram = get_segment_histogram<Uncompressed, size_type>(segment_data_copy);

	// This next loop uses the sortedness of segment_histograms to
	// find the best window w.r.t. coverage of segment element; the code
	// is a bit convoluted, but simplifying it would bring is to Omega(m^2) time
	// for m being the number of distinct elements in the segment

	optional<Uncompressed> best_window_start_value;
	size_type candidate_window_end_pos = 0;
	window_t best_window = {0, 0, segment_histogram[0].count };
	window_t candidate_window = best_window;
	candidate_window.num_elements_captured = 0;
	for(size_type window_start_pos = 0;
	    window_start_pos < segment_histogram.size() and
	    candidate_window_end_pos < segment_histogram.size();
	    candidate_window.num_elements_captured -= segment_histogram[window_start_pos].count,
	    window_start_pos++)
	{
		candidate_window.min = segment_histogram[window_start_pos].value;
		while (candidate_window_end_pos < segment_histogram.size() and
		       ((size_type) segment_histogram[candidate_window_end_pos + 1].value - candidate_window.min) <
		       util::capped_domain_size<Compressed>::value) {
			candidate_window_end_pos++;
		    candidate_window.num_elements_captured += segment_histogram[candidate_window_end_pos].count;
		}
		if (candidate_window.num_elements_captured > best_window.num_elements_captured) {
			best_window = candidate_window;
		}
	}

	segment_model_coefficients[0] = std::is_unsigned<Uncompressed>::value ?
		best_window.min : (best_window.min + best_window.max) / 2;

	return UnaryModelFunction{segment_model_coefficients};
}

// TODO: Split off the code for computing the models into an entirely separate function

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
static void compress(
	span<Compressed>          compressed,
	span<typename UnaryModelFunction::coefficients_type>
                              segment_model_coefficients,
	span<const Uncompressed>  uncompressed,
	uint_t<IndexSize>   segment_length,
	bool                      compute_models,
	bool                      fail_on_invalid_uncompressed_data,
	Compressed&               effective_minimum_difference,
	Compressed&               effective_maximum_difference,
	double&                   average_absolute_difference)
{
	using size_type = size_type_by_index_size<IndexSize>;
	auto num_segments = util::div_rounding_up(uncompressed.size(), segment_length) ;
	effective_minimum_difference = uncompressed[0];
	effective_maximum_difference = uncompressed[0];
	double total_absolute_difference = 0.0;
	size_type pos = 0;
	for (size_type segment_index = 0 ; segment_index < num_segments; segment_index++)
	{
		auto segment_start_pos = pos;
		auto this_segment_s_length = (segment_index == (num_segments - 1)) ?
			(uncompressed.size() % segment_length) : segment_length;
		UnaryModelFunction model_function = compute_models ?
			determine_segment_model_function<IndexSize, Uncompressed, Compressed, UnaryModelFunction>(
				span<const Uncompressed>(uncompressed.subspan(segment_start_pos, this_segment_s_length)),
				fail_on_invalid_uncompressed_data) :
			segment_model_coefficients[segment_index];
		auto segment_end_pos = segment_start_pos + this_segment_s_length;

		segment_model_coefficients[segment_index] = model_function.model_coefficients;
		for (; pos < segment_end_pos; pos++)
		{
			auto model_expected_value = model_function(pos);
			auto uncompressed_value = uncompressed[pos];
			auto difference = uncompressed[pos] - model_expected_value;
			total_absolute_difference +=
				uncompressed[pos] > model_expected_value ?
				uncompressed[pos] - model_expected_value :
				model_expected_value - uncompressed[pos];
			if (model_expected_value + difference != uncompressed_value and fail_on_invalid_uncompressed_data) {
				std::invalid_argument(util::concat_string(
					"Overflow: Could not derive a valid difference between model_estimate ",
					model_expected_value, " and the actual value to compress, ", uncompressed_value
				));
			}
			// if not fail_on_invalid_uncompressed_data - then we'll write in junk as the data;
			// it's probably going to be covered by a patch
			compressed[pos] = difference;
			util::update_min(effective_minimum_difference, difference);
			util::update_max(effective_maximum_difference, difference);
		}
	}
	average_absolute_difference = total_absolute_difference / uncompressed.length();
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
KernelTestAdapter::BufferSizes
FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		if (need_to_generate("compressed_input")) {
			auto compressed_input          = buffers.at("compressed_input"         ).as_span<Compressed>();
			generate_compressed_input(compressed_input);
		}
		if (need_to_generate("segment_model_coefficients")) {
			auto segment_model_coefficients = buffers.at("segment_model_coefficients").as_span<model_coefficients_type>();
			generate_model_function_parameters(segment_model_coefficients);
		}
	}
	else {
		bool do_compression = need_to_generate("compressed_input") ;
		bool compute_models = need_to_generate("segment_model_coefficients");
		if (not do_compression and not compute_models) { return NoBuffersToResize; }
		if (not do_compression and compute_models) {
			throw util::invalid_argument("Invalid combination of buffers to generate");
		}
		if (compute_models and not fail_on_invalid_uncompressed_data) {
			if (not std::is_same<UnaryModelFunction, cuda::functors::unary::parametric_model::constant<IndexSize, Uncompressed>>::value) {
				util::invalid_argument("Compression not supported for non-constant models (Need an LP solver for polynomials");
				// ... and also note that even with an LP solver, we would still be in trouble if
				// we need to compress data for which offsets don't fit into the Compressed type,
				// since those necessitate patches. We can resolve that for constant models pretty
				// easily, but, again, no code for higher degrees at the moment.
			}
		}
		auto uncompressed                = buffers.at("decompressed"               ).as_span<const Uncompressed>();
		auto compressed_input            = buffers.at("compressed_input"           ).as_span<Compressed>();
		auto segment_model_coefficients = buffers.at("segment_model_coefficients").as_span<model_coefficients_type>();
		Compressed min_diff, max_diff;
		double average_diff;
		compress<IndexSize, Uncompressed, Compressed, UnaryModelFunction>(
			compressed_input, segment_model_coefficients, uncompressed, segment_length,
			compute_models, fail_on_invalid_uncompressed_data, min_diff, max_diff, average_diff);
		if (config.be_verbose) {
			std::cout << "Compression complete. Data divergence from segment models: Minimum "
				<< min_diff << ", maximum " << max_diff << ", average " << std::dec
				<< std::setprecision(2) << std::fixed << average_diff << "\n";
		}
	}

	return NoBuffersToResize;
}

template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
KernelTestAdapter::BufferSizes FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto compressed_input          = buffers.at("compressed_input"        ).as_span<const Compressed>();
		auto segment_model_coefficients = buffers.at("segment_model_coefficients").as_span<const model_coefficients_type>();
		auto decompressed   = expected_buffers.at("decompressed").as_span<Uncompressed>();

		generate_expected_decompressed(decompressed, compressed_input, segment_model_coefficients);
	}
	return NoBuffersToResize;
}


template <unsigned IndexSize, typename Uncompressed, typename Compressed, typename UnaryModelFunction>
void FrameOfReference<IndexSize, Uncompressed, Compressed, UnaryModelFunction>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto decompressed              = buffers.at("decompressed"              ).as_span<Uncompressed>().data();
	auto compressed_input          = buffers.at("compressed_input"          ).as_span<const Compressed>().data();
	auto segment_model_coefficients = buffers.at("segment_model_coefficients").as_span<const model_coefficients_type>().data();

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;
	size_type length = config.test_length;
	cuda::kernels::decompression::frame_of_reference
		::kernel_t<IndexSize, Uncompressed, Compressed, UnaryModelFunction>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(decompressed, compressed_input, segment_model_coefficients, length, segment_length)
	);

}

static_block {
	namespace functors = cuda::functors;
	namespace unary_models = ::cuda::functors::unary::parametric_model;

	/*
	 * Note: to enable model functions with floating-point coefficients you must make sure
	 * the device-side (and host-side, for the test adapter) floating-point calculations
	 * are consistent. And NB that it's the _consistency_ rather than the _accuracy_ that matters,
	 * because all we need is for the compressor and the decompressor to having the same model-predicted
	 * value, regardless of what it is exactly.
	 *
	 * To do so, read about the IEEE rounding options for floating-point operation result rounding
	 * (nearest even, nearest odd, towards zero, towards infinity, down, up) and, on the device, use the
	 * __fmul_[rn,rz,ru,rd](x,y),   __fadd_[rn,rz,ru,rd](x,y) etc intrinsics
	 */

	//                 IndexSize  Uncompressed   Compressed   UnaryModelFuction
	//----------------------------------------------------------------------------------------------------------------------
	FrameOfReference < 4,         int16_t,       int8_t,      unary_models::linear  < 4, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 4,         int32_t,       int8_t,      unary_models::linear  < 4, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 4,         int32_t,       int16_t,     unary_models::linear  < 4, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 8,         int32_t,       int16_t,     unary_models::linear  < 4, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 8,         int32_t,       int16_t,     unary_models::linear  < 8, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 4,         int16_t,       int8_t,      unary_models::constant< 4, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 4,         int32_t,       int8_t,      unary_models::constant< 4, int32_t >  >::registerInSubclassFactory();
	FrameOfReference < 4,         int32_t,       int16_t,     unary_models::constant< 4, int32_t >  >::registerInSubclassFactory();
}

} // namespace decompression
} // namespace kernel_tests
