#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_DISCARD_ZERO_BYTES_FIXED_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_DISCARD_ZERO_BYTES_FIXED_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

/**
 * @note It's the unpatched, exceptions-not-allowed compression scheme; see also the
 * @ref PatchedDiscardZeroBytesFixed test adapter
 */
template <unsigned IndexSize, unsigned UncompressedSize, unsigned CompressedSize>
class DiscardZeroBytesFixed:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<DiscardZeroBytesFixed<IndexSize, UncompressedSize, CompressedSize>>
{
public: // types
	using index_type         = uint_t<IndexSize>;
	using size_type          = size_type_by_index_size<IndexSize>;
	using compressed_type    = uint_t<CompressedSize>;
	using uncompressed_type  = uint_t<UncompressedSize>;

protected:
	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(DiscardZeroBytesFixed)

protected:
	// In this method we actually generate (from scratch) rather than compress
	void generate_compressed_input(
		span<compressed_type>          compressed);
	// This method actually compresses, rather than generating compressed data; it is
	// guaranteed to only take the lower bytes of the input elements, regardless of
	// whether the higher bytes actually
	void compress(
		span<compressed_type>          compressed,
		span<const uncompressed_type>  decompressed);
	void generate_expected_decompressed(
		span<uncompressed_type>        decompressed,
		span<const compressed_type>    compressed);

public: // data members
	// This should really be protected and let the patched adapters be friends, but - expediency

	bool               compressed_input_uniformly_random     { false };
	bool               compressed_input_modular_iota         { true  };
	bool               use_all_ones_as_compressed_data       { false };
	distribution_t<compressed_type>
	                   compressed_input_random_part          { distribution_t<compressed_type>::uniform(0, 9) };
	double             compressed_input_random_contribution  {   0.9 };
	double             compressed_input_linear_contribution  {   0.1 };
	uncompressed_type  compressed_input_linear_scale         {    10 };
	uncompressed_type  compressed_input_linear_offset        {    20 };
};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_DISCARD_ZERO_BYTES_FIXED_H_ */
