#pragma once
#ifndef SRC_TEST_ADAPTERS_DECOMPRESSION_RUN_LENGTH_ENCODING_H_
#define SRC_TEST_ADAPTERS_DECOMPRESSION_RUN_LENGTH_ENCODING_H_

#include "tester/test_adapters/decompression/common.h"

namespace kernel_tests {
namespace decompression {

template <unsigned UncompressedIndexSize, unsigned UncompressedSize, unsigned RunLengthSize, unsigned RunIndexSize = UncompressedIndexSize>
class RunLengthEncoding:
	public KernelTestAdapter,
	public kernel_tests::mixins::TestAdapterNameBuilder<RunLengthEncoding<UncompressedIndexSize, UncompressedSize, RunLengthSize>>
{
	static_assert(UncompressedIndexSize >= RunLengthSize,
		"Run lengths cannot have size larger than the size of an input index");
	static_assert(util::is_power_of_2(UncompressedIndexSize), "UncompressedIndexSize is not a power of 2");
	static_assert(util::is_power_of_2(RunIndexSize         ), "RunIndexSize is not a power of 2");
	static_assert(util::is_power_of_2(RunLengthSize        ), "RunLengthSize is not a power of 2");

public: // types
	using uncompressed_index_type         = uint_t<UncompressedIndexSize>;
	using uncompressed_index_size_type    = size_type_by_index_size<UncompressedIndexSize>;
	using index_type                      = uncompressed_index_type;
	using size_type                       = uncompressed_index_size_type;
	using uncompressed_type               = uint_t<UncompressedSize>;
	using run_length_type                 = uint_t<RunLengthSize>;
	using run_index_type                  = uint_t<RunIndexSize>;
	using run_index_size_type             = size_type_by_index_size<RunIndexSize>;

protected:
	DECOMPRESSION_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(RunLengthEncoding)

protected:
	void generate_run_data(
		span<uncompressed_type>         run_data);
	void generate_run_lengths_and_anchors(
		span<run_length_type>           run_lengths,
		span<run_index_type>            position_anchors,
		span<run_length_type>           intra_run_anchor_offsets
	);
	void generate_expected_decompressed(
		span<uncompressed_type>        decompressed,
		span<const uncompressed_type>  run_data,
		span<const run_length_type>    run_lengths,
		span<const run_index_type>     position_anchors,
		span<const run_length_type>    intra_run_anchor_offset
	) const;

protected:
	// TODO: Make this optional
	run_index_size_type  num_element_runs;
		// ... not to be confused with num_test_runs, which is the number of
		// runs the kernel test will be conducted by the tester binary;
		// this value is used when generating synthetic compressed data (
		// not when compressing input data read from a DB)

	uncompressed_index_size_type       segment_length                         { 1024    };
		// Values which are not a power of 2 are less well tested

	distribution_t<run_length_type> random_run_length       { distribution_t<run_length_type>::geometric(10.0, 1) };
		// ... Note that the distribution is capped, so it's not really
		// geometric, i.e. the probabilities for 0 and 1 are added up and are
		// the probability of 1.


	// Note: We can't "use all ones" as the run data, but we can do something similar:
	bool             use_identity_for_run_data               { false   };
	bool             run_data_uniformly_random               { false   };
	distribution_t<uncompressed_type> random_datum           { distribution_t<uncompressed_type>::uniform(11, 99) };
	double           run_data_random_contribution            { 0.9     };
	double           run_data_linear_contribution            { 0.1     };
	run_length_type  run_data_linear_scale                   { 10      };
	run_length_type  run_data_linear_offset                  { 20      };
};

} // namespace decompression
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_DECOMPRESSION_RUN_LENGTH_ENCODING_H_ */
