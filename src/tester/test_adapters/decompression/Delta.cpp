
#include "Delta.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include "util/type_name.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/math.hpp"
#include "util/poor_mans_reflection.h"

#include <algorithm>
#include <map>
#include <unordered_set>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "kernel_wrappers/decompression/delta.cu"

namespace kernel_tests {
namespace decompression {

template <unsigned IndexSize, typename Uncompressed, typename Difference>
std::string Delta<IndexSize, Uncompressed, Difference>::buildPrettyKernelName() const
{
	return util::concat_string(
		"Decompression kernel for the DELTA (FOR-DELTA) scheme, with "
		"difference elements of type ", util::type_name<Difference>(), " and uncompressed "
		"elements of size", util::type_name<Uncompressed>(), "."
	);
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
std::string Delta<IndexSize, Uncompressed, Difference>::getDescription() const
{
	return
		"A decompression kernel for the (unpatched) DELTA (FOR-DELTA) scheme. This scheme is used "
		"when consecutive elements in the input data are relatively close to each other; it then "
		"makes sense to store their differences rather than their actual values, and retrieve an "
		"element's value as the prefix sum of preceding elements, starting from some base value.";
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
KernelTestAdapter::BufferDescriptors Delta<IndexSize, Uncompressed, Difference>::getBufferDescriptors() const
{
	size_t num_segments =
		util::div_rounding_up(config.test_length, segment_length);
	return {
		{ "decompressed",     BufferDescriptor::make<uncompressed_type>(config.test_length, BufferDirection::Output) },
		{ "compressed_input", BufferDescriptor::make<compressed_type>  (config.test_length, BufferDirection::Input)  },
		{ "anchor_values",    BufferDescriptor::make<uncompressed_type>(num_segments,       BufferDirection::Input)  },
	};
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
inline Delta<IndexSize, Uncompressed, Difference>::Delta(const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	using std::make_pair;

	resolveOption(
		initial_anchor_value,
		ConfigurationKeySet {
			"base-value", "initial-value", "frame-of-reference",
			"frame-of-reference-value", "for-value", "offset"}
	);

	resolveOption(
		segment_length,
		ConfigurationKeySet {
			"segment-length", "anchoring-period", "baselining-period" }
	);

	resolveOption(
		compressed_input_random_contribution,
		ConfigurationKeySet {
		"compressed-input-random", "compressed-input-random-contribution",
		"compressed-input-random-date"}
	);
	resolveOption(
		compressed_input_random_contribution_factor,
		ConfigurationKeySet {
			"compressed-input-random-contribution", "data-value-random-contribution",
			"compressed-random-contribution", "data-random-contribution"}
	);
	resolveOption(
		compressed_input_linear_scale,
		ConfigurationKeySet {
			"compressed-input-linear-scale", "data-value-linear-scale",
			"compressed-linear-scale", "data-linear-scale"}
	);
	resolveOption(
		compressed_input_linear_offset,
		ConfigurationKeySet {
			"compressed-input-linear-offset", "data-value-linear-offset",
			"compressed-linear-offset", "data-linear-offset"}
	);
	resolveOption(
		compressed_input_linear_contribution_factor,
		ConfigurationKeySet {
			"compressed-input-linear-contribution", "data-value-linear-contribution",
			"compressed-linear-contribution", "data-linear-contribution"}
	);

	resolveOption(use_all_ones_as_compressed_data,
		ConfigurationKeySet {
			"use-all-ones-as-input", "data-all-ones", "all-ones-data",
			"all-ones", "all-ones-input"});

		// TODO: Move this code "up" to main.cu somehow
	maybe_print_extra_configuration(
		make_pair("Baselining period",                               segment_length                       ),
		make_pair("Initial base value (reference value)",            initial_anchor_value                 ),
		make_pair("Set all compressed data to 1",                    use_all_ones_as_compressed_data      ),
		make_pair("Generate uniformly random compressed data",       compressed_input_uniformly_random    ),
		make_pair("Difference input generation random contribution", compressed_input_random_contribution ),
		make_pair("Difference input generation - random factor contribution",
			                                              compressed_input_random_contribution_factor     ),
		make_pair("Difference input generation - linear factor contribution",
			                                              compressed_input_linear_contribution_factor     ),
		make_pair("Difference input generation - linear scale",      compressed_input_linear_scale        ),
		make_pair("Difference input generation - linear offset",     compressed_input_linear_offset       )
	);

	// TODO: Perhaps do this before the printing?
	if (compressed_input_uniformly_random) {
		compressed_input_linear_contribution_factor = 0;
		compressed_input_linear_offset = 0;
		compressed_input_linear_scale = 0;
		compressed_input_random_contribution_factor = 1;
		compressed_input_random_contribution = distribution_t<compressed_type>::uniform();
	}

	return;
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
LaunchConfigurationSequence Delta<IndexSize, Uncompressed, Difference>
	::resolveLaunchConfigurations() const
{
	auto device_properties = cuda::device::get(config.test_device).properties();
	cuda::kernel_t::arguments_type extra_args = {
		{ "length",            config.test_length },
		{ "segment_length", (size_t) segment_length },
	};

	::cuda::kernels::decompression::delta::kernel_t<IndexSize, Uncompressed, Difference> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("decompression::delta::decomrpess");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
void Delta<IndexSize, Uncompressed, Difference>::generate_compressed_input(
	span<compressed_type>          compressed_input)
{
	static_assert(! std::is_floating_point<compressed_type>::value,
		"Delta compression of floating-point data is unuspported.");

	if (use_all_ones_as_compressed_data) {
		fill(compressed_input, 1);
		return;
	}

	auto random_engine = this->random_engine;

	for(index_type i = 0; i < config.test_length; i++) {
		auto linear_component =
			compressed_input_linear_scale * i + compressed_input_linear_offset;
		compressed_type random_component =
			util::random::sample_from(compressed_input_random_contribution, random_engine);
		compressed_type combined =
			linear_component * compressed_input_linear_contribution_factor +
			random_component * compressed_input_random_contribution_factor;
			compressed_input[i] = combined; // % std::numeric_limits<compressed_type>::max();
	}
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
void Delta<IndexSize, Uncompressed, Difference>::generate_anchor_values(
	span<uncompressed_type>        anchor_values,
	span<const compressed_type>    compressed_input)
{
	anchor_values[0] = initial_anchor_value;
	auto num_segments = util::div_rounding_up(config.test_length, segment_length);
	uncompressed_type inclusive_prefix_sum = initial_anchor_value;
	for(index_type segment_index = 0; segment_index + 1 < num_segments; segment_index++) {
		for(size_t pos = segment_index * segment_length;
			pos < (segment_index+1) * segment_length; pos++)
		{
			inclusive_prefix_sum += compressed_input[pos];
		}
		anchor_values[segment_index+1] = inclusive_prefix_sum;
	}
}


template <unsigned IndexSize, typename Uncompressed, typename Difference>
void Delta<IndexSize, Uncompressed, Difference>::generate_expected_decompressed(
	span<uncompressed_type>        decompressed,
	span<const compressed_type>    compressed_input,
	span<const uncompressed_type>  anchor_values)
{
	auto num_segments = util::div_rounding_up(config.test_length, segment_length);
	for(index_type segment_index = 0; segment_index < num_segments; segment_index++) {
		auto running_value = anchor_values[segment_index];
		auto segment_end_pos = std::min((index_type) config.test_length, (segment_index+1) * segment_length);
		for(index_type pos = segment_index * segment_length; pos < segment_end_pos; pos++) {
			running_value += compressed_input[pos];
			decompressed[pos] = running_value;
		}
	}
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
static void compress(
	span<Difference>          compressed,
	span<Uncompressed>        anchor_values,
	span<const Uncompressed>  uncompressed,
	uint_t<IndexSize>         segment_length)
{
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using compressed_type = Difference;

	size_type num_elements = uncompressed.size();
	util::enforce(compressed.size() == num_elements);
	// Note: completely ignoring the possibility of floating-point or other
	// error-accumulating types
	index_type segment_index = 0;
	compressed[0] = 0;
	anchor_values[0] = uncompressed[0];
	index_type intra_segment_pos = 1;
	for(size_type pos = 1; pos < num_elements; pos++, intra_segment_pos++) {
		compressed[pos] = uncompressed[pos] - uncompressed[pos-1];
		if (intra_segment_pos == segment_length) {
			// Note: We're not ignoring  uncompressed[pos-1] here in favor
			// of just using uncompressed[pos] as the baseline/anchor value;
			// that interferes with all sorts of potentially useful features
			// of the sequence of delta (for example, if the uncompressed
			// data is linear, the delta will be constant)
			anchor_values[++segment_index] = uncompressed[pos-1];
			intra_segment_pos = 0;
			continue;
		}

		if (uncompressed[pos-1] + compressed[pos] != uncompressed[pos]) {
			throw util::invalid_argument(util::concat_string(
				"Differences between consecutive uncompressed elements exceed the representable range: We ",
				"have apriori decompressed[", pos-1, "] = ", uncompressed[pos-1], " and decompressed[",
				pos, "] = ", uncompressed[pos], ", with difference ", uncompressed[pos] - uncompressed[pos-1],
				" - while the range of differences representable with type ", util::type_name<Difference>(),
				" is ", util::promote_for_streaming(std::numeric_limits<compressed_type>::min()), "...",
				util::promote_for_streaming(std::numeric_limits<compressed_type>::max())));
		}

	}
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
KernelTestAdapter::BufferSizes Delta<IndexSize, Uncompressed, Difference>::generateInputs(const HostBuffers& buffers)
{
	auto compressed = buffers.at("compressed_input").as_span<compressed_type>();
	if (need_to_generate("decompressed")) {
		if (need_to_generate("compressed_input")) {
			generate_compressed_input(compressed);
		}
		if (need_to_generate("anchor_values")) {
			auto anchor_values  = buffers.at("anchor_values" ).as_span<uncompressed_type>();
			generate_anchor_values(anchor_values, compressed);
		}
	}
	else {
		util::enforce(
			need_to_generate("compressed_input") and need_to_generate("anchor_values"),
			"Invalid combination of buffers to generate");
		// Need to compress...

		auto anchor_values  = buffers.at("anchor_values" ).as_span<uncompressed_type>();
		auto decompressed = buffers.at("decompressed").as_span<const uncompressed_type>();

		compress<IndexSize, Uncompressed, Difference>(
			compressed, anchor_values, decompressed, segment_length);
	}

	if (config.be_verbose) {
		auto min_diff = *min_element(compressed);
		auto max_diff = *max_element(compressed);
		std::cout << "Range of delta (= compressed) values: "
			<< util::promote_for_streaming(min_diff) << " .. "
			<< util::promote_for_streaming(max_diff) << "\n";
	}

	return NoBuffersToResize;
}

template <unsigned IndexSize, typename Uncompressed, typename Difference>
KernelTestAdapter::BufferSizes Delta<IndexSize, Uncompressed, Difference>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("decompressed")) {
		auto compressed_input = buffers.at("compressed_input").as_span<const compressed_type>();
		auto anchor_values  = buffers.at("anchor_values"   ).as_span<const uncompressed_type>();
		auto decompressed = expected_buffers.at("decompressed").as_span<uncompressed_type>();

		generate_expected_decompressed(decompressed, compressed_input, anchor_values);
	}
	return NoBuffersToResize;
}


template <unsigned IndexSize, typename Uncompressed, typename Difference>
void Delta<IndexSize, Uncompressed, Difference>::launchKernels(
	const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto decompressed     = buffers.at("decompressed"    ).as_span<uncompressed_type      >().data();
	auto compressed_input = buffers.at("compressed_input").as_span<const compressed_type  >().data();
	auto anchor_values    = buffers.at("anchor_values"   ).as_span<const uncompressed_type>().data();
	size_type length = config.test_length;

	auto launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::decompression::delta::kernel_t<IndexSize, Uncompressed, Difference>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(decompressed, compressed_input, anchor_values, length, segment_length)
	);
}

static_block {
	//      IndexSize   Uncompressed   Difference
	// ----------------------------------------------------
	Delta < 4,          uint_t<2>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 4,          uint_t<4>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 4,          uint_t<4>,     sint_t<2> >::registerInSubclassFactory();
	Delta < 4,          uint_t<8>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 4,          uint_t<8>,     sint_t<2> >::registerInSubclassFactory();
	Delta < 4,          uint_t<8>,     sint_t<4> >::registerInSubclassFactory();

	Delta < 4,          sint_t<2>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 4,          sint_t<4>,     sint_t<2> >::registerInSubclassFactory();
	Delta < 4,          sint_t<2>,     uint_t<1> >::registerInSubclassFactory();
	Delta < 4,          sint_t<4>,     uint_t<2> >::registerInSubclassFactory();
	Delta < 4,          uint_t<2>,     uint_t<1> >::registerInSubclassFactory();
	Delta < 4,          uint_t<4>,     uint_t<2> >::registerInSubclassFactory();

	Delta < 8,          uint_t<2>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 8,          uint_t<4>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 8,          uint_t<4>,     sint_t<2> >::registerInSubclassFactory();
	Delta < 8,          uint_t<8>,     sint_t<1> >::registerInSubclassFactory();
	Delta < 8,          uint_t<8>,     sint_t<2> >::registerInSubclassFactory();
	Delta < 8,          uint_t<8>,     sint_t<4> >::registerInSubclassFactory();

}

} // namespace decompression
} // namespace kernel_tests
