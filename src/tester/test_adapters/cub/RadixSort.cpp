
#include "RadixSort.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/miscellany.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/random.h"
#include "util/type_name.hpp"
#include "util/stl_algorithms.hpp"
#include "tester/test_adapters/comparators.h"
#include "tester/parallel_cpu_sort.hpp"

#include "kernel_wrappers/cub.h"

#include <boost/range/irange.hpp>

#include <algorithm>
#include <iostream>
#include <vector>

namespace kernel_tests {
namespace cub {

template <typename Datum>
KernelTestAdapter::BufferDescriptors RadixSort<Datum>::getBufferDescriptors() const
{
	// Note: If we ever want to sort data whose comparison is not bitwise
	// we will need a custom match checker here

	KernelTestAdapter::BufferDescriptors descriptors = {
		{ "sorted",   BufferDescriptor::make<Datum  >(config.test_length, BufferDirection::Output ) },
		{ "unsorted", BufferDescriptor::make<Datum  >(config.test_length, BufferDirection::Input  ) },
	};
	return descriptors;
}

template <typename Datum>
void RadixSort<Datum>::generate_unsorted(
	span<Datum>                   buffer_to_fill)
{
	auto random_engine = this->random_engine;

	if (data_generation_option._to_integral() == data_generation_option_t::sampling) {
		auto distribution = this->data_distribution;

		generate(
			buffer_to_fill,
			[&distribution, &random_engine]() {
				return util::random::sample_from(distribution, random_engine);
			}
		);
	}
	else { // permute
		if (element_multiplicity.is_degenerate() ) {
			iota(buffer_to_fill, 0);
				// this->random_engine);
			return;
		}
		else {
			std::vector<Datum> vec(config.test_length);
			iota(vec, 0);
			std::random_shuffle(vec.begin(), vec.end()); // Yes, I know, I should use std::shuffle
			size_t pos = 0;
			for(size_t i = 0; i < config.test_length && pos < config.test_length; i++) {
				auto multiplicity = std::min<size_t>(
					util::random::sample_from(element_multiplicity, random_engine),
					config.test_length - pos);
				std::fill_n(buffer_to_fill.begin() + pos, multiplicity, vec[i]);
				pos += multiplicity;
			}
		}
		std::random_shuffle(buffer_to_fill.begin(), buffer_to_fill.end());
//		shuffle(buffer_to_fill, std::mt19937(this->random_device));
	}
}

template <typename Datum>
void RadixSort<Datum>::generate_expected_sorted_data(
	span<Datum>                   sorted,
	span<const Datum>             unsorted)
{
	copy(unsorted, sorted.begin());
/*
	auto comparator = ascending ?
		[&](Datum lhs, Datum rhs) { return lhs < rhs; } :
		[&](Datum lhs, Datum rhs) { return lhs > rhs; };
*/

	if (ascending) {
		kernel_tests::sort<decltype(sorted), std::less<Datum>>(sorted);
	}
	else {
		kernel_tests::sort<decltype(sorted), std::greater<Datum>>(sorted);
	}
}

template<typename Datum>
LaunchConfigurationSequence RadixSort<Datum>::resolveLaunchConfigurations() const
{
	// CUB does its own launch config and doesn't even ensure it's just a single kernel that's run
	throw util::logic_error("unsupported");
}

template <typename Datum>
size_t RadixSort<Datum>::getRequiredScratchSize() const
{
	return ::cub::radix_sort::get_scratch_size<Datum>(config.test_length);
}

// TODO: Have either Buffer.h or some other .h/.hpp have named constructor idioms -
// for constants, arithmetic progressions, geometric progressions, and
// any lambda

template <typename Datum>
KernelTestAdapter::BufferSizes RadixSort<Datum>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("unsorted")) {
		auto unsorted = buffers.at("unsorted").as_span<Datum>();

		generate_unsorted(unsorted);
	}
	return NoBuffersToResize;
}

template <typename Datum>
KernelTestAdapter::BufferSizes RadixSort<Datum>::generateExpectedOutputs(
	const HostBuffers& expected_buffers, const HostBuffers& buffers)
{
	if (need_to_generate("sorted")) {
		auto unsorted = buffers.at("unsorted").as_span<const Datum>();
		auto sorted = expected_buffers.at("sorted").as_span<Datum>();
		generate_expected_sorted_data(sorted, unsorted);
	}
	return NoBuffersToResize;
}


template <typename Datum>
void RadixSort<Datum>::launchKernels(
	const DeviceBuffers&                 buffers,
	typename DeviceBuffers::mapped_type  device_scratch_area,
	cuda::stream_t&                      stream) const
{
	util::enforce(!device_scratch_area.empty(),
		"A device-side scratch area is necessary, but has not been provided"); // TODO: check the size here?

	auto sorted    = buffers.at("sorted"  ).as_span<Datum      >();
	auto unsorted  = buffers.at("unsorted").as_span<const Datum>();

	size_t num_items = config.test_length;

	if (config.be_verbose) {
		std::cout << "Calling cub::DeviceRadixSort::SortKeys().\n";
	}
	::cub::radix_sort::sort<Datum>(
		device_scratch_area.data(),
		device_scratch_area.size(),
		sorted.data(),
		unsorted.data(),
		num_items,
		stream,
		ascending);

}

template <typename Datum>
std::string RadixSort<Datum>::buildPrettyKernelName() const
{
	return util::concat_string("Sort data of type ", util::type_name<Datum>(),
			"using libcub's Radix Sort device-wide primitive");
}

template <typename Datum>
std::string RadixSort<Datum>::getDescription() const {
	return
		"DeviceRadixSort::SoryKeys from LibCUB. Sorts an array of integral data "
		"using a Radix sort approach (stable-sorting by subspaces of the key space, e.g."
		"by digits)\n"
		"\n"
		"For more details on CUB, see: https://nvlabs.github.io/cub/\n"
		"For details on CUB's DeviceRadixSort primitive, see: "
		"https://nvlabs.github.io/cub/structcub_1_1_device_radix_sort.html";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<typename Datum>
RadixSort<Datum>::RadixSort(const TestConfiguration& config)
	: KernelTestAdapter(config)
{
	auto maybe_data_distribution = resolveOption<decltype(data_distribution)>(ConfigurationKeySet { "key-distribution"} );
	data_distribution = maybe_data_distribution.value_or(distribution_t<Datum>::uniform(0, config.test_length * 2));
	resolveOption(ascending, ConfigurationKeySet { "ascending" } );
	auto ascending_option_str = resolveOption<std::string>(
		ConfigurationKeySet{"sort-ascending", "ascending", "ascending-sort-order"}
	);
	if (ascending_option_str) {
		data_generation_option_t::_from_string(ascending_option_str.value().c_str());
	}
	auto maybe_element_multiplicity =
		(data_generation_option._to_integral() == data_generation_option_t::permute) ?
			optional<distribution_t<size_t>>(element_multiplicity) : nullopt;
	KernelTestAdapter::maybe_print_extra_configuration(
		make_pair("Data (sort key) distribution",      data_distribution),
		make_pair("Sort in ascending order",           ascending),
		make_pair("Data generation option",            data_generation_option._to_string()),
		make_pair("Element multiplicity distribution", maybe_element_multiplicity)
	);
}

static_block {
	 RadixSort<unsigned char>::registerInSubclassFactory();
	 RadixSort<unsigned short>::registerInSubclassFactory();
	 RadixSort<unsigned>::registerInSubclassFactory();
	 RadixSort<unsigned long>::registerInSubclassFactory();
	 RadixSort<unsigned long long>::registerInSubclassFactory();
}

} // namespace cub
} // namespace kernel_tests


