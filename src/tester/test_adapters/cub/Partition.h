#ifndef TEST_ADAPTERS_CUB_PARTITION_H_
#define TEST_ADAPTERS_CUB_PARTITION_H_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace cub {

// TODO: Need to implement a mismatch checker
// for the partitioned_data array which either sorts it
// or puts data in two multisets, to allow for partition
// instability
template <typename SelectionOp>
class Partition : public KernelTestAdapter, public mixins::TestAdapterNameBuilder<Partition<SelectionOp>> {
public:
	using Datum = typename SelectionOp::result_type;
	using input_size_t = uint_t<4>; // Can I get this from elsewhere? I don't want to depend on cub.cu here...
	static_assert(std::is_same<
		typename SelectionOp::result_type,
		typename SelectionOp::argument_type>::value,
		"Attempt to use a selection operator with different input and result types");

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Partition);

	size_t getRequiredScratchSize() const override final;
	virtual bool canReportLaunchDetails() const override { return false; }

protected:
	// Too bad we can't put namespaces within class
	void generate_data(span<Datum> data);
	// This is actually just a scalar, but we don't support those at the moment
	void generate_num_selected(span<input_size_t> num_selected);
	void generate_expected_partitioned_data(
		span<Datum>         partitioned_data,
		span<const Datum>   data);

protected:
	enum {default_range_size = 100};

	distribution_t<Datum> data_distribution {
		distribution_t<Datum>::uniform(
			std::is_unsigned<Datum>::value ? 0                      : -default_range_size / 2,
			std::is_unsigned<Datum>::value ? default_range_size - 1 :  default_range_size / 2
		)
	};

};

} // namespace cub
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_CUB_PARTITION_H_ */
