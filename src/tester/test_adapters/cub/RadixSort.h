#ifndef TEST_ADAPTERS_CUB_SORT_H_
#define TEST_ADAPTERS_CUB_SORT_H_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace cub {

BETTER_ENUM(data_generation_option_t, uint8_t, permute, sampling );

template <typename Datum>
class RadixSort : public KernelTestAdapter, public mixins::TestAdapterNameBuilder<RadixSort<Datum>> {
public:

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(RadixSort);

	size_t getRequiredScratchSize() const override final;
	virtual bool canReportLaunchDetails() const override { return false; }

protected:
	void generate_unsorted(
		span<Datum>                   buffer_to_fill);
	void generate_expected_sorted_data(
		span<Datum>                   sorted,
		span<const Datum>             unsorted);

protected:
	distribution_t<Datum> data_distribution { distribution_t<Datum>::uniform() };

	bool  ascending           { true };
//	bool  unique_keys         { true };
	data_generation_option_t data_generation_option { data_generation_option_t::sampling };
	distribution_t<size_t> element_multiplicity { distribution_t<size_t>::geometric(5) };
};

} // namespace cub
} // namespace kernel_tests

#endif /* TEST_ADAPTERS_CUB_SORT_H_ */
