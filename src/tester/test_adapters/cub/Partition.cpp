
#include "Partition.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/miscellany.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/random.h"
#include "util/type_name.hpp"
#include "tester/test_adapters/comparators.h"

#include "kernel_wrappers/cub.h"

#include <algorithm>
#include <iostream>

namespace kernel_tests {
namespace cub {

template <typename SelectionOp>
KernelTestAdapter::BufferDescriptors Partition<SelectionOp>::getBufferDescriptors() const
{
	BufferDescriptor::MatchChecker match_checker = &comparators::cub_partitioned_data<SelectionOp>;
	// Not using any scratch for a reduce - although we theoretically could have
	// auto scratch_length = cuda::device::get(config.test_device).properties().maxThreadsPerBlock;
	return {
		{ "partitioned_data", BufferDescriptor::make<Datum        >(config.test_length, BufferDirection::Output, match_checker) },
		{ "data",             BufferDescriptor::make<Datum        >(config.test_length, BufferDirection::Input                ) },
		{ "num_selected",     BufferDescriptor::make<input_size_t >(1,                  BufferDirection::Output               ) },
	};
}

template <typename SelectionOp>
void Partition<SelectionOp>::generate_data(span<Datum> buffer_to_fill)
{
	auto random_engine = this->random_engine;
	auto data_distribution = this->data_distribution;

	std::generate(
		buffer_to_fill.begin(), buffer_to_fill.end(),
		[&data_distribution, &random_engine]() {
			return util::random::sample_from(data_distribution, random_engine);
		}
	);
}

template <typename SelectionOp>
void Partition<SelectionOp>::generate_expected_partitioned_data(
	span<Datum>         partitioned_data,
	span<const Datum>   data)
{
	// TODO: Try the parallelism TS partition!

	// Note the following requires SelectionOp to have a host-side as well as a
	// device-side implementation
	copy(data, partitioned_data.begin());
	std::partition(partitioned_data.begin(), partitioned_data.end(), SelectionOp());
}


// TODO: Have either Buffer.h or some other .h/.hpp have named constructor idioms -
// for constants, arithmetic progressions, geometric progressions, and
// any lambda
template <typename SelectionOp>
KernelTestAdapter::BufferSizes Partition<SelectionOp>::generateInputs(const HostBuffers& buffers)
{
	if (need_to_generate("data")) {
		auto data   = buffers.at("data").as_span<Datum>();

		generate_data(data);
	}
	return NoBuffersToResize;
}

template <typename SelectionOp>
KernelTestAdapter::BufferSizes Partition<SelectionOp>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("partitioned_data")) {
		auto data             = buffers.at("data").as_span<const Datum>();

		auto num_selected     = expected_buffers.at("num_selected"    ).as_span<input_size_t>();
		auto partitioned_data = expected_buffers.at("partitioned_data").as_span<Datum       >();

		generate_expected_partitioned_data(partitioned_data, data);
		// Note the following requires SelectionOp to have a host-side as well as a
		// device-side implementation
		num_selected[0] = count_if(data, SelectionOp());
	}
	return NoBuffersToResize;
}

template<typename SelectionOp>
LaunchConfigurationSequence Partition<SelectionOp>::resolveLaunchConfigurations() const
{
	// CUB does its own launch config and doesn't even ensure it's just a single kernel that's run
	throw util::logic_error("unsupported");
}

template <typename SelectionOp>
size_t Partition<SelectionOp>::getRequiredScratchSize() const
{
	return ::cub::device_partition::get_scratch_size<SelectionOp>(config.test_length);
}

template <typename SelectionOp>
void Partition<SelectionOp>::launchKernels(
	const DeviceBuffers&                 buffers,
	typename DeviceBuffers::mapped_type  device_scratch_area,
	cuda::stream_t&                      stream) const
{
	util::enforce(not device_scratch_area.empty(),
		"A device-side scratch area is necessary, but has not been provided"); // TODO: check the size here?

	auto data              = buffers.at("data"            ).as_span<const Datum  >();
	auto partitioned_data  = buffers.at("partitioned_data").as_span<Datum        >();
	auto num_selected      = buffers.at("num_selected"    ).as_span<input_size_t >();

	util::enforce(partitioned_data.size() == config.test_length);
	cuda::memory::device::zero(partitioned_data.begin(), partitioned_data.size() * sizeof(Datum));
	cuda::memory::device::zero(num_selected.begin(), 1);

	size_t scratch_size = getRequiredScratchSize();
	cuda::memory::device::zero(device_scratch_area.data(), scratch_size);
	input_size_t num_items = config.test_length;

	if (config.be_verbose) {
		std::cout << "Calling cub::DevicePartition::If().\n";
	}
	::cub::device_partition::partition<SelectionOp>(
		device_scratch_area.data(),
		scratch_size,
		data.data(),
		partitioned_data.data(),
		num_selected.data(),
		num_items,
		stream);
}

template <typename SelectionOp>
std::string Partition<SelectionOp>::buildPrettyKernelName() const
{
	return
		"write me.";
}

template <typename SelectionOp>
std::string Partition<SelectionOp>::getDescription() const {
	return
		"DevicePartition::If from LibCUB. Moves all 'selected' elements (those elements "
		"meeting a unary predicate, the 'selection' predicate) to the beginning of an "
		"array, and all unselected elements at the end. The kernel also returns the "
		"number of 'selected' elements.\n"
		"\n"
		"For more details on CUB, see: https://nvlabs.github.io/cub/\n"
		"For details on CUB's DevicePartition primitive, see: "
		"https://nvlabs.github.io/cub/structcub_1_1_device_partition.html";
}

// Some This can be moved into KernelTestAdapter with the same code for all subclasses -
// for example, handling key aliases
template<typename SelectionOp>
Partition<SelectionOp>::Partition(const TestConfiguration& config)
	: KernelTestAdapter(config)
{
	KernelTestAdapter::resolveOption(
		data_distribution, ConfigurationKeySet { "data-distribution"	}
	);
	KernelTestAdapter::maybe_print_extra_configuration(
		make_pair("Data distribution",     data_distribution)
	);
}

static_block {
	//          SelectionOp
	//---------------------------------------------------------------
	Partition < cuda::functors::is_non_negative<int>    >::registerInSubclassFactory();
	Partition < cuda::functors::is_non_negative<float>  >::registerInSubclassFactory();
	Partition < cuda::functors::is_non_negative<double> >::registerInSubclassFactory();
}

} // namespace cub
} // namespace kernel_tests


