#include "tester/test_adapters/comparators.h"
#include "tester/parallel_cpu_sort.hpp"

#include "util/macro.h"
#include "util/string.hpp" // for concat_string
#include "util/poor_mans_reflection.h"
#include "util/exception.h"
#include "util/integer.h"

#include <cuda/bit_vector.cuh>
#include <cuda/functors.hpp>

#include <gsl/gsl-lite.hpp>

#include <unordered_set>
#include <vector>

namespace kernel_tests {
namespace comparators {

using util::memory_region;
using util::uint_t;

template <typename SelectionOp>
bool cub_partitioned_data(
	const TestConfiguration&                    config               __attribute((unused)),
	const HostBuffers&                          all_actual_inputs    __attribute((unused)),
	const HostBuffers&                          all_actual_outputs   __attribute((unused)),
	const HostBuffers&                          all_expected_outputs __attribute((unused)),
	const BufferDescriptor&                     descriptor,
	const memory_region                         actual,
	const memory_region                         expected)
{
	using Datum = typename SelectionOp::result_type;
	using cub_input_size_t = uint_t<4>;

	auto actual_num_selected   = static_cast<cub_input_size_t*>(all_actual_outputs.at("num_selected").data());
	auto expected_num_selected = static_cast<cub_input_size_t*>(all_expected_outputs.at("num_selected").data());
	util::enforceNonNull(actual_num_selected);
	util::enforceNonNull(expected_num_selected);
	if (*actual_num_selected != *expected_num_selected) {
		// No sense in even making the comparison in this case
		return false;
	}
	auto num_selected = *actual_num_selected;
	const Datum* typed_actual_buffer = static_cast<const Datum*>(actual.data());
	const Datum* typed_expected_buffer = static_cast<const Datum*>(expected.data());

	util::enforce(descriptor.capacity % sizeof(Datum) == 0,
		util::concat_string("Buffer of supposed element type ", util::type_name<Datum>(), " has size ",
		descriptor.capacity, ", which is not a multiple of sizeof(", util::type_name<Datum>(), ") == ",
		sizeof(Datum)));
	auto length = descriptor.capacity / sizeof(Datum);
	std::unordered_multiset<Datum> actual_selected_items(typed_actual_buffer, typed_actual_buffer + num_selected);
	std::unordered_multiset<Datum> actual_unselected_items(
		typed_actual_buffer + num_selected, typed_actual_buffer + length);
	std::unordered_multiset<Datum> expected_selected_items(typed_expected_buffer, typed_expected_buffer + num_selected);
	std::unordered_multiset<Datum> expected_unselected_items(
		typed_expected_buffer + num_selected, typed_expected_buffer + length);

	return
		actual_selected_items   == expected_selected_items   &&
		actual_unselected_items == expected_unselected_items;
}


template <unsigned IndexSize>
bool unsorted_sparse_output(
	const TestConfiguration&  config               __attribute((unused)),
	const HostBuffers&        all_inputs           __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	using index_type = uint_t<IndexSize>;

	UNUSED(descriptor);
	index_type*  num_actual_elements   = all_actual_outputs.at("sparse_length").as_span<index_type>().data();
	index_type*  num_expected_elements = all_expected_outputs.at("sparse_length").as_span<index_type>().data();
	util::enforceNonNull(num_actual_elements);
	util::enforceNonNull(num_expected_elements);
	if (*num_actual_elements != *num_expected_elements) { return false; }
	auto set_size = *num_actual_elements;
	gsl::span<const index_type> typed_actual = util::as_span<const index_type>(actual);
	gsl::span<const index_type> typed_expected = util::as_span<const index_type>(expected);

	std::vector<index_type> actual_set_elements(
		typed_actual.cbegin(), typed_actual.cbegin() + set_size);
	std::vector<index_type> expected_set_elements(
		typed_expected.cbegin(), typed_expected.cbegin() + set_size);

	kernel_tests::sort(actual_set_elements);
	kernel_tests::sort(expected_set_elements);

	return(actual_set_elements == expected_set_elements);

	// if we're past this point, it must be the case that all actual elements are
	// distinct, as otherwise the size could not match

//	bool result = true;
//	util::ios_flags_saver ifs(std::cout);
//	std::cout << std::right;
//	for (T i = 0; i < set_size; i++) {
//		bool pair_result = actual_set_elements[i] == expected_set_elements[i];
//		if (!pair_result) {
//			std::cout << std::setw(10) << i << ":   " << std::setw(5)
//				<< actual_set_elements[i] << "   " << std::setw(5) << expected_set_elements[i];
//			auto it1 = std::find(actual_set_elements.cbegin(), actual_set_elements.cend(), expected_set_elements[i]);
//			if (it1 == actual_set_elements.cend()) {
//				std::cout << " and " << std::setw(4) << expected_set_elements[i] << " isn't in actual,    ";
//			}
//			else {
//				std::cout << " and " << std::setw(4) << expected_set_elements[i] << " is in actual[" << std::setw(4) << (it1 - actual_set_elements.cbegin()) << "],";
//			}
//			auto it2 = std::find(expected_set_elements.cbegin(), expected_set_elements.cend(), actual_set_elements[i]);
//			if (it2 == expected_set_elements.cend()) {
//				std::cout << " and " << std::setw(4) << actual_set_elements[i] << " isn't in sorted expected";
//			}
//			else {
//				std::cout << " and " << std::setw(4) << actual_set_elements[i] << " is in sorted expected[" << std::setw(4) << (it2 - expected_set_elements.cbegin()) << "]";
//			}
//			std::cout << '\n';
//
//		}
//		result = result && pair_result;
//	}
//	return result;
}

// This is _almost_ a generic set match checker, except for obtaining
// the lengths.
template <unsigned IndexSize, typename Datum>
bool unsorted_selected_elements(
	const TestConfiguration&  config               __attribute((unused)),
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	using index_type = uint_t<IndexSize>;

	UNUSED(descriptor);
	// TODO: Take this from the memory region.
	index_type*  num_actual_elements   = all_actual_outputs.at("num_selected").as_span<index_type>().data();
	index_type*  num_expected_elements = all_expected_outputs.at("num_selected").as_span<index_type>().data();
	util::enforceNonNull(num_actual_elements);
	util::enforceNonNull(num_expected_elements);
	if (*num_actual_elements != *num_expected_elements) { return false; }
	auto set_size = *num_actual_elements;
	gsl::span<const Datum> typed_actual = util::as_span<const Datum>(actual);
	gsl::span<const Datum> typed_expected = util::as_span<const Datum>(expected);

	std::vector<Datum> actual_set_elements(
		typed_actual.cbegin(), typed_actual.cbegin() + set_size);
	std::vector<Datum> expected_set_elements(
		typed_expected.cbegin(), typed_expected.cbegin() + set_size);

	kernel_tests::sort(actual_set_elements);
	kernel_tests::sort(expected_set_elements);

	return(actual_set_elements == expected_set_elements);
}


template <unsigned IndexSize>
bool bit_vectors_by_test_length(
	const TestConfiguration&  config               __attribute((unused)),
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	using cuda::bit_vector;
	using cuda::const_bit_vector;
	using index_type = uint_t<IndexSize>;

	UNUSED(descriptor);
	size_t length_in_bits = config.test_length;
		// Yes, this is terribly ugly; but the signature doesn't allow us to pass the length in bits,
		// so that we can account for the slack

	util::enforce(expected.size() != bit_vector<index_type>::num_elements_necessary_for(length_in_bits),
		"Expected size is incompatible with a bit vector with the length specified as config.test_length");
	util::enforce(expected.size() > 0, "This should not be possible - internal test harness bug");

	if (actual.size() != expected.size()) { return false; }

	using bit_container_type = cuda::standard_bit_container_t;

	// Compare all mask bytes except for the last container element, which has some slack bits that
	// are allowed to diverge
	if (std::memcmp(actual.data(), expected.data(), actual.size() - sizeof(bit_container_type)) != 0) { return false; }

	const_bit_vector<index_type> typed_actual(reinterpret_cast<const bit_container_type*>(actual.data()),   length_in_bits);
	const_bit_vector<index_type> typed_expected(reinterpret_cast<const bit_container_type*>(expected.data()), length_in_bits);

	auto actual_last_container   = typed_actual.last_container();
	auto expected_last_container = typed_expected.last_container();

	return
		bit_vector<index_type>::mask_out_slack_bits(actual_last_container,   length_in_bits) ==
		bit_vector<index_type>::mask_out_slack_bits(expected_last_container, length_in_bits);
}

MAP_BINARY(INSTANTIATE_TEMPLATED_CLASS, bit_vectors_by_test_length,	1, 2, 4, 8)

MAP_BINARY(INSTANTIATE_TEMPLATED_CLASS, unsorted_sparse_output, 1, 2, 4, 8)

MAP_TRINARY(INSTANTIATE_TEMPLATED_CLASS, unsorted_selected_elements, 4,
	char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long, long long, unsigned long long)
MAP_TRINARY(INSTANTIATE_TEMPLATED_CLASS, unsorted_selected_elements, 8,
	char, unsigned char, short,unsigned short, int, unsigned int, long, unsigned long, long long, unsigned long long)

INSTANTIATE_TEMPLATED_CLASS(cub_partitioned_data, cuda::functors::is_non_negative<int>)
INSTANTIATE_TEMPLATED_CLASS(cub_partitioned_data, cuda::functors::is_non_negative<float>)
INSTANTIATE_TEMPLATED_CLASS(cub_partitioned_data, cuda::functors::is_non_negative<double>)

} // namespace comparators
} // namespace kernel_tests

