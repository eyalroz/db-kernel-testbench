#pragma once
#ifndef COMPARATORS_H_
#define COMPARATORS_H_

#include "tester/TestConfiguration.h"
#include "tester/BufferDescriptor.h"
#include "util/memory_region_extra.h"

namespace kernel_tests {
namespace comparators {

using util::memory_region;

template <typename SelectionOp>
bool cub_partitioned_data(
	const TestConfiguration&                    config               __attribute((unused)),
	const HostBuffers&                          all_actual_inputs    __attribute((unused)),
	const HostBuffers&                          all_actual_outputs   __attribute((unused)),
	const HostBuffers&                          all_expected_outputs __attribute((unused)),
	const BufferDescriptor&                     descriptor,
	const memory_region                         actual,
	const memory_region                         expected);

template <unsigned IndexSize>
bool unsorted_sparse_output(
	const TestConfiguration&  config               __attribute((unused)),
	const HostBuffers&        all_inputs           __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected);

// This is _almost_ a generic set match checker, except for obtaining
// the lengths.
template <unsigned IndexSize, typename Datum>
bool unsorted_selected_elements(
	const TestConfiguration&  config               __attribute((unused)),
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected);


template <unsigned IndexSize>
bool bit_vectors_by_test_length(
	const TestConfiguration&  config               __attribute((unused)),
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected);

} // namespace comparators
} // namespace kernel_tests

#endif /* COMPARATORS_H_ */
