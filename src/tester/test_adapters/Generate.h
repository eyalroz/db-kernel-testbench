#ifndef KERNEL_TEST_ADAPTER_GENERATE_CUH_
#define KERNEL_TEST_ADAPTER_GENERATE_CUH_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {

// TODO:
// - Consider having some common subclass for all elementwise operations; they might
//   share some code, I suppose
template<unsigned IndexSize, typename UnaryFunction>
class Generate : public KernelTestAdapter, public mixins::TestAdapterNameBuilder<Generate<IndexSize, UnaryFunction>>
{
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array
	using Datum = typename UnaryFunction::result_type;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Generate);

private:
	void generate_expected_result(span<Datum> expected_result);

};

template <unsigned IndexSize, typename T, T Value>
class Fill : public Generate<IndexSize, cuda::functors::constant<T, Value>> {
	using parent = Generate<IndexSize, cuda::functors::constant<T, Value>>;
	using parent::parent;
};

template <unsigned IndexSize, typename T>
class Zero : public Generate<IndexSize, cuda::functors::constant<T, 0>> {
	using parent = Generate<IndexSize, cuda::functors::constant<T, 0>>;
	using parent::parent;
};

template <unsigned IndexSize, typename T, typename NullaryForStartValue = cuda::functors::constant<T, 0>>
class Iota : public Generate<IndexSize, cuda::functors::affine_transform_nullaries<T, cuda::functors::constant<T, 1>, NullaryForStartValue>> {
	using parent = Generate<IndexSize, cuda::functors::affine_transform_nullaries<T, cuda::functors::constant<T, 1>, NullaryForStartValue>>;
	using parent::parent;
};

template <unsigned IndexSize, typename T, T Value = 0>
class SimpleIota : public Generate<IndexSize, cuda::functors::affine_transform<T, 0, Value>> {
	using parent = Generate<IndexSize, cuda::functors::affine_transform<T, 0, Value>>;
	using parent::parent;
};

} // namespace kernel_tests


#endif /* KERNEL_TEST_ADAPTER_GENERATE_CUH_ */
