
#include "Generate.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"


#include "kernel_wrappers/generate.cu"

#include <ratio>

namespace kernel_tests {


template<unsigned IndexSize, typename UnaryFunction>
KernelTestAdapter::BufferDescriptors Generate<IndexSize, UnaryFunction>
	::getBufferDescriptors() const
{
	return {
		{"result", BufferDescriptor::make<Datum>(config.test_length, BufferDirection::Output) },
	};
}

template<unsigned IndexSize, typename UnaryFunction>
LaunchConfigurationSequence Generate<IndexSize, UnaryFunction>
	::resolveLaunchConfigurations() const
{
	auto device_properties(cuda::device::get(config.test_device).properties());
	cuda::kernel_t::arguments_type args;
	args["length"] = config.test_length;

	::cuda::kernels::generate::kernel_t<IndexSize, UnaryFunction> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
//		::cuda::kernels::generate::resolve_launch_configuration_wrapper(
			device_properties, args,
			cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("generate");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template<unsigned IndexSize, typename UnaryFunction>
KernelTestAdapter::BufferSizes Generate<IndexSize, UnaryFunction>
	::generateInputs(const HostBuffers& buffers)
{
	UNUSED(buffers);
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename UnaryFunction>
void Generate<IndexSize, UnaryFunction>::generate_expected_result(span<Datum> expected_result)
{
	auto f = UnaryFunction();
	for (size_t i = 0; i < config.test_length; i++) {
		expected_result[i] = f(i);
	}
}

template<unsigned IndexSize, typename UnaryFunction>
KernelTestAdapter::BufferSizes Generate<IndexSize, UnaryFunction>
	::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	UNUSED(buffers);
	if (need_to_generate("result")) {
		auto result = expected_buffers.at("result").as_span<Datum>();

		generate_expected_result(result);
	}
	return NoBuffersToResize;
}


template<unsigned IndexSize, typename UnaryFunction>
void Generate<IndexSize, UnaryFunction>::launchKernels(
	const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area,
	cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	Datum* result = buffers.at("result").as_span<Datum>().data();
	index_type length = config.test_length;

	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;

	cuda::kernels::generate::kernel_t<IndexSize, UnaryFunction>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(result, length)
	);
}

template<unsigned IndexSize, typename UnaryFunction>
std::string Generate<IndexSize, UnaryFunction>::buildPrettyKernelName() const
{
	std::ostringstream ss;

	ss	<< "Generates an array of data, of type " << util::type_name<typename UnaryFunction::result_type>() << ","
		<< "using the generating function " << util::type_name<UnaryFunction>()
		<< "(input size and input indics having size " << IndexSize << " bytes)";
	return ss.str();
}

template<unsigned IndexSize, typename UnaryFunction>
std::string Generate<IndexSize, UnaryFunction>::getDescription() const {
	return
		"Data is generated in GPU global memory using a generating function, "
		"which takes as input the index of the element it's generating and "
		"the total data length.";
}

template<unsigned IndexSize, typename UnaryFunction>
Generate<IndexSize, UnaryFunction>::Generate(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	maybe_print_extra_configuration();
}

static_block {
	namespace functors = cuda::functors;
//	using half = functors::constant_by_ratio<float, std::ratio<1,2>>;
//	using third = functors::constant_by_ratio<float, std::ratio<1,3>>;


	Zero       <4, uint8_t     >::registerInSubclassFactory();

	Fill       <4, int32_t, 1  >::registerInSubclassFactory();

	SimpleIota <4, int32_t      >::registerInSubclassFactory();
	SimpleIota <4, int32_t, 123 >::registerInSubclassFactory();

	//          IndexSize UnaryFunction
	//-----------------------------------------------------------------------------
	Generate   <4, functors::identity<int32_t>                              >::registerInSubclassFactory();
//	Generate   <4, functors::constant_by_ratio<float, std::ratio<1, 3>>     >::registerInSubclassFactory();
//	Generate   <4, functors::linear_transform<int32_t, -2>                  >::registerInSubclassFactory();
//	Generate   <4, functors::affine_transform_nullaries<float, third, half> >::registerInSubclassFactory();
//	Generate   <4, functors::fixed_modulus<uint32_t, 7>                     >::registerInSubclassFactory();
}

} // namespace kernel_tests
