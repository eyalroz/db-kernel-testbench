
#include "BinaryOperation.h"

#include "tester/test_adapters/make_kernel_arguments.h"

#include <cuda/functors.hpp>

#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"

#include "kernel_wrappers/elementwise/binary_operation.cu"

namespace kernel_tests {

namespace mixins {

using elementwise::KernelScalarity;

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
struct TestAdapterNameBuilder<elementwise::BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>> {
static std::string  buildName() {
		std::string s = KernelTestAdapter::buildName<elementwise::BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>>();
		util::replace_all(s, "(kernel_tests::elementwise::KernelScalarity)0", "Array");
		util::replace_all(s, "(kernel_tests::elementwise::KernelScalarity)1", "Scalar");
		return s;
	}
};
} // namespace mixins


namespace elementwise {

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
KernelTestAdapter::BufferDescriptors BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::getBufferDescriptors() const
{
	static_assert(LHSScalarity == KernelScalarity::Array || RHSScalarity == KernelScalarity::Array,
		"Attempt to instantiate a scalar - op - scalar kernel");

	BufferDescriptors result {
		{"result",  BufferDescriptor::make<result_type>(config.test_length, BufferDirection::Output) }
	};

	if (LHSScalarity == KernelScalarity::Array) {
		result.emplace("left_hand_side",  BufferDescriptor::make<lhs_type>(config.test_length, BufferDirection::Input));
	}
	if (RHSScalarity == KernelScalarity::Array) {
		result.emplace("right_hand_side",  BufferDescriptor::make<lhs_type>(config.test_length, BufferDirection::Input));
	}
	return result;
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
LaunchConfigurationSequence BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::resolveLaunchConfigurations() const
{
	auto device_properties(cuda::device::get(config.test_device).properties());
	using namespace cuda::kernels::elementwise::binary;
	cuda::kernel_t::arguments_type args = { { "length", config.test_length } };

	kernel_t<IndexSize, BinaryOp, (bool) LHSScalarity, (bool) RHSScalarity> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));

	std::string kernel_name("binary_operation");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
void BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::generate_left_hand_side(lhs_type* __restrict__ left_hand_side)
{
	lhs_type value = std::is_unsigned<lhs_type>::value ? 0 : -10;
	if (std::is_floating_point<lhs_type>::value) { value += 0.5; }
	util::enforceNonNull(left_hand_side, "left_hand_side");
	std::generate(left_hand_side, left_hand_side + config.test_length,
			[&value]() { return value += 1; });
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
void BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::generate_right_hand_side(rhs_type* __restrict__ right_hand_side)
{
	rhs_type value = 0;
	util::enforceNonNull(right_hand_side, "right_hand_side");
	std::generate(right_hand_side, right_hand_side + config.test_length,
		[&value]() { return value += 10; });
}


template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
KernelTestAdapter::BufferSizes BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::generateInputs(const HostBuffers& buffers)
{
	if (LHSScalarity == KernelScalarity::Array) {
		if (need_to_generate("left_hand_side")) {
			generate_left_hand_side(buffers.at("left_hand_side").as_span<lhs_type>().data());
		}
	}
	if (RHSScalarity == KernelScalarity::Array) {
		if (need_to_generate("right_hand_side")) {
			generate_right_hand_side(buffers.at("right_hand_side").as_span<rhs_type>().data());
		}
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
void BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::generate_expected_results(
	result_type*     __restrict__  expected_results,
	const lhs_type*  __restrict__  left_hand_side,
	const rhs_type*  __restrict__  right_hand_side)
{
	BinaryOp op;
	for (size_t i = 0; i < config.test_length; i++) {
		expected_results[i] = op(*left_hand_side, *right_hand_side);
		if (LHSScalarity == KernelScalarity::Array) { left_hand_side++; }
		if (RHSScalarity == KernelScalarity::Array) { right_hand_side++; }
	}
}


template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
KernelTestAdapter::BufferSizes BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("result")) {
		const lhs_type* left_hand_side = (LHSScalarity == KernelScalarity::Array) ?
			buffers.at("left_hand_side").as_span<lhs_type>().data() : &scalar_lhs;
		const rhs_type* right_hand_side = (RHSScalarity == KernelScalarity::Array) ?
			buffers.at("right_hand_side").as_span<rhs_type>().data() : &scalar_rhs;
		result_type* expected_results =
				expected_buffers.at("result").as_span<result_type>().data();

		generate_expected_results(expected_results, left_hand_side, right_hand_side);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
void BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>
	::launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	result_type* result = buffers.at("result").as_span<result_type>().data();
	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	index_type length = config.test_length;

	// Launch just one of three variants: array-array (none-fixed), array-scalar and scalar-array)

	using namespace cuda::kernels::elementwise::binary;

	if ((LHSScalarity == KernelScalarity::Array) && (RHSScalarity == KernelScalarity::Array)) {
		const lhs_type* left_hand_side        = buffers.at("left_hand_side").as_span<lhs_type>().data();
		const rhs_type* right_hand_side        = buffers.at("right_hand_side").as_span<rhs_type>().data();
		cuda::kernels::elementwise::binary::kernel_t<IndexSize, BinaryOp, (bool) LHSScalarity, (bool) RHSScalarity>().enqueue_launch(
			stream, launch_config,
			MAKE_KERNEL_ARGUMENTS(result, left_hand_side, right_hand_side, length)
		);
	}
	if ((LHSScalarity == KernelScalarity::Array) && (RHSScalarity == KernelScalarity::Scalar)) {
		const lhs_type* left_hand_side        = buffers.at("left_hand_side").as_span<lhs_type>().data();
		auto right_hand_side = scalar_rhs;
		cuda::kernels::elementwise::binary::kernel_t<IndexSize, BinaryOp, (bool) LHSScalarity, (bool) RHSScalarity>().enqueue_launch(
			stream, launch_config,
			MAKE_KERNEL_ARGUMENTS(result, left_hand_side, right_hand_side, length)
		);
	}
	if ((LHSScalarity == KernelScalarity::Scalar) && (RHSScalarity == KernelScalarity::Array)) {
		auto left_hand_side = scalar_lhs;
		const rhs_type* right_hand_side        = buffers.at("right_hand_side").as_span<rhs_type>().data();
		cuda::kernels::elementwise::binary::kernel_t<IndexSize, BinaryOp, (bool) LHSScalarity, (bool) RHSScalarity>().enqueue_launch(
			stream, launch_config,
			MAKE_KERNEL_ARGUMENTS(result, left_hand_side, right_hand_side, length)
		);
	}
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
std::string BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>::buildPrettyKernelName() const
{
	using result_type = typename BinaryOp::result_type;
	using lhs_type    = typename BinaryOp::first_argument_type;
	using rhs_type    = typename BinaryOp::second_argument_type;
	std::stringstream ss;
	bool all_same_type = (std::is_same<result_type, rhs_type>::value &&  std::is_same<rhs_type, result_type>::value);

	ss	<< "Apply the binary operation "
		<< util::type_name<BinaryOp>() << "elementwise, between ";
	ss	<< (LHSScalarity == KernelScalarity::Scalar ?
			"each element of the left-hand side array" :
			"the single left-hand-side value" );
	if (!all_same_type) {
		ss << ", of type " << util::type_name<lhs_type>() << ", ";
	}
	ss << "and "
	   << (RHSScalarity == KernelScalarity::Scalar ?
		   "each element of the right-hand side array" :
		   "the single value on the right-hand side" );
	if (!all_same_type) {
		ss << ", of type " << util::type_name<rhs_type>() << ", ";
	}
	ss << "with the results ";
	if (!all_same_type) {
		ss << ", having type" << util::type_name<result_type>();
	}
	ss << " placed in a third, " << util::type_name<result_type>() << " array.";
	return ss.str();
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
std::string BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>::getDescription() const {
	return
		"A single implementation of the application of any side-effects-free binary operation.\n"
		"Supporting scalar-vector, vector-scalar and vector-vector.\n"
		"Does _not_ support (for now) in-place application into the LHS or the RHS.";
}

template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>::BinaryOperation(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	optional<lhs_type> maybe_scalar_lhs;
	optional<lhs_type> maybe_scalar_rhs;
	if (RHSScalarity == KernelScalarity::Scalar) {
		resolveOption(
			scalar_rhs,
			ConfigurationKeySet { "scalar-rhs", "scalar-right-hand-side", "right-hand-side", "rhs" }
		);
		maybe_scalar_rhs = scalar_rhs;
	}
	if (LHSScalarity == KernelScalarity::Scalar) {
		resolveOption(
			scalar_lhs,
			ConfigurationKeySet { "scalar-lhs", "scalar-left-hand-side", "left-hand-side", "lhs" }
		);
		maybe_scalar_lhs = scalar_lhs;
	}
	using std::make_pair;
	maybe_print_extra_configuration(
		make_pair("Scalar left-hand-side value",   maybe_scalar_lhs),
		make_pair("Scalar right-hand-side value",  maybe_scalar_rhs),
		make_pair("Unrecognized Configuration options",  config.extra_options)
	);
}


static_block {
	const auto Array __attribute__((unused)) = KernelScalarity::Array;
	const auto Scalar __attribute__((unused)) = KernelScalarity::Scalar;

	using namespace ::kernel_tests::elementwise;
	namespace functors = ::cuda::functors;

	// Arithmetic

	//                                                               Scalarity
	//                IndexSize   BinaryOp                          LHS     RHS
	//--------------------------------------------------------------------------------
	BinaryOperation < 4,          functors::plus<int8_t>,           Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<int16_t>,          Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<int32_t>,          Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<int64_t>,          Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::minus<int32_t>,         Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::multiplies<int32_t>,    Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<int32_t>,          Array,  Scalar >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<int32_t>,          Scalar, Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<float>,            Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<float>,            Scalar, Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::plus<float>,            Array,  Scalar >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::minus<float>,           Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::minus<float>,           Scalar, Array  >::registerInSubclassFactory();
	BinaryOperation < 4,          functors::minus<float>,           Array,  Scalar >::registerInSubclassFactory();

	BinaryOperation < 8,         functors::plus<float>,             Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::plus<float>,             Scalar, Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::plus<float>,             Array,  Scalar >::registerInSubclassFactory();

	// Comparisons

	BinaryOperation < 8,         functors::greater_equal<float>,   Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::less_equal<int32_t>,    Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::equals<int32_t>,        Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::equals<float>,          Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::equals<double>,         Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::equals<int16_t>,        Array,  Array  >::registerInSubclassFactory();
	BinaryOperation < 8,         functors::equals<uint64_t>,       Array,  Array  >::registerInSubclassFactory();
}

} // namespace elementwise
} // namespace kernel_tests
