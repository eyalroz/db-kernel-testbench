#ifndef KERNEL_TEST_ELEMENTWISE_APPLY_H_
#define KERNEL_TEST_ELEMENTWISE_APPLY_H_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace elementwise {

template<
	unsigned               IndexSize,
	typename               Function,
	typename...            Parameters>
class Apply :
	public KernelTestAdapter,
	public mixins::TestAdapterNameBuilder<Apply<IndexSize, Function, Parameters...>>
{
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	enum { num_input_params = sizeof...(Parameters), arity = num_input_params };

	using result_type = typename std::result_of<Function(Parameters...)>::type;
	using argument_types_tuple_type = std::tuple<Parameters...>;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(Apply);

protected:
	std::array<bool, arity> use_identity_as_input_argument; // all false by default
	std::array<optional<distribution_t<long long int>>, arity> input_argument_distributions;
	// TODO: Why can't I just initialize it here with
	//  {
	//	distribution_t<long long int>::uniform(
	//		(std::is_unsigned<Parameters>::value ?   0 :  -50),
	//		(std::is_unsigned<Parameters>::value ? 100 :   50)
	//	)... };
	//
	// ?
};

} // namespace elementwise
} // namespace kernel_tests

#endif /* KERNEL_TEST_ELEMENTWISE_APPLY_H_ */
