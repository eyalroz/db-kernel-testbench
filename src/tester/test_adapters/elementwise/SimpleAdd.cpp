
#include <cuda/api/kernel_launch.cuh>

#include "tester/test_adapters/make_kernel_arguments.h"

#include "SimpleAdd.h"

#include <iostream>
#include "util/static_block.h"
#include "kernel_wrappers/elementwise/simple_add.cu"

namespace kernel_tests {
namespace elementwise {

template<typename Datum>
KernelTestAdapter::BufferDescriptors SimpleAdd<Datum>::getBufferDescriptors() const
{
	return {
		{ "result",          BufferDescriptor::make<Datum>(config.test_length, BufferDirection::Output) },
		{ "left_hand_side",  BufferDescriptor::make<Datum>(config.test_length, BufferDirection::Input)  },
		{ "right_hand_side", BufferDescriptor::make<Datum>(config.test_length, BufferDirection::Input)  },
	};
}

template<typename Datum>
LaunchConfigurationSequence SimpleAdd<Datum>::resolveLaunchConfigurations() const
{
	auto device_properties(cuda::device::get(config.test_device).properties());
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };
	::cuda::kernels::elementwise::simple_add::kernel_t<Datum> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(
		kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(
			config.max_threads_per_block));
	std::string kernel_name("simple_add");
	return { { kernel_name, kernel.get_device_function(), launch_config } };
}

// TODO: Have either Buffer.h or some other .h/.hpp have named constructor idioms -
// for constants, arithmetic progressions, geometric progressions, and
// any lambda
template <typename Datum>
KernelTestAdapter::BufferSizes SimpleAdd<Datum>::generateInputs(const HostBuffers& buffers)
{
	auto left_hand_side = buffers.at("left_hand_side").as_span<Datum>();
	auto right_hand_side = buffers.at("right_hand_side").as_span<Datum>();

	if (use_all_ones_as_data) {
		if (need_to_generate("left_hand_side"))  { fill(left_hand_side,  1); }
		if (need_to_generate("right_hand_side")) { fill(right_hand_side, 1); }
		return { };
	}

	Datum value = std::is_unsigned<Datum>::value ? 0 : -10 ;
	if (std::is_floating_point<Datum>::value) { value += 0.5; }
	if (need_to_generate("left_hand_side")) {
		generate(left_hand_side, [&value]() { return value += 1; });
	}

	if (need_to_generate("right_hand_side")) {
		value = 0;
		generate(right_hand_side, [&value]() { return value += 1000; });
	}
	return NoBuffersToResize;
}

template<typename Datum>
KernelTestAdapter::BufferSizes SimpleAdd<Datum>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("result")) {
		auto result = expected_buffers.at("result").as_span<Datum>();
		auto left_hand_side    = buffers.at("left_hand_side" ).as_span<Datum>();
		auto right_hand_side   = buffers.at("right_hand_side").as_span<Datum>();

		for(size_t i = 0; i < config.test_length; i++) {
			result[i] = left_hand_side[i] + right_hand_side[i];
		}
	}
	return NoBuffersToResize;
}

template<typename Datum>
void SimpleAdd<Datum>::launchKernels(const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const
{
	util::enforce(device_scratch_area.empty());
	auto result  = buffers.at("result").as_span<Datum>().data();
	const Datum* left_hand_side = buffers.at("left_hand_side").as_span<const Datum>().data();
	const Datum* right_hand_side = buffers.at("right_hand_side").as_span<const Datum>().data();
	size_t length __attribute__((unused)) = config.test_length;
		// length _is_ used (see below), but nvcc (CUDA 7.5) is complaining
		// that it supposedly isn't


	cuda::launch_configuration_t launch_config = resolveLaunchConfigurations()[0].launch_config;
	cuda::kernels::elementwise::simple_add::kernel_t<Datum>().enqueue_launch(
		stream, launch_config,
		MAKE_KERNEL_ARGUMENTS(result, left_hand_side, right_hand_side, length)
	);
}

template <typename Datum>
std::string SimpleAdd<Datum>::buildPrettyKernelName() const
{
	return "Plain-vanilla vector elementwise addition with both operands and result of type " + util::type_name<Datum>() + ".";
}

template <typename Datum>
std::string SimpleAdd<Datum>::getDescription() const
{
	return std::string(
		"A plain vanilla, no frills and no tricks, elementwise addition kernel.\n"
		"Implemented as a pseudo-mock for develoing the kernel tester. For attempts "
		"at speeding up elementwise operations, see other adapters in the elementwise/ "
		"subdirectory.");
}

template<typename Datum>
SimpleAdd<Datum>::SimpleAdd(const TestConfiguration& config) : parent(config)
{
	use_all_ones_as_data   = resolveOption(ConfigurationKeySet { "trivial-data", "trivial", "data-all-ones", "all_ones" }, false);
	maybe_print_extra_configuration(
		make_pair("Use trivial data (all-1's)", use_all_ones_as_data)
	);
}

// We don't really care about this kernel, use BinaryOperation<...> with plus<...> instead
#ifdef DEBUG
static_block {
	SimpleAdd < int8_t    >::registerInSubclassFactory();
	SimpleAdd < int16_t   >::registerInSubclassFactory();
	SimpleAdd < int32_t   >::registerInSubclassFactory();
	SimpleAdd < int64_t   >::registerInSubclassFactory();
	SimpleAdd < uint8_t   >::registerInSubclassFactory();
	SimpleAdd < uint16_t  >::registerInSubclassFactory();
	SimpleAdd < uint32_t  >::registerInSubclassFactory();
	SimpleAdd < uint64_t  >::registerInSubclassFactory();
	SimpleAdd < double    >::registerInSubclassFactory();
	SimpleAdd < float     >::registerInSubclassFactory();
}
#endif

} // namespace elementwise
} // namespace kernel_tests

