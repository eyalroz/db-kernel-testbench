
#include "Apply.h"

#include <cuda/functors.hpp>

#include "util/math.hpp"
#include "util/static_block.h"
#include "util/string.hpp"
#include "util/algorithm_container_adapters.hpp"

#include "kernel_wrappers/elementwise/apply.cu"

/*
 * This is ugly. Make it go away.
 */
enum { DefaultSerializationFactor = 16 };

namespace kernel_tests {
namespace elementwise {

using cuda::kernels::elementwise::apply::input_argument_name;

template<unsigned IndexSize, typename Function, typename... Parameters>
KernelTestAdapter::BufferDescriptors Apply<IndexSize, Function, Parameters...>
	::getBufferDescriptors() const
{
	KernelTestAdapter::BufferDescriptors result = {
		{ "result",	BufferDescriptor::make<result_type>  (config.test_length, BufferDirection::Output) },
	};
	auto add_input_argument_buffer_description = [&](const auto& dummy, size_t index) {
		result.insert(
			{
				input_argument_name(index),
				BufferDescriptor::make<typename std::remove_reference<decltype(dummy)>::type>(
					config.test_length, BufferDirection::Input )
			}
		);
	};
	util::for_each_parameter_index<decltype(add_input_argument_buffer_description),Parameters...>(
		add_input_argument_buffer_description);
	return result;
}

template<unsigned IndexSize, typename Function, typename... Parameters>
LaunchConfigurationSequence Apply<IndexSize, Function, Parameters...>::resolveLaunchConfigurations() const
{
	auto device_properties(cuda::device::get(config.test_device).properties());
	cuda::kernel_t::arguments_type extra_args = { { "length", config.test_length } };
	::cuda::kernels::elementwise::apply::kernel_t<
		IndexSize, Function, DefaultSerializationFactor, Parameters...
	> kernel;
	auto launch_config = cuda::kernel::resolve_launch_configuration(kernel,
		device_properties, extra_args,
		cuda::launch_configuration_limits_t::limit_block_size(config.max_threads_per_block));
	std::string kernel_name("elementwise::apply");
	return { { kernel_name, kernel.get_device_function(), launch_config } };

}

template<typename Function, typename T>
static void generate_argument_buffer(
	span<T>                        buffer,
	bool                           use_iota,
	distribution_t<long long int>  distribution,
	std::default_random_engine&    random_engine )
{
	if (use_iota) {
		iota(buffer, 0);
		return;
	}

	generate(buffer,
		[&distribution, &random_engine]() {
			T additional_offset = std::is_floating_point<T>::value ? 0 : 1.0/3.0;
			return additional_offset + util::random::sample_from(distribution, random_engine);
		});
}

template<unsigned IndexSize, typename Function, typename... Parameters>
KernelTestAdapter::BufferSizes Apply<IndexSize, Function, Parameters...>::generateInputs(const HostBuffers& buffers)
{
	auto maybe_generate_buffer =
		[&](const auto& dummy, size_t argument_index) {
			using argument_type =
				std::remove_cv_t<std::remove_reference_t<decltype(dummy)>>;
			if (this->need_to_generate(input_argument_name(argument_index))) {
				generate_argument_buffer<Function, argument_type>(
					buffers.at(input_argument_name(argument_index)).as_span<argument_type>(),
					use_identity_as_input_argument[argument_index],
					input_argument_distributions[argument_index].value(),
					random_engine);
		}
	};
	util::for_each_parameter_index<decltype(maybe_generate_buffer), Parameters...>(maybe_generate_buffer);
	return NoBuffersToResize;
}

template<typename Function, typename Result, typename... Parameters>
static void generate_expected_result(
	Function                            f,
	span<Result>                        expected_result,
	span<Parameters>...                 input_arguments)
{
	// If range-for allowed for getting the index somehow, I'd use it here
	for (size_t i = 0; i < expected_result.length(); i++) {
		expected_result[i] = f( input_arguments[i]...);
	}
}

template<typename Function, typename Result, typename... Parameters, size_t... Indices>
static void generateExpectedOutputs_helper(
	std::tuple<Parameters...>, // used only for type deduction
	Function                                  f,
	span<Result>                              result,
	const HostBuffers&                        buffers,
	std::index_sequence<Indices...>
)
{
	generate_expected_result(
		f, result,
		buffers.at(input_argument_name(Indices)).as_span<Parameters>()...
	);
}

template<unsigned IndexSize, typename Function, typename... Parameters>
KernelTestAdapter::BufferSizes Apply<IndexSize, Function, Parameters...>::generateExpectedOutputs(
	const HostBuffers& expected_buffers,
	const HostBuffers& buffers)
{
	if (need_to_generate("result")) {
		auto result = expected_buffers.at("result").as_span<result_type>();
		generateExpectedOutputs_helper(
			argument_types_tuple_type{},
			Function{},
			result,
			buffers,
			std::make_index_sequence<sizeof...(Parameters)> {}
		);
	}
	return NoBuffersToResize;
}

template<unsigned IndexSize, typename Function, typename... Parameters, size_t... Indices>
static void launchKernels_helper(
	std::integral_constant<unsigned, IndexSize>, // used only to deduce IndexSize
	std::tuple<Parameters...>,                   // used only to deduce Parameters
	cuda::launch_configuration_t               launch_config,
	Function                                   f,
	cuda::size_type_by_index_size<IndexSize>   length,
	const DeviceBuffers&                       buffers,
	typename DeviceBuffers::mapped_type        device_scratch_area,
	cuda::stream_t&                            stream,
	std::index_sequence<Indices...>            // unused
)
{
	using result_type = typename std::result_of<Function(Parameters...)>::type;
	UNUSED(f);
	util::enforce(not device_scratch_area);
	auto result  = buffers.at("result").as_span<result_type>();
	cuda::kernels::elementwise::apply::kernel_t<
		IndexSize, Function, DefaultSerializationFactor, Parameters...
	>().enqueue_launch(
		stream, launch_config,
		{
			{"result", result.data()},
			{"length", length},
			{
				input_argument_name(Indices),
				buffers.at(input_argument_name(Indices)).as_span<const Parameters>().data(),
			}...
		}
	);
}

template<unsigned IndexSize, typename Function, typename... Parameters>
void Apply<IndexSize, Function, Parameters...>::launchKernels(
	const DeviceBuffers& buffers,
	typename DeviceBuffers::mapped_type device_scratch_area,
	cuda::stream_t& stream) const
{
	auto launch_config = resolveLaunchConfigurations()[0].launch_config;
	launchKernels_helper(
		std::integral_constant<unsigned, IndexSize>{}, // used only to deduce IndexSize
		argument_types_tuple_type{},
		launch_config,
		Function{},
		config.test_length,
		buffers,
		device_scratch_area,
		stream,
		std::make_index_sequence<sizeof...(Parameters)>{}
	);
}

template<unsigned IndexSize, typename Function, typename... Parameters>
std::string Apply<IndexSize, Function, Parameters...>::buildPrettyKernelName() const
{
	using result_type = typename Function::result_type;

	std::ostringstream ss;
	ss	<< "Apply the arity-" << arity << " function "
		<< util::type_name<Function>() << " elementwise, to each "
		<< arity << "-tuple of same-index elements in the " << arity << " input arrays of types ";
	ss << util::type_names<Parameters...>();
	ss << ", with the results, of type " << util::type_name<result_type>();
	ss << ", placed in another array.";
	return ss.str();
}

template<unsigned IndexSize, typename Function, typename... Parameters>
std::string Apply<IndexSize, Function, Parameters...>::getDescription() const {
	return
		"Apply any function taking a fixed number of scalars, elementwise to that"
		" number of columns of the appropriate types.\n";
}

template<unsigned IndexSize, typename Function, typename... Parameters>
Apply<IndexSize, Function, Parameters...>::Apply(
	const TestConfiguration& config_) : KernelTestAdapter(config_)
{
	std::array<bool, arity> input_arg_is_unsigned { std::is_unsigned<Parameters>::value... };

	for(int i = 0; i < num_input_params; i++) {

		use_identity_as_input_argument[i] =
			resolveOption(ConfigurationKeySet {
			"use-identity-as-argument-" + input_argument_name(i),
	        input_argument_name(i) + "-is-identity"
		}, false);

		auto maybe_argument_distribution =
			resolveOption<distribution_t<long long int>,ConfigurationKeySet>(ConfigurationKeySet {
				 input_argument_name(i) +"-distribution",
				"distribution-for-" + input_argument_name(i),
			});
		input_argument_distributions[i] = maybe_argument_distribution.value_or(
			distribution_t<long long int>::uniform(
				(input_arg_is_unsigned[i] ?   0 :  -50),
				(input_arg_is_unsigned[i] ? 100 :   50)
			)
		);
	}

	// TODO: Make separate pairs here instead of a
	// a single stream with the whole array
	std::stringstream ss;
	ss << std::boolalpha << use_identity_as_input_argument;
	auto use_identity_as_input_argument_str = ss.str();
	maybe_print_extra_configuration(
		make_pair("Set input arguments to identity", use_identity_as_input_argument_str ),
		make_pair("Value distribution for input arguments", input_argument_distributions )
	);
}

static_block {
	using namespace ::kernel_tests::elementwise;
	namespace functors = cuda::functors;

	//      IndexSize   Function                                   Parameters...
	//--------------------------------------------------------------------------------------------
	Apply < 4,          functors::clip<int>,                       int,      int,      int      >::registerInSubclassFactory();
	Apply < 4,          functors::clip<uint16_t>,                  uint16_t, uint16_t, uint16_t >::registerInSubclassFactory();
	Apply < 4,          functors::clip<float>,                     float,    float,    float    >::registerInSubclassFactory();
	Apply < 4,          functors::between_or_equal_ternary<float>, float,    float,    float    >::registerInSubclassFactory();
	Apply < 4,          functors::if_then_else<float>,             float,    float,    float    >::registerInSubclassFactory();

	Apply < 4,          functors::plus<int8_t>,                    int8_t,   int8_t             >::registerInSubclassFactory();
	Apply < 4,          functors::plus<int16_t>,                   int16_t,  int16_t            >::registerInSubclassFactory();
	Apply < 4,          functors::plus<int32_t>,                   int32_t,  int32_t            >::registerInSubclassFactory();
	Apply < 4,          functors::plus<int64_t>,                   int64_t,  int64_t            >::registerInSubclassFactory();
	Apply < 4,          functors::minus<int32_t>,                  int32_t,  int32_t            >::registerInSubclassFactory();
	Apply < 4,          functors::multiplies<int32_t>,             int32_t,  int32_t            >::registerInSubclassFactory();
	Apply < 4,          functors::plus<float>,                     float,    float              >::registerInSubclassFactory();
	Apply < 4,          functors::minus<float>,                    float,    float              >::registerInSubclassFactory();

	Apply < 4,          functors::greater_equal<float>,            float,    float              >::registerInSubclassFactory();
	Apply < 4,          functors::less_equal<int32_t>,             int32_t,  int32_t            >::registerInSubclassFactory();
	Apply < 4,          functors::equals<int32_t>,                 int32_t,  int32_t            >::registerInSubclassFactory();
	Apply < 4,          functors::equals<float>,                   float,    float              >::registerInSubclassFactory();
	Apply < 4,          functors::equals<double>,                  double,   double             >::registerInSubclassFactory();
	Apply < 4,          functors::equals<int16_t>,                 int16_t,  int16_t            >::registerInSubclassFactory();
	Apply < 4,          functors::equals<uint64_t>,                uint64_t, uint64_t           >::registerInSubclassFactory();

	Apply < 8,          functors::plus<float>,                     float,    float              >::registerInSubclassFactory();
	Apply < 8,          functors::clip<float>,                     float,    float,  float      >::registerInSubclassFactory();
}

} // namespace elementwise
} // namespace kernel_tests
