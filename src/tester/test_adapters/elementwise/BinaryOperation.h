#ifndef KERNEL_TEST_ADAPTER_ARITHMETIC_OPERATION_H_
#define KERNEL_TEST_ADAPTER_ARITHMETIC_OPERATION_H_
/*
 * TODO:
 *
 * - Get rid of the one-side-is-scalar variant, that's actually a unary operation;
 *   and we now have the appropriate unary operation for it (well, almost)
 * - Try to get rid or generalize of the "KernelScalarity" definition, it's too
 *   idiosyncratic
 */


#include "tester/KernelTestAdapter.h"


namespace kernel_tests {
namespace elementwise {

enum class KernelScalarity : bool {
	Array  = false,
	Scalar = true,
};

template<
	unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity,
	KernelScalarity RHSScalarity> class BinaryOperation;

// TODO:
// - Consider having some common subclass for all elementwise operations; they might
//   share some code, I suppose
template<unsigned IndexSize, typename BinaryOp, KernelScalarity LHSScalarity, KernelScalarity RHSScalarity>
class BinaryOperation : public KernelTestAdapter, public mixins::TestAdapterNameBuilder<BinaryOperation<IndexSize, BinaryOp, LHSScalarity, RHSScalarity>>
{
public:
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>; // also used for the overall length of the indexed array
	using result_type = typename BinaryOp::result_type;
	using lhs_type    = typename BinaryOp::first_argument_type;
	using rhs_type    = typename BinaryOp::second_argument_type;

	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(BinaryOperation);

protected:

	void generate_left_hand_side(
		lhs_type*        __restrict__ left_hand_side);
	void generate_right_hand_side(
		rhs_type*        __restrict__ right_hand_side);
	void generate_expected_results(
		result_type*     __restrict__ expected_results,
		const lhs_type*  __restrict__ left_hand_side,
		const rhs_type*  __restrict__ right_hand_side);

	lhs_type scalar_lhs  { static_cast<lhs_type>(std::is_floating_point<lhs_type>::value ? 12.3 : 12) };
	rhs_type scalar_rhs  { static_cast<rhs_type>(std::is_floating_point<rhs_type>::value ? 45.6 : 45) };
};

} // namespace elementwise
} // namespace kernel_tests

namespace std {
inline std::string to_string(kernel_tests::elementwise::KernelScalarity s)
{
	return  (s == kernel_tests::elementwise::KernelScalarity::Scalar) ? "Scalar" : "Array";
}
} // namespace std

std::ostream& operator<<(std::ostream&& os, kernel_tests::elementwise::KernelScalarity s) { return (os << std::to_string(s)); }


#endif /* KERNEL_TEST_ADAPTER_ARITHMETIC_OPERATION_H_ */
