/**
 * This simplest-possible kernel test adapter exists mostly
 * to test the parent class; its functionality should be wholly subsumed
 * by the more general BinaryOperation kernel test adapter...
 */
#ifndef SRC_TEST_ADAPTERS_SIMPLE_ADD_H_
#define SRC_TEST_ADAPTERS_SIMPLE_ADD_H_

#include "tester/KernelTestAdapter.h"

namespace kernel_tests {
namespace elementwise {

template<typename Datum>
class SimpleAdd : public KernelTestAdapter, public mixins::TestAdapterNameBuilder<SimpleAdd<Datum>> {
public:
	KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(SimpleAdd);

protected:
	bool        use_all_ones_as_data; // Use all-ones as the data
};

} //namespace elementwise
} // namespace kernel_tests

#endif /* SRC_TEST_ADAPTERS_SIMPLE_ADD_H_ */
