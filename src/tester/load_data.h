/*
 * load_data.h
 *
 * I want to separate this from the general kernel test adapter code, both because it's not
 * really test-adapter-specific, and because the implementation might use Boost or other libraries
 * which CUDA might not agree with too well.
 *
 * Notes:
 * - No Unicode support for now, tough cookies
 * - Still haven't figured
 *
 */

#ifndef SRC_LOAD_DATA_H_
#define SRC_LOAD_DATA_H_

#include "util/better_enums.hpp"

// 
#include <cstddef> // for size_t
#include <string>

namespace kernel_tests {

namespace data_loading {

BETTER_ENUM(FileFormat, char,
	csv,
	whitespace_delimited,  // and unescaped!
	separator_delimited,   // and unescaped!
	json,
	binary, // ignoring endianness and crossing our fingers
	binary_little_endian,
	binary_big_endian
)

bool can_be_multirecord(const std::string& file_format_name);

struct Configuration {
	FileFormat::_enumerated format;
	unsigned   column_index; // 0-based; only used in multi-column settings
	char       element_delimiter;
	char       record_delimiter;
	//bool       drop_extra_whitespace; // only relevant for non-whitespace-delimited data
	bool       elements_are_within_records;
		// when false, only the element delimiter will be used
		// (and no guarantee about newlines if they're not element delimiters, or other funky
		// white space; simples spaces should work and tabs also probably
	size_t     num_elements_to_skip; // !!!NOT IMPLEMENTED!!! Skip this many elements/records before reading
	size_t     desired_num_elements; // Do not attempt to read more than this many elements
};

template<typename T>
size_t load_from_single_line_buffer(
	const char* __restrict__  line,
	size_t                    line_length,
	T* __restrict__           destination,
	size_t                    allocated_length,
	const Configuration       config,
	size_t                    line_number = 1); // The first line has number 1

template<typename T>
size_t load_from_ifstream(
	std::ifstream&       stream,
	size_t               stream_data_length,
	T* __restrict__      destination,
	size_t               allocated_length,
	const Configuration  config);


/**
 * Read a sequence of values from a file (in a specified format), and place
 * them in a destination array
 *
 * @tparam T the type of value to read from the file
 * @param filename Locating of the data
 * @param destination
 * @param allocated_length The capacity of {@ref destination} in units of T;
 * no more than this number of elements may be read (and their presence may trigger an exception
 * @param config information regarding how the data is laid out and how to load it
 * @return The number of elements read (which will necessarily be between min_num_elements_to_read and
 * desired_num_elements - otherwise an exception will have been thrown)
 */
template <typename T>
size_t load_from_file(
	const std::string&   source_filename,
	T* __restrict__      destination,
	size_t               allocated_length,
	const Configuration  config);

} // namespace data_loading

} // namespace kernel_tests

std::ostream& operator<< (std::ostream& os, const ::kernel_tests::data_loading::Configuration& c);


#endif /* SRC_LOAD_DATA_H_ */
