
#include "tester/TestConfiguration.h"
#include "tester/BufferDescriptor.h"
#include "util/dump.h"
#include "util/poor_mans_reflection.h"
#include "util/miscellany.hpp"

namespace kernel_tests {

using util::memory_region;

bool BufferDescriptor::compareMemoryRepresentations(
	const TestConfiguration&  config,
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	UNUSED(config);
	UNUSED(descriptor);
	return (actual.size() == expected.size()) &&
		(std::memcmp(actual.data(), expected.data(), actual.size()) == 0);
}

template <typename T>
bool BufferDescriptor::trivial_comparator(
	const TestConfiguration&  config,
	const HostBuffers&        all_inputs           __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	UNUSED(config);
	UNUSED(descriptor);
	if (actual.size() != expected.size()) { return false; }
	if (!std::is_floating_point<T>::value) {
		return (std::memcmp(actual.data(), expected.data(), actual.size()) == 0);
	}
	gsl::span<const T> typed_actual = util::as_span<const T>(actual);
	gsl::span<const T> typed_expected = util::as_span<const T>(expected);
	return std::equal(
		typed_actual.cbegin(), typed_actual.cend(),
		typed_expected.cbegin(), [&config](const T& x, const T& y) {
			return util::kinda_equal<T>(x, y,
				config.use_absolute_difference_for_floating_point,
				config.floating_point_equality_threshold);
		});
}

POOR_MANS_REFLECTION_RESOLVER(BufferDescriptor::get_trivial_comparator, BufferDescriptor::trivial_comparator)

// TODO: Only pass the non-matching here
void BufferDescriptor::trivialMismatchPrinter(
	std::ostream&             os,
	const TestConfiguration&  config,
	const std::string&        buffer_name,
	const HostBuffers&        inputs               __attribute((unused)),
	const HostBuffers&        all_actual_outputs   __attribute((unused)),
	const HostBuffers&        all_expected_outputs __attribute((unused)),
	const BufferDescriptor&   descriptor,
	const memory_region       actual,
	const memory_region       expected)
{
	util::mismatch_listing_parameters params;
	params.max_mismatches_to_print             = config.num_elements_to_print;
	params.extra_info.length_and_data_type          = config.be_verbose;
	params.floating_point_absolute_differences = config.use_absolute_difference_for_floating_point;
	params.equality_threshold                  = config.floating_point_equality_threshold;
	params.numeric.floating_point_precision   = config.floating_point_printing_precision;
	params.numeric.printing_base =
		descriptor.prefer_hexadecimal_printing ?
			util::dump_parameters_t::numeric_t::printing_base_t::hex :
			util::dump_parameters_t::numeric_t::printing_base_t::dec;
	params.print_difference                    = descriptor.difference_value_is_meaningful_;

	// TODO: starting offset for printing
	params.terse = !config.be_verbose;
	util::print_mismatches(
		os,
		actual.data(), expected.data(),
		descriptor.data_type,
		actual.size() / util::size_of(descriptor.data_type),
		expected.size() / util::size_of(descriptor.data_type),
		buffer_name,
		params);
	os << '\n';
}

} // namespace kernel_tests
