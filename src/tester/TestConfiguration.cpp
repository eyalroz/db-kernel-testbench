/**
 * Notes:
 * - There is _no_ dependency on CUDA here (!)
 * - Too bad C++ doesn't have reasnable reflection yet, that would have saved a lot of code here.
 */

#include "tester/TestConfiguration.h"

#include <boost/program_options.hpp>
#include <fstream>
#include <stdexcept>
#include <cctype>
#include <array>

#include "util/exception.h"
#include "util/files.h"
#include "util/stl_algorithms.hpp"
#include "util/contains.hpp"
#include "util/miscellany.hpp"
#include "util/poor_mans_reflection.h"
#include "util/string.hpp"
#include "util/prettyprint.hpp"

using util::die;
namespace po = boost::program_options;
using std::string;
using util::optional;

USE_CONTAINER_FIND_FOR_CONTAINS_METHOD(po::variables_map)

static filesystem::path default_config_filename()
{
	return util::get_home_directory().value_or("/") / ".kerneltesterrc";
}

/**
 * @todo consider returning an optional
 */
static optional<std::pair<string, string>> explode_pair(string const& s, char delimiter)
{
	auto pos = s.find(delimiter);
	if (pos == std::string::npos) { return {}; }
	return std::make_pair(s.substr(0, pos), s.substr(pos+1));
}

namespace kernel_tests {


template <typename V>
V& update_with(V& updatee, const string& key, const po::variables_map& vm)
{
	if (util::contains(vm, key)) {
		updatee = vm[key].template as<V>();
	}
	return updatee;
}

template <> bool& update_with<bool>(
	bool& updatee, const string& key, const po::variables_map& vm)
{
	// Not very efficient I guess
	if (util::contains(vm, key)) {
		auto value = vm[key];
		try {
			updatee = vm[key].as<bool>();
		} catch(std::exception& e) { updatee = true; }
	}
	else { updatee = false; }
	return updatee;
}

template <> std::string& update_with<std::string>(
	std::string& updatee, const string& key, const po::variables_map& vm)
{
	if (util::contains(vm, key)) {
		updatee = util::lrchomp(vm[key].as<std::string>());
	}
	return updatee;
}

template <typename V>
optional<V>& update_with(optional<V>& updatee, const string& key, const po::variables_map& vm)
{
	if (util::contains(vm, key)) {
		// the "template" keyword is here for disambiguation only
		// (yes, it's kind of confusing).
		updatee = vm[key].template as<V>();
	}
	return updatee;
}


static void add_unrecognized_options(TestConfiguration& config, const std::vector<string>& unrecognized_options)
{

	// TODO: make sure all unrecognized options are of the form --option=value

	// I'm sure the following can be made more elegant...

	enum { OptionName, OptionValue } now_expecting = OptionName;
	std::string option_name;
	for(auto it = unrecognized_options.begin(); it != unrecognized_options.end(); it++) {
		std::string option_value;
		if (now_expecting == OptionValue) {
			option_value = *it;
			// Note! Not checking for '=' or ' ' characters in option values, if we know it's a value
			if (option_value.find_first_not_of('-') == 2) {
				// Got another option instead of a value, must be a flag option
				// std::cout << "Inserting " << option_name << " as empty\n";
				config.extra_options.insert(make_pair(option_name, std::string()));
				option_name = option_value;
				now_expecting = OptionValue;
				continue;
			}
		}
		else {
			std::string s(*it);
			auto key_starts = s.find_first_not_of('-');
			util::enforce(key_starts != string::npos, "Couldn't determine option name for " + s);
			auto option_name_ends = s.find_first_of(" =\n", key_starts);
			if (option_name_ends == string::npos) {
				now_expecting = OptionValue;
				option_name = s.substr(key_starts);
				continue;
			}
			option_name = s.substr(key_starts, option_name_ends - key_starts);
			auto value_starts = s.find_first_not_of(" =\n", option_name_ends + 1);
			if (value_starts == string::npos) {
				now_expecting = OptionValue;
				continue;
			}
			// string value = s.substr(value_starts);
			option_value = s.substr(value_starts);
		}
		// std::cout << "Inserting " << option_name << " = " << option_value << '\n';
		config.extra_options.insert(make_pair(option_name, option_value));
		now_expecting = OptionName;
	}
	if (now_expecting != OptionName) {
		// std::cout << "Inserting " << option_name << " as empty.\n";
		config.extra_options.insert(make_pair(option_name, std::string()));
	}

}

static tester_action_t determine_required_action(const po::variables_map vm)
{
	// determine action
	if (util::contains(vm,"list-platforms-and-devices") ||
		util::contains(vm,"list-devices")) {
		return +tester_action_t::list_platforms_and_devices;
	}
	if (util::contains(vm,"list-device-properties")) {
		return  +tester_action_t::list_device_properties;
	}
	if (util::contains(vm,"list-test-adapters") ||
		util::contains(vm,"list-tests")) {
		return  +tester_action_t::list_test_adapters;
	}
	if (util::contains(vm,"list-kernels")) {
		return  +tester_action_t::list_kernels;
	}
	if (util::contains(vm,"describe-kernel-test") ||
		util::contains(vm,"describe-test") ||
		util::contains(vm,"describe-a-test") ||
		util::contains(vm,"describe-a-kernel-test") ||
		util::contains(vm,"describe")) {
		return  +tester_action_t::describe_a_kernel_test;
	}
	if (util::contains(vm,"print-number-of-kernels-in-test") ||
		util::contains(vm,"print-num-kernels-in-test") ||
		util::contains(vm,"number-of-kernels-in-test") ||
		util::contains(vm,"num-kernels-in-test")
		) {
		return  +tester_action_t::print_number_of_kernels_in_a_test;
	}
	if (util::contains(vm,"only-print-config")
		) {
		return  +tester_action_t::print_test_configuration;
	}
	return +tester_action_t::conduct_a_test;

}

// Notes:
// - Don't worry about returning by value. 1. It happens once 2. The compiler optimizes it
// - This could have been a constructor... but we don't want to expose po:variables_map
//   in the class.
static TestConfiguration make_test_configuration(
	const po::variables_map vm,
	const std::vector<string> unrecognized_options = std::vector<string>())
{
	TestConfiguration config;

	config.tester_action = determine_required_action(vm);

	update_with(config.config_file,                  "config-file",             vm);
	update_with(config.num_test_runs,                "runs",                    vm);
	update_with(config.floating_point_equality_threshold,
		"floating-point-equality-threshold",                                    vm);
	update_with(config.floating_point_printing_precision,
		"floating-point-printing-precision",                                    vm);
	update_with(config.use_absolute_difference_for_floating_point,
		"floating-point-absolute-diff",                                         vm);

	update_with(config.print_test_config,            "print-test-config",       vm);
	update_with(config.print_inputs,                 "print-inputs",            vm);
	update_with(config.print_outputs,                "print-outputs",           vm);
	update_with(config.print_expected_outputs,       "print-expected-outputs",  vm);
	update_with(config.print_result,                 "print-result",            vm);
	update_with(config.print_mismatches,             "print-mismatches",        vm);
	update_with(config.print_launch_details,         "print-launch-details",    vm);
	update_with(config.print_device_function_attributes, "print-device-function-attributes",    vm);
	update_with(config.print_buffer_listing,         "print-buffer-listing",    vm);
	update_with(config.print_result,                 "print-result",            vm);
	update_with(config.test_length,                  "length",                  vm);
	update_with(config.num_elements_to_print,        "elements-to-print",       vm);
	update_with(config.start_printing_from,          "start-printing-from",     vm);
	update_with(config.thread_serialization_factor,  "serialization-factor",    vm);
	update_with(config.use_mapped_region_pairs,      "use-mapped-region-pairs", vm);
	update_with(config.backend,                      "backend",                 vm);
	update_with(config.execution_platform,           "platform",                vm);
	update_with(config.test_device,                  "device",                  vm);
	update_with(config.max_threads_per_block,        "limit-block-size",        vm);
	update_with(config.test_adapter_name,            "test-adapter",            vm);
	update_with(config.random_seed,                  "random-seed",             vm);
	update_with(config.seed_by_time,                 "seed-by-time",            vm);
	update_with(config.assume_results_correct,       "assume-results-correct",  vm);
	update_with(config.monet_db_directory,           "monetdb-dir",             vm);
	update_with(config.input_file,                   "input-file",              vm);
	update_with(config.input_file_format,            "input-file-format",       vm);
	update_with(config.input_file_element_delimiter, "input-file-element-delimiter",  vm);
	update_with(config.input_file_record_delimiter,  "input-file-record-delimiter",  vm);

	config.zero_output_and_scratch_buffers =
		(util::contains(vm,"zero-output-and-scratch-buffers")
	     and vm["zero-output-and-scratch-buffers"].as<std::vector<bool>>().size() > 0 );

	if (not config.test_adapter_name.empty()) {
		// Just in case somebody added spaces or newlines at the end of the adapter name. It happens!
		config.test_adapter_name = util::rtrim(config.test_adapter_name);
	}

	// TODO: I'm sure program_options has some mechanism to automate the following...
	std::vector<std::string> inputs_from_db_with_eq_separator;
	update_with(inputs_from_db_with_eq_separator, "read-from-db", vm);
	for(const auto& s : inputs_from_db_with_eq_separator ) {
		if (s.find('=') == std::string::npos) {
			throw util::invalid_argument("To read a buffer from the DB, "
				"the specification must have the form name=db_schema.table_column pair; specification \""
				+ s + "\" has no '=' character");
		}
		// Each occurrence of --read-from-db should be followed by foo=bar,
		// i.e. use DB column bar for input with tneame foo
		auto target_and_source = explode_pair(s,'=');
		if (has_value(target_and_source)) {
			config.db_column_mappings.insert(target_and_source.value());
		}
		else throw util::invalid_argument("Failed parsing buffer_name=db_schema.table_column pair \"" + s + "\"");
	}

	if (util::contains(vm,"verbose") and vm["verbose"].as<std::vector<bool>>().size() > 0 )
	{
		config.be_verbose                        = true;
		config.print_test_config                 = true;
		config.print_inputs                      = true;
		config.print_outputs                     = true;
		config.print_expected_outputs            = true;
		config.print_result                      = true;
		config.print_mismatches                  = true;
		config.print_launch_details              = true;
		config.print_buffer_listing              = true;
		config.print_device_function_attributes  = true;
	}

	if (has_value(config.input_file)) {
		util::enforce(has_value(config.input_file_format),
			"An input file format must be specified along with an input file");
	}
	util::enforce(config.max_threads_per_block.value_or(1) > 0,
		"Blocks must have _some_ threads.");
	util::enforce(config.thread_serialization_factor.value_or(1) > 0,
		"Threads must process _some_ input elements.");
	util::enforce(config.test_length > 0, "Test length must be positive");

	add_unrecognized_options(config, unrecognized_options);
	return config;
}


static void update_with(
		po::variables_map& vm, const string& filename, const po::options_description& desc,
		std::vector<string>& unrecognized_options)
{
	if (not filesystem::exists(filename)) {
		die("Missing configuration file \"" + filename + "\"");
	}
	// TODO: Make sure config file values dont override existing values
	if (not util::is_readable(filename)) {
		die("Unreadable configuration file \"" + filename + "\"");
	}
	std::ifstream settings_file(filename);
	// This allows reading options which are recognized by specific
	// test adapters, but not by the code here
	auto allow_unregistered_options = true;
	auto parse_result = po::parse_config_file(settings_file, desc, allow_unregistered_options);
	po::store(parse_result, vm);
	auto unrecognized_options_in_file =
		po::collect_unrecognized(parse_result.options, po::include_positional);
	po::notify(vm);
	util::append(unrecognized_options, unrecognized_options_in_file);
}

} //namespace kernel_tests

namespace boost {
namespace program_options {
template <typename T>
void set_in_options(variables_map m, const string& option_name, const T& value) {
	m.insert(std::make_pair(option_name, variable_value(value, false)));
}
void set_in_options(variables_map m, const string& option_name) {
	m.insert(std::make_pair(option_name, variable_value(true, false)));
}
} // namespace program_options
} // namespace boost

namespace kernel_tests {

/**
 *
 * For a full path name such as "/path/to/foo" or "relative/path/to/foo" ,
 * returns just "foo"
 *
 * @param path Full or relative path with many potential subdirectories
 * @return The last element in the path, be it a filename or a directory name
 */

void print_usage(char* invoked_binary_path, const po::options_description& cmdline_options) {
	filesystem::path binary_path(invoked_binary_path);
	std::cout
		<< "Usage: " << binary_path.leaf() << " [OPTION]... \n"
		<< "Executes a run of the CUDA kernel test adapter (which _must_ be specified.\n\n"
		<< cmdline_options	<< '\n' << "Note: Default options are read from "
		<< default_config_filename() << '\n' << '\n';
}

TestConfiguration TestConfiguration::getFromProgramArguments(int argc, char** argv)
{

	po::options_description generic_options("Command-line-only options");
	generic_options.add_options()
		("help,h",                                     "print this help messages")
		("config-file,f", po::value<string>(),         "use the specified configuration file")
		("list-platforms-and-devices,L",               "list the execution platforms (for all backends) and their known devices")
		("list-device-properties,S",                   "list the properties of the designated test device")
		("list-test-adapters,T",                       "list all available kernel test adapters")
		("list-kernels,K",                             "list all registered kernels")
		("describe-kernel-test,D",                     "describe the specified kernel test adapter rather than run the test")
		("only-print-config,O",                        "Only print a kernel test's configuration, but don't run it")
		("print-num-kernels-in-test,N",                "print the number of kernels launched by the specified test adapter")
		;

	po::options_description config_options("Config file / command-line options");
	std::string db_input_assignment;
	config_options.add_options()
		("test-adapter,t", po::value<string>(), "name of the kernel tester adapter to run (essentially, which kernel to test)")
		("verbose,v",
			po::value<std::vector<bool>>()
				->default_value(std::vector<bool>(), "false")
				->implicit_value(std::vector<bool>(1), "true")
				->zero_tokens(),
		                                        "print everything and be verbose about test progression")
		("print-test-config,c",                 "print the test configuration")
		("print-inputs,i",                      "print generated input data")
		("print-outputs,o",                     "print kernel output data")
		("print-expected-outputs,e",            "print expected output data")
		("print-mismatches,m",                  "print mismatches between actual and expected kernel output")
		("print-launch-details,a",              "print the kernel launch configuration details")
		("print-device-function-attributes,A",  "print the kernels' attributes as compiled on-device functions")
		("print-buffer-listing,B",              "List the buffers related to the kernell tests after their sizes are finalized")
		("print-result,r",po::value<bool>(),    "print the kernel test's final result")
		("backend,b",po::value<std::string>(),  "execution backend to use (CUDA or OpenCL)")
		("execution-platform,P", po::value<unsigned>(),
			                                    "index of execution platform with which the device is associated")
		("device,d", po::value<unsigned>(),     "index of CUDA device to test the kernel on")
		("random-seed", po::value<unsigned long long>(),
		                                        "Pseudo-random number generation seed to use")
		("seed-by-time",                        "Use the current time as the seed for pseudo-random number generation")
		("zero-output-and-scratch-buffers,z",
			po::value<std::vector<bool>>()
				->default_value(std::vector<bool>(), "false")
				->implicit_value(std::vector<bool>(1), "true")
				->zero_tokens(),
			"Set output buffers and scratch area contents to zero before execution")
		("assume-results-correct,C",            "Assume the results are correct and don't check them")
		("monetdb-dir,M", po::value<string>(),  "Directory of a MonetDB database to use for input columns")
 		("input-file,I", po::value<string>(),   "Read test input buffer data from the specified file")
		("input-file-format,F", po::value<string>(),
		                                        "Format of the input data file. Supported values: "
		                                        "csv, whitespace_delimited, separator_delimited, binary")
		("read-from-db,R", po::value<std::vector<std::string>>()->composing(),
			                                    "Use a DB column as one of the input columns")
		("length,l", po::value<size_t>(),       "basic length of generated kernel inputs")
		("runs,num-runs,number-of-runs,n", po::value<size_t>(),
		                                        "Number of runs to execute (for profiling/stability purposes)")
		("elements-to-print,p", po::value<size_t>(),
		                                        "maximum number of elements to print in each vector")
		("start-printing-from,s", po::value<size_t>(),
		                                        "element index in each vector from which to start printing")
		("limit-block-size", po::value<unsigned>(),
		                                        "Limit on the maximum number of threads per GPU grid block")
		("serialization-factor", po::value<unsigned short>(),
		                                        "The number of input elements each GPU grid thread will handle "
		                                        "(before any communication with other threads.)")
		("use-mapped-region-pairs", po::value<bool>(),
		                                        "Used CUDA's mapped memory for no-explicit-copy buffers "
		                                        "for testing kernels.")
		("input-file-element-delimiter", po::value<char>(),
		                                        "Input file inter-element delimiter character (default = '|')")
		("input-file-record-delimiter", po::value<char>(),
		                                        "Input file inter-record (inter-line) delimiter character "
		                                        "(default is a newline)")
		("floating-point-equality-threshold", po::value<double>(),
		                                        "Relative difference  up to which two floating-point values"
		                                        "being compared are considered to be equal")
		("floating-point-printing-precision", po::value<unsigned>(),
		                                        "The number of significant digits of a floating point value to print")
		("floating-point-absolute-diff",po::value<bool>(),
		                                        "Use absolute, rather than relative, differences when comparing "
		                                        "floating point values")
		;


	po::options_description cmdline_options;
	cmdline_options.add(generic_options).add(config_options);

	po::variables_map variables_map;

	try {
		auto parse_result = po::basic_command_line_parser<char>(argc, argv)
				.options(cmdline_options)
				.allow_unregistered()  // to pass those forward to the test adapter
				.run();
		po::store(parse_result, variables_map);
		if (util::contains(variables_map, "help") || argc == 1)
		{
			print_usage(argv[0], cmdline_options);
			exit(EXIT_SUCCESS);
		}
		po::notify(variables_map); // Remember this throws on error
		std::vector<string> unrecognized_options =
				po::collect_unrecognized(parse_result.options, po::include_positional);

		auto first_remaining_argument_is_positional =
			not unrecognized_options.empty() and (unrecognized_options.front().front() != '-');

		// This does not cover all cases of us getting positional parameters. In fact, it's quite
		// likely the position parameter will be _last. Unfortunately,t hough, we don't get
		// "collected key-value pairs", just a vector of strings, from collect_unrecognized.
		if (first_remaining_argument_is_positional) {
			throw boost::program_options::error("Positional parameters are not used.");
		}
		if (util::contains(variables_map, "config-file")) {
			update_with(
				variables_map, variables_map["config-file"].as<string>(), config_options, unrecognized_options);
		}
		filesystem::path p = default_config_filename();
		if (filesystem::exists(p) && util::is_readable(p)) {
			update_with(variables_map, p.string(), config_options, unrecognized_options);
		}

		if (util::contains(variables_map, "list-platforms-and-devices") ||
			util::contains(variables_map, "list-device-properties") ||
			util::contains(variables_map, "list-kernels") ||
			util::contains(variables_map, "list-test-adapters")) {
			// Don't care about the test adapter
			return make_test_configuration(variables_map);
		}
		else {
			if (not util::contains(variables_map, "test-adapter")) {
				die("No test adapter specified");
			}
		}

		return make_test_configuration(variables_map, unrecognized_options);
	}
	catch (boost::program_options::required_option& e) {
		die(e, "Program invocation error");
	}
	catch (boost::program_options::error& e) {
		die(e, "Program invocation error");
	}
	// Can't get here
	return TestConfiguration();
}

std::string pretty_print(const TestConfiguration& c)
{
	  std::stringstream ss;
	  using std::setw;
	  auto cw = TestConfiguration::pretty_printing_name_column_width;
	  //auto toft = util::true_or_false_title;
	  ss << std::boolalpha << std::left;
	  ss  <<             "Kernel Test Configuration \n"
		  <<             "--------------------------\n"
		  << setw(cw) << "Test action"                     << c.tester_action._to_string() << '\n'
		  << setw(cw) << "Configuration file used"         << c.config_file << '\n'
		  << setw(cw) << "Kernel test adapter name"        << c.test_adapter_name << '\n'
		  << setw(cw) << "Test data length in elements"    << c.test_length << '\n'
		  << setw(cw) << "Read columns from the MonetDB database at"
		                                                   << c.monet_db_directory << '\n'
          << setw(cw) << "Buffers to read from DB columns" << c.db_column_mappings << '\n'

		  << setw(cw) << "Input data file"                 << c.input_file << '\n'
		  << setw(cw) << "Input data file format"          << c.input_file_format << '\n'
		// fugly; perhaps include "data_loading.h" here?
		  << setw(cw) << "Input data file element delimiter" << (c.input_file_element_delimiter ?
			  	util::sanitize(c.input_file_element_delimiter.value(), 0, true) : "(unset)"  ) << '\n'
		// fugly; perhaps include "data_loading.h" here?
		  << setw(cw) << "Input data file record delimiter" << (c.input_file_record_delimiter ?
			  	util::sanitize(c.input_file_record_delimiter.value(), 0, true) : "(unset)"  ) << '\n'
		  << setw(cw) << "Pseudo-random number seed"       << c.random_seed << '\n'
		  << setw(cw) << "Seed pseudo-random numbers by time"
		                                                   << c.seed_by_time << '\n'
		  << setw(cw) << "Set output and scratch buffers to zero"
		                                                   << c.zero_output_and_scratch_buffers << '\n'
		  << setw(cw) << "Assume results are correct"      << c.assume_results_correct << '\n'
		  << setw(cw) << "Print test-related buffer listing" << c.print_buffer_listing << '\n'
		  << setw(cw) << "Print test result"               << c.print_result << '\n'
		  << setw(cw) << "Print test configuration"        << c.print_test_config << '\n'
		  << setw(cw) << "Print inputs"                    << c.print_inputs << '\n'
		  << setw(cw) << "Print outputs"                   << c.print_outputs << '\n'
		  << setw(cw) << "Print expected outputs"          << c.print_expected_outputs << '\n'
		  << setw(cw) << "Print mismatches"                << c.print_mismatches << '\n'
		  << setw(cw) << "Print kernel launch details"     << c.print_launch_details << '\n'
		  << setw(cw) << "Print kernel device function attributes"
		  	                                               << c.print_device_function_attributes << '\n'
		  << setw(cw) << "Be generally verbose"            << c.be_verbose << '\n'
		  << setw(cw) << "Execution backend"               << c.backend << '\n'
		  << setw(cw) << "Execution platform"              << c.execution_platform << '\n'
		  << setw(cw) << "Test device"                     << c.test_device << '\n'
		  << setw(cw) << "Thread-per-block limit"          << c.max_threads_per_block << '\n'
		  << setw(cw) << "Per-thread serialization factor" << c.thread_serialization_factor << '\n'
		  << setw(cw) << "Number of test runs to perform"  << c.num_test_runs << '\n'
		  << setw(cw) << "Floating point equality threshold"
		                                                   << c.floating_point_equality_threshold << '\n'
		  << setw(cw) << "Floating point printing_precision" << c.floating_point_printing_precision << '\n'
		  << setw(cw) << "Floating point differencing"
		  	  << (c.use_absolute_difference_for_floating_point ? "absolute" : "relative") << '\n'
		  << setw(cw) << "Number of elements to print"     << c.num_elements_to_print << '\n'
	  	  << setw(cw) << "Start printing from index"       << c.start_printing_from << '\n'
  	  	  << setw(cw) << "Use mapped host-device regions for buffers"
  	  	  	                                               << c.use_mapped_region_pairs << '\n';

	  return ss.str();
}

} // namespace kernel_tests
