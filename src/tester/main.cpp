/**
 * A kernel test program
 * (relatively) easily adaptible to many simple (DBMS-related) kernels
 * by Eyal Rozenberg
 */

#include "tester/TestConfiguration.h"
#include "DualAspectBuffer.h"
#include "tester/KernelTestAdapter.h"
#include "list_producible_subclasses.hpp"

#include "kernel_wrappers/registered_wrapper.h" // for listing available CUDA kernels

#include <cuda/printing.h>
#include <cuda/api_wrappers.hpp>

#include "util/dump.h"
#include "util/exception.h"
#include "util/math.hpp"
#include "util/miscellany.hpp"
#include "util/poor_mans_reflection.h"
#include "util/random.h"
#include "util/stl_algorithms.hpp"
#include "util/contains.hpp"
#include "util/algorithm_container_adapters.hpp"
#include "util/maps.hpp"
#include "util/type_name.hpp"
#include "util/chrono.hpp"
#include "util/program.h"
#include "util/files.h" // for util::file_contents
#include "util/statistics.hpp"

#include "monetdb-buffer-pool/buffer_pool.h"

#include <gsl/gsl-lite.hpp>
#include <boost/iterator/filter_iterator.hpp>

#include <cuda_runtime_api.h>

#include <iostream>
#include <cstddef> // for size_t
#include <cstdlib> // for EXIT_SUCCESS/EXIT_FAILURE
#include <algorithm>
#include <string>
#include <unordered_map>
#include <type_traits>
#include <memory>
#include <chrono>
#include <array>
#include <iomanip> // for put_time

using cuda::HostSide;
using cuda::DeviceSide;

using std::string;
using std::shared_ptr;
using std::unique_ptr;
using std::cout;
using std::flush;
using std::cerr;
using std::exception;
using std::unordered_map;
using std::setw;
using std::setprecision;
using std::size_t;
using kernel_tests::DualAspectBuffer;
using DualAspectBuffers = unordered_map<string, shared_ptr<DualAspectBuffer>>;
using namespace kernel_tests;
using BufferDescriptors = KernelTestAdapter::BufferDescriptors;
using access_type_t = DualAspectBuffer::access_type_t;
using util::optional;

void dump_buffer(
	std::ostream&            destination,
	const std::string&       name,
	const BufferDescriptor&  descriptor,
	memory_region            raw_buffer,
	optional<unsigned>       floating_point_printing_precision,
	optional<size_t>         start_element_index = nullopt,
	optional<size_t>         max_num_elements = nullopt)
{
	util::dump_parameters_t dump_params;
	dump_params.subrange_to_print.start = start_element_index;
	if (has_value(max_num_elements)) {
		dump_params.subrange_to_print.set_length(max_num_elements.value());
	}
	dump_params.numeric.printing_base =
		descriptor.prefer_hexadecimal_printing ?
			util::dump_parameters_t::numeric_t::printing_base_t::hex :
			util::dump_parameters_t::numeric_t::printing_base_t::dec;
	dump_params.numeric.floating_point_precision =
		floating_point_printing_precision;
	if (descriptor.dump_bits) {
		dump_params.dump_bits = true;
//		dump_params.numeric.fill_with_zeros = true;
//		dump_params.numeric.printing_base =
//			util::dump_parameters_t::numeric_t::printing_base_t::hex;
	}
	size_t length = raw_buffer.size() / util::size_of(descriptor.data_type);
	if (start_element_index and max_num_elements) {
		dump_params.subrange_to_print.end = start_element_index.value() + max_num_elements.value();
	}
	else {
		dump_params.subrange_to_print.end = std::max<size_t>(length, max_num_elements);
	}
	util::dump(destination, raw_buffer.data(), descriptor.data_type, length, name, dump_params);
}

void dump_buffers(
	const BufferDescriptors&  descriptors,
	size_t                    start_element_index,
	size_t                    max_num_elements,
	const HostBuffers&        buffers,
	optional<unsigned>        floating_point_printing_precision,
	std::ostream&             destination,
	string                    name_suffix = "")
{
	destination << '\n';
	for (const auto& map_entry: buffers) {
		auto buffer_name = map_entry.first;
		auto raw_buffer = map_entry.second;
		auto descriptor = descriptors.at(buffer_name);
		auto name_with_suffix = buffer_name + ' ' + name_suffix;

		dump_buffer(destination, name_with_suffix, descriptor, raw_buffer,
			floating_point_printing_precision,
			start_element_index, max_num_elements);
		destination << '\n';
	}
}

inline void reconcile_buffer_aspects(const TestConfiguration& config, const DualAspectBuffers& buffers)
{
	size_t actively_reconciled = 0;
	// TODO: The buffer I/O code assumes no exceptions - change that maybe
	for(auto& map_entry : buffers) {
		DualAspectBuffer& buffer = *map_entry.second;
		if (buffer.reconciled()) { continue; }
		if (config.be_verbose) {
			cout << "Reconciling the aspects of buffer \"" << map_entry.first << "\""
				<< ": Copying "	<< buffer.size() << " bytes "
				<< (buffer.last_modified_aspect() == HostSide ?
				"Host -> Device" : "Device -> Host") << '\n';
		}
		buffer.reconcile_aspects();
		actively_reconciled++;
	}
	if (config.be_verbose) {
		if (!buffers.empty()) {
			cout << buffers.size() << " buffer(s) are now reconciled ("
				<< actively_reconciled << " of them required copying).\n";
		}
	}
}

// This is just a pretty-print into a string of the key set. Combine
// those components
std::string buffer_name_set(const DualAspectBuffers& buffers) {
	std::stringstream ss;
	auto keys = util::get_keys<DualAspectBuffers, std::unordered_set<string>>(buffers);
	ss << "{ ";
	auto separator = "";
	for(const auto & key : keys) {
		ss << separator << key;
		separator = ", ";
	}
	ss << " }" ;
	return ss.str();
}

enum : bool { SetSizeToCapacity = true, DontSetSizeToCapacity = false };

template <cuda::Aspect AspectOfInterest>
Buffers<AspectOfInterest> get_single_aspect(
	const DualAspectBuffers& buffers,
	access_type_t access_type = access_type_t::read_only,
	bool set_size_to_capacity = DontSetSizeToCapacity)
{
	Buffers<AspectOfInterest> result;
	for (const auto& map_entry : buffers) {
		auto& dual_aspect_buffer = map_entry.second;
		auto region = dual_aspect_buffer->get(AspectOfInterest, access_type);
		auto sized_region = memory_region(region.data(),
			set_size_to_capacity ? dual_aspect_buffer->capacity() : dual_aspect_buffer->size()
		);
		result.insert(std::make_pair(map_entry.first, sized_region));
	}
	return result;
}

HostBuffers check_kernel_output_correctness(
	const TestConfiguration& config,
	const BufferDescriptors& descriptors,
	const HostBuffers& all_inputs,
	const HostBuffers& all_actual,
	const HostBuffers& all_expected)
{
	util::enforce(all_expected.size() > 0,
		"Test harness error: No expected outputs to check against!");
	util::enforce(all_actual.size() == all_expected.size(),
		"Different number of actual and expected outputs (not a kernel error!)");
	// TODO: Ensure there are no dupe names between outputs and inouts.

	HostBuffers diverging;

	for(const auto& entry : all_expected) {
		const string& buffer_name = entry.first;
		memory_region expected_raw_buffer = entry.second;
		auto buffer_descriptor = descriptors.at(buffer_name);
		memory_region actual_raw_buffer = all_actual.at(buffer_name);
		if (!buffer_descriptor.match_checker(
			config,
			all_inputs,	all_actual, all_expected,
			buffer_descriptor, actual_raw_buffer, expected_raw_buffer))
		{
			diverging.insert(make_pair(buffer_name, actual_raw_buffer));
		}
	}
	return diverging;
}

void print_mismatches(
	const KernelTestAdapter& test_adapter,
	const BufferDescriptors& descriptors,
	const HostBuffers& mismatched_inouts_and_outputs,
	const HostBuffers& inputs,
	const HostBuffers& all_actual_inouts_and_outputs,
	const HostBuffers& expected_inouts_and_outputs)
{
	cout << '\n';
	for (const auto& map_entry: mismatched_inouts_and_outputs) {
		auto buffer_name = map_entry.first;
		const BufferDescriptor& descriptor = descriptors.at(buffer_name);
		memory_region actual = map_entry.second;
		memory_region expected = expected_inouts_and_outputs.at(buffer_name);
		auto data_type = descriptor.data_type;
		descriptor.mismatch_printer(
			std::cout, test_adapter.config,
			buffer_name, inputs, all_actual_inouts_and_outputs,
			expected_inouts_and_outputs, descriptor, actual, expected);
		cout << '\n';
	}
}

DualAspectBuffers create_uninitialized_buffers(
	const TestConfiguration& config,
	const BufferDescriptors& buffer_descriptors,
	string which_kind_buffers)
{
	DualAspectBuffers buffers;

	for (const auto& map_entry : buffer_descriptors) {
		auto& buffer_name = map_entry.first;
		auto& buffer_descriptor = map_entry.second;
		auto& buffer_capacity = buffer_descriptor.capacity;
		const std::string& buffer_data_type = buffer_descriptors.at(buffer_name).data_type;

		if (config.be_verbose) {
			cout << "Creating " << which_kind_buffers << " buffer \"" <<
				buffer_name << "\" (" << buffer_descriptor.capacity / util::size_of(buffer_data_type) <<
				" " << buffer_descriptor.data_type << " elements, " <<
				buffer_capacity << " bytes; " <<
				(config.use_mapped_region_pairs ? "" : "not ") << "using mapped memory)\n";
		}
		auto buffer_ptr = std::make_shared<DualAspectBuffer>(
			config.test_device, buffer_capacity, config.use_mapped_region_pairs);
		// TODO: Maybe use shared_ptr's so as not to leak?
		buffers.insert(std::make_pair(buffer_name, buffer_ptr));
	}
	return buffers;
}

void update_buffer_sizes(
	const KernelTestAdapter&       test_adapter,
	DualAspectBuffers              buffers,
	KernelTestAdapter::BufferSizes buffer_size_updates)
{
	auto config = test_adapter.config;
	auto descriptors = test_adapter.getBufferDescriptors();

	for(const auto& entry : buffer_size_updates) {
		auto buffer_name = entry.first;
		auto filled_size = entry.second;
		if (not util::contains(buffers, buffer_name)) {
			throw util::logic_error(util::concat_string(
				"Kernel test adapter requested a size update of non-existent buffer \"",
				buffer_name, "\""));
		}
		buffers[buffer_name]->set_size(filled_size);
		auto capacity = descriptors.at(buffer_name).capacity;
		util::enforce(filled_size <= capacity,
			"Buffer \"", buffer_name, "\" filled beyond capacity (fill size ",
			filled_size, ", capacity ", capacity, ")");
		if (config.be_verbose) {
			std::cout << "Buffer \"" << buffer_name << "\" has size "
				<< util::value_and_shortened_form(filled_size)
				<< " bytes after being filled (its capacity is "
				<< util::value_and_shortened_form(capacity) << ").\n";
		}
	}

}

BufferDescriptors keep_only_direction(const BufferDescriptors& descriptors, const BufferDirection direction)
{
	return util::keep_only(descriptors,
		[&direction](const typename BufferDescriptors::value_type& e) {
			BufferDescriptor d = e.second;
			return d.direction == direction;
		});
}

DualAspectBuffers keep_only_direction(const DualAspectBuffers& buffers,
	const BufferDescriptors& descriptors, const BufferDirection direction)
{
	return util::keep_only(buffers,
		[&descriptors, direction](const DualAspectBuffers::value_type& e) {
			auto name = e.first;
			auto descriptor = descriptors.at(name);
			return descriptor.direction == direction;
		});
}

// TODO: Perhaps call this function once for inputs and once for inouts?
std::pair<DualAspectBuffers, DualAspectBuffers> generate_inputs_and_inouts(
	KernelTestAdapter& test_adapter, const DualAspectBuffers& apriori_buffers)
{
	auto config = test_adapter.config;
	auto descriptors = test_adapter.getBufferDescriptors();
	auto input_descriptors = keep_only_direction(descriptors, BufferDirection::Input);
	auto inout_descriptors = keep_only_direction(descriptors, BufferDirection::InOut);

	auto is_not_apriori = [&apriori_buffers](const BufferDescriptors::value_type& name_and_descriptor) {
		auto buffer_name = name_and_descriptor.first;
		return !util::contains(apriori_buffers, buffer_name);
	};
	auto generated_input_descriptors = util::keep_only(input_descriptors, is_not_apriori);
	auto generated_inout_descriptors = util::keep_only(inout_descriptors, is_not_apriori);

	auto generated_inputs = create_uninitialized_buffers(config, generated_input_descriptors, "input");
	auto generated_inouts = create_uninitialized_buffers(config, generated_inout_descriptors, "inout");
	// ... we've not generated them just yet though.

	// Note: For the newly-created buffers, we replace their size (which is 0 on creation)
	// with the capacity, so that we have memory regions we can work with in the actual generation;
	// we'll resize them later
	auto input_host_side_aspects = get_single_aspect<HostSide>(generated_inputs, access_type_t::read_write, SetSizeToCapacity);
	auto inout_host_side_aspects = get_single_aspect<HostSide>(generated_inouts, access_type_t::read_write, SetSizeToCapacity);
	auto apriori_buffers_host_side_aspects =
		get_single_aspect<HostSide>(apriori_buffers, access_type_t::read_only, DontSetSizeToCapacity);
	auto all_host_aspects = util::get_union(
		input_host_side_aspects, inout_host_side_aspects, apriori_buffers_host_side_aspects);

	if (!generated_inputs.empty() or !generated_inout_descriptors.empty()) {
		if (config.be_verbose) {
			cout << "Generating contents of ";
			if (!generated_inputs.empty()) {
				cout << "input buffers: " << buffer_name_set(generated_inputs);
			}
			if (!generated_inputs.empty() && !generated_inouts.empty()) { cout << " and "; }
			if (!generated_inouts.empty()) {
				cout << "in-out buffers " << buffer_name_set(generated_inouts);
			}
			if (!apriori_buffers.empty()) {
				cout << " (having apriori buffers " << buffer_name_set(apriori_buffers) << ")";
			}
			cout << '\n';
		}
		auto input_size_updates = test_adapter.generateInputs(all_host_aspects);
		update_buffer_sizes(test_adapter, generated_inputs, input_size_updates);
		if (config.be_verbose) {
			cout << "Done generating input and/or in-out buffers.\n";
		}
	}
	else {
		if (config.be_verbose) {
			cout << "No need to generate any input or in-out buffers (but some non-buffer work may be necessary).\n";
		}
		auto input_size_updates = test_adapter.generateInputs(all_host_aspects);
		util::enforce(input_size_updates.empty(),
			"Size updates requested while there are no input or in-out buffers");
	}

	return std::make_pair(generated_inputs, generated_inouts);
}

DualAspectBuffers generate_expected(
	KernelTestAdapter& test_adapter, const DualAspectBuffers& inputs,
	const DualAspectBuffers& inouts, const DualAspectBuffers& apriori_expected)
{
	auto config = test_adapter.config;
	auto all_descriptors = test_adapter.getBufferDescriptors();
	auto inout_descriptors = keep_only_direction(all_descriptors,BufferDirection::InOut);
	auto output_descriptors = keep_only_direction(all_descriptors,BufferDirection::Output);
	auto is_not_apriori = [&apriori_expected](const BufferDescriptors::value_type& name_and_descriptor) {
		auto buffer_name = name_and_descriptor.first;
		return !util::contains(apriori_expected, buffer_name);
	};
	auto generated_inout_descriptors = util::keep_only(inout_descriptors, is_not_apriori);
	auto generated_output_descriptors = util::keep_only(output_descriptors, is_not_apriori);

	if (generated_inout_descriptors.empty() and generated_output_descriptors.empty()) {
		if (config.be_verbose) {
			cout << "No expected buffers to generate.\n";
		}
		return {};
	}

	if (config.be_verbose) {
		cout << "Generating " << generated_inout_descriptors.size() << " expected in-out buffers "
			 << "and " << generated_output_descriptors.size() << " expected output buffers \n" << flush;
	}
	DualAspectBuffers generated_expected_inouts  = create_uninitialized_buffers(config,
		generated_inout_descriptors, "expected in-out buffers");
	DualAspectBuffers generated_expected_outputs = create_uninitialized_buffers(config,
		generated_output_descriptors, "expected output buffers");
	DualAspectBuffers all_generated_expected_buffers =
		util::get_union(generated_expected_inouts, generated_expected_outputs);
	//auto host_side_aspect = get_single_aspect<HostSide>(generated_expected_buffers, access_type_t::read_only);

	// Generating the expected output data yields the intended sizes
	// of the output data buffers as well (where initially we only had
	// upper bounds on those sizes)

	auto expected_size_updates = test_adapter.generateExpectedOutputs(
		get_single_aspect<HostSide>(all_generated_expected_buffers, access_type_t::read_write, SetSizeToCapacity),
		util::get_union(
			get_single_aspect<HostSide>(inputs, access_type_t::read_only),
			get_single_aspect<HostSide>(inouts, access_type_t::read_only),
			get_single_aspect<HostSide>(apriori_expected, access_type_t::read_only)
		)
	);
	update_buffer_sizes(test_adapter, all_generated_expected_buffers, expected_size_updates);
	if (config.be_verbose) {
		cout << "Done generating " << all_generated_expected_buffers.size()
		     << " expected output/inout buffers.\n" << std::flush;
	}
	return all_generated_expected_buffers;
}

/**
 * Requests the test adapter to launch the kernel and waits for it to conclude
 * (successfully); does not perform any I/O, neither before or after execution.
 *
 * @note assumes the device on which we want to schedule work is already current

 * @param test_adapter Chosen test adapter
 * @param stream the stream on which to schedule work
 * @param inputs Buffers for the kernel to run with - inputs (must have an up-to-date on-device facet)
 * @param inoutsBuffers for the kernel to run with - in-out buffers (must have an up-to-date on-device facet)
 * @param outputs Buffers for the kernel to run with - outputs (must have an up-to-date on-device facet)
 *
 * @return the measured execution time, in (non-integral) number of milliseconds
 *
 */
void execute_kernel(
	const KernelTestAdapter& test_adapter,
	cuda::stream_t& stream,
	DualAspectBuffers& inputs,
	DualAspectBuffers& inouts,
	DualAspectBuffers& outputs,
	memory_region device_scratch_area,
	bool scope_cuda_profiler = true)
{
 	optional<cuda::profiling::scope> profiling_scope;
	if (scope_cuda_profiler) {
		profiling_scope.emplace();
	}

	auto device = cuda::device::current::get();

	bool printing_launch_details =
		test_adapter.canReportLaunchDetails() && test_adapter.config.print_launch_details;
	if (printing_launch_details) {
		// Usually the test adapter resolves the config on its own
		// right before execution; but we can also do it from the
		// outside - for the purpose of printing
		auto launch_configs = test_adapter.resolveLaunchConfigurations();
		util::enforce(!launch_configs.empty(), "Adapter says it can report launch "
			"configurations, but does not do so.");
		std::cout << '\n';
		if (launch_configs.size() > 1) {
			std::cout << "Kernel launch configurations (" << launch_configs.size() << " kernels):\n\n";
		}
		else {
			std::cout << "(Single) kernel launch configuration:\n";
		}
		std::cout << "------------------------------------------------\n";

		for(unsigned i = 0; i < launch_configs.size(); i++) {
			if (launch_configs.size() > 1) {
				std::cout << "Kernel " << i + 1 << " of " << launch_configs.size() << ": \n";
			}
			cuda::print_kernel_launch_configuration(
				cout, launch_configs[i].name, launch_configs[i].launch_config);
			if (test_adapter.config.print_device_function_attributes) {
				std::cout << "\nDevice function attributes:\n";
				auto device_function = launch_configs[i].device_function;
				cuda::print_device_function_attributes(	cout, device_function.attributes());
			}
			std::cout << "\n";
		}
	}

	try {
		if (!printing_launch_details && test_adapter.config.be_verbose) {
			cout << "Launching kernel/s.\n" << std::flush;
		}
		auto all_buffers = util::get_union(get_union(
			get_single_aspect<DeviceSide>(inputs, access_type_t::read_only),
			get_single_aspect<DeviceSide>(inouts, access_type_t::read_write)),
			get_single_aspect<DeviceSide>(outputs, access_type_t::read_write));
		test_adapter.launchKernels(all_buffers, device_scratch_area, stream);
		// Ditto - nothing happens on the stream until this point; specifically,
		// the kernel has not begun to execute
	}
	catch (cuda::runtime_error& e) {
		util::die(e, string("Failure launching kernel ") + test_adapter.getName());
	}
	if (test_adapter.config.print_launch_details) {
		cout << "Kernel(s) launched successfully on device " << device.id() << " stream " << stream.id() << ".\n";
	}

	try {
		device.synchronize(stream);
	}
	catch (cuda::runtime_error& e) {
		// TODO: This could through if the adapter is null etc.
		util::die(e, string("Failure executing kernel(s) for test adapter ") + test_adapter.getName());
	}

	if (test_adapter.config.be_verbose) {
		cout << "Kernel(s) executed successfully\n";
	}
	return;
}

struct categorized_buffers_t {
	DualAspectBuffers inputs, inout_templates, inouts, outputs, expected;
	// Notes:
	// * The reason for 'duplication' of having both inout_templates and inouts is,
	//   that the actual inouts get overwritten, so we have to keep copies as
	//   templates, to initialize before every run
	// * This structure does not distinguish between buffers which were generated
	//   as opposed to being read from a DB or a file
};


bool handle_and_check_kernel_outputs(
	const KernelTestAdapter& test_adapter, const categorized_buffers_t& buffers)
{
	auto config = test_adapter.config;

	if (config.assume_results_correct && !config.print_outputs) { return true; }

	reconcile_buffer_aspects(test_adapter.config, buffers.inouts);
	reconcile_buffer_aspects(test_adapter.config, buffers.outputs);

	auto input_host_buffers  = get_single_aspect<HostSide>(buffers.inputs,  access_type_t::read_only);
	auto inout_host_buffers  = get_single_aspect<HostSide>(buffers.inouts,  access_type_t::read_only);
	auto output_host_buffers = get_single_aspect<HostSide>(buffers.outputs, access_type_t::read_only);
	auto actual_inout_and_out_host_buffers = util::get_union(inout_host_buffers, output_host_buffers);
	auto expected_host_buffers = get_single_aspect<HostSide>(buffers.expected, access_type_t::read_only);

	auto descriptors = test_adapter.getBufferDescriptors();

	if (config.print_outputs) {
		dump_buffers(descriptors,
			config.start_printing_from, config.num_elements_to_print,
			actual_inout_and_out_host_buffers, config.floating_point_printing_precision,
			cout, "(actual)");
	}

	if (config.assume_results_correct) { return true; }

	auto diverging_outputs = check_kernel_output_correctness(config, descriptors, input_host_buffers,
		actual_inout_and_out_host_buffers, expected_host_buffers);
	bool valid_output = diverging_outputs.empty();

	if (config.be_verbose || !valid_output) {
		cout
			<< "Actual outputs "
			<< (valid_output ? "" : "DO NOT ")
			<< "conform to expected outputs.";
		if (!valid_output) {
			auto keys = util::get_keys<decltype(diverging_outputs), std::unordered_set<string>>(diverging_outputs);
			cout << " Divergent buffers: " <<  keys;
		}
		cout << '\n';
	}

	if (!valid_output && config.print_mismatches) {
		cout << '\n';
		print_mismatches(test_adapter, descriptors,
			diverging_outputs,
			input_host_buffers,
			actual_inout_and_out_host_buffers,
			expected_host_buffers);
	}
	return valid_output;
}

void print_test_result(const KernelTestAdapter& adapter, bool result)
{
	UNUSED(adapter);
	cout
		<< "TEST " << (result ? "SUCCEEDED" : "FAILED   ")  << '\n' << flush;
}

DualAspectBuffers create_uninitialized_buffers(
	KernelTestAdapter& test_adapter,
	BufferDirection direction,
	const std::string& which_kind_buffers)
{
	const auto& config = test_adapter.config;
	auto new_buffers = create_uninitialized_buffers(
		config, keep_only_direction(test_adapter.getBufferDescriptors(), direction), which_kind_buffers);
	return new_buffers;
}

// TODO: Perhaps add a conversion operator from our Buffers to Buffers<aspect> ?
// That would save us explicitly making the conversion every time
// - Use a const& instead of a ptr?

DualAspectBuffers create_outputs(KernelTestAdapter& test_adapter)
{
	const auto& config = test_adapter.config;
	DualAspectBuffers output_buffers =
		create_uninitialized_buffers(test_adapter, BufferDirection::Output, "output");

	if (config.zero_output_and_scratch_buffers) {
		for(auto& e : output_buffers) {
			auto buffer_name = e.first;
			DualAspectBuffer& buffer = *e.second;
			if (config.be_verbose) {
				cout << "Zero'ing contents of output buffer \"" << buffer_name << "\"\n";
			}
			cuda::memory::device::zero(buffer.get(DeviceSide, access_type_t::read_write).data(), buffer.capacity());
		}
	}
	return output_buffers;
}

memory_region allocate_scratch_area(const KernelTestAdapter& test_adapter)
{
	if (test_adapter.config.be_verbose) {
		cout << "Determining required scratch area size: ";
	}

	size_t scratch_size = test_adapter.getRequiredScratchSize();
	if (scratch_size == 0) {
		if (test_adapter.config.be_verbose) {
			cout << "No scratch area required.\n";
		}
		return memory_region(nullptr, 0);
	}
	else if (test_adapter.config.be_verbose) {
		cout << scratch_size << " bytes.\n";
	}
	if (test_adapter.config.be_verbose) {
		cout << "Allocating a scratch area of size " << scratch_size << " bytes.\n";
	}
	auto scratch_area = memory_region(
		cuda::device::current::get().memory().allocate(scratch_size),
		scratch_size);
	if (test_adapter.config.zero_output_and_scratch_buffers) {
		if (test_adapter.config.be_verbose) {
			cout << "Setting contents of scratch area to zero.\n";
		}
		cuda::memory::device::zero(scratch_area.data(), scratch_area.size());
	}
	if (test_adapter.config.be_verbose) {
		cout << "Scratch area ready.\n";
	}
	return scratch_area;
}

size_t get_device_memory_requirement(const KernelTestAdapter& test_adapter)
{
	size_t result = 0;
	auto descriptors = test_adapter.getBufferDescriptors();
	std::for_each(descriptors.begin(), descriptors.end(),
		[&result](const BufferDescriptors::value_type& name_and_descriptor){
			result += name_and_descriptor.second.capacity;
			// InOut buffers are "double-buffered" - we keep a copy of the
			// initial values on the device which does not get altered
			if (name_and_descriptor.second.direction == BufferDirection::InOut) {
				result += name_and_descriptor.second.capacity;
			}
		});
	return result + test_adapter.getRequiredScratchSize();
}

void precheck_free_memory(const KernelTestAdapter& test_adapter)
{
	// TODO: Also check memory on the host

	size_t free_device_memory = cuda::device::get(test_adapter.config.test_device).memory().amount_free();
	size_t  required_device_memory = get_device_memory_requirement(test_adapter);
	if (test_adapter.config.be_verbose) {
		cout << "Free device memory: " << free_device_memory << " ("
			<< util::rough_form_decimal(free_device_memory, "GB", 1) << ')' << '\n';
		cout << "Minimum required device memory for test: " << required_device_memory
			<< " (" << util::rough_form_decimal(required_device_memory, "GB", 1) << ')' << '\n';
	}
	if (free_device_memory < required_device_memory) {
		util::die(util::concat_string(
			"CUDA device ", test_adapter.config.test_device, " only has ", free_device_memory, " bytes (",
			util::rough_form_decimal(free_device_memory, "GB", 1), ") free in ",
			"device-wide memory, while at least ", required_device_memory, " (",
			util::rough_form_decimal(required_device_memory, "GB", 1), ") are required ",
			"to execute the test adapter for kernel \"", test_adapter.getName(), "\"."
		));
	}
}

void initialize_buffers_by_copying_from_a_template(
	const TestConfiguration& config,
	const DualAspectBuffers& to_initialize,
	const DualAspectBuffers& templates)
{
	for(auto& template_map_entry : templates) {
		auto buffer_name = template_map_entry.first;
		const auto& template_buffer = *template_map_entry.second;
		auto& buffer = *to_initialize.at(buffer_name);
		if (config.be_verbose) {
			std::cout << "Initializing working copy of in-out buffer "
				<< buffer_name << " (capacity " << buffer.capacity()
				<< ") from its template (size " << template_buffer.size() << ").\n";
		}
		buffer.overwrite_using(template_buffer, DeviceSide);
	}
}

void add_test_description_to_profile_data(const TestConfiguration& config)
{
	std::ostringstream oss;
	cuda::profiling::mark::point("Kernel tester run");
	cuda::profiling::mark::point("Current time: " + current_datetime());
	cuda::profiling::mark::point("Kernel test adapter name: " + config.test_adapter_name);
	oss << util::escape_nonprinting(util::command_line::get_formatted());
	cuda::profiling::mark::point("Command line: " + oss.str());
	oss.clear();
	cuda::profiling::mark::point("Working directory: " + util::current_working_directory());
	cuda::profiling::mark::point("Path to executing binary: " + util::path_to_executing_binary());
	// TODO: Perhaps a less-pretty version of pretty_print? With no extra spaces and newlines?
	oss << util::escape_nonprinting(kernel_tests::pretty_print(config));
	cuda::profiling::mark::point("Test configuration: " + oss.str());
	if (!config.config_file.empty()) {
		oss.clear();
		oss << util::escape_nonprinting(util::file_contents(config.config_file));
		cuda::profiling::mark::point("Escaped configuration file contents: " + oss.str());
	}
}

DualAspectBuffers create_inouts(KernelTestAdapter& test_adapter)
{
	return create_uninitialized_buffers(test_adapter, BufferDirection::InOut, "inout-working-copy");
}

DualAspectBuffers read_buffers_from_db(const TestConfiguration& config, const BufferDescriptors& all_descriptors)
{
	// Note:
	//
	// This is currently a naive implementation, which...
	//
	// 1. loads the data as a separate copy rather than/ directly into a DualAspectBuffer (or
	//    rather than constructing a DualAspectBuffer using the loaded data - which would
	//     allow for transparently supporting having mmap()ed input.
	// 2. Loads all columns regardless of what has been requested (albeit some of them only
	//    via mmap); this is due to the bbp-reader code, not the code here.
	// 3. Doesn't check type compatibility between the descriptor and the column in the DB
	//    (but does check the width at least)
	// 4. Doesn't support string columns

	if (not has_value(config.monet_db_directory)) {
		if (!config.db_column_mappings.empty()) {
			throw util::invalid_argument("Cannot read buffers from a persisted DB when no path has been specified");
		}
		return {};
	}
	auto db_directory = config.monet_db_directory.value();
	if (config.be_verbose) {
		std::cout
		   << "Will read buffers " << util::get_keys(config.db_column_mappings)
		   << " from columns in the MonetDB database persisted at " << db_directory << "...\n";
	}
	monetdb::gdk::buffer_pool buffer_pool(db_directory);
	if (config.be_verbose) {
		std::cout  << "Loaded the MonetDB buffer pool at " << db_directory << "\n";
	}

	// Note: We are silently ignoring column_mapping entries which are not used by our
	// kernel test adapter, i.e. which are not in all_descriptors

	auto is_apriori = [&config](const BufferDescriptors::value_type& name_and_descriptor) {
		auto buffer_name = name_and_descriptor.first;
		return util::contains(config.db_column_mappings, buffer_name);
	};

	auto apriori_descriptors = util::keep_only(all_descriptors, is_apriori);
	auto apriori_buffers = create_uninitialized_buffers(config, apriori_descriptors, "to-be-read-from-DB");

	for (const auto& name_and_descriptor : apriori_descriptors)
	{
		const auto& buffer_name = name_and_descriptor.first;
		const auto& buffer_descriptor = name_and_descriptor.second;
		const auto& column_name_in_db = config.db_column_mappings.at(buffer_name);

		if (!util::contains(all_descriptors, buffer_name)) {
			std::cout << "Not reading buffer \"" << buffer_name << "\" from the persisted DB, as this "
				<< "buffer is not used in this kernel test adapter.\n";
			continue;
		}

		std::string table_name, column_name;
		auto name_elements = util::explode(column_name_in_db, '.');
		optional<monetdb::gdk::buffer_pool::index_type> maybe_pool_index;
		switch(name_elements.size()) {
		case 3:
			maybe_pool_index = buffer_pool.find_column(
				monetdb::make_sql_name(name_elements[0], name_elements[1], name_elements[2]));
			break;
		case 2:
			maybe_pool_index = buffer_pool.find_column(monetdb::make_sql_name(name_elements[0], name_elements[1]));
			break;
		default:
			throw std::invalid_argument(
				"Invalid DB column specifier " + column_name_in_db +
				": Specify either schema.table.column or table.column");
		}
		if (!maybe_pool_index) {
			throw util::invalid_argument(util::concat_string("Column ", column_name_in_db,
				" not found in the DB at ", db_directory));
		}
		auto pool_index = maybe_pool_index.value();
		auto column_proxy = buffer_pool[pool_index];
		auto db_column_length = column_proxy.length();

		auto addr = column_proxy.at(0);
		auto db_column_type = column_proxy.type();
		auto db_column_type_size = monetdb::gdk::type_size(db_column_type);

		// TODO: What if the data type is not one of the basic ones? Hmm. Perhaps
		// I should consider the possibility of making the static string-to-size map
		// of util::type_size support adding elements dynamically
		auto& buffer = *(apriori_buffers.at(buffer_name));
		auto host_side_aspect = buffer.get(HostSide, access_type_t::read_write);
		auto type_size = util::size_of(buffer_descriptor.data_type);
		if (db_column_type_size != type_size) {
			throw util::invalid_argument(util::concat_string(
				"Cannot use DB column ", column_name_in_db,
				" as buffer \"", buffer_name, "\" since their type sizes don't match: ",
				"sizeof(", buffer_descriptor.data_type, ") = ",
				type_size, " while sizeof(", monetdb::gdk::type_name(db_column_type),
				") = ", db_column_type_size
			));
		}
		if (db_column_length < buffer_descriptor.capacity / type_size) {
			throw util::invalid_argument(util::concat_string(
				"Buffer \"", buffer_name, "\" must",
				" be filled with ", buffer_descriptor.capacity / type_size, " elements",
				" of type \"", buffer_descriptor.data_type, "\", so cannot read it from DB column \"",
				column_name_in_db, "\" (having length ", db_column_length, "."
			));

		}
		else if (db_column_length > buffer_descriptor.capacity / type_size) {
			if (config.be_verbose) {
				std::cout <<  "Using the first "<< (buffer_descriptor.capacity / type_size) << " elements "
				<< "of DB column \"" << column_name_in_db << "\" as buffer \"" << buffer_name
				<< "\", as the buffer fits " << (buffer_descriptor.capacity / type_size) << " elements while the column has "
				<< db_column_length << ".\n";
			}
		}


		auto copied_length = std::min<size_t>(db_column_length, buffer.capacity() / type_size);
		auto copied_size = copied_length * type_size;
		if (config.be_verbose) {
			std::cout
			    << "Copying " << copied_length << " elements of DB type \"" << monetdb::gdk::type_name(db_column_type)
			    << "\" (size " << db_column_type_size << ") into buffer \"" << buffer_name << "\" (of type "
			    << buffer_descriptor.data_type << ")... " << std::flush;
		}
		std::memcpy(host_side_aspect.data(), addr, copied_size);
		if (config.be_verbose) { std::cout << "done.\n" << flush; }
		buffer.set_size(copied_size);
	}
	return apriori_buffers;
}

static const std::map<BufferDirection, std::string> BufferDirectionNames = {
	{ BufferDirection::Input,  "Input"  },
	{ BufferDirection::Output, "Output" },
	{ BufferDirection::InOut,  "InOut"  },
};

/**
 * A more top-level function which marshals the buffer
 * allocation/creation, generation, reading from the DB etc.
 *
 * @param test_adapter
 * @return
 */
categorized_buffers_t prepare_all_buffers(KernelTestAdapter& test_adapter)
{
	struct {
		DualAspectBuffers inputs;
		DualAspectBuffers inout_templates;
		// Note: We currently do not support reading expected inout buffers
		// from a DB. That would be kind of tricky, since the buffer name
		// would need to be used twice - once for the initial buffer, then
		// again for the expected final buffer
		DualAspectBuffers expected;
	} apriori, generated;

	// TODO: We do not currently support reading both expected inouts and
	// initial inouts from the DB!

	auto all_descriptors = test_adapter.getBufferDescriptors();
	auto all_apriori_buffers =  test_adapter.config.db_column_mappings.empty() ? DualAspectBuffers{} :
		read_buffers_from_db(test_adapter.config, all_descriptors);
	apriori.inputs = keep_only_direction(all_apriori_buffers, all_descriptors, BufferDirection::Input);
	apriori.inout_templates = keep_only_direction(all_apriori_buffers, all_descriptors, BufferDirection::InOut);
	apriori.expected = keep_only_direction(all_apriori_buffers, all_descriptors, BufferDirection::Output);
		// Note again: only ::Output, no ::InOut's here

	std::tie(generated.inputs, generated.inout_templates) = generate_inputs_and_inouts(test_adapter, all_apriori_buffers);

	if (test_adapter.config.print_inputs) {
		auto apriori_inputs = keep_only_direction(apriori.inputs, all_descriptors, BufferDirection::Input);
		auto apriori_inouts = keep_only_direction(apriori.inout_templates, all_descriptors, BufferDirection::Input);
		auto all_inputs_and_inouts = util::get_union(
			apriori_inputs, apriori_inouts,
			generated.inputs, generated.inout_templates);
		// We re-get the buffers, since the sizes may have changed
		auto host_side_aspects = get_single_aspect<HostSide>(all_inputs_and_inouts, access_type_t::read_only);
		// This needs to be rewritten with some "poor man's reflection" mechanism
		dump_buffers(all_descriptors,
			test_adapter.config.start_printing_from, test_adapter.config.num_elements_to_print,
			host_side_aspects, test_adapter.config.floating_point_printing_precision, cout);
	}

	generated.expected = generate_expected(
		test_adapter, generated.inputs, generated.inout_templates, all_apriori_buffers);
	DualAspectBuffers all_expected = util::get_union(apriori.expected, generated.expected);

	if (test_adapter.config.print_expected_outputs) {
		auto host_side_aspects = get_single_aspect<HostSide>(
			util::get_union(apriori.expected, generated.expected), access_type_t::read_only);
		dump_buffers(all_descriptors,
			test_adapter.config.start_printing_from, test_adapter.config.num_elements_to_print, host_side_aspects,
			test_adapter.config.floating_point_printing_precision, cout, "(expected)");
	}

	// And we're a go. Note that everything here happens synchronously, i.e.
	// first all the cudaMemcpy's, then the kernel, than the outgoing cudaMemcpys,
	// despite any hardware capabilities for parallel Compute and I/O.

	auto all_inputs = util::get_union(apriori.inputs , generated.inputs);
	reconcile_buffer_aspects(test_adapter.config, all_inputs);
	auto all_inout_templates = util::get_union(apriori.inout_templates, generated.inout_templates);
	reconcile_buffer_aspects(test_adapter.config, all_inout_templates);

	DualAspectBuffers inouts = create_inouts(test_adapter);
	DualAspectBuffers outputs = create_outputs(test_adapter);
		// This doesn't need any of the other buffers, since the buffer contents is junk (or zeros), and the
		// length can't depend on the contents of input and inout buffer - it has to be predetermined;
		// dynamically resizing outbuffers is not supported at the moment

	if (test_adapter.config.print_buffer_listing) {
		auto buffers = util::get_union(all_inputs, all_inout_templates, all_expected);
		std::cout
			<< "\n"
			<< "All " << buffers.size() << " buffers (with final sizes set):\n"
//			<< "-------------------------------------------------------------------------------------------------------------\n";
			<< "Direction  Buffer Name               Element type              Total size (bytes)\n"
			<< "---------------------------------------------------------------------------------\n";
		for (const auto& map_entry: buffers) {
			auto buffer_name = map_entry.first;
			auto buffer_shared_ptr = map_entry.second;
			auto descriptor = all_descriptors.at(buffer_name);
			auto dual_aspect_buffer = buffer_shared_ptr.get();
			std::cout
				<< std::left
				<< '[' << std::setw(6) <<  BufferDirectionNames.at(descriptor.direction) << "]   "
				<< std::setw(25) << buffer_name << ' '
				<< std::setw(25) << descriptor.data_type << ' '
				<< std::setw(20) << dual_aspect_buffer->size()
				<< '\n';
			;
			// TODO: Add some more flags/options, e.g. printing as a sequence of bits
		}
		std::cout << '\n';
	}
	return { all_inputs, all_inout_templates, inouts, outputs, all_expected };
}

bool conduct_test(KernelTestAdapter& test_adapter)
{
	CUDA_DEVICE_FOR_THIS_SCOPE(cuda::device::get(test_adapter.config.test_device));
		// consider replacing this with a scope guard

	add_test_description_to_profile_data(test_adapter.config);

	auto device = cuda::device::current::get();
	if (device.cache_preference() != cuda::multiprocessor_cache_preference_t::prefer_shared) {
		if (test_adapter.config.be_verbose) {
			std::cout << "Setting test device cache preference to prefer "
				<< "shared memory over L1 cache\n";
		}
		device.set_cache_preference(cuda::multiprocessor_cache_preference_t::prefer_shared);
	}
	auto stream = cuda::stream::create(device,
		cuda::stream::implicitly_synchronizes_with_default_stream);
	device.synchronize();

	precheck_free_memory(test_adapter);
	auto buffers = prepare_all_buffers(test_adapter);
	memory_region device_scratch_area = allocate_scratch_area(test_adapter);

	bool result = true;
	std::vector<float> run_times_in_msec;
	run_times_in_msec.reserve(test_adapter.config.num_test_runs);
	for(size_t run_index = 0; run_index < test_adapter.config.num_test_runs; run_index++) {

		initialize_buffers_by_copying_from_a_template(test_adapter.config, buffers.inouts, buffers.inout_templates);
			// All other buffers don't need further initialization
			// TODO: But what about zeroing outputs?

		execute_kernel(
			test_adapter, stream,
			buffers.inputs, buffers.inouts, buffers.outputs,
			device_scratch_area);

		auto run_result = handle_and_check_kernel_outputs(test_adapter, buffers);
		result = result and run_result;

		// TODO: Print intermediary SUCCESS/FAILURE test result?
	}
	if (test_adapter.config.print_result) {
		print_test_result(test_adapter, result);
	}
	return result;
}

void print_kernel_description(const std::unique_ptr<KernelTestAdapter>& test_adapter)
{
	cout << "Kernel name:          "
		 << test_adapter->getName() << '\n' << "What it does:         "
		 << test_adapter->getPrettyName() << '\n'
		 << "Further description:\n"
		 <<	test_adapter->getDescription() << '\n' << '\n';
}

void validate_configuration(const TestConfiguration& config)
{
	if (!util::contains(std::array<std::string, 2>{ "CUDA", "cuda" }, config.backend)) {
		util::die("Unknown/unsupported execution backend " + config.backend);
	}

	if (config.execution_platform != nullopt) {
		util::die("CUDA does not support execution platforms.");
	}

	if (config.tester_action != +tester_action_t::conduct_a_test) { return; }

	// To conduct an actual test, we need a device

	if (cuda::device::count() == 0)  {
		util::die("There are no CUDA devices on this system, and no other backend is currently supported.");
	}

	unsigned num_devices = cuda::device::count();
	if(config.test_device >= num_devices) {
		util::die("Invalid CUDA device index " + std::to_string(config.test_device) + " specified (highest valid CUDA"
			" device index on this system is " + std::to_string(num_devices - 1) + ").");
	}
}

int main(int argc, char** argv)
{
	util::command_line::save(argc, argv);
#ifdef DEBUG
	util::disable_stdout_buffering();
#endif

	TestConfiguration config(TestConfiguration::getFromProgramArguments(argc, argv));
	validate_configuration(config);

	int exit_value = EXIT_SUCCESS;

	switch(config.tester_action) {
	case tester_action_t::list_platforms_and_devices:
		cuda::list_all_devices(cout);
		break;
	case tester_action_t::list_device_properties:
		cuda::list_device_properties(config.test_device, cout);
		break;
	case tester_action_t::list_test_adapters:
		util::list_producible_subclasses<KernelTestAdapter>(std::cout, config.be_verbose, "kernel test adapters", "\n");
		break;
	case tester_action_t::list_kernels:
		util::list_producible_subclasses<cuda::registered::kernel_t>(std::cout, config.be_verbose, "kernels", "\n");
		break;
	case tester_action_t::print_number_of_kernels_in_a_test: {
		std::unique_ptr<KernelTestAdapter> test_adapter(KernelTestAdapter::produceAdapter(config));
		if (config.be_verbose) {
			std::cout << "Number of kernels used in test adapter " << test_adapter.get()->getPrettyName() << ": ";
		}
		std::cout << test_adapter.get()->getNumberOfKernels() << '\n';
		break;
	}
	case tester_action_t::print_test_configuration: {
		std::cout << kernel_tests::pretty_print(config) << '\n';
		break;
	}
	case tester_action_t::describe_a_kernel_test: {
		std::unique_ptr<KernelTestAdapter> test_adapter(KernelTestAdapter::produceAdapter(config));
		print_kernel_description(test_adapter);
		break;
	}
	case tester_action_t::conduct_a_test: {
		if (config.print_test_config) {
			cout << kernel_tests::pretty_print(config) << '\n';
		}
		std::unique_ptr<KernelTestAdapter> test_adapter(KernelTestAdapter::produceAdapter(config));
		bool test_result = conduct_test(*test_adapter.get());
		exit_value = test_result ? EXIT_SUCCESS : EXIT_FAILURE;
	}
	} // switch config.tester_action

	return exit_value;
}
