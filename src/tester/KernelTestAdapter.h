#pragma once
#ifndef SRC_KERNELTESTADAPTER_H_
#define SRC_KERNELTESTADAPTER_H_

#include "tester/BufferDescriptor.h"
#include "tester/TestConfiguration.h"
#include "Aspect.h"
#include "util/distribution.h"

#include "util/stl_algorithms.hpp"
#include "util/algorithm_container_adapters.hpp"
#include "util/maps.hpp"
#include "util/string.hpp" // for substr_after
#include "util/factory_producible.h"
#include "util/prettyprint.hpp"
#include "util/miscellany.hpp"
#include "util/type_name.hpp"
#include "util/exception.h"
#include "util/optional.hpp"
#include "util/integer.h"

#include <gsl/gsl-lite.hpp>

#include <cuda/api/types.hpp> // for launch configurations and streams (used anywhere other than printing?)
#include <cuda/api/device_function.hpp> // for device functions - printing only
#include "kernel_wrappers/common.h"

#include <string>
#include <sstream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <cstddef>

using gsl::span;
using util::random::distribution_t;
using std::size_t;
using util::uint_t;
using util::sint_t;
using cuda::size_type_by_index_size;

namespace kernel_tests {

struct launch_configuration_sequence_element_t {
	std::string name;
	cuda::device_function_t device_function;
	cuda::launch_configuration_t launch_config;
};

using LaunchConfigurationSequence =	std::vector<launch_configuration_sequence_element_t>;
using std::make_pair;


/**
 * A sort of a wrapper around a kernel for unit-testing it with the kernel_tester harness.
 * Almost all non-kernel specific work is done by the harness, and instances of this class
 * (or rather) is what we expose for it to actually test any specific kernel.
 *
 * TODO:
 * - Fill input buffers on the GPU rather than copying - should save a lot of running time.
 * - What about "kernels" which are actually pairs of __global__ functions? e.g. a two-phase
 *   reduction which calls the same kernel again after the first time?
 */

class KernelTestAdapter :
	protected util::mixins::factory_producible_t<std::string, KernelTestAdapter, const TestConfiguration&>
{
protected:
	using factory_producible_t = util::mixins::factory_producible_t<std::string, KernelTestAdapter, const TestConfiguration&>;

public:

	// Factory-related statics

	using factory_producible_t::getSubclassFactory;
	using factory_producible_t::registerInSubclassFactory;

	static std::unique_ptr<KernelTestAdapter> produceAdapter(const TestConfiguration& config);


public:
	// Types

	using BufferDescriptor    = kernel_tests::BufferDescriptor;
	using BufferDescriptors   = std::unordered_map<std::string, BufferDescriptor>;
	using ConfigurationKeySet = std::unordered_set<std::string>;
	using BufferSizes         = std::unordered_map<std::string, size_t>; // sizes are in bytes


public:

	// Getters & Setters

	// Note that these buffer sets need not be exclusive! And if a buffer with the same key
	// (same name) appears in both sets, it's the _same_ buffer
	virtual BufferDescriptors getBufferDescriptors() const = 0;

	virtual size_t getRequiredScratchSize() const { return 0; }

	virtual std::string  getName() const = 0;
	virtual std::string  buildPrettyKernelName() const = 0;
	virtual std::string  getPrettyName() const = 0;
	virtual std::string  getDescription() const = 0;
	virtual unsigned getNumberOfKernels() const { return resolveLaunchConfigurations().size(); };

	void setConfig(const TestConfiguration& new_config) { config = new_config; }

	virtual bool canReportLaunchDetails() const { return true; }
	virtual LaunchConfigurationSequence resolveLaunchConfigurations() const = 0;

	// Ctor & Dtor

	KernelTestAdapter(const TestConfiguration& config);
	virtual ~KernelTestAdapter();

	// Everything else

	// We do not yet have generic code for printing mismatches, so for now different adapters
	// can offer to do it on their own
	virtual bool canPrintMismatches() const { return false; }
	virtual void printMismatches(
		const HostBuffers & inputs,
		const HostBuffers & actual_outputs,
		const HostBuffers & expected_outputs) const;

	/**
	 * Determines whether a specified input buffer
	 * is to be read from disk or to be generated dynamically
	 * by the test adapter's code
	 *
	 * @todo move this into the test configuration!
	 * @param buffer_name
	 * @return true if generation is necessary, false otherwise
	 */
	bool need_to_generate(const std::string& buffer_name) const;

	/**
	 * Generate all (of the host-side aspects of) of the input and in-out buffers,
	 * which have not been obtained from some other source.
	 *
	 * @param buffers a map of regions in host memory, to fill - keyed by name
	 * @return a map from buffer names (a subset of the names specified in @p buffers),
	 * to their filled sizes, in bytes; whichever buffers are not represented in this
	 * map are assumed to be filled to their respective capacities
	 */
	virtual BufferSizes generateInputs(const HostBuffers& buffers) = 0;
	// This is done on the host, and does not use the kernel, of course
	virtual BufferSizes generateExpectedOutputs(
		const HostBuffers& expected_buffers,
		const HostBuffers& buffers) = 0;
	virtual void launchKernels(
		const DeviceBuffers& buffers,
		typename DeviceBuffers::mapped_type device_scratch_area,
		cuda::stream_t& stream) const = 0;

	template <typename U>
	static inline std::string buildName()
	{
		return massage_type_name(util::type_name<U>());
	}

private:
	static std::string massage_type_name(const std::string& s);

protected:

	/**
	 * This is the adapter-specific logic for filling inputs, which should
	 * essentially be the generation of those columns not read from disk.
	 */

	/**
	 * We don't have reflection, nor can adapters currently parse their specific options
	 * using boost program_options, so we get them as string. this requires several calls
	 * by each test adapter, for each of its specific options.
	 *
	 * @note successful calls remove the matched key from the key container (eventually
	 * we expect it to become empty)
	 *
	 * @param keys aliases for the option (e.g. { "max_value", "maximum_value", "max" } as
	 * an unordered set.
	 * @return the option value - if it was found
	 */
	template<typename T, typename KeyContainer>
	util::optional<T> resolveOption(const KeyContainer& keys) const {
		auto extra_option_keys =
			util::get_keys<decltype(config.extra_options), std::vector<std::string>>(config.extra_options);
		KeyContainer key_intersection = util::get_intersection(keys, extra_option_keys);
		if (key_intersection.empty()) { return util::nullopt; }
		util::enforce(key_intersection.size() <= 1, util::concat_string(
			"Multiple specifications of an equivalent option: ", key_intersection));
		const auto& key = *(key_intersection.cbegin());
		auto value_as_str = config.extra_options.at(key);
		T value;
		if (std::is_same<typename std::decay<T>::type,bool>::value &&
			(value_as_str.empty() || value_as_str == "1"))
		{
			value = true;
		}
		else {
			std::istringstream stream(value_as_str);
			value = util::read<T>(stream);
		}
		config.extra_options.erase(key);
		return value;
	}

	/**
	 * We don't have reflection, nor can adapters currently parse their specific options
	 * using boost program_options, so we get them as string. this requires several calls
	 * by each test adapter, for each of its specific options.
	 *
	 * @param keys aliases for the option (e.g. { "max_value", "maximum_value", "max" } as
	 * an unordered set.
	 * @param default_value The value to assign when an option was not specified
	 * @return
	 */
	template<typename T, typename KeyContainer>
	T resolveOption(const KeyContainer& keys, const T& default_value) const {
		util::optional<T> v = resolveOption<T>(keys);
		return v.value_or(default_value);
	}

	template<typename T, typename KeyContainer>
	bool resolveOption(T& target, const KeyContainer& keys) const {
		util::optional<T> v = resolveOption<T>(keys);
		if (v) { target = v.value(); return true; }
		return false;
	}

	/**
	 * Reads a buffer from a file.
	 *
	 * @param buffer the raw buffer to fill with data from the file
	 * @param length the number of elements to try and read from the file (even
	 * if there are more available)
	 * @param column_index when the file is in a multi-column format, an indication
	 * of which column to read into the buffer
	 */
	template <typename T>
	void readBuffer(
		const std::string& buffer_name, T* buffer, size_t length,
		util::optional<unsigned> column_index) const;

	void print_extra_configuration_inner() { }

	template <typename T, typename... Ts>
	void print_extra_configuration_inner(
		const std::pair<const char*, T> head_pair,
		const std::pair<const char*, Ts>... tail_pairs)
	{
		auto cw = TestConfiguration::pretty_printing_name_column_width;
		const char* description = head_pair.first;
		T arg = head_pair.second;

		std::cout << std::setw(cw - 2) << std::left << description << "  ";
		if (std::is_same<T, bool>::value ) { std::cout << std::boolalpha; }
/*
		if (std::is_same<T, unsigned char>::value || std::is_same<T, char>::value) {
			util::sanitize_to(std::cout, *(reinterpret_cast<char*>(&arg)));
		}
		else { std::cout << arg; }
*/
		std::cout << util::promote_for_streaming(arg) << '\n';

		print_extra_configuration_inner(tail_pairs...);
	}

	template <typename... Ts>
	void maybe_print_extra_configuration(const std::pair<const char*, Ts>... config_arguments)
	{
		if (!config.print_test_config) { return; }
		// TODO: Use the values here to determine the column wiodth you actually need
		auto cw = TestConfiguration::pretty_printing_name_column_width;
		if (sizeof...(Ts) != 0) {
			std::cout
				<< "Additional Test Configuration\n"
				<< "-----------------------------\n";

			print_extra_configuration_inner(config_arguments...);
		}
		if (!config.extra_options.empty()) {
			std::cout
				<< std::setw(cw) << std::left << "Unrecognized Configuration options"
				<< config.extra_options << '\n';
		}
		std::cout << '\n';
	}

public: // data members
	TestConfiguration  config;

protected: // data members
	mutable std::random_device random_device;  // Note this is a callable object.
	mutable std::default_random_engine random_engine;

	/**
	 * The buffers here will be generated regardless of anything else we might notice;
	 * useful in composition of adapters, where we might want to force generation of
	 * intermediary results.
	 */
	std::unordered_set<std::string> forced_generation_buffers { };
};

namespace mixins {
/**
 * Individual kernel test adapters are responsible for fixing up their generated
 * names, even if they can get them using the general buildName<U> method
 * of this class; that's why each subclass should inherit this CRTP mixin adapter.
 * If a sublcass actually needs any custom name manipualtion it can specialize
 * this mixin
 */
template <typename TestAdapter>
struct TestAdapterNameBuilder {
	static std::string buildName() {
		return KernelTestAdapter::buildName<TestAdapter>();
	}
};
}

static const KernelTestAdapter::BufferSizes NoBuffersToResize = KernelTestAdapter::BufferSizes();

// All subclasses have to define all of these, unfortunately - C++ doesn't have a
// "overrides all pure-virtual methods" class designator
#define KERNEL_TEST_ADAPTER_BOILERPLATE_DEFINITIONS(subclass_name) \
	using parent = KernelTestAdapter; \
	std::string getName() const override { return buildName(); } \
	std::string buildPrettyKernelName() const override; \
	std::string getPrettyName() const override { return buildPrettyKernelName(); } \
	BufferDescriptors getBufferDescriptors() const override; \
	std::string  getDescription() const; \
	LaunchConfigurationSequence resolveLaunchConfigurations() const override; \
	KernelTestAdapter::BufferSizes generateInputs(const HostBuffers& buffers) override; \
	KernelTestAdapter::BufferSizes generateExpectedOutputs( \
		const HostBuffers& expected_buffers, \
		const HostBuffers& buffers) override; \
	void launchKernels(const DeviceBuffers& buffers, typename DeviceBuffers::mapped_type device_scratch_area, cuda::stream_t& stream) const override; \
	static void registerInSubclassFactory() { \
		factory_producible_t::registerInSubclassFactory<subclass_name>(buildName()) ; \
	} \
	using ::kernel_tests::mixins::TestAdapterNameBuilder<subclass_name>::buildName; \
	subclass_name(const TestConfiguration& config)

#define KERNEL_TEST_ADAPTER_BOILERPLATE_FOR_SIMPLE_NAME_OVERRIDE(full_subclass_name, full_parent_class_name) \
	using full_parent_class_name::full_parent_class_name; \
	static std::string buildName() { \
		return kernel_tests::mixins::TestAdapterNameBuilder<this_class>::buildName(); \
	} \
	std::string getName() const override { return buildName(); }; \
	using KernelTestAdapter::need_to_generate; \
	static void registerInSubclassFactory() { \
		KernelTestAdapter::factory_producible_t::registerInSubclassFactory<this_class>(buildName()) ; \
	}
} // namespace kernel_tests

template <typename T>
void debug_print_(const char* x_name, T&& x)
{
	std::cout << x_name << ": type " << util::type_name<T>() << ", value " << x << '\n';
}

#ifndef NDEBUG
#define debug_print(x) debug_print_( #x , x)
#endif
#endif /* SRC_KERNELTESTADAPTER_H_ */
