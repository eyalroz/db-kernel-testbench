#pragma once
#ifndef SIMPLE_PARALLELIZED_CPU_SORT_HPP_
#define SIMPLE_PARALLELIZED_CPU_SORT_HPP_

/**
 * @file A simple adaptation of std::sort to use multiple threads on a CPU:
 * One thread per core sorts a segment of the input data, then phases
 * of segment-pair merges are repeated until a single sorted range
 * is achieved.
 *
 * Adapted from: https://github.com/sol-prog/Sort_data_parallel
 * by Eyal Rozenberg
 *
 * The original is Copyright (c) 2016, https:://solarianprogrammer.com
 * The author has allowed the distribution this modified code under
 * the following license (the "BSD 2-clause license"):
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice,this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <thread>
#include <iterator>
#include "util/poor_mans_reflection.h"

namespace kernel_tests {

/**
 * Split the integer range [0, length-1] into "parts",
 *  e.g. if length = 10 and parts = 4 you will have: 0,2,4,6,10
 * if possible the function will split the range into equal chunks,
 * if not the last chunck will be slightly larger
 */
static inline std::vector<size_t> partition_range(size_t num_parts, size_t length) {
	std::vector<size_t>bnd(num_parts + 1);
	size_t delta = length / num_parts;
	size_t reminder = length % num_parts;
	size_t N1 = 0, N2 = 0;
	bnd[0] = N1;
	for (size_t i = 0; i < num_parts; ++i) {
		N2 = N1 + delta;
		if (i == num_parts - 1)
			N2 += reminder;
		bnd[i + 1] = N2;
		N1 = N2;
	}
	return bnd;
}

template <typename Container, typename Comparator>
static void serial_sort_segment(Container& container, size_t left, size_t right) {
	std::sort(std::begin(container) + left, std::begin(container) + right);
}

/**
 * Merge data[n0:n1] with data[n2:n3]. The result is put back to data[n0:n3]
 */
template <typename Container, typename Comparator>
static void serial_merge_contiguous_segments(Container &container, size_t n0, size_t n1, size_t n2, size_t n3)
{
	UNUSED(n2);
	auto first   = std::begin(container) + n0;
	auto middle  = std::begin(container) + n1;
	auto last    = std::begin(container) + n3;
	std::inplace_merge(first, middle, last, Comparator());
}

template <typename Container, typename Comparator>
static void serial_sorts(Container& container, const std::vector<size_t> bounds)
{
	size_t num_parts = bounds.size() - 1;
	std::vector<std::thread> threads;

	for(size_t i = 0; i < num_parts; ++i) {
		threads.push_back(
			std::thread(
				serial_sort_segment<Container, Comparator>,
				std::ref(container),
				bounds[i], bounds[i + 1]
			)
		);
	}

	for(auto &sorter_thread : threads) { sorter_thread.join(); }
}

/**
 * Merge the sorted ranges of @p data - between every pair
 * of consecutive elements of @p bounds - using several
 * threads
 */
template <typename Container, typename Comparator>
static void merges(Container& container, std::vector<size_t> bounds)
{
	size_t num_parts = bounds.size() - 1;

	//Merge data
	while(num_parts >= 2) {
		std::vector<size_t> limits;
		std::vector<std::thread> threads;

		// A merge for consecutive pairs of segments
		for(size_t i = 0; i < num_parts - 1; i += 2) {
			threads.push_back(
				std::thread(
					serial_merge_contiguous_segments<Container, Comparator>,
					std::ref(container),
					bounds[i], bounds[i + 1],
					bounds[i + 1], bounds[i + 2]
				)
			);
			// Ugh, ugly code
			size_t naux = limits.size();
			if(naux > 0) {
				if(limits[naux - 1] != bounds[i]) {
					limits.push_back(bounds[i]);
				}
				limits.push_back(bounds[i + 2]);
			}
			else {
				limits.push_back(bounds[i]);
				limits.push_back(bounds[i + 2]);
			}
		}

		for(auto &merger_thread : threads) {
			merger_thread.join();
		}

		num_parts /= 2;
		bounds = limits;
	}
}

namespace detail {
template <typename Container, typename Comparator = std::less<typename Container::value_type>>
inline void sort(Container& container, size_t num_parts)
{
	if (num_parts == 1) {
		std::sort(std::begin(container), std::end(container), Comparator());
		return;
	}
	// Define a partition of the input array;  every thread will serial-sort one part
	std::vector<size_t> bounds = partition_range(num_parts, container.size());
	serial_sorts<Container, Comparator>(container, bounds);
	merges<Container, Comparator>(container, bounds);
}

} // namespace detail

template <typename Container, typename Comparator = std::less<typename Container::value_type>>
inline void sort(Container& container)
{
	unsigned concurrency = std::thread::hardware_concurrency();
	// We might get 0 if we can't determine the corrent value
	if (concurrency == 0) {
		std::sort(std::begin(container), std::end(container), Comparator());
	}
	else { detail::sort(container, concurrency); }
}

} // namespace kernel_tests

#endif /* SIMPLE_PARALLELIZED_CPU_SORT_HPP_ */
