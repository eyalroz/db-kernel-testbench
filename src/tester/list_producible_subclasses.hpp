#include "util/factory_producible.h"
#include "util/maps.hpp"
#include "util/miscellany.hpp"

#include <map>
#include <unordered_map>

namespace util {

template<class K, class V>
static void print_sorted_keys(std::unordered_map<K,V> const& map, std::ostream& os, const std::string& separator){
	auto keys = util::get_keys(map);
	std::sort(keys.begin(), keys.end());
	util::print_separated(os, separator, keys.cbegin(), keys.cend());
}

template <typename factory_producible_t>
void list_producible_subclasses(
	std::ostream&       os,
	bool                be_verbose,
	const std::string&  name_for_a_subclass_in_plural,
	const std::string&  separator = "\n")
{
	auto subclass_factory = factory_producible_t::getSubclassFactory();
	auto subclass_instnatiator_map = subclass_factory.getInsantiators();
	if (be_verbose) {
		auto num_insantiators = subclass_instnatiator_map.size();
		if (subclass_instnatiator_map.empty()) {
			os << "There are no registered " << name_for_a_subclass_in_plural << ".";
		}
		else {
			os << "There are " << num_insantiators
			   << " registered " << name_for_a_subclass_in_plural << ":\n";
		}
	}
	print_sorted_keys(subclass_instnatiator_map, os, separator);
	os << '\n';
}

} // namespace util
