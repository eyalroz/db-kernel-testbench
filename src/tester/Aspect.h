#pragma once
#ifndef ASPECT_H_
#define ASPECT_H_

#include <stdexcept>

#include <string>

namespace cuda {

typedef enum { HostSide = 0, DeviceSide = 1, NumberOfAspects = 2} Aspect;
static const std::string AspectNames[] = {"HostSide", "DeviceSide"};

inline Aspect opposite(Aspect aspect) {
	return aspect == HostSide ? DeviceSide : HostSide;
}

inline std::string to_string(Aspect aspect) {
	if (aspect >= NumberOfAspects) {
		throw std::logic_error("Invalid aspect used");
	}
	return AspectNames[aspect];
}

inline std::ostream& operator<<(std::ostream& os, Aspect aspect) {
	return os << to_string(aspect);
}

} // namespace cuda

#endif /* ASPECT_H_ */
