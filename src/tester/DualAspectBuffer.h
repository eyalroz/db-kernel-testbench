#pragma once
#ifndef DUAL_ASPECT_BUFFER_H
#define DUAL_ASPECT_BUFFER_H

#include "Aspect.h"
#include <cuda/api/types.hpp>
#include <cuda/api/memory.hpp>
#include <cuda/api/multi_wrapper_impls.hpp>
#include <cuda/api/device.hpp>
#include <cuda/api/stream.hpp>

#include <gsl/gsl-lite.hpp>
#include "util/exception.h"
#include "util/memory_region_extra.h"
#include "util/macro.h"

#include <string>
#include <cstring> // for memcpy

/*
 * TODO:
 * - This class is a mess and I don't like it; but it's improving somewhat
 * - Consider using owner<T>, shared/unique ptrs
 */

namespace kernel_tests {

/**
 * An abstraction for a raw, untyped region of memory with two 'aspects' - it is
 * (potentially) present both in your host (CPU) memory and on a (specific) discrete
 * GPU. This buffer is managed strictly through its own methods, i.e. it has ownership
 * of whatever is allocated.
 *
 * Notes:
 * - Not thread-safe at all!
 * - No support for striding for now.
 * - No support for alignment or padding.
 * - No CUDA synching, i.e. memory copies may or may not have concluded when methods here return control
 *   Synch yourself, or add sync/async flags.
 * - Don't expect an incredibly consistent policy on CUDA-related failures.
 *
 * TODO:
 * - Use an enum for allocations strategy (unmapped buffers, mapped buffers, single unified-addressing buffer).
 * - Don't store two mr's internally, but rather two unique ptr's; that will allow for a more uniform destruction
 *   and avoid the need to remember the allocation strategy
 */
class DualAspectBuffer
{
public: // data types and constants
	using aspect_t = cuda::Aspect;
	static constexpr const aspect_t HostSide = cuda::HostSide;
	static constexpr const aspect_t DeviceSide = cuda::DeviceSide;
	enum class access_type_t { read_only = 0, read_write = 1 };
	using device_id_t = cuda::device::id_t;

protected: // statics
	static util::memory_region allocate(
		device_id_t device,
		aspect_t aspect, size_t size,
		bool pinned_host_memory = false)
	{
		auto allocated = (aspect == DeviceSide) ?
			cuda::device::get(device).memory().allocate(size) :
			(pinned_host_memory ? cuda::memory::host::allocate(size) : new char[size]);
		auto r = util::memory_region(allocated, size);
		return r;
	}
	static void deallocate(
		device_id_t device,
		util::memory_region buffer, aspect_t aspect, bool is_pinned_host_memory = false)
	{
		UNUSED(device); // CUDA doesn't need this, apparently
		if (!buffer) { return; }
		if (aspect == HostSide) {
			if (is_pinned_host_memory) { cuda::memory::host::free(buffer.data()); }
			else { delete static_cast<char*>(buffer.data()); }
		}
		else { cuda::memory::device::free(buffer.data()); }
	}

	void overwrite_opposite_aspect_with(aspect_t source_aspect) {
		auto destination_aspect = opposite(source_aspect);
		auto source_mr = data_[source_aspect];
		auto destination_mr = data_[destination_aspect];
		util::enforce(source_mr, "attempt to overwrite an aspect with its null opposite");
		if (!destination_mr) {
			allocate(destination_aspect);
			destination_mr = data_[destination_aspect];
		}
		auto copy_size = (last_modified_aspect_ == source_aspect) ? size_ : capacity_;
		cuda::memory::copy(destination_mr.data(), source_mr.data(), copy_size);
		last_modified_aspect_ = source_aspect;
		reconciled_ = true;
	}

protected: // mutators

	void allocate(aspect_t aspect) {
		if (using_mapped_region_pair_or_same_region_) {
			throw util::logic_error(
				"Attempt to allocate a single aspect when a buffer region pair has already been allocated.");
		}
		data_[aspect] = allocate(device_, aspect, capacity_, use_pinned_host_memory_);
		// Not touching the size here
	}

	void copy_aspect_from(const DualAspectBuffer& other, aspect_t aspect)
	{
		auto mr = data_[aspect];
		auto other_mr = other.data_[aspect];
		if (aspect == HostSide) {
			memcpy(mr.data(), other_mr.data(), other.size_);
		}
		else {
			cuda::memory::copy(mr.data(), other_mr.data(), other.size_);
		}
	}

public: // const getters
	device_id_t  device()               const { return device_;               }
	size_t       size()                 const { return size_;                 } // in bytes!
	size_t       capacity()             const { return capacity_;             } // in bytes!
	bool         reconciled()           const { return reconciled_;           }
	aspect_t     last_modified_aspect() const { return last_modified_aspect_; }

public: // other const methods

	bool is_up_to_date(aspect_t aspect) const
	{
		// a mapped region pair is always reconciled
		return reconciled_ || (last_modified_aspect_ == aspect);
	}
	bool is_writable(aspect_t aspect) const
	{
		return is_up_to_date(aspect);
	}

public: // mutators

	/**
	 * Indicate how much of the buffer is actually used. Nothing
	 * is actually done to the buffer itself nor to its location - no
	 * contraction, reallocation, synchronization, etc - and we're not
	 * even marking it unreconciled.
	 *
	 * @param new_size The new effective size of the buffer that's in user
	 * (beginning from the start); cannot exceed the @ref capacity.
	 */
	void set_size(size_t new_size) {
		util::enforce(size_ <= capacity_,
			"The effective size cannot exceed the capacity (you haven't "
			"written past the end of the bufferm, have you?)");
		if (!reconciled_) { touch(last_modified_aspect_); }
		size_ = new_size;
	}

	/**
	 * Copies the contents of another DualAspectBuffer (and sets
	 * this buffer's effective size to that buffers').
	 *
	 * @todo perhaps this would be better as a personal utilit
	 *
	 * @param other the DualAspectBuffer to copy from
	 * @param aspect_to_prefer_for_reconciled_buffers if the source buffer's
	 * aspects are not reconciled, we'll simply copy the last modified aspect
	 * to this buffer's corresponding aspect; but if @p other's aspects _are_
	 * reconciled we need to choose which to copy.
	 * @param copy_both_aspects Ignore which aspect of the other buffer has
	 * last been modified, and just copy both of its aspects to this buffer's
	 * corresponding aspects
	 */
	void overwrite_using(const DualAspectBuffer& other,
		aspect_t aspect_to_prefer_for_reconciled_buffers,
		bool copy_both_aspects = false)
	{
		util::enforce(device_ == other.device_, "Can only copy from buffers on the same device");
		util::enforce(capacity_ >= other.size_, "Cannot copy from buffers with used size beyond target buffer capacity_");

		if (using_mapped_region_pair_or_same_region_) {
			copy_aspect_from(other, HostSide);
			reconciled_ = true;
			last_modified_aspect_ = HostSide; // not that it matters
		}
		else {
			if ((!other.reconciled_ && other.last_modified_aspect_ == HostSide) ||
				( other.reconciled_ && (aspect_to_prefer_for_reconciled_buffers == HostSide || copy_both_aspects) )) {
				copy_aspect_from(other, HostSide);
				last_modified_aspect_ = HostSide;
			}
			if ((!other.reconciled_ && other.last_modified_aspect_ == DeviceSide) ||
				( other.reconciled_ && (aspect_to_prefer_for_reconciled_buffers == DeviceSide || copy_both_aspects) )) {
				copy_aspect_from(other, DeviceSide);
				last_modified_aspect_ = DeviceSide;
			}
			reconciled_ = copy_both_aspects && other.reconciled_;
		}
		size_ = other.size_;
	}

	/**
	 * Ensures any changes to the last modified aspect are propagated to the other aspect.
	 * After executing this, both aspects are up-to-date and writable.
	 */
	void reconcile_aspects() {
		if (reconciled_) { return; }// Note that a mapped-region-pair never becomes unreconciled...
		overwrite_opposite_aspect_with(last_modified_aspect_);
	}

	/**
	 * Change object state to reflect a write to one aspect without actually writing anything to it
	 * (or fail if that aspect cannot be written to at the moment).
	 */
	void touch(aspect_t aspect) {
		util::enforce(is_writable(aspect),
			"Attempt to write to aspect ", aspect, " of a DualAspectBuffer - after "
			"the other aspect was potentially written to, but before the aspects "
			"were reconciled");
		if (!using_mapped_region_pair_or_same_region_) { reconciled_ = false; }
		last_modified_aspect_ = aspect;
		// ... note we haven't changed the size. 'touch'ing is more delicate than
		// getting a pointer you can write to
	}


public: // this deserves a section of its own...
	/**
	 * Get access to the underlying aspect-specific raw buffer of one of the aspects -
	 * either in a form safe for reading or a form relevant for writing. Use at your
	 * own risk...
	 */
	util::memory_region get(aspect_t aspect, access_type_t access_type)
	{
		if (access_type == access_type_t::read_write) {
			touch(aspect);
			size_ = capacity_;
		}
		else { // access_type == access_type_t::read_only
			util::enforce(is_up_to_date(aspect));
		}
		return data_[aspect];
	}

public: // constructors and destructor

	DualAspectBuffer(
		device_id_t              device_id,
		size_t                   capacity,
		bool                     use_mapped_region_pair = true)
		:
		device_(device_id), use_pinned_host_memory_(true), using_mapped_region_pair_or_same_region_(use_mapped_region_pair),
		capacity_(capacity), last_modified_aspect_(HostSide), reconciled_(true), size_(0)

	{
		if (using_mapped_region_pair_or_same_region_) {
			auto device = ::cuda::device::get(device_id);
			auto pair = cuda::memory::mapped::allocate(device, capacity/*,
				{
					cuda::memory::mapped::region_pair::isnt_portable_across_cuda_contexts,
					cuda::memory::mapped::region_pair::without_cpu_write_combining
				}*/
			);
			data_[DeviceSide] = { pair.device_side, capacity } ;
			data_[HostSide] = { pair.host_side, capacity } ;
		}
		else {
			allocate(HostSide);
			allocate(DeviceSide);
		}
		// Should we really have reconciled false?
	}

	// Takes ownership!
	// TODO: Use gsl's owner<T> wrapper
	DualAspectBuffer(
		device_id_t              device_,
		void*                    raw_buffer,
		aspect_t                 aspect,
		size_t                   capacity,
		size_t                   size)
		:
		device_(device_), use_pinned_host_memory_(true),
		using_mapped_region_pair_or_same_region_(false), capacity_(capacity),
		last_modified_aspect_(aspect), reconciled_(false), size_(size)

	{
		util::enforceNonNull(raw_buffer);
		data_[aspect] = { raw_buffer, capacity };
		allocate(opposite(aspect));
	}

	DualAspectBuffer(
		device_id_t              device_,
		void*                    raw_buffer,
		aspect_t                 aspect,
		size_t                   capacity)
		:
		DualAspectBuffer(device_, raw_buffer, aspect, capacity, capacity) { };

	// Takes ownership!
	DualAspectBuffer(
		device_id_t              device_,
		void*                    host_aspect_raw_buffer,
		void*                    device_aspect_raw_buffer,
		bool                     are_reconciled,
		size_t                   capacity,
		size_t                   size_)
		:
		device_(device_), use_pinned_host_memory_(true), using_mapped_region_pair_or_same_region_(false), capacity_(capacity),
		last_modified_aspect_(HostSide), reconciled_(are_reconciled), size_(size_)
	{
		util::enforceNonNull(host_aspect_raw_buffer);
		util::enforceNonNull(device_aspect_raw_buffer);
		data_[HostSide]   = { host_aspect_raw_buffer, capacity };
		data_[DeviceSide] = { device_aspect_raw_buffer, capacity };
	}

	// Takes ownership!
	DualAspectBuffer(
		device_id_t              device_,
		void*                    host_aspect_raw_buffer,
		void*                    device_aspect_raw_buffer,
		bool                     are_reconciled,
		size_t                   capacity)
		:
		DualAspectBuffer(device_, host_aspect_raw_buffer, device_aspect_raw_buffer, capacity, capacity, are_reconciled)
		{ };

	~DualAspectBuffer() {
		if (using_mapped_region_pair_or_same_region_) {
			cuda::memory::mapped::free( {
				data_[HostSide].data(),
				data_[DeviceSide].data()
			});
		}
		else {
			deallocate(device_, data_[HostSide], HostSide, use_pinned_host_memory_);
			deallocate(device_, data_[DeviceSide], DeviceSide);
		}
	}

	DualAspectBuffer(const DualAspectBuffer& other) = delete;

private:
	const device_id_t                 device_;
	const bool                        use_pinned_host_memory_;
	const bool                        using_mapped_region_pair_or_same_region_;
	const size_t                      capacity_;  // in bytes; and it's the same for both aspects
	aspect_t                          last_modified_aspect_;
	bool                              reconciled_;
	util::memory_region               data_[2]; // one for HostSide, one for DeviceSide
	/**
	 * The number of bytes that is actually in use (as opposed to @ref capacity).
	 * Of course it is up to the code using this class to set this to the appropriate
	 * value; this class itself will always assume the full capacity is used when someone
	 * get()s one of the aspects with write access.
	 *
	 * @see @ref set_size
	 */
	size_t                             size_;
};

} // namespace cuda

#endif /* DUAL_ASPECT_BUFFER_H */
