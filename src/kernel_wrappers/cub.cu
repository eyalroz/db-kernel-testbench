
#include "cub.h"

#include "util/poor_mans_reflection.h"

#include <cub/device/device_partition.cuh>
#include <cub/device/device_radix_sort.cuh>
#include <cuda/api/stream.hpp>
#include <cuda/functors.hpp>


namespace cub {
namespace device_partition {


template <typename SelectionOp>
size_t get_scratch_size(input_size_t num_items)
{
	using Datum       = typename SelectionOp::result_type;

	scratch_size_t scratch_size = 0;
	auto cub_primitive = &::cub::DevicePartition::If<const Datum*, Datum*, decltype(num_items)*, SelectionOp>;
	auto status = cub_primitive(
		nullptr,               // d_temp_storage
		scratch_size,          // temp_storage_bytes,
		nullptr,               // d_in,
		nullptr,               // d_out,
		nullptr,               // d_num_selected_out,
		num_items,             // num_items,
		SelectionOp(),         // select_op,
		cuda::stream::default_stream_id,
		                       // stream             = 0  ; should not matter since nothing is launched
		false                  // debug_synchronous  = false  ; should not matter since nothing is launched)
	);
	cuda::throw_if_error(status, "cub::DevicePartition::If() (scratch size determination) failed");
	return scratch_size;
}

template <typename SelectionOp>
void partition(
	void*                                     device_scratch_area,
	size_t                                    scratch_size,
	typename SelectionOp::result_type const*  data,
	typename SelectionOp::result_type*        partitioned_data,
	input_size_t*                             num_selected,
	input_size_t                              num_items,
	cudaStream_t                              stream_id)
{
	using Datum = typename SelectionOp::result_type;

	if (num_items > (size_t) std::numeric_limits<int>::max()) {
		throw std::invalid_argument(
			"Unfortunately, CUB only supports a number of items "
			"which fits in a (signed) int value.");
	}

	auto status =
		::cub::DevicePartition::If<const Datum*, Datum*, input_size_t*, SelectionOp>(
			device_scratch_area, // d_temp_storage
			scratch_size,        // temp_storage_bytes,
			data,                // d_in,
			partitioned_data,    // d_out,
			num_selected,        // d_num_selected_out,
			num_items,           // num_items,
			SelectionOp(),       // select_op,
			stream_id,              // stream             = 0,
			false                // debug_synchronous  = false)
		);
	cuda::throw_if_error(status, "cub::DevicePartition::If() failed");
};

MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, get_scratch_size, cuda::functors::is_non_negative< int    >);
MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, get_scratch_size, cuda::functors::is_non_negative< float  >);
MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, get_scratch_size, cuda::functors::is_non_negative< double >);

MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, partition, cuda::functors::is_non_negative< int    >);
MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, partition, cuda::functors::is_non_negative< float  >);
MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, partition, cuda::functors::is_non_negative< double >);

} // namespace device_partition

namespace radix_sort {

template <typename Datum>
scratch_size_t get_scratch_size(input_size_t num_items)
{
	scratch_size_t scratch_size = 0;
	auto status = ::cub::DeviceRadixSort::SortKeys<Datum>(
		nullptr,               // d_temp_storage
		scratch_size,          // temp_storage_bytes
		nullptr,               // d_keys_out
		nullptr,               // d_keys_in
		num_items              // num_items
	);
	cuda::throw_if_error(status,
		"cub::DeviceRadixSort::SortKeys() (scratch size determination) failed");

	return scratch_size;
}

template <typename Datum>
void sort(
	void*                       device_scratch_area,
	scratch_size_t              scratch_size,
	Datum*        __restrict__  sorted_data,
	const Datum*  __restrict__  input_data,
	input_size_t                num_items,
	cudaStream_t                stream_id,
	bool                        ascending)
{

	cuda::status_t status;
	status = ascending ?
		::cub::DeviceRadixSort::SortKeys<Datum>(
			device_scratch_area,       // d_temp_storage
			scratch_size,              // temp_storage_bytes
			(Datum*) input_data,       // d_keys_in
			sorted_data,               // d_keys_out
			num_items,                 // num_items
			0,                         // begin_bit
			(int) sizeof(Datum) * 8,   // end_bit
			stream_id,                 // stream
			false                      // debug_synchronous
		) :
		::cub::DeviceRadixSort::SortKeysDescending<Datum>(
			device_scratch_area,       // d_temp_storage
			scratch_size,              // temp_storage_bytes
			(Datum*) input_data,       // d_keys_in
			sorted_data,               // d_keys_out
			num_items,                 // num_items
			0,                         // begin_bit
			(int) sizeof(Datum) * 8,   // end_bit
			stream_id,                 // stream
			false                      // debug_synchronous
		);
	cuda::throw_if_error(status, "cub::RadixSort::SortKeys() failed");
};

MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, get_scratch_size, \
	char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long, long long, unsigned long long);
MAP_BINARY(INSTANTIATE_FREESTANDING_FUNCTION, sort, \
	char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long, long long, unsigned long long);


//#undef INSTANTIATE

} // namespace radix_sort
} // namespace cub
