
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/data_layout/set_representation/dense_to_sparse.cuh"
#endif

namespace cuda {
namespace kernels {
namespace set_representation {
namespace dense_to_sparse {

// TODO: This currently ignores the possibility of a sorted variant of the kernel

template<unsigned IndexSize>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;
	using bit_container_type = standard_bit_container_t;
};

#ifdef __CUDACC__

template<unsigned IndexSize>
launch_configuration_t kernel_t<IndexSize>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	namespace kernel_ns = cuda::kernels::set_representation::dense_to_sparse;

	auto input_data_length = any_cast<size_t>(extra_arguments.at("domain_size"));
	kernel_ns::launch_config_resolution_params_t<IndexSize> params(
		device_properties,
		input_data_length,
		limits.dynamic_shared_memory);

	return cuda::kernels::resolve_launch_configuration(params, limits);
}

template<unsigned IndexSize>
void kernel_t<IndexSize>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{
	auto sparse        = any_cast<index_type*               >(arguments.at("sparse"        ));
	auto sparse_length = any_cast<size_type*                >(arguments.at("sparse_length" ));
	auto dense_bits    = any_cast<const bit_container_type* >(arguments.at("dense_bits"    ));
	auto domain_size   = any_cast<size_type                 >(arguments.at("domain_size"   ));

	cuda::kernel::enqueue_launch(*this,
		stream, launch_config,
		sparse, sparse_length, dense_bits, domain_size
	);
}

template<unsigned IndexSize>
const device_function_t kernel_t<IndexSize>::get_device_function() const
{
	return reinterpret_cast<const void*>(
		cuda::kernels::set_representation::dense_to_sparse::dense_to_unsorted_sparse<IndexSize>);
}

static_block {
	kernel_t < 4 >::registerInSubclassFactory();
	kernel_t < 8 >::registerInSubclassFactory();
}
#endif /* __CUDACC__ */

} // namespace dense_to_sparse
} // namespace set_representation
} // namespace kernels
} // namespace cuda
