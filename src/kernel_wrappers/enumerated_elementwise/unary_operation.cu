
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/enumerated_elementwise/unary_operation.cuh"
#endif

namespace cuda {
namespace kernels {
namespace enumerated_elementwise {
namespace unary {

#ifndef __CUDACC__
enum : serialization_factor_t { DefaultSerializationFactor = 16 };
#endif

template <typename Op, serialization_factor_t SerializationFactor = DefaultSerializationFactor>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using input_type   = typename Op::argument_type;
	using index_type   = typename Op::enumerator_type;
	using output_type  = typename Op::result_type;
	using size_type    = size_type_by_index_size<sizeof(index_type)>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t           device_properties,
		device_function::attributes_t  kernel_function_attributes,
		size_t                         length,
		launch_configuration_limits_t  limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			Op, SerializationFactor
		> params(
			device_properties,
			length);

		return cuda::kernels::resolve_launch_configuration(params, limits, SerializationFactor);
	}
#else
	;
#endif
};

#ifdef __CUDACC__

template <typename Op, serialization_factor_t SerializationFactor>
launch_configuration_t kernel_t<Op, SerializationFactor>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length = any_cast<size_t>(extra_arguments.at("length"));

	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length,
		limits);
}

template <typename Op, serialization_factor_t SerializationFactor>
void kernel_t<Op, SerializationFactor>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{
	auto output = any_cast<output_type*      >(arguments.at("output"   ));
	auto input  = any_cast<const input_type* >(arguments.at("input"    ));
	auto length = any_cast<index_type        >(arguments.at("length"   ));

	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		output, input, length
	);
}

template <typename Op, serialization_factor_t SerializationFactor>
const device_function_t kernel_t<Op, SerializationFactor>::get_device_function() const
{
	return reinterpret_cast<const void*>(cuda::kernels::enumerated_elementwise::unary::unary_operation<Op, SerializationFactor>);
}


static_block {
	using namespace cuda;
	using cuda::functors::enumerated::as_enumerated_unary;
	namespace functors = cuda::functors;

	//         Op
	//                                                     LHS        RHS       Result
	//------------------------------------------------------------------------------------
	kernel_t < as_enumerated_unary < functors::plus      < uint32_t, int32_t, int32_t > > >::registerInSubclassFactory();
	kernel_t < as_enumerated_unary < functors::multiplies< uint32_t, float,   float   > > >::registerInSubclassFactory();
	kernel_t < as_enumerated_unary < functors::plus      < uint32_t, int32_t, int32_t > > >::registerInSubclassFactory();
	kernel_t < as_enumerated_unary < functors::multiplies< uint64_t, float,   float   > > >::registerInSubclassFactory();
}


#endif /* __CUDACC__ */

} // namespace unary
} // namespace enumerated_elementwise
} // namespace kernels
} // namespace cuda
