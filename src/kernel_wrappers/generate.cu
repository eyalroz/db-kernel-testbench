
#include "common.h"
#ifdef __CUDACC__
#include "kernels/generate.cuh"
#endif

#include <ratio>

namespace cuda {
namespace kernels {
namespace generate {

#ifndef __CUDACC__
enum : serialization_factor_t { DefaultSerializationFactor = 16 };
#endif

template<unsigned IndexSize, typename UnaryFunction, serialization_factor_t SerializationFactor = DefaultSerializationFactor>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using Datum      = typename UnaryFunction::result_type;
	using index_type = uint_t<IndexSize>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t              device_properties,
		device_function::attributes_t     kernel_function_attributes,
		size_t                            length,
		launch_configuration_limits_t     limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
		IndexSize, UnaryFunction, SerializationFactor
		> params(
			device_properties,
			length);

		return cuda::kernels::resolve_launch_configuration(params, limits, SerializationFactor);
	}
#else
	;
#endif
};

#ifdef __CUDACC__

template<unsigned IndexSize, typename UnaryFunction, serialization_factor_t SerializationFactor>
launch_configuration_t kernel_t<IndexSize, UnaryFunction, SerializationFactor>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length = any_cast<size_t>(extra_arguments.at("length"));
	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length,
		limits);
}

template<unsigned IndexSize, typename UnaryFunction, serialization_factor_t SerializationFactor>
void kernel_t<IndexSize, UnaryFunction, SerializationFactor>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{
	auto result = any_cast<Datum*      >(arguments.at("result"));
	auto length = any_cast<index_type  >(arguments.at("length"));

	cuda::kernel::enqueue_launch(*this, stream, launch_config, result, length);
}

template<unsigned IndexSize, typename UnaryFunction, serialization_factor_t SerializationFactor>
const device_function_t kernel_t<IndexSize, UnaryFunction, SerializationFactor>::get_device_function() const
{
	return reinterpret_cast<const void*>(cuda::kernels::generate::generate<IndexSize, UnaryFunction, SerializationFactor>);
}

static_block {
	using half = functors::constant_by_ratio<float, std::ratio<1,2>>;
	using third = functors::constant_by_ratio<float, std::ratio<1,3>>;

	//         IndexSize  UnaryFunction
	//-----------------------------------------------------------------------------------------
	kernel_t < 4,         functors::identity<int8_t>                               >::registerInSubclassFactory();
	kernel_t < 4,         functors::identity<uint8_t>                              >::registerInSubclassFactory();
	kernel_t < 4,         functors::identity<int32_t>                              >::registerInSubclassFactory();
	kernel_t < 4,         functors::constant_by_ratio<float, std::ratio<1, 3> >    >::registerInSubclassFactory();
	kernel_t < 4,         functors::affine_transform_nullaries<float, third, half> >::registerInSubclassFactory();
	kernel_t < 4,         functors::fixed_modulus<uint32_t, 7>                     >::registerInSubclassFactory();
	kernel_t < 4,         functors::constant<uint8_t, 0>                           >::registerInSubclassFactory();
	kernel_t < 4,         functors::constant<int32_t, 1>                           >::registerInSubclassFactory();
	kernel_t < 4,         functors::affine_transform<int32_t, 0, 0>                >::registerInSubclassFactory();
	kernel_t < 4,         functors::affine_transform<int32_t, 0, 123>              >::registerInSubclassFactory();
	kernel_t < 8,         functors::identity<int32_t>                              >::registerInSubclassFactory();
	kernel_t < 8,         functors::constant_by_ratio<float, std::ratio<1, 3>>     >::registerInSubclassFactory();
	kernel_t < 8,         functors::affine_transform_nullaries<float, third, half> >::registerInSubclassFactory();
	kernel_t < 8,         functors::fixed_modulus<uint32_t, 7>                     >::registerInSubclassFactory();
	kernel_t < 8,         functors::identity<int8_t>                               >::registerInSubclassFactory();
	kernel_t < 8,         functors::identity<uint8_t>                              >::registerInSubclassFactory();
	kernel_t < 8,         functors::constant<uint8_t, 0>                           >::registerInSubclassFactory();
	kernel_t < 8,         functors::constant<int32_t, 1>                           >::registerInSubclassFactory();
	kernel_t < 8,         functors::affine_transform<int32_t, 0, 0>                >::registerInSubclassFactory();
	kernel_t < 8,         functors::affine_transform<int32_t, 0, 123>              >::registerInSubclassFactory();
}
#endif /* __CUDACC__ */

} // namespace generate
} // namespace kernels
} // namespace cuda
