
#include "kernel_wrappers/common.h"

#ifdef __CUDACC__
#include "kernels/elementwise/apply.cuh"
#endif

#include <functional>
#include <tuple>
#if __cplusplus < 201402L
#include "util/index_sequence.hpp"
	// we need std::make_index_sequence, but CUDA (8.0) does not
	// allow us to use C++14 constructs; luckily, that's entirely
	// implementable in C++11.
#endif

namespace cuda {
namespace kernels {
namespace elementwise {
namespace apply {


template<typename... Parameters>
std::string input_argument_name(unsigned zero_based_argument_index)
{
	auto one_based_argument_index = zero_based_argument_index + 1;
	return ("input_argument_" + std::to_string(one_based_argument_index));
}

#ifndef __CUDACC__
enum : serialization_factor_t { DefaultSerializationFactor = 16 };
#endif

template<
	unsigned               IndexSize,
	typename               Function,
	serialization_factor_t SerializationFactor,
	typename...            Parameters>
class kernel_t: public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using result_type = typename std::result_of<Function(Parameters...)>::type;
	using argument_types_tuple = std::tuple<Parameters...>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t           device_properties,
		device_function::attributes_t  kernel_function_attributes,
		size_t                         length,
		launch_configuration_limits_t  limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			IndexSize, Function, SerializationFactor, Parameters...
		> params(
			device_properties,
			length);

		return cuda::kernels::resolve_launch_configuration(params, limits, SerializationFactor);
	}
#else
	;
#endif

};

#ifdef __CUDACC__

template<
	unsigned               IndexSize,
	typename               Function,
	serialization_factor_t SerializationFactor,
	typename...            Parameters>
launch_configuration_t kernel_t<IndexSize, Function, SerializationFactor, Parameters...>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length = any_cast<size_t>(extra_arguments.at("length"));

	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length,
		limits);
}

template<
	unsigned               IndexSize,
	typename               Function,
    typename               ResultType,
	serialization_factor_t SerializationFactor,
	//typename               ParametersTuple,
	typename...            Parameters,
	std::size_t...         ParameterIndices>
static inline void launch_helper(
	device_function_t                      device_function,
	stream_t&                              stream,
	const launch_configuration_t&          launch_config,
	const cuda::kernel_t::arguments_type&  arguments,
	std::index_sequence<ParameterIndices...> seq// yes, not using this parameter
)
{
	using size_type = size_type_by_index_size<IndexSize>;
	// Note: not using our cuda::any_cast, to avoid ambiguity
	//
	cuda::enqueue_launch(
		device_function, stream, launch_config,
		boost::any_cast<size_type           >(arguments.at("length"           )),
		boost::any_cast<ResultType*         >(arguments.at("result"           )),
		boost::any_cast<const Parameters *  >(arguments.at(input_argument_name(ParameterIndices))) ...
	);
}

template<
	unsigned               IndexSize,
	typename               Function,
	serialization_factor_t SerializationFactor,
	typename...            Parameters>
void kernel_t<IndexSize, Function, SerializationFactor, Parameters...>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{

	using params_tuple_type = std::tuple<Parameters...>;
	return launch_helper<
		IndexSize, Function, result_type, SerializationFactor, Parameters...
	>(
		get_device_function(),
		stream,
		launch_config,
		arguments,
#if __cplusplus >= 201402L
		typename std::make_index_sequence<sizeof...(Parameters)> {}
#else
		typename std::make_index_sequence<sizeof...(Parameters)>::type {}
#endif
	);
}

template<
	unsigned               IndexSize,
	typename               Function,
	serialization_factor_t SerializationFactor,
	typename...            Parameters>
const device_function_t kernel_t<IndexSize, Function, SerializationFactor, Parameters...>::get_device_function() const
{
	return reinterpret_cast<const void*>(
		cuda::kernels::elementwise::apply::apply<IndexSize, Function, SerializationFactor, Parameters...>
	);
}

static_block {
	namespace functors = cuda::functors;
	enum { dsf = cuda::kernels::elementwise::apply::DefaultSerializationFactor };

	//         IndexSize   Function                   SerializationFactor  Parameters...
	//--------------------------------------------------------------------------------------------
	kernel_t < 4,          functors::clip<int>,                       dsf, int,      int,      int      >::registerInSubclassFactory();
	kernel_t < 4,          functors::clip<uint16_t>,                  dsf, uint16_t, uint16_t, uint16_t >::registerInSubclassFactory();
	kernel_t < 4,          functors::clip<float>,                     dsf, float,    float,    float    >::registerInSubclassFactory();
	kernel_t < 4,          functors::between_or_equal_ternary<float>, dsf, float,    float,    float    >::registerInSubclassFactory();
	kernel_t < 4,          functors::if_then_else<float>,             dsf, float,    float,    float    >::registerInSubclassFactory();

	kernel_t < 4,          functors::plus<int8_t>,                    dsf, int8_t,   int8_t             >::registerInSubclassFactory();
	kernel_t < 4,          functors::plus<int16_t>,                   dsf, int16_t,  int16_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::plus<int32_t>,                   dsf, int32_t,  int32_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::plus<int64_t>,                   dsf, int64_t,  int64_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::minus<int32_t>,                  dsf, int32_t,  int32_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::multiplies<int32_t>,             dsf, int32_t,  int32_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::plus<float>,                     dsf, float,    float              >::registerInSubclassFactory();
	kernel_t < 4,          functors::minus<float>,                    dsf, float,    float              >::registerInSubclassFactory();

	kernel_t < 4,          functors::greater_equal<float>,            dsf, float,    float              >::registerInSubclassFactory();
	kernel_t < 4,          functors::less_equal<int32_t>,             dsf, int32_t,  int32_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::equals<int32_t>,                 dsf, int32_t,  int32_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::equals<float>,                   dsf, float,    float              >::registerInSubclassFactory();
	kernel_t < 4,          functors::equals<double>,                  dsf, double,   double             >::registerInSubclassFactory();
	kernel_t < 4,          functors::equals<int16_t>,                 dsf, int16_t,  int16_t            >::registerInSubclassFactory();
	kernel_t < 4,          functors::equals<uint64_t>,                dsf, uint64_t, uint64_t           >::registerInSubclassFactory();

	kernel_t < 8,          functors::plus<float>,                     dsf, float,    float              >::registerInSubclassFactory();
	kernel_t < 8,          functors::clip<float>,                     dsf, float,    float,  float      >::registerInSubclassFactory();
}
#endif /* __CUDACC__ */

} // namespace apply
} // namespace elementwise
} // namespace kernels
} // namespace cuda
