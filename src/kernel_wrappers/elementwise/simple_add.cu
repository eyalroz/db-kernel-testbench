
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/elementwise/simple_add.cuh"
#endif

namespace cuda {
namespace kernels {
namespace elementwise {
namespace simple_add {

template <typename Datum>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	launch_configuration_t resolve_launch_configuration(
		device::properties_t           device_properties,
		device_function::attributes_t  kernel_function_attributes,
		size_t                         length,
		launch_configuration_limits_t  limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<Datum> params(device_properties, length);
		return cuda::kernels::resolve_launch_configuration(params, limits);
	}
#else
	;
#endif
};

#ifdef __CUDACC__

template <typename Datum>
launch_configuration_t kernel_t<Datum>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length = any_cast<size_t>(extra_arguments.at("length"));
	return resolve_launch_configuration(
		device_properties, kernel_function_attributes, length, limits);
}

template <typename Datum>
void kernel_t<Datum>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{
	auto result          = any_cast<Datum*        >(arguments.at("result"         ));
	auto left_hand_side  = any_cast<const Datum*  >(arguments.at("left_hand_side" ));
	auto right_hand_side = any_cast<const Datum*  >(arguments.at("right_hand_side"));
	auto length          = any_cast<size_t>        (arguments.at("length"         ));

	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		result, left_hand_side, right_hand_side
	);
}

template <typename Datum>
const device_function_t kernel_t<Datum>::get_device_function() const
{
	return reinterpret_cast<const void*>(cuda::kernels::elementwise::simple_add::simple_add<Datum>);
}


#ifdef DEBUG
static_block {
	kernel_t < int8_t    >::registerInSubclassFactory();
	kernel_t < int16_t   >::registerInSubclassFactory();
	kernel_t < int32_t   >::registerInSubclassFactory();
	kernel_t < int64_t   >::registerInSubclassFactory();
	kernel_t < uint8_t   >::registerInSubclassFactory();
	kernel_t < uint16_t  >::registerInSubclassFactory();
	kernel_t < uint32_t  >::registerInSubclassFactory();
	kernel_t < uint64_t  >::registerInSubclassFactory();
	kernel_t < double    >::registerInSubclassFactory();
	kernel_t < float     >::registerInSubclassFactory();
}
#endif
#endif /* __CUDACC__ */

} // namespace simple_add
} // namespace elementwise
} // namespace kernels
} // namespace cuda
