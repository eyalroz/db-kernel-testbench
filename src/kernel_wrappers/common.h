
#pragma once
#ifndef KERNEL_WRAPPERS_COMMON_CUH_
#define KERNEL_WRAPPERS_COMMON_CUH_

#include "kernel_wrappers/registered_wrapper.h"
#include "kernels/resolve_launch_configuration.h"
#include <cuda/functors.hpp>         // enough kernels need this to merit always including it
#include <cuda/miscellany.h>
#ifdef __CUDACC__
#include "util/static_block.h"       // for registration in the factory during static init
#endif
#include "util/endianness.h"
#include "util/integer.h"

#include <functional>
#include <utility>

#ifndef __CUDACC__

namespace cuda {

	// TODO: Code duplication of the next bit with kernels/common.h ;
	// Perhaps move this to a separate file? But...
	// under src/cuda we wouldn't like to depend on util/integer.hpp, and
	// there doesn't seem to be someplace else that's good.

	using util::uint_t;
	using util::sint_t;

	/*
	 * If you're wondering why we couldn't just do the simpler
	 * sizeof(cuda::promoted_size_t<uint_t<N>, and instead take the size
	 * and go back to a type - it's in order to "canonicalize" the type,
	 * so we don't get mismatches such as unsigned int vs long unsigned int
	 */
	template <unsigned N>
	using size_type_by_index_size = uint_t<sizeof(cuda::promoted_size_t<uint_t<N>>)>;

	template <typename T>
	using size_type_by_index_type = size_type_by_index_size<sizeof(T)>;
}

#endif

/**
 * We sometimes need to reconcile the types util::endianness_t
 * and cuda::endianness_t. We can't just unify them, since the
 * CUDA API stand alone as does the code in util/ ...
 */
inline constexpr cuda::endianness_t translate(util::endianness_t endianness)
{
	return (endianness == util::endianness_t::little_endian) ?
		cuda::endianness_t::little_endian :
		cuda::endianness_t::big_endian;
}

#endif /* KERNEL_WRAPPERS_COMMON_CUH_ */
