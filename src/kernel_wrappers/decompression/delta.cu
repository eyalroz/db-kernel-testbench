
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/decompression/delta.cuh"
#endif

namespace cuda {
namespace kernels {
namespace decompression {
namespace delta {

// TODO: This currently ignores the possibility of a sorted variant of the kernel

template<unsigned IndexSize, typename Uncompressed, typename Difference>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using compressed_type   = Difference;
	using uncompressed_type = Uncompressed;
	using size_type         = size_type_by_index_size<IndexSize>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t           device_properties,
		device_function::attributes_t  kernel_function_attributes,
		size_t                         length,
		size_t                         segment_length,
		launch_configuration_limits_t  limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			IndexSize, Uncompressed, Difference
		> params(
			device_properties,
			length, segment_length,
			limits.dynamic_shared_memory);

		return cuda::kernels::resolve_launch_configuration(params, limits);
	}
#else
	;
#endif

};

#ifdef __CUDACC__

template<unsigned IndexSize, typename Uncompressed, typename Difference>
inline launch_configuration_t kernel_t<IndexSize, Uncompressed, Difference>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length         = any_cast<size_t>(extra_arguments.at("length"        ));
	auto segment_length = any_cast<size_t>(extra_arguments.at("segment_length"));

	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length, segment_length,
		limits);
}

template<unsigned IndexSize, typename Uncompressed, typename Difference>
inline void kernel_t<IndexSize, Uncompressed, Difference>::enqueue_launch(
		cuda::stream_t&                stream,
		const launch_configuration_t&  launch_config,
		arguments_type                 arguments) const
{
	auto decompressed      = any_cast<uncompressed_type*       >(arguments.at("decompressed"    ));
	auto compressed_input  = any_cast<const compressed_type*   >(arguments.at("compressed_input"));
	auto anchor_values     = any_cast<const uncompressed_type* >(arguments.at("anchor_values"   ));
	auto length            = any_cast<size_type                >(arguments.at("length"          ));
	auto segment_length    = any_cast<size_type                >(arguments.at("segment_length"  ));

	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		decompressed, compressed_input, anchor_values, length, segment_length
	);
}

template<unsigned IndexSize, typename Uncompressed, typename Difference>
inline const device_function_t kernel_t<IndexSize, Uncompressed, Difference>::get_device_function() const
{
	return cuda::kernels::decompression::delta::decompress<IndexSize, Uncompressed, Difference>;
}

static_block {
	//         IndexSize   Uncompressed   Difference
	// ----------------------------------------------------
	kernel_t < 4,          uint_t<2>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<4>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<4>,     sint_t<2> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<8>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<8>,     sint_t<2> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<8>,     sint_t<4> >::registerInSubclassFactory();

	kernel_t < 4,          sint_t<2>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 4,          sint_t<4>,     sint_t<2> >::registerInSubclassFactory();
	kernel_t < 4,          sint_t<2>,     uint_t<1> >::registerInSubclassFactory();
	kernel_t < 4,          sint_t<4>,     uint_t<2> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<2>,     uint_t<1> >::registerInSubclassFactory();
	kernel_t < 4,          uint_t<4>,     uint_t<2> >::registerInSubclassFactory();

	kernel_t < 8,          uint_t<2>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 8,          uint_t<4>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 8,          uint_t<4>,     sint_t<2> >::registerInSubclassFactory();
	kernel_t < 8,          uint_t<8>,     sint_t<1> >::registerInSubclassFactory();
	kernel_t < 8,          uint_t<8>,     sint_t<2> >::registerInSubclassFactory();
	kernel_t < 8,          uint_t<8>,     sint_t<4> >::registerInSubclassFactory();

}

#endif /* __CUDACC__ */

} // namespace delta
} // namespace decompression
} // namespace kernels
} // namespace cuda
