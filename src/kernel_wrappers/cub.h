/**
 * CUB library wrappers and instantiators
 *
 * This file-pair is necessary so that non-CUDA C++ code can
 * make CUB calls without having to compile with CUDA (and hence actually
 * include the CUB code itself).
 *
 * Now, we could replicate the entirety of CUB and keep just the function
 * declarations, but - we won't do that, since, well, I don't have the time
 * and I don't need all of it right now. In fact, the headers here will just
 * suit my needs rather than allow invocation of CUB functions as-is.
 *
 * @note I have minimized dependence here on any of my other code - just
 * the standard C++ library and CUDA are necessary for the using C++ code
 * to know about (and the implementation relatively frugal also, although
 * not entirely so since the instantiationscan get specific.
 *
 */
#pragma once
#ifndef CUB_WRAPPERS_H_
#define CUB_WRAPPERS_H_

#include <driver_types.h> // for cudaStream_t
#include <cstddef>        // for size_t

#include "util/integer.h"

namespace cub {

enum { input_size_type_size = 4 };
	// What can I do? That's what CUB offers; and it's even worse, it takes the
	// values as signed integers :-(
using input_size_t = util::uint_t<input_size_type_size>;
using scratch_size_t = size_t; // Yes, it's dumb - scratch size type is larger than the input length

namespace device_partition {

template <typename SelectionOp>
size_t get_scratch_size(input_size_t num_items);

template <typename SelectionOp>
void partition(
	void*                                     device_scratch_area,
	scratch_size_t                            scratch_size,
	typename SelectionOp::result_type const*  data,
	typename SelectionOp::result_type*        partitioned_data,
	input_size_t*                             num_selected,
	input_size_t                              num_items,
	cudaStream_t                              stream);

} // namespace device_partition

namespace radix_sort {

template <typename Datum>
scratch_size_t get_scratch_size(input_size_t num_items);

template <typename Datum>
void sort(
	void*                       device_scratch_area,
	scratch_size_t              scratch_size,
	Datum*        __restrict__  sorted_data,
	const Datum*  __restrict__  input_data,
	input_size_t                num_items,
	cudaStream_t                stream_id,
	bool                        ascending = true);

} // namespace radix_sort
} // namespace cub

#endif /* CUB_WRAPPERS_H_ */
