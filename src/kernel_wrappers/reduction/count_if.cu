
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/reduction/count_if.cuh"
#endif

namespace cuda {
namespace kernels {
namespace reduction {
namespace count_if {

template<unsigned IndexSize, typename UnaryPredicate, unsigned CountSize = sizeof(size_type_by_index_size<IndexSize>) >
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using predicate_argument_type = typename UnaryPredicate::argument_type;
	using index_type              = uint_t<IndexSize>;
	using count_type              = uint_t<CountSize>;
	using size_type               = size_type_by_index_size<IndexSize>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t              device_properties,
		device_function::attributes_t     kernel_function_attributes,
		size_t                            length,
		launch_configuration_limits_t     limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			IndexSize, UnaryPredicate, CountSize
		> params(
			device_properties,
			length);

		return cuda::kernels::resolve_launch_configuration(params, limits);
	}
#else
	;
#endif
};

#ifdef __CUDACC__

template<unsigned IndexSize, typename UnaryPredicate, unsigned CountSize>
launch_configuration_t kernel_t<IndexSize, UnaryPredicate, CountSize>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length = any_cast<size_t>(extra_arguments.at("length"));
	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length,
		limits);
}

template<unsigned IndexSize, typename UnaryPredicate, unsigned CountSize>
void kernel_t<IndexSize, UnaryPredicate, CountSize>::enqueue_launch(
	stream_t&                       stream,
	const launch_configuration_t&   launch_config,
	arguments_type                  arguments) const
{
	auto result = any_cast<count_type*                    >(arguments.at("result" ));
	auto data   = any_cast<const predicate_argument_type* >(arguments.at("data"   ));
	auto length = any_cast<size_type                      >(arguments.at("length" ));

	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		result, data, length
	);
}

template<unsigned IndexSize, typename UnaryPredicate, unsigned CountSize>
const device_function_t kernel_t<IndexSize, UnaryPredicate, CountSize>::get_device_function() const
{
	return reinterpret_cast<const void*>(
		cuda::kernels::reduction::count_if::count_if<IndexSize, UnaryPredicate, CountSize>);
}


static_block {

	// 32-bit count results

	//         IndexSize UnaryPredicate
	//--------------------------------------------------------------
	kernel_t < 4,        functors::non_zero<uint32_t>            >::registerInSubclassFactory();
	kernel_t < 4,        functors::non_zero<uint64_t>            >::registerInSubclassFactory();
	kernel_t < 4,        functors::about_zero<float,  bool, 20>  >::registerInSubclassFactory();
	kernel_t < 4,        functors::about_zero<double, bool, 20>  >::registerInSubclassFactory();
	kernel_t < 4,        functors::is_zero<int32_t>              >::registerInSubclassFactory();

	// 64-bit count results

	//         IndexSize UnaryPredicate
	//--------------------------------------------------------------
	kernel_t < 8,        functors::non_zero<uint32_t>           >::registerInSubclassFactory();
	kernel_t < 8,        functors::non_zero<uint64_t>           >::registerInSubclassFactory();
	kernel_t < 8,        functors::about_zero<float,  bool, 20> >::registerInSubclassFactory();
	kernel_t < 8,        functors::about_zero<double, bool, 20> >::registerInSubclassFactory();
	kernel_t < 8,        functors::is_zero<int32_t>             >::registerInSubclassFactory();
}
#endif /* __CUDACC__ */

} // namespace count_if
} // namespace reduction
} // namespace kernels
} // namespace cuda

