
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/reduction/multi_reduce/sparse_to_dense.cuh"
#endif

namespace cuda {
namespace kernels {
namespace reduction {
namespace multi_reduce {
namespace dynamic_num_reductions {
namespace sparse_to_dense {

#ifndef __CUDACC__
enum : serialization_factor_t { DefaultSerializationFactor = 32 };
#endif

template <unsigned InputIndexSize, typename ReductionOp, typename InputDatum,
		  unsigned ReductionIndexSize>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using result_type           = typename ReductionOp::result_type;
	using input_type            = InputDatum;
	using index_type            = uint_t<InputIndexSize>;
	using input_size_type       = size_type_by_index_size<InputIndexSize>;
	using reduction_index_type  = uint_t<ReductionIndexSize>;
	using num_reductions_type   = size_type_by_index_size<ReductionIndexSize>;
	using reduction_op_type     = ReductionOp;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t              device_properties,
		device_function::attributes_t     kernel_function_attributes,
		size_t                            length,
		size_t                            num_distinct_indices,
		optional<serialization_factor_t>  serialization_factor,
		launch_configuration_limits_t     limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			InputIndexSize, ReductionOp, InputDatum, ReductionIndexSize
		> params(
			device_properties,
			length, num_distinct_indices,
			limits.dynamic_shared_memory);

		return cuda::kernels::resolve_launch_configuration(params, limits, serialization_factor);
	}
#else
	;
#endif
};

#ifdef __CUDACC__

template <unsigned InputIndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
launch_configuration_t kernel_t<InputIndexSize, ReductionOp, InputDatum, ReductionIndexSize>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length               = any_cast<size_t>(extra_arguments.at("length"));
	auto num_distinct_indices = any_cast<size_t>(extra_arguments.at("num_distinct_indices"));
	auto serialization_factor =
		any_cast<serialization_factor_t>(maybe_at(extra_arguments, "serialization_factor"));

	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length, num_distinct_indices, serialization_factor,
		limits);
}

template <unsigned InputIndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
void kernel_t<InputIndexSize, ReductionOp, InputDatum, ReductionIndexSize>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{
	using index_type = uint_t<InputIndexSize>;

	auto target               = any_cast<result_type*                >(arguments.at("target"              ));
	auto data                 = any_cast<const input_type*           >(arguments.at("data"                ));
	auto indices              = any_cast<const reduction_index_type* >(arguments.at("indices"             ));
	auto length               = any_cast<input_size_type             >(arguments.at("length"              ));
	auto num_distinct_indices = any_cast<num_reductions_type         >(arguments.at("num_distinct_indices"));


	auto maybe_serialization_factor =
		any_cast<serialization_factor_t>(maybe_at(arguments, "serialization_factor"));
	auto serialization_factor = maybe_serialization_factor.value_or(DefaultSerializationFactor);

	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		target, data, indices, length, num_distinct_indices,serialization_factor
	);
}

template <unsigned InputIndexSize, typename ReductionOp, typename InputDatum, unsigned ReductionIndexSize>
const device_function_t kernel_t<InputIndexSize, ReductionOp, InputDatum, ReductionIndexSize>::get_device_function() const
{
	return reinterpret_cast<const void *>(
		sparse_to_dense<InputIndexSize, ReductionOp, InputDatum, ReductionIndexSize>);
}

static_block {
	namespace functors = cuda::functors;

	//         InputIndexSize  ReductionOp                   InputDatum   ReductionIndexSize
	//-------------------------------------------------------------------------------------
	kernel_t < 4,              functors::plus<int>,          int32_t,     4 >::registerInSubclassFactory();
	kernel_t < 4,              functors::plus<uint32_t>,     uint32_t,    4 >::registerInSubclassFactory();
	kernel_t < 4,              functors::maximum<int>,       int32_t,     4 >::registerInSubclassFactory();
	kernel_t < 4,              functors::maximum<uint32_t>,  uint32_t,    4 >::registerInSubclassFactory();
	kernel_t < 8,              functors::plus<uint32_t>,     uint32_t,    4 >::registerInSubclassFactory();
	kernel_t < 8,              functors::plus<uint32_t>,     uint32_t,    8 >::registerInSubclassFactory();
}
#endif


} // namespace sparse_to_dense
} // namespace dynamic_num_reductions
} // namespace multi_reduce
} // namespace reduction
} // namespace kernels
} // namespace cuda

