
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/reduction/multi_reduce/histogram.cuh"
#endif

namespace cuda {
namespace kernels {
namespace reduction {
namespace histogram {


#ifndef __CUDACC__
enum : serialization_factor_t { DefaultSerializationFactor = 32 };
#endif

template <
	unsigned InputIndexSize,
	unsigned BinIndexSize,
	unsigned CountSize = sizeof(size_type_by_index_size<InputIndexSize>)
>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	// Remember: A histogram has 'bins'. Each bins corresponds to
	// to a possible value in the input data; in the case of this
	// kernel, the only data we get is the application of this
	// correspondence, i.e. someone has computed the relevent
	// bin indices, and we just need to tally them (to throw
	// values into the different bins, so to speak); the bin
	// is like a counter, and bin_type is the type we use for
	// counting.
	//
	// So if CountSize (and hence bin_Type) is not larger than
	// InputIndexSize, you run the risk of your bins overflowing,
	// in case you get an input array with (1 << InputIndexSize)
	// or more elements of the same value. Have fun.

	using bin_index_type       = uint_t<BinIndexSize>;
	using histogram_size_type  = size_type_by_index_size<BinIndexSize>;
	using count_type           = uint_t<CountSize>;
	using bin_type             = count_type;
	using input_index_size     = uint_t<InputIndexSize>;
	using input_size_type      = size_type_by_index_size<InputIndexSize>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t              device_properties,
		device_function::attributes_t     kernel_function_attributes,
		size_t                            data_length,
		size_t                            histogram_length,
		optional<serialization_factor_t>  serialization_factor,
		launch_configuration_limits_t     limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			InputIndexSize, BinIndexSize, CountSize
		> params(
			device_properties,
			data_length, histogram_length,
			limits.dynamic_shared_memory);

		return cuda::kernels::resolve_launch_configuration(params, limits, serialization_factor);
	}
#else
	;
#endif
};

#ifdef __CUDACC__


template <unsigned InputIndexSize, unsigned BinIndexSize, unsigned CountSize>
launch_configuration_t kernel_t<InputIndexSize, BinIndexSize, CountSize>::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto data_length      = any_cast<size_t >(extra_arguments.at("data_length"     ));
	auto histogram_length = any_cast<size_t >(extra_arguments.at("histogram_length"));
	auto serialization_factor = any_cast<serialization_factor_t>(
		maybe_at(extra_arguments, "serialization_factor"));

	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		data_length, histogram_length, serialization_factor,
		limits);
}


template <unsigned InputIndexSize, unsigned BinIndexSize, unsigned CountSize>
void kernel_t<InputIndexSize, BinIndexSize, CountSize>::enqueue_launch(
	cuda::stream_t&                stream,
	const launch_configuration_t&  launch_config,
	arguments_type                 arguments) const
{
	auto histogram        = any_cast<bin_type*             >(arguments.at("histogram"        ));
	auto bin_indices      = any_cast<const bin_index_type* >(arguments.at("bin_indices"      ));
	auto data_length      = any_cast<input_size_type       >(arguments.at("data_length"      ));
	auto histogram_length = any_cast<histogram_size_type   >(arguments.at("histogram_length" ));

	auto maybe_serialization_factor =
		any_cast<serialization_factor_t>(maybe_at(arguments, "serialization_factor"));
	auto serialization_factor = maybe_serialization_factor.value_or(DefaultSerializationFactor);
	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		histogram, bin_indices, data_length, histogram_length, serialization_factor
	);
}

template <unsigned InputIndexSize, unsigned BinIndexSize, unsigned CountSize>
const device_function_t kernel_t<InputIndexSize, BinIndexSize, CountSize>::get_device_function() const
{
	return reinterpret_cast<const void*>(
		cuda::kernels::reduction::histogram::histogram<InputIndexSize, BinIndexSize, CountSize>);
}

static_block {
	//         InputIndexSize  BinIndexSize
	//----------------------------------------------
	kernel_t < 4,             1 >::registerInSubclassFactory();
	kernel_t < 4,             2 >::registerInSubclassFactory();
	kernel_t < 4,             4 >::registerInSubclassFactory();
	kernel_t < 4,             8 >::registerInSubclassFactory();
	kernel_t < 8,             4 >::registerInSubclassFactory();
	kernel_t < 8,             8 >::registerInSubclassFactory();
}
#endif /* __CUDACC__ */

} // namespace histogram
} // namespace reduction
} // namespace kernels
} // namespace cuda

