
#include "kernel_wrappers/common.h"
#ifdef __CUDACC__
#include "kernels/reduction/reduce.cuh"
#endif

namespace cuda {
namespace kernels {
namespace reduction {
namespace reduce {

template <
	unsigned IndexSize,
	typename ReductionOp,
	typename InputDatum,
	typename PretransformOp = functors::identity<InputDatum>
>
class kernel_t : public cuda::registered::kernel_t {
public:
	REGISTERED_KERNEL_WRAPPER_BOILERPLATE_DEFINITIONS(kernel_t);

	using result_type  = typename ReductionOp::result_type;
	using index_type   = uint_t<IndexSize>;
	using size_type    = size_type_by_index_size<IndexSize>;

	launch_configuration_t resolve_launch_configuration(
		device::properties_t              device_properties,
		device_function::attributes_t     kernel_function_attributes,
		size_t                            length,
		launch_configuration_limits_t     limits) const
#ifdef __CUDACC__
	{
		launch_config_resolution_params_t<
			IndexSize, ReductionOp, InputDatum, PretransformOp
		> params(
			device_properties,
			length,
			limits.dynamic_shared_memory);

		return cuda::kernels::resolve_launch_configuration(params, limits);
	}
#else
	;
#endif
};

#ifdef __CUDACC__


template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp>
launch_configuration_t kernel_t<IndexSize, ReductionOp, InputDatum, PretransformOp>
::resolve_launch_configuration(
	device::properties_t           device_properties,
	device_function::attributes_t  kernel_function_attributes,
	arguments_type                 extra_arguments,
	launch_configuration_limits_t  limits) const
{
	auto length = any_cast<size_t>(extra_arguments.at("length"));
	return resolve_launch_configuration(
		device_properties, kernel_function_attributes,
		length,
		limits);
}

template <unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp>
void kernel_t<IndexSize, ReductionOp, InputDatum, PretransformOp>::enqueue_launch(
	stream_t&                        stream,
	const launch_configuration_t&   launch_config,
	arguments_type                  arguments) const
{
	auto result = any_cast<result_type*      >(arguments.at("result" ));
	auto data   = any_cast<const InputDatum* >(arguments.at("data"   ));
	auto length = any_cast<size_type         >(arguments.at("length" ));

	cuda::kernel::enqueue_launch(
		*this, stream, launch_config,
		result, data, length
	);
}

template <
	unsigned IndexSize, typename ReductionOp, typename InputDatum, typename PretransformOp>
const device_function_t kernel_t<IndexSize, ReductionOp, InputDatum, PretransformOp>::get_device_function() const
{
	return reinterpret_cast<const void*>(cuda::kernels::reduction::reduce::reduce
		<IndexSize, ReductionOp, InputDatum, PretransformOp>);
}

template <unsigned CountSize, typename BitContainer = unsigned int>
using count_bits_set_kernel = kernel_t<CountSize, cuda::functors::plus<uint_t<CountSize>>, BitContainer,
	cuda::functors::population_count<uint_t<CountSize>, BitContainer>>;
template <unsigned IndexSize, typename BitContainer = unsigned>
using dense_minimum_kernel = kernel_t<IndexSize, cuda::functors::minimum<uint_t<IndexSize>>, BitContainer,
	functors::global_index_of_first_set_bit<IndexSize, BitContainer>>;

template <unsigned IndexSize, typename BitContainer = unsigned>
using dense_maximum_kernel = kernel_t<IndexSize, cuda::functors::maximum<uint_t<IndexSize>>, BitContainer,
	functors::global_index_of_last_set_bit<IndexSize, BitContainer>>;

static_block {
	namespace functors = cuda::functors;

	// no pretransform

	//         IndexSize   ReductionOp                InputDatum
	//-----------------------------------------------------------------------
	kernel_t < 4,         functors::plus<int32_t>,   int32_t     >::registerInSubclassFactory();
	kernel_t < 4,         functors::plus<int64_t>,   int32_t     >::registerInSubclassFactory();
	kernel_t < 8,         functors::plus<int64_t>,   int32_t     >::registerInSubclassFactory();
	kernel_t < 4,         functors::plus<float>,     float       >::registerInSubclassFactory();
	kernel_t < 4,         functors::plus<double>,    float       >::registerInSubclassFactory();
	kernel_t < 4,         functors::plus<double>,    double      >::registerInSubclassFactory();

	// unenumerated pretransform

	//         IndexSize  ReductionOp                InputDatum  PretransformOp
	//------------------------------------------------------------------------
	kernel_t < 4,         functors::plus<uint32_t>,  uint32_t,   functors::is_zero<uint32_t> >::registerInSubclassFactory();


	count_bits_set_kernel <4>::registerInSubclassFactory();
	count_bits_set_kernel <8>::registerInSubclassFactory();

	// enumerated pretransform

	//         IndexSize  ReductionOp                InputDatum  PretransformOp
	//------------------------------------------------------------------------
	kernel_t < 4,         functors::plus<uint32_t>,  uint32_t,   functors::enumerated::as_enumerated_unary<functors::plus<uint32_t> > >::registerInSubclassFactory();

	dense_minimum_kernel < 4 >::registerInSubclassFactory();
	dense_minimum_kernel < 8 >::registerInSubclassFactory();
	dense_maximum_kernel < 4 >::registerInSubclassFactory();
	dense_maximum_kernel < 8 >::registerInSubclassFactory();
}
#endif /* __CUDACC__ */

} // namespace reduce
} // namespace reduction
} // namespace kernels
} // namespace cuda
