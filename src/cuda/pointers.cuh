#pragma once
#ifndef CUDA_POINTERS_CUH_
#define CUDA_POINTERS_CUH_

#include <type_traits>

#include <cuda/define_specifiers.hpp>

namespace cuda {

using address_t = uint64_t;
	// So, that should be true both for the host and the device, but I guess it's
	// not impossible someone might want to use this code in some 32-bit-address-space
	// context, I guess... tough luck, in that case you'll need something smarter here.
	// That "smarter thing" could be uint_t<sizeof(void *)>, but I would really rather
	// avoid dragging uint_t into the mix here


//__fhd__ address_t address_as_number (address_t       address) { return address; }
template <typename T>
__fhd__ address_t address_as_number (const T*        address) { return reinterpret_cast<address_t>(address); }
//template <typename T>
//__fhd__ T*        address_as_pointer(const T*        address) { return address; }
template <typename T>
__fhd__ T*        address_as_pointer(address_t address) { return reinterpret_cast<T*>(address); }


// TODO: Code dupication with math.cuh
template <typename T>
__fhd__ constexpr bool is_power_of_2(T val) { return (val & (val-1)) == 0; }

/**
 * @tparam T a type whose size is a power of 2 (and thus has natural alignment)
 * @param A possibly-unaligned pointer
 * @return A pointer to the closest aligned T in memory upto and including @p ptr
 */
template <typename T>
__fhd__ const T* align_down(const T* ptr)
{
	static_assert(is_power_of_2(sizeof(T)),"Invalid type for alignment");
	address_t potential_disalignment_bits = sizeof(T) - 1;
	return (const T*) address_as_pointer<const T*>(address_as_number(ptr) & ~potential_disalignment_bits);
}

template <typename T>
__fhd__ T* align_down(T* ptr)
{
	return const_cast<T*>(align_down<T>(reinterpret_cast<const T*>(ptr)));
}


template <typename T> __fhd__ bool is_aligned(const T* ptr)
{
	static_assert(is_power_of_2(sizeof(T)),"Invalid type for alignment");

	// If we had our math utilities here, we would now do:
	//
	//  return power_of_2_divides(sizeof(T), address_as_number(ptr));
	//
	// but we don't, so here' the implementation of that:
	//
	return address_as_number(ptr) & (sizeof(T) - 1) == 0;
}

} // namespace cuda


#endif /* CUDA_POINTERS_CUH_ */
