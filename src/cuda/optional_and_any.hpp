#ifndef CUDA_OPTIONAL_AND_ANY_HPP_
#define CUDA_OPTIONAL_AND_ANY_HPP_

#include <boost/any.hpp>
#include <util/optional.hpp>

namespace cuda {

const auto nullopt = util::nullopt;
using nullopt_t = util::nullopt_t;

template <typename T>
using optional = util::optional<T>;

using any = boost::any;

template<typename T>
inline T any_cast(const any& operand)
{
    return boost::any_cast<T>(operand);
}

template<typename ValueType>
inline optional<ValueType> any_cast(const optional<any>& operand)
{
    return operand ?
    	optional<ValueType>{boost::any_cast<ValueType>(operand.value())} :
    	optional<ValueType>{};
}

// Note:: util/stl_algorithms.hpp has something similar using the standard library optional
template <typename C, typename E>
inline optional<typename C::mapped_type> maybe_at(const C& container, const E& element) {
	auto find_result = container.find(element);
	return (find_result == container.end()) ?
		optional<typename C::mapped_type>{} :
		optional<typename C::mapped_type>(find_result->second);
}

} // namespace cuda

#endif /* CUDA_OPTIONAL_AND_ANY_HPP_ */
