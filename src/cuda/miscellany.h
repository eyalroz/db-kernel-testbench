#ifndef CUDA_MISCELLANY_H_
#define CUDA_MISCELLANY_H_

#include <cuda/api/types.hpp>

#include <type_traits>

namespace cuda {

/**
 * Use this type when walking index variables collaboratively among
 * multiple threads along some array. The reason you can't just use
 * the original size type is twofold:
 *
 * 1. Occasionally you might have
 *
 *      pos += blockDim.x * blockDim.y * blockDim.z;
 *
 *    e.g. when you work at block stride on a linear input. Well,
 *    if the type of pos is not large enough (e.g. char) - you would
 *    get into infinite loops.
 *
 * 2. The native integer type on a GPU is 32-bit - at least on CUDA;
 *    so there's not much sense of keeping an index variable in some
 *    smaller type. At best, the compiler will switch it to a 32-bit
 *    value; at worst, you'll waste time putting it into and out of
 *    32-bit variables
 *
 * @note You might have issues with essentially-identical unsigned
 * integer types, particularly unsigned int vs long unsigned int.
 */
template <typename Size>
using promoted_size_t = typename std::common_type<Size, native_word_t>::type;

/**
 * This type is intended for kernel parameters and kernel template parameters
 * indicating how many elements of an input or output array a single thread
 * will do computational work for, rather than having each thread do work for
 * a single element. Thus for example when performing elementwise array addition
 * on arrays of length n, with serialization factor 1 we would use n threads,
 * while with serialization factor s we would use ceil(n/s) threads (ignoring
 * rounding up to the nearest multiple of the block size).
 */
using serialization_factor_t = unsigned short;


enum synchronicity_t : bool {
	asynchronous = false,
	synchronous  = true,
	sync         = synchronous,
	async        = asynchronous,
};

enum class endianness_t : bool { big, big_endian = big, little, little_endian = little };

// For the time being, all CUDA-enabled GPUs are little-endian, and this constant
// reflects that fact
static const endianness_t compilation_target_endianness = endianness_t::little;

enum : native_word_t { log_warp_size   = 5 };

} // namespace cuda

#endif /* CUDA_MISCELLANY_H_ */
