#pragma once
#ifndef CUDA_ON_DEVICE_MISCELLANY_CUH_
#define CUDA_ON_DEVICE_MISCELLANY_CUH_

#include <kat/on_device/common.cuh>
#include <type_traits>


__fd__ void* memzero(void* destination, size_t size) { return memset(destination, 0, size); }

template <typename T>
constexpr __device__ __forceinline__
typename std::enable_if<std::is_integral<T>::value, T>::type all_one_bits()
{
	return ~((T)0);
}


#endif /* CUDA_ON_DEVICE_MISCELLANY_CUH_ */
