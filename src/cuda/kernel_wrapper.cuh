#pragma once
#ifndef CUDA_KERNEL_WRAPPER_CUH_
#define CUDA_KERNEL_WRAPPER_CUH_

#include <cuda/api/types.hpp>
#include <cuda/api/device_function.hpp>
#include <cuda/api/device.hpp>
#include <cuda/api/kernel_launch.cuh>
#include <cuda/api/multi_wrapper_impls.hpp>
#include <cuda/optional_and_any.hpp>

#include <string>
#include <tuple>
#include <unordered_map>
#include <exception>

namespace cuda {

struct launch_configuration_limits_t {
	optional<cuda::grid::block_dimension_t> block_size;
	optional<cuda::grid::dimension_t>       grid_size;
	optional<cuda::memory::shared::size_t>   dynamic_shared_memory;

	static launch_configuration_limits_t limit_block_size(optional<cuda::grid::block_dimension_t> limit) {
		return { limit, {}, {} };
	}

	bool none() const
	{
		return !block_size and !grid_size and !dynamic_shared_memory;
	}
};

// Note this class has no data members (just a vtable)

/**
 * This is an abstraction of a kernel. Bringing together its actual device function
 * and the additional code it requires for coming up with the launch configuration.
 *
 * @note I would have liked to say it's a very thin low-level abstraction, but it's actually
 * a bit high-level in the sense of using the multi-type boost::any construct, as well as
 * maps. It's still thin though - There's no state, and incurs (relatively) little overhead;
 * also, the benefit of being somewhat higher-level is that there is no need for the code
 * using a kernel_t* to "know" the executing code, compilation-wise - just pass it the arguments
 *
 * @note the API is _very_ sensitive and flakey, since arguments are checked at run time,
 * not compile time; and casts are impossible, i.e. you have to pass in the exact right
 * type. So expect map::at and boost::any_cast to throw
 *
 * @todo expose the required arguments via a set (or a map from string to typeid)
 */
struct kernel_t {
public:
	/**
	 * Type-erased (or type-hidden) arguments for either the launch config
	 * resolution and the actual kernel enqueue.
	 *
	 * @note May be replaced (or augmented) in the future with something
	 * less costly
	 */
	using arguments_type = std::unordered_map<std::string, boost::any>;

	// Only two methods for subclasses to override - one for the device function
	// (the "kernel itself"), one for the launch config resolution. The override
	// launches on the current device.
	//
	virtual void enqueue_launch(
		cuda::stream_t&                 stream,
		const launch_configuration_t&   launch_config,
		arguments_type                  args) const = 0;

	/**
	 * Uses meta-data other than the actual (large and on-device) inputs
	 * to resolve the parameters for launching the wrapped kernel. These
	 * can then be used with the @ref launch function, or one can launch
	 * on his/her own using @ref get_device_function.
	 *
	 * @todo: I don't like how we pass the attributes here, but
	 * otherwise we would have to get them with every implementation.
	 *
	 * @param device_properties relevant properties of the CUDA device on
	 * which the kernel is to be launched
	 * @param extra_arguments the resolver might take custom arguments
	 * which this, the base class, is not aware of - yet the user may be;
	 * they are therefore passed anonymously and with type 'hiding' via a
	 * map of boost::any's.
	 * @param limits arbitrarily-imposed limits on the lauch configuration
	 * @return a valid launch configuration, in the sense that this kernel
	 * wrapper will not balk on getting it as input. Specifically, a
	 * configuration which requires _no_ launch may also be returned if
	 * the launcher supports that.
	 */
	virtual launch_configuration_t resolve_launch_configuration(
		device::properties_t            device_properties,
		arguments_type                  extra_arguments,
		launch_configuration_limits_t   limits = { {}, {}, {} } ) const
	{
		auto device_function_attributes = get_device_function().attributes();
		return resolve_launch_configuration(
			device_properties, device_function_attributes, extra_arguments, limits);
	}

	/**
	 * @brief a performance-oriented variant in which the wrapper will not
	 * have to trouble the driver to retrieve device function properties again.
	 * Frankly, this is a bit silly, since the arguments_type performance is
	 * currently quite dismal; but if we replaced it with, say, an @ref std::array
	 * or even an @ref std::vector likely to be in cache after just having been
	 * populated, config resolution performance could actually brought down to
	 * something reasonable.
	 *
	 * @param kernel_function_attributes CUDA attribute of the {@code __global__}
	 * function which constitues the actual kernel (i.e. the function each instance
	 * of this class wraps).
	 */

	virtual launch_configuration_t resolve_launch_configuration(
		device::properties_t            device_properties,
		device_function::attributes_t   kernel_function_attributes,
		arguments_type                  extra_arguments,
		launch_configuration_limits_t   limits = { {}, {}, {}} ) const = 0;

	virtual const cuda::device_function_t get_device_function() const = 0;

	virtual ~kernel_t() { }

};

namespace kernel {

// Note that the following overload the enqueue_launch() method that's
// a part of the CUDA API wrappers

template <typename... KernelParameters>
inline void enqueue_launch(
	const kernel_t&                  kernel,
	cuda::stream_t&                  stream,
	const launch_configuration_t&    launch_config,
	KernelParameters&&...            parameters)
{
	cuda::enqueue_launch( //)<kernel_function_type, KernelParameters...>(
		kernel.get_device_function(), stream, launch_config,
		std::forward<KernelParameters>(parameters)...);
}

template <typename... KernelParameters>
inline void enqueue_launch(
	const kernel_t&                  kernel,
	const launch_configuration_t&    launch_config,
	KernelParameters&&...              parameters)
{
	auto default_stream = cuda::device::current::get().default_stream();
	enqueue_launch<KernelParameters...>(
		kernel, default_stream, launch_config,
		std::forward<KernelParameters>(parameters)...);
}

template <typename... KernelParameters>
inline void enqueue_launch(
	const kernel_t&                  kernel,
	cuda::device_t&                  device,
	cuda::stream_t&                  stream,
	const launch_configuration_t&    launch_config,
	KernelParameters&&...              parameters)
{
	device::current::scoped_override_t<false> using_device(device);
	enqueue_launch(kernel, stream, launch_config, std::forward<KernelParameters>(parameters)...);
}

inline launch_configuration_t resolve_launch_configuration(
	kernel_t&                       kernel,
	device::properties_t            device_properties,
	kernel_t::arguments_type        extra_arguments,
	launch_configuration_limits_t   limits = {})
{
	auto device_function = kernel.get_device_function();
	auto attributes = device_function.attributes();
	return kernel.resolve_launch_configuration(
		device_properties,
		attributes,
		extra_arguments,
		limits);
}

inline launch_configuration_t resolve_launch_configuration(
	kernel_t&                       kernel,
	cuda::device_t&                 device,
	kernel_t::arguments_type        extra_arguments,
	launch_configuration_limits_t   limits = {})
{
	auto device_properties = device.properties();
	return resolve_launch_configuration(
		kernel, device_properties, extra_arguments, limits);
}

template <typename... ResolutionParameters>
launch_configuration_t resolve_launch_configuration(
	kernel_t&                       kernel,
	device::properties_t            device_properties,
	device_function::attributes_t   kernel_function_attributes,
	launch_configuration_limits_t   limits = { },
	ResolutionParameters...         extra_params)
{
	return kernel.resolve_launch_configuration(
		device_properties,
		extra_params...,
		limits);
}



} // namespace kernel
} // namespace cuda

#endif /* CUDA_KERNEL_WRAPPER_CUH_ */
