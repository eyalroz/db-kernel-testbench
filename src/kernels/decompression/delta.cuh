#pragma once
#ifndef SRC_KERNELS_DECOMPRESSION_DELTA_CUH
#define SRC_KERNELS_DECOMPRESSION_DELTA_CUH

/*
 * Decompressing DELTA-compressed data is essentially performing an inclusive
 * prefix sum (scan with binary operator +) starting from some baseline :
 * discrete determinate integration being the opposite of discrete derivation.
 * However, we do not want to be in a situation where the reference value is
 * so far away, and the number of elements to scan so high, as to require a
 * two-phase operation, like in a general-case scan. For this reason the
 * decompression is assisted by baseline/anchor values for every every
 * segment. That is, we give it the results of something like the first phase
 * of a prefix sum, for free.
 *
 * The segment length is uniform length over the entire compressed input
 * (except of course for the end of the input), but it is not known at
 * compile time.
 */

#include "kernels/common.cuh"
#include <cuda/functors.hpp>
#include "kernels/reduction/scan.cuh"
#include <kat/on_device/collaboration/grid.cuh>

namespace cuda {
namespace kernels {
namespace decompression {
namespace delta {

using namespace kat::linear_grid::grid_info;

/**
 * The elements in @p compressed_input are differences between the original,
 * uncompressed elements, to be reproduced in @p decompressed. There's also
 * an initial baseline value at @p segment_baseline_values[0] - which is essentially
 * enough to perform the decompression. However, to break the long chains of
 * data dependencies (and to allow for easier chunking, and to avoid the need
 * for two-phased decompression) - the kernel is provided with a baseline
 * value for every consecutive @p compressed_segment_length  (i.e. with the additional
 * partial sums of elements 0...compressed_segment_length -1, 0...2*compressed_segment_length -1
 * etc.)
 *
 * Theoretically we could still want to use a two-phased kernel for large
 * segment lengths, but that's not implemented right now.
 *
 * TODO: Template this kernel on the single segment scan parameter, so that
 * the kernel wrapper can benefit from a power-of-two-length block and from
 * nicely-aligning segment lengths.
 *
 * @param decompressed the decompression results
 * @param compressed_input the differences between consecutive elements of
 * the uncompressed input
 * @param segment_baseline_values a baseline value for every segment,
 * i.e. for every consecutive stretch of @p compressed_segment_length  elements of the
 * input, constituting the sum of all elements before the segment
 * @param length the total length of the input the kernel is to decompress
 * @param compressed_segment_length  the actual, non-delta, baseline value is provided
 * for different points within the data; this is the difference between
 * consecutive such points
 */
template<unsigned IndexSize, typename Uncompressed, typename Difference>
__global__ void decompress(
	Uncompressed*        __restrict__   decompressed,
	const Difference*    __restrict__   compressed_input,
	const Uncompressed*  __restrict__   segment_baseline_values,
	size_type_by_index_size<IndexSize>  length,
	size_type_by_index_size<IndexSize>  compressed_segment_length )
{
	// Note:
	// We have two kinds of "segments" in the context of this kernel:
	// * Compression segments (i.e. every how many compressed elements of
	//   the input do we have an anchoring baseline value), and
	// * (Block) scan segments - what's a good length at which to perform
	//   a prefix sum
	//
	// Let's try not to confuse the two...


	using compressed_type = Difference;
	using uncompressed_type = Uncompressed;
	using index_type = uint_t<IndexSize>;
	using size_type = size_type_by_index_size<IndexSize>;

	auto scratch_area = kat::shared_memory::dynamic::proxy<uncompressed_type>();
	auto compressed_segments_per_block = kat::unsafe::div_rounding_up<size_type>(block::length(), compressed_segment_length );
		// This could very well be 1. Specifically, it's possible that the compressed segment
		// length exceeds the block size.
	auto length_of_full_block_scan = compressed_segment_length  * compressed_segments_per_block;

	for(size_type block_scan_index = block::index();
		block_scan_index * length_of_full_block_scan < length;
		block_scan_index += kat::linear_grid::grid_info::grid::num_blocks())
	{
		struct {
			index_type         start_position;
			size_type          length;
			uncompressed_type  baseline_value;
		} block_scan_params;
		block_scan_params.start_position = block_scan_index * length_of_full_block_scan;
		// Note: The following makes the (trivial) assumption that the segment length
		// is shorter than the maximum possible input length
		block_scan_params.length = kat::builtins::minimum(
			length - block_scan_params.start_position, length_of_full_block_scan);
		block_scan_params.baseline_value = segment_baseline_values[block_scan_index * compressed_segments_per_block];

		reduction::scan::detail::scan_segment
			<IndexSize, functors::plus<uncompressed_type>, compressed_type,
			reduction::scan::inclusivity_t::Inclusive,
			reduction::scan::detail::DontAssumeInitialChunkIsBlockAligned,
			reduction::scan::detail::FinalChunkIsntBlockAligned,
			reduction::scan::detail::BlockLengthIsAPowerOfTwo,
			functors::identity<uncompressed_type>>(
				decompressed,
				scratch_area,
				compressed_input,
				block_scan_params.length,
				block_scan_params.start_position,
				block_scan_params.baseline_value);
	}
}

template<unsigned IndexSize, typename Uncompressed, typename Difference>
class launch_config_resolution_params_t final : public kernels::launch_config_resolution_params_t {
public:
	launch_config_resolution_params_t(
		device::properties_t            device_properties_,
		size_t                          data_length,
		size_t                          compressed_segment_length ,
		optional<memory::shared::size_t>  dynamic_shared_mem_limit = nullopt) :
		kernels::launch_config_resolution_params_t(
			device_properties_,
			device_function_t(decompress<IndexSize, Uncompressed, Difference>)
		)
	{
		auto num_segments = util::div_rounding_up(data_length, compressed_segment_length );

		grid_construction_resolution            = block;
		serialization_option                    = auto_maximized;
		dynamic_shared_memory_requirement.per_block =
			warp_size * sizeof(Uncompressed); // for the "single-segment scan"
		length                                  = std::max<grid::block_dimension_t>(warp_size, num_segments);
		keep_gpu_busy_factor                    = 32; // for now, chosen pretty arbitrarily

		// This is only due to the fact that we have not yet implemented
		// the single-segment-prefix-sum primitive for blocks whose length is
		// not a power of 2; there's nothing inherent to delta decompression or
		// our implementation of it that requires this
		must_have_power_of_2_threads_per_block  = true;
	};
};

} // namespace delta
} // namespace decompression
} // namespace kernels
} // namespace cuda

#endif /* SRC_KERNELS_DECOMPRESSION_DELTA_CUH */
