
#include "kernels/common.cuh"
#include <kat/on_device/collaboration/block.cuh>
#include <cuda/bit_vector.cuh>

namespace cuda {
namespace kernels {
namespace gather_bits {

namespace detail {

// TODO: Currently ignoring this and always reading from main device memory
inline bool should_cache_all_input_data_in_shared_memory(
	size_t    available_shared_memory,
	size_t    input_bit_vector_length,
	size_t    num_bit_indices,
	size_t    size_of_index_into_input,
	size_t    size_of_bit_container_type)
{
	auto total_size_of_input_data = input_bit_vector_length / size_of_bit_container_type;
	return
		available_shared_memory >= total_size_of_input_data &&
		num_bit_indices * size_of_index_into_input > total_size_of_input_data * 4;
			// I just picked a factor here, it doesn't really matter
			// much, since if the first condition holds then the overall
			// time is short anyway
}

namespace warp {

/**
 * @note don't try to gather more than warp_size * warp_size bits at a time - for now
 */
template <unsigned OutputIndexSize, unsigned InputIndexSize>
__device__ void gather_bits(
	standard_bit_container_t*        __restrict__  warp_gathered,
	const standard_bit_container_t*  __restrict__  input,
	const uint_t<InputIndexSize>*    __restrict__  indices,
	size_type_by_index_size<InputIndexSize>        num_input_bits,
	size_type_by_index_size<OutputIndexSize>       num_bits_to_gather)
{
	using input_index_type = uint_t<InputIndexSize>;
	using output_index_type = uint_t<OutputIndexSize>;
	using output_size_type = size_type_by_index_size<OutputIndexSize>;

	const bit_vector<promoted_size_t<input_index_type>> input_bit_vector(
		const_cast<standard_bit_container_t*>(input),
			// The const semantics for the bit_vector class are a bit hazy.
			// See also:
			// http://stackoverflow.com/a/1703896/1593077
			// regarding what we might do here instead of const_cast'ing
			// we might do
		num_input_bits);

	auto predicate = [&input_bit_vector, &indices](output_size_type pos) {
		return input_bit_vector.is_set(indices[pos]);
	};

	return kat::linear_grid::collaborative::warp::compute_predicate_at_warp_stride<
		decltype(predicate), output_size_type
	>(
		warp_gathered,
		predicate,
		num_bits_to_gather);
}

} // namespace warp

} // namespace detail


template <unsigned OutputIndexSize, unsigned InputIndexSize>
__global__ void gather_bits(
	standard_bit_container_t*        __restrict__  gathered,
	const standard_bit_container_t*  __restrict__  input,
	const uint_t<InputIndexSize>*    __restrict__  indices,
	size_type_by_index_size<InputIndexSize>        num_input_bits,
	size_type_by_index_size<OutputIndexSize>       num_bit_indices)
{
	using namespace kat::linear_grid::grid_info;
	using input_index_type = uint_t<InputIndexSize>;
	using output_index_type = uint_t<OutputIndexSize>;
	using output_size_type = size_type_by_index_size<OutputIndexSize>;

	static_assert(size_in_bits<standard_bit_container_t>::value == warp_size,
		"This implementation assumes the unsigned type, having the GPU's basic register's size, "
		"has one bit per element of a warp");

	enum : output_index_type  {
		bit_containers_to_gather_by_each_warp = warp_size,
		bits_to_gather_by_each_warp = warp_size * bit_containers_to_gather_by_each_warp,
	};

	auto first_output_container_index_for_this_warp =
		kat::linear_grid::grid_info::warp::global_id() * bit_containers_to_gather_by_each_warp;
	auto this_warp_s_output_containers = gathered + first_output_container_index_for_this_warp;
	output_index_type first_output_bit_index_for_this_warp =
		first_output_container_index_for_this_warp * size_in_bits<standard_bit_container_t>::value;
	auto this_warp_s_indices = indices + first_output_bit_index_for_this_warp;

	if (not block::is_last_in_grid()) {
		detail::warp::gather_bits<OutputIndexSize, InputIndexSize>(
			this_warp_s_output_containers,
			input,
			this_warp_s_indices,
			num_input_bits,
			bits_to_gather_by_each_warp);
		return;
	}

	if (first_output_bit_index_for_this_warp > num_bit_indices) { return; }

	output_index_type bits_to_gather_by_this_warp = kat::builtins::minimum<output_index_type>(
		bits_to_gather_by_each_warp,
		num_bit_indices - first_output_bit_index_for_this_warp);

	detail::warp::gather_bits<OutputIndexSize, InputIndexSize>(
		this_warp_s_output_containers,
		input,
		this_warp_s_indices,
		num_input_bits,
		bits_to_gather_by_this_warp);
}

template <unsigned OutputIndexSize, unsigned InputIndexSize>
class launch_config_resolution_params_t final : public kernels::launch_config_resolution_params_t {
public:
	launch_config_resolution_params_t(
		device::properties_t            device_properties_,
		size_t                          num_input_bits_,
		size_t                          num_bit_indices_,
		optional<memory::shared::size_t>  dynamic_shared_mem_limit = nullopt) :
		kernels::launch_config_resolution_params_t(
			device_properties_,
			device_function_t(gather_bits<OutputIndexSize, InputIndexSize>),
			dynamic_shared_mem_limit
		)
	{
		num_input_bits                          = num_input_bits_;
		num_bit_indices                         = num_bit_indices_;
		grid_construction_resolution            = warp;
		size_t indices_covered_by_each_warp     = warp_size * warp_size;
		length                                  = util::div_by_power_of_2_rounding_up(
			num_bit_indices, indices_covered_by_each_warp);
		serialization_option                    = none;
		bool cache_input_data_in_shared_memory = false; // change this when caching is supported
//			detail::should_cache_all_input_data_in_shared_memory(
//				available_dynamic_shared_memory_per_block,
//				data_length_,
//				num_indices_, sizeof(Datum), sizeof(InputIndexSize));
		if (cache_input_data_in_shared_memory)
			dynamic_shared_memory_requirement.per_block =
				num_input_bits * sizeof(standard_bit_container_t);
//		dynamic_shared_memory_requirement.per_length_unit = 0;
	};

public:
	size_t                          num_input_bits;
	size_t                          num_bit_indices;
};


} // namespace gather_bits
} // namespace kernels
} // namespace cuda

