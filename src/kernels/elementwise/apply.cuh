#pragma once
#ifndef SRC_KERNELS_ELEMENTWISE_APPLY_CUH_
#define SRC_KERNELS_ELEMENTWISE_APPLY_CUH_

#include "kernels/common.cuh"
#include <functional>

namespace cuda {

/**
 * A workaround for a strange NVCC bug; despite serialization_factor_t from <cuda/api/types.hpp>
 * being visible, I still get a compilation error with the NVCC of CUDA 8.0 RC.
 */
using local_serialization_factor_t = unsigned short;

namespace kernels {
namespace elementwise {
namespace apply {

using namespace kat::linear_grid::grid_info;

/**
 * Yes, this is lame. It should be more general, it should depend on the choice of GPU,
 * it should depend on the unary function used - and maybe the kernel should even be
 * instantiated for multiple factors. Not to mention the fact I have no particularly
 * good reason to think the current value is the right one. It's just a "not so small"
 * power of 2.
 */
enum : local_serialization_factor_t { DefaultSerializationFactor = 16 };


template<
	unsigned               IndexSize,
	typename               Function,
	serialization_factor_t SerializationFactor = DefaultSerializationFactor,
	typename...            Parameters>
__global__ void apply(
	size_type_by_index_size<IndexSize> length,
	typename  std::result_of<Function(Parameters...)>::type *   __restrict__      result,
	Parameters                                         const *  __restrict__  ... inputs)
{
	auto applicator = [&](uint_t<IndexSize> pos) {
		Function f;
		result[pos] = f( inputs[pos]... );
	};
	kat::linear_grid::collaborative::grid::at_block_stride(length, applicator, SerializationFactor);
}

template<
	unsigned               IndexSize,
	typename               Function,
	serialization_factor_t SerializationFactor = DefaultSerializationFactor,
	typename...            Parameters>
class launch_config_resolution_params_t final : public kernels::launch_config_resolution_params_t {
public:
	launch_config_resolution_params_t(
		device::properties_t            device_properties_,
		size_t                          length_) :
		kernels::launch_config_resolution_params_t(
			device_properties_,
			device_function_t(apply<IndexSize, Function, SerializationFactor, Parameters...>)
		)
	{
		grid_construction_resolution            = thread;
		length                                  = length_;
		serialization_option                    = fixed_factor;
		default_serialization_factor            = DefaultSerializationFactor;
	};
};

} // namespace apply
} // namespace elementwise
} // namespace kernels
} // namespace cuda

#endif /* SRC_KERNELS_ELEMENTWISE_APPLY_CUH_ */
