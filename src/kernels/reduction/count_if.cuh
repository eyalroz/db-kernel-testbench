#pragma once
#ifndef SRC_KERNELS_REDUCTION_COUNT_IF_CUIH
#define SRC_KERNELS_REDUCTION_COUNT_IF_CUIH


#include "kernels/common.cuh"
#include "reduce.cuh"

namespace cuda {
namespace kernels {
namespace reduction {
namespace count_if {

/*
 *  TODO: This has some code duplication with the reduce() kernel;
 *  in fact, if we have an apply-then-reduce with a unary function, we
 *  could implement this kernel using that one, with our unary function
 *  being a predicate guaranteed to produce zero or one, and the binary
 *  function being plus.
 */

template<unsigned IndexSize, typename UnaryPredicate, unsigned CountSize>
__global__ void count_if(
	uint_t<CountSize>*                             __restrict__  result,
	const typename UnaryPredicate::argument_type*  __restrict__  data,
	size_type_by_index_size<IndexSize>                           length)
{
	using index_type = uint_t<IndexSize>;
	using element_type = typename UnaryPredicate::argument_type;
	using namespace kat::linear_grid::grid_info;
	using reduction_op = cuda::functors::plus<index_type>;
	using count_type = uint_t<CountSize>;
	using size_type = size_type_by_index_size<IndexSize>;

	// single threads reduce independently

	count_type thread_result = 0;
	kat::linear_grid::collaborative::grid::at_grid_stride(length,
		[&thread_result, data](size_type pos) {
			UnaryPredicate predicate;
			// Cross your fingers and pray that the compiler optimizes this to not have a conditional jmp
			thread_result += predicate(data[pos]) ? 1 : 0;
		}
	);
	__syncthreads();

	using accumulator = typename reduction_op::accumulator;
	auto block_result =
		kat::linear_grid::collaborative::block::reduce(thread_result, accumulator{});

	if (thread::is_first_in_block()) {
		typename reduction_op::accumulator::atomic atomic_accumulation_op;
		atomic_accumulation_op(*result, block_result); // e.g. *result += block_result
	}
}

template<unsigned IndexSize, typename UnaryPredicate, unsigned CountSize>
class launch_config_resolution_params_t final : public kernels::launch_config_resolution_params_t {
public:
	launch_config_resolution_params_t(
		device::properties_t            device_properties_,
		size_t                          length_)
		:
		kernels::launch_config_resolution_params_t(
			device_properties_,
			device_function_t(count_if<IndexSize, UnaryPredicate, CountSize>)
		)
	{
		grid_construction_resolution            = block;
		length                                  = length_;
		serialization_option                    = none;
		quanta.threads_in_block                 = warp_size;
//		block_resolution_constraints.max_threads_per_block
//		                                        = 1;
	};
};


} // namespace count_if
} // namespace reduction
} // namespace kernels
} // namespace cuda

#endif /* SRC_KERNELS_REDUCTION_COUNT_IF_CUIH */
