/**
 * @file Utility code for working with files, tty's and paths.
 * It should really be switched to relying on the standard library's
 * <filesystem> header, but that doesn't go into the standard until
 * C++17 and we're mostly C++11 here
 *
 */
#pragma once
#ifndef UTIL_TERMINAL_HPP_
#define UTIL_TERMINAL_HPP_

#include "optional.hpp"

#include <iostream>

namespace util {

util::optional<unsigned> get_terminal_width(std::ostream& os);
util::optional<unsigned> get_terminal_width();

} /* namespace util */

#endif /* UTIL_TERMINAL_HPP_ */
