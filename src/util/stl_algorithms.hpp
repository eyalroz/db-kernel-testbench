/**
 * Additions over the C++ standard library's <algorithm>,
 * including their adaptations for containers
 */
#pragma once
#ifndef SRC_UTIL_STL_CONTAINERS_HPP_
#define SRC_UTIL_STL_CONTAINERS_HPP_

#include "util/optional.hpp"
#include "util/contains.hpp"

#include <type_traits>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <functional>
#include <unordered_map>

namespace util {

namespace detail {

#ifndef UTIL_STL_CONTAINERS_DETAIL_ALL_SAME_TYPE
#define UTIL_STL_CONTAINERS_DETAIL_ALL_SAME_TYPE
template<typename ... T>
struct all_same_type: std::false_type {};

template<>
struct all_same_type<> : std::true_type {};

template<typename T>
struct all_same_type<T> : std::true_type {};

template<typename T>
struct all_same_type<T, T> : std::true_type {};

template<typename T>
struct all_same_type<T, T, T> : std::true_type {};

template<typename T>
struct all_same_type<T, T, T, T> : std::true_type {};

template<typename T, typename ... Ts>
struct all_same_type<T, T, T, T, T, Ts...> : all_same_type<T, Ts...> {};

#endif /* UTIL_STL_CONTAINERS_DETAIL_ALL_SAME_TYPE */
}

template<typename FwdIterator>
void fill_bits(FwdIterator begin, FwdIterator end, int bit_value)
{
	using value_type = typename std::iterator_traits<FwdIterator>::value_type;
	std::fill(begin, end, bit_value ? ~((value_type) 0) : 0);
}


template<class ForwardIt, class Compare = std::less<typename std::iterator_traits<ForwardIt>::value_type>>
std::pair<ForwardIt, ForwardIt> extremal_elements(
	ForwardIt first, ForwardIt last, Compare cmp = Compare())
{
    if (first == last) {
        return { last, last };
    }
    ForwardIt smallest = first;
    ForwardIt largest = first;
    auto it = first;
    ++it;
    for (; it != last; ++it) {
        if (cmp(*largest, *it)) {
            largest = it;
        }
        else if (cmp(*smallest, *it)) {
            smallest = it;
        }
    }
    return {smallest, largest};
}

// TODO: Do we really need this? I don't think so
template<class ForwardIt, class Compare = std::less<typename std::iterator_traits<ForwardIt>::value_type>>
std::pair<typename std::iterator_traits<ForwardIt>::value_type, typename std::iterator_traits<ForwardIt>::value_type>
extremal_values(ForwardIt first, ForwardIt last, Compare cmp = Compare())
{
    if (first == last) {
        return { *last, *last };
    }
    auto smallest = *first;
    auto largest = *first;
    for (auto it = first + 1; it != last; ++it) {
    	auto value = *it;
        if (cmp(largest, value)) {
            largest = value;
        }
        else if (cmp(value, smallest)) {
            smallest = value;
        }
    }
    return {smallest, largest};
}


template<typename FwdOutputIterator, typename FwdIndicesIterator, typename RAIterator>
inline void lookup(
	FwdIndicesIterator  indices_start,
	FwdIndicesIterator  indices_end,
	FwdOutputIterator   output_start,
	RAIterator          input)
{
	auto output_it = output_start;
	auto indices_it = indices_start;
	for(; indices_it < indices_end; output_it++, indices_it++) {
		*output_it = input[*indices_it];
	}
}

/**
 * Finds the first element not equal to @p value in @p container.
 *
 * @param container the haystack to search
 * @param value the "trailing filler" value we are not interested in
 * @return an iterator to the occurrence of the non-{@p value} element
 */
template<typename InputIterator>
InputIterator find_first_unequal(InputIterator begin, InputIterator end,
	const typename std::iterator_traits<InputIterator>::value_type& value)
{
	auto equals_value =
		[&value](const typename std::iterator_traits<InputIterator>::value_type& x) { return x == value; };
	return std::find_if_not(begin, end, equals_value);
}

template <typename InputIterator>
InputIterator get_past(
	InputIterator iterator,
	InputIterator end,
	typename std::iterator_traits<InputIterator>::value_type value)
{
	return find_first_unequal(iterator, end, value);
}

template <typename InputIterator>
InputIterator find_next_value(InputIterator iterator, InputIterator end)
{
	return get_past(iterator, end, *iterator);
}

template<typename IndicesContainer, typename InputContainer, typename FwdOutputIterator>
inline void lookup(const IndicesContainer& indices, InputContainer input, FwdOutputIterator output)
{
	return lookup(std::begin(indices), std::end(indices), std::begin(output), std::begin(input));
}

template<
	typename RAIterator,
	typename Size = size_t,
	typename Compare = std::less<typename std::iterator_traits<RAIterator>::value_type>
>
inline std::vector<Size> sorted_indices(RAIterator first, RAIterator last, Compare compare = Compare())
{
	std::vector<Size> indices(last - first);
	std::iota(indices.begin(), indices.end(), (Size) 0);
	auto lookup_and_compare = [&first, compare](Size lhs, Size rhs) {
		return compare(first[lhs], first[rhs]);
	};
	std::sort(indices.begin(), indices.end(), lookup_and_compare);
	return indices;
}

template<
	typename RAIterator,
	typename Size = size_t,
	typename Compare = std::less<typename std::iterator_traits<RAIterator>::value_type>
>
inline std::vector<Size> partial_sorted_indices(
	RAIterator  first,
	RAIterator  last,
	Size        num_sorted,
	Compare     compare = Compare())
{
	std::vector<Size> indices(last - first);
	std::iota(indices.begin(), indices.end(), (Size) 0);
	auto lookup_and_compare = [&first, compare](Size lhs, Size rhs) {
		return compare(first[lhs], first[rhs]);
	};
	std::partial_sort(indices.begin(), indices.begin() + num_sorted, indices.end(), lookup_and_compare);
	indices.resize(num_sorted);
	return indices;
}

template<
	typename Container,
	typename Size = typename Container::size_type,
	typename Compare = std::less<typename Container::value_type>
>
inline std::vector<Size> partial_sorted_indices(
	const Container&  container,
	Size              num_sorted,
	Compare           compare = Compare())
{
	return partial_sorted_indices<typename Container::const_iterator, Size, Compare>(
		std::begin(container), std::end(container), num_sorted, compare);
}

template<typename Container, typename AppendageContainer>
inline void append(Container& recipient, const AppendageContainer& appendage)
{
	recipient.insert(std::end(recipient), std::begin(appendage), std::end(appendage));
}

template <typename Container, typename AppendageContainer>
inline Container& add_all(Container& c1, const AppendageContainer& c2)
{
       c1.insert(std::begin(c2), std::end(c2));
       return c1;
}


template<typename T1, typename T2, typename T3>
using triple = std::tuple<T1, T2, T3>;


// TODO: Try to get rid of this, the general version _should_ be enough
// (but I'm having trouble using it with Containers being a single initializer list)
template <typename Container>
inline Container get_union(const Container& c1, const Container& c2)
{
	Container result(c1);
	add_all(result, c2);
	return result;
}

template <typename Container, typename... Containers>
inline
typename std::enable_if<detail::all_same_type<Containers...>::value, Container>::type
get_union(const Container& c1, const Containers&... more_containers)
{
	static_assert(sizeof...(Containers) > 0, "Attempt to apply a union to a single container");
	Container result(c1);
	auto f = [&result](const Container& c) { add_all(result, c); };
	[](...){}(( f(more_containers), 0)...);
	return result;
}


template <typename Container, typename OtherContainer = Container>
inline Container get_intersection(const Container& c1, const OtherContainer& c2)
{
	Container result;
	auto result_inserter = std::inserter(result, result.end());
	// hopefully_back_inserter(result) doesn't work for std::unordered_set! aargh!
	std::copy_if(
		c1.cbegin(), c1.cend(),
		result_inserter,
		[&c2](const typename Container::value_type& c1_element) {
			return util::contains(c2, c1_element);
		});
	return result;
}


// Slow?
template <typename C, typename P>
inline C filter_out(C const & container, P pred) {
  C filtered;
  for (const auto& element : container) {
	  if (!pred(element)) { filtered.insert(element); }
  }
  return filtered;
}

template <typename C, typename P>
inline C keep_only(C const & container, P pred) {
  C filtered;
  for (const auto& element : container) {
	  if (pred(element)) { filtered.insert(element); }
  }
  return filtered;
}

/**
 * Usage: if (is_in(ny_num, { 1, 4, 9, 16 })) { cout << "small square"; }
 *
 * Based on: http://stackoverflow.com/a/15181949/1593077
 */
template <typename T>
static bool is_in(const T& value, const std::initializer_list<T>& search_set)
{
    for (const auto& e : search_set) {
        if (value == e) { return true; }
    }
    return false;
}

template <typename Container>
std::unordered_map<typename std::remove_cv<typename Container::value_type>::type, size_t>
count_multiplicities(const Container& container)
{
	std::unordered_map<typename std::remove_cv<typename Container::value_type>::type, size_t> counts_;

	for(const auto& e : container) {
		auto it = counts_.find(e);
		if (it != counts_.end()) {
			auto& ocurrence_count = it->second;
			ocurrence_count++;
		} else {
			counts_.insert( { e, 1 });
		}

	}
	return counts_;
}

template<typename Destination, typename Source>
Destination container_cast(Source&& source)
{
    return Destination(source.begin(), source.end(), source.get_allocator());
}

template<typename Destination, typename Source>
Destination container_cast(const Source& source)
{
    return Destination(source.begin(), source.end(), source.get_allocator());
}

template <typename C, typename E>
inline util::optional<typename C::mapped_type> maybe_at(const C& container, const E& element) {
	auto find_result = container.find(element);
	return (find_result == std::end(container)) ?
		util::nullopt :
		util::optional<typename C::mapped_type>(find_result->second);
}

template <typename C, typename... ElementTypes>
inline bool contains_all(const C& container, const ElementTypes&... elements) {
	auto elements_container = { elements... };
	auto i = util::get_intersection(container, elements_container);
	return (i == elements_container);
}

template <typename C, typename... ElementTypes>
inline bool contains_any(const C& container, const ElementTypes&... elements) {
	auto i = util::get_intersection(container, { elements... });
	return (!i.empty());
}

template<typename Container, typename Function>
inline Function indexed_for_each(Container& container, Function f)
{
	typename Container::size_type i = 0;
	for(auto it = std::begin(container); it < std::end(container); it++, i++) {
		f(i, *it);
	}
}



// -------------------------------------------------------------
// Adapters for earlier functions in this file, for containers
// rather than pairs of iterators
// -------------------------------------------------------------

template<typename Container>
inline std::pair<typename Container::iterator , typename Container::iterato> extremal_elements(const Container& container)
{
	return extremal_elements(container.cbegin(), container.cend());
}

// TODO: Do we really need this?
template<class Container>
std::pair<typename Container::value_type, typename Container::value_type> extremal_values(const Container& container)
{
	return extremal_values(container.cbegin(), container.cend());
}

template<
	typename Container,
	typename Size = typename Container::size_type,
	typename Compare = std::less<typename Container::value_type>
>
inline std::vector<Size> sorted_indices(const Container& container, Compare compare = Compare())
{
	return sorted_indices<typename Container::const_iterator, Size, Compare>(
		std::begin(container), std::end(container), compare);
}

template<
	typename RAIterator,
	typename Size = size_t,
	typename Compare = std::less<typename std::iterator_traits<RAIterator>::value_type>
>
inline std::vector<Size> partial_sorted_indices(
	RAIterator  first,
	RAIterator  middle,
	RAIterator  last,
	Compare     compare = Compare())
{
	return partial_sorted_indices(first, last, middle-first, compare);
}

/**
 * Convert  (if possible) a reverse iterator into the original-type iterator
 *
 * @note sourced from http://stackoverflow.com/a/4408182/1593077
 *
 * @param rit a reverse iterator
 * @return a forward iterator of the appropriate type, pointing to the same
 * element as @p rit (or the end iterator if @p rit is rbegin()) - if @p rit
 * is not rend(); otherwise result is undefined
 */

template <class ReverseIterator>
typename ReverseIterator::iterator_type unreverse_iterator(ReverseIterator rit)
{
    return --(rit.base()); // move result of .base() back by one.
    // alternatively
    // return (++rit).base() ;
    // or
    // return (rit+1).base().
}

/**
 * Finds the last element not equal to @p value in @p container.
 *
 * @param container the haystack to search
 * @param value the "trailing filler" value we are not interested in
 * @return an iterator to the occurrence of the non-{@p value} element;
 * if no such element exists, the result is undefined
 */
template<typename Container>
typename Container::iterator find_last_unequal(
	Container&                             container,
	const typename Container::value_type&  value)
{
	return unreverse_iterator(
		find_first_unequal(container.rbegin(), container.rend(), value));
}

/**
 * Finds the first element not equal to @p value in @p container.
 *
 * @param container the haystack to search
 * @param value the "trailing filler" value we are not interested in
 * @return an iterator to the occurrence of the non-{@p value} element
 */
template<typename Container>
typename Container::iterator find_first_unequal(
	Container&                             container,
	const typename Container::value_type&  value)
{
	return find_first_unequal(std::begin(container), std::end(container), value);
}

template<typename Container>
inline void fill_bits(Container& container, int bit_value)
{
	util::fill_bits(std::begin(container), std::end(container), bit_value);
}




} // namespace util


#endif /* SRC_UTIL_STL_CONTAINERS_HPP_ */
