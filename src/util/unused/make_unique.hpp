/**
 * A make_unique implementation, for C++11 rather than C++14 compilers
 * (e.g. CUDA nvcc 7.5 ...), lifted from here:
 * http://stackoverflow.com/a/17902439/1593077
 * and originally by Stephan T. Lavavej
 */
#ifndef SRC_UTIL_MAKE_UNIQUE_HPP_
#define SRC_UTIL_MAKE_UNIQUE_HPP_

#if __cplusplus == 201103L

#include <cstddef>
#include <memory>
#include <type_traits>
#include <utility>

namespace std {
    template<class T> struct _Unique_if {
        typedef unique_ptr<T> _Single_object;
    };

    template<class T> struct _Unique_if<T[]> {
        typedef unique_ptr<T[]> _Unknown_bound;
    };

    template<class T, size_t N> struct _Unique_if<T[N]> {
        typedef void _Known_bound;
    };

    template<class T, class... Args>
        typename _Unique_if<T>::_Single_object
        make_unique(Args&&... args) {
            return unique_ptr<T>(new T(std::forward<Args>(args)...));
        }

    template<class T>
        typename _Unique_if<T>::_Unknown_bound
        make_unique(size_t n) {
            typedef typename remove_extent<T>::type U;
            return unique_ptr<T>(new U[n]());
        }

    template<class T, class... Args>
        typename _Unique_if<T>::_Known_bound
        make_unique(Args&&...) = delete;
}

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
#endif /* __cplusplus == 201103L */

#endif /* SRC_UTIL_MAKE_UNIQUE_HPP_ */
