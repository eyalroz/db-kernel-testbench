/*
 * Include this file if you want to use std::unordered_map or
 * std::unordered_set with the set element/map keys being
 * C-style char* strings rather than std::string's.
 *
 * Using char*'s as map keys is not automatically and trivially possible,
 * as the comparison operator my_charp == another_charp will not compare
 * the contents, but rather the pointers themselves. If you're sure your
 * strings have been optimized so as to avoid dulicates, this is fine,
 * and you can even hash the pointer as an integral value; but usually
 * that's not the case, which is why you need:
 *
 *   1. An appropriate hasher, hashing the char elements of the string
 *      until the nul-termination.
 *   2. A comparator of the pointer-to strings rather than the pointer.
 *
 * These are provided here. Usage:
 *
 * [WRITEME; see earlier versions of poor_mans_reflection.h]
 *
 */
#ifndef SRC_UTIL_UNUSED_CSTING_SETS_AND_MAPS_HPP_
#define SRC_UTIL_UNUSED_CSTING_SETS_AND_MAPS_HPP_

#include <cstring>
#include <unordered_map>

namespace util {
namespace detail {

// The definitions in this detail section allow for using C-style strings (char* strings) as map keys.

class cstring_equal_to: public std::binary_function<const char*, const char*, bool> {
public:
	bool operator()(const char* lhs, const char* rhs) const
	{
        return std::strcmp(lhs, rhs) == 0;
	}
};

// This next line may not be very portable... it accesses the C++ library's internal
// function for actually hashing a sequence of bytes - which is not part of the standard,
// nor the technical specifications etc. It works with GCC.
#include <bits/hash_bytes.h>

class cstring_hasher
{
public:
	using result_type = typename std::hash<std::string>::result_type;
	using argument_type = const char*;

	result_type operator()(argument_type const& arg) const
	{
		// The following is what std::hash<std::string>() uses - but I don't
		// know how to acces it in properly - this is fugly.
		size_t seed = static_cast<size_t>(0xc70f6907UL);
		return std::_Hash_bytes(arg, strlen(arg), seed);
	}
};

} // namespace detail
} // namespace util

#endif /* SRC_UTIL_UNUSED_CSTING_SETS_AND_MAPS_HPP_ */
