#pragma once
#ifndef _TEMPLATE_METAPROGRAMMING_H
#define _TEMPLATE_METAPROGRAMMING_H

#include <type_traits>

namespace util {

/*
 * Some meta-programming magic...
 * ------------------------------------------------------
 *
 * (see http://loungecpp.net/cpp/enable-if/ )
 *
 * TODO:
 * - Use conjunction functionality in the standard library extensions v2 (pretty new as of 2015...)
 * - Use std::conditional_t instead of Conditional

 */

/**
 * Opposite of std::enable_if ; substitution of disable_if will fail when the condition holds.
 */
template <bool B, typename T = void>
struct disable_if { typedef T type; };

template <typename T>
struct disable_if<true,T> { };

/* 
 * Invoke() is what it says - it invokes a function - a TPF - and you end up with the "return value" of a TPF,
 * which is the ::type field of the templated type. But this semantic is bit flakey, since we also have ::value...
 */
template <typename T>
using Invoke = typename T::type;

// Invoked versions of std::conditional (same as std::conditional_t
template <typename If, typename Then, typename Else>
using Conditional = Invoke<std::conditional<If::value, Then, Else>>;

// All::value is true if the conjuction of the conditions in the parameter pack is true, and false otherwise
template <typename... T>
struct All : std::true_type {};
template <typename Head, typename... Tail>
struct All<Head, Tail...> : Conditional<Head, All<Tail...>, std::false_type> {};

// Any is the same as All, except with a disjunction rather than a conjunction (specifically, it is
// false for the empty pack)
template <typename... T>
struct Any : std::false_type {};
template <typename Head, typename... Tail>
struct All<Head, Tail...> : Conditional<Head, std::true_type, Any<Tail...> > {};

namespace detail {

enum class enabled {}; // no value necessary - this is just a marker
enum class disabled {}; // no value necessary - this is just a marker

} // namespace detail

// Invoked versions of std::enable_if (for clearer syntax when using it)
template <typename... Condition>
using EnableIf = Invoke<std::enable_if<All<Condition...>::value, detail::enabled>>;
  // if the condition holds, the EnableIf will be the 'enabled' type; otherwise EnableIf can't be substituted

// Invoked versions of disable_if (for clearer syntax when using it)
// TODO: can I use Invoke here on All?
template <typename... Condition>
using DisableIf = Invoke<disable_if<All<Condition...>::value, detail::disabled>>;
  // if the condition doesn't hold, the DisabledIf will be the 'disabled' type; otherwise EnableIf can't be substituted


/*
 * So, how do you use these magical constructs? You add one of them as
 * template parameter pack to your template statement, like so (with the elipsis!):
 *
 *   template <typename T, EnableIf<std::is_integral<T>>...>
 *   void foo(const T& t) { std::cout << t << " is of an integral type!\n"; }
 *
 * and substitution will fail if the condition doesn't hold, i.e. the compiler will
 * only find foo() for integral types. Note the fact that EnableIf is a parameter
 * pack is necessary to avoid the need for some default value (i.e.
 * EnableIf<whatever> = dummy_default ), since it defaults to the empty pack.
 */

} // namespace util

#endif /* _TEMPLATE_METAPROGRAMMING_H */
