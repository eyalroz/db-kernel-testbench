#pragma once
#ifndef SRC_UTIL_HASH_HPP_
#define SRC_UTIL_HASH_HPP_

#include <cstdint>
#include "util/builtins.hpp"

namespace util {
namespace hash {

/**
 * XOR-Shift hashing function family; see description and code at:
 * https://en.wikipedia.org/wiki/Xorshift
 */
template<unsigned StateSize> uint32_t xorshift(uint32_t state[StateSize]);
template<> uint32_t xorshift<1>(uint32_t state[1])
{
	uint32_t x = state[0];
	x ^= x << 13;
	x ^= x >> 7;
	x ^= x << 17;
	return x;
};
template<> uint32_t xorshift<4>(const uint32_t& state[4])
{
	uint32_t t = state[3];
	t ^= t << 11;
	t ^= t >> 8;
	state[3] = state[2]; state[2] = state[1]; state[1] = state[0];
	t ^= state[0];
	t ^= state[0] >> 19;
	state[0] = t;
	return t;
};
uint32_t xorshift(uint32_t x) { return xorshift<1>({ x }); }

uint32_t knuth(uint32_t v) { return v * 2654435761u; }

template<typename T, uint32_t seed = 1337>
T crc32(const T& x) { return util::builtins::crc32_castagnioli(seed, x); }
	// or should it be (x, seed) ? I wonder

template <typename T> T identity(T v) { return v; }

/*
 * Additional hash function families to implement, or
 * wrap-the-implementation-of:
 *
 * xxHash                 https://github.com/Cyan4973/xxHash
 * MurmurHash             https://github.com/aappleby/smhasher
 * SBox
 * Lookup3
 * CityHash64             https://github.com/google/cityhash
 * Fowler-Noll-Vo (FNV)   http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-source
 * MD5-32
 * SHA1/2/3               https://github.com/vog/sha1 for example
 *
 */

} // namespace hash
} // namespace util

#endif /* SRC_UTIL_HASH_HPP_ */
