#include <memory>

template <typename T>
class placement_memory_allocator: public std::allocator<T>
{
	void* pre_allocated_memory;

public:
	using size_type = size_t;
	using pointer =  T*;
	using const_pointer = const T* ;

	template<typename _Tp1>
	struct rebind
	{
		using other =  placement_memory_allocator<_Tp1>;
	};

	pointer allocate(size_type n, const void *hint=0)
	{
		char* p = new(pre_allocated_memory)char[n * sizeof(T)];
		return (T*)p;
	}

	void deallocate(pointer p, size_type n)
	{
		//delete p;
	}

	placement_memory_allocator(void* p = 0) throw(): std::allocator<T>(), pre_allocated_memory(p) { }
	placement_memory_allocator(const placement_memory_allocator &a) throw(): std::allocator<T>(a) 
	{
		pre_allocated_memory = a.pre_allocated_memory; 
	}
	~placement_memory_allocator() throw() { }
};

/*
	// usage:

	// create allocator object, intialized with DirectX memory ptr.
	placement_memory_allocator<MyClass> pl(DIRECT_X_MEMORY_PTR);
	//Create vector object, which use the created allocator object.
	vector<MyClass, placement_memory_allocator<MyClass>> v(pl);
	// !important! reserve all the memory of directx buffer.
	// try to comment this line and rerun to see the difference
	v.reserve( DIRECT_X_MEMORY_SIZE_IN_BYTES / sizeof(MyClass));

*/
