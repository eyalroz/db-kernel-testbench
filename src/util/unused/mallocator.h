/*
 * mallocator
 *
 * Stephen J. Lavavej's simple toy malloc-based allocator for STL containers,
 * in the new-and-revamped C++11ish version.
 *
 * Code lifted from: http://stackoverflow.com/a/36521845/1593077
 * And based on STL's talk at CppCon 2014, available here:
 * https://www.youtube.com/watch?v=dTeKf5Oek2c
 *
 */
#ifndef UTIL_MALLOCATOR_H_
#define UTIL_MALLOCATOR_H_

#include <stdlib.h> // size_t, malloc, free
#include <new> // bad_alloc, bad_array_new_length

namespace util {

template<class T> struct mallocator {
	typedef T value_type;
	mallocator() noexcept { } // default ctor not required
	template<class U> mallocator(const mallocator<U>&) noexcept	{ }
	template<class U> bool operator==(const mallocator<U>&) const noexcept { return true; }
	template<class U> bool operator!=(const mallocator<U>&) const noexcept { return false; }

	T* allocate(const size_t n) const
	{
		if (n == 0) { return nullptr; }
		if (n > static_cast<size_t>(-1) / sizeof(T)) { throw std::bad_array_new_length(); }
		void * const pv = malloc(n * sizeof(T));
		if (!pv) { throw std::bad_alloc(); }
		return static_cast<T *>(pv);
	}
	void deallocate(T * const p, size_t) const noexcept { free(p); }
};

} // namespace util

#endif /* UTIL_MALLOCATOR_H_ */
