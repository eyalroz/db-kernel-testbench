#pragma once
#ifndef UTIL_ZIP_ITERATOR_H_
#define UTIL_ZIP_ITERATOR_H_

/* 
 * A super-simple variadic lambda-based iterator, based on:
 * http://stackoverflow.com/a/18771618/1593077
 *
 * Example use:
 *
 *  int main () {
 *     std::vector<int> v1{1,2,3};
 *     std::vector<int> v2{3,2,1};
 *     std::vector<float> v3{1.2,2.4,9.0};
 *     std::vector<float> v4{1.2,2.4,9.0};
 *      zip ([](int i,int j,float k,float l) {
 *              std::cout << i << " " << j << " " << k << " " << l << '\n';
 *           },
 *           v1.begin(),v1.end(),v2.begin(),v3.begin(),v4.begin());
 *  }
 *
 */


// TODO: Instead of linear recursion in template definition here,
// just expand the pack all at once! Like in what's-his-face's lecture
// at Meeting C++ 2015 about porting libraries to C++14

template <typename Iterator>
void advance_all (Iterator & iterator) { ++iterator; }

template <typename Iterator, typename ... Iterators>
void advance_all (Iterator & iterator, Iterators& ... iterators) {
	++iterator; 
	advance_all(iterators...); 
} 

template <typename Function, typename Iterator, typename ... Iterators>
Function zip_for_each (Function func, Iterator begin, Iterator end, Iterators ... iterators)
{
	for(;begin != end; ++begin, advance_all(iterators...))
	    func(*begin, *(iterators)... );
	//could also make this a tuple
	return func;
}

#endif /*  UTIL_ZIP_ITERATOR_H_ */
