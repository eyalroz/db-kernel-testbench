#pragma once
#ifndef UTIL_NIL_H_
#define UTIL_NIL_H_

namespace util {
struct nil_t { };
constexpr nil_t nil;

template <typename T>
bool operator==(const boost::optional<T>& lhs, nil_t rhs) { return !lhs; }

template <typename T>
bool operator==(nil_t lhs, const boost::optional<T>& rhs) { return !rhs; }

template <typename T>
bool operator==(const T* t, nil_t ) { return t == nullptr; }

template <typename T>
bool operator==(nil_t, const T* t ) { return t == nullptr; }

}

#endif /* UTIL_NIL_H_ */
