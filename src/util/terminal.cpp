#include "terminal.hpp"
#include "string.hpp"

#include <ext/stdio_filebuf.h>
#include <cstdio>
#ifdef WIN32
#include <windows.h>
#else
#include <sys/ioctl.h>
#include <sys/stat.h>
#endif
#include <climits>
#include <unistd.h>
//#include <sstream>
//#include <fstream>
#include <stdexcept>
#ifdef DEBUG
#include <cstring>
#include <iostream>
#endif

namespace util {
template <typename T>
using optional = util::optional<T>;
using util::nullopt;

namespace detail {

/* Code due to this StackOverflow answer:
 * http://stackoverflow.com/a/19749019/1593077
 ****************************************************
 */
typedef std::basic_ofstream<char>::__filebuf_type buffer_t;
typedef __gnu_cxx::stdio_filebuf<char>            io_buffer_t;
static FILE* cfile_impl(buffer_t* fb)
{
	return (static_cast<io_buffer_t*>(fb))->file(); //type std::__c_file
}

static FILE* cfile(std::ofstream const& ofs) {return cfile_impl(ofs.rdbuf());}
static FILE* cfile(std::ifstream const& ifs) {return cfile_impl(ifs.rdbuf());}

static FILE* cfile(std::ostream const& os)
{
	if(std::ofstream const* ofsP = dynamic_cast<std::ofstream const*>(&os)) return cfile(*ofsP);
	if(&os == &std::cerr) return stderr;
	if(&os == &std::cout) return stdout;
	if(&os == &std::clog) return stderr;
	if(dynamic_cast<std::ostringstream const*>(&os) != 0){
	   throw std::runtime_error("don't know cannot extract FILE pointer from std::ostringstream");
	}
	return nullptr; // stream not recognized
}

static FILE* cfile(std::istream const& is)
{
	if(std::ifstream const* ifsP = dynamic_cast<std::ifstream const*>(&is)) return cfile(*ifsP);
	if(&is == &std::cin) return stdin;
	if(dynamic_cast<std::ostringstream const*>(&is) != 0){
		throw std::runtime_error("don't know how to extract FILE pointer from std::istringstream");
	}
	return nullptr; // stream not recognized
}

} // namespace detail

/*
 ****************************************************
 */

namespace detail {

#ifdef WIN32
std::string windows_error_description(DWORD error_code)
{
    if(errorMessageID == 0) {
        return {}; //No error message has been recorded
    }

    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    std::string message(messageBuffer, size);

    //Free the buffer.
    LocalFree(messageBuffer);

    return message;
}
#endif

// TODO: Avoid having to ioctl every time; either
// run this just once by the app, or perhaps
// allow static caching here of the results? Hmm.
optional<unsigned> get_terminal_width(int file_descriptor)
{
#if WIN32
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    int columns, rows;

    auto succeeded = GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    if (not succeeded) {
#ifdef DEBUG
		std::cout << std::flush;
		std::cerr << "GetConsoleScreenBufferInfo() for GetStdHandle(STD_OUTPUT_HANDLE) - "
			<< " failed. Reported error: " << windows_error_description(GetLastError()) << '\n' << std::flush;
#endif
		return optional<unsigned>();
    }
    return csbi.srWindow.Right - csbi.srWindow.Left + 1;
#else
	struct winsize w;
	int result = ioctl(file_descriptor, TIOCGWINSZ, &w);
	if (result != 0) {
#ifdef DEBUG
		std::cout << std::flush;
		std::cerr << "ioctl TIOCGWINSZ for file descriptor " << file_descriptor
			<< " failed. Reported error: " << std::strerror(errno) << '\n' << std::flush;
#endif
		return optional<unsigned>();
	}
	return w.ws_col;
#endif
}

optional<unsigned> get_terminal_width(FILE* file)
	{
	auto fd = fileno(file);
	if (!isatty(fd)) { return nullopt; }
	return get_terminal_width(fd);
}

} // namespace detail

optional<unsigned> get_terminal_width(std::ostream& os) {
	try {
		return detail::get_terminal_width(detail::cfile(os));
	}
	catch(std::exception& e) {
		return optional<unsigned>();
	}
}

optional<unsigned> get_terminal_width()
{
	using namespace detail;
	using std::exception;
	try { return get_terminal_width(cfile(std::cout)); } catch(exception& e) { };
	try { return get_terminal_width(cfile(std::cerr)); } catch(exception& e) { };
	try { return get_terminal_width(cfile(std::cin));  } catch(exception& e) { };
	// We could try some system calls here, or opening /dev/tty -
	// but that would be quite unportable, I think, so let's
	// just give up.
	return nullopt;
}

} /* namespace util */
