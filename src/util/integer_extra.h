#pragma once
#ifndef UTIL_INTEGER_EXTRA_H_
#define UTIL_INTEGER_EXTRA_H_

#include "integer.h"

namespace util {

namespace detail {

/**
 * (UNTESTED!) A hopefully-fast integer-like class with arbitrary size
 *
 *
 * @note Heavily dependent on compiler optimizations...
 * @note For now, assumes little-endianness
 * @note For now, limited to small sizes
 *
 */
template <unsigned NBytes, bool Signed>
class int_t final
{
	static_assert(NBytes <= sizeof(unsigned long long), "larger sizes not supported, for now");

public:
	using traits = integer_traits_t<NBytes, Signed>;
	using fast_builtin_type = typename traits::fast_builtin_type;
	using least_builtin_type = typename traits::least_builtin_type;
	using byte = unsigned char; // That should really happen elsewhere...

protected: // data members
	typename traits::value_type value; // Note it is _not_ necessarily aligned

public: // constructors
	int_t() noexcept = default;
	int_t(const int_t& x) noexcept = default;
	int_t(int_t&& x) noexcept = default;

protected: // building blocks for converting ctors, assignments and conversion operators

	static constexpr size_t min(size_t x, size_t y) { return x < y ? x : y; }

	// TODO: This doesn't have sign extension!
	template <typename I>
	int_t& assign(I x) noexcept
	{
		auto x_bytes = (const byte*) &x;

		for (auto j = 0; j < min(sizeof(I), NBytes); j++) {
			value[j] = x_bytes[j];
		}
		for (auto j = min(sizeof(I), NBytes); j < NBytes; j++) {
			value[j] = 0;
		}
		return *this;
	}

	// TODO: This doesn't have sign extension!
	template <typename I>
	I as_integer() const noexcept
	{
		I result;

		if (sizeof(I) > NBytes) { result = 0; }

		auto result_bytes = (byte*) &result;
		for (auto j = 0; j < min(sizeof(I), NBytes); j++) {
			result_bytes[j] = value[j];
		}
		return result;
	}

public: // converting constructors
	int_t(char                x) noexcept { assign<char               >(x); }
	int_t(signed char         x) noexcept { assign<signed char        >(x); }
	int_t(unsigned char       x) noexcept { assign<unsigned char      >(x); }
	int_t(short               x) noexcept { assign<short              >(x); }
	int_t(unsigned short      x) noexcept { assign<unsigned short     >(x); }
	int_t(int                 x) noexcept { assign<int                >(x); }
	int_t(unsigned            x) noexcept { assign<unsigned           >(x); }
	int_t(long                x) noexcept { assign<long               >(x); }
	int_t(unsigned long       x) noexcept { assign<unsigned long      >(x); }
	int_t(long long           x) noexcept { assign<long long          >(x); }
	int_t(unsigned long long  x) noexcept { assign<unsigned long long >(x); }
	~int_t() = default;

public: // operators
	int_t& operator = (const int_t& other) noexcept = default;
	int_t& operator = (int_t&& other) noexcept = default;

	int_t& operator = (char                x) noexcept { return assign<char               >(x); }
	int_t& operator = (signed char         x) noexcept { return assign<signed char        >(x); }
	int_t& operator = (unsigned char       x) noexcept { return assign<unsigned char      >(x); }
	int_t& operator = (short               x) noexcept { return assign<short              >(x); }
	int_t& operator = (unsigned short      x) noexcept { return assign<unsigned short     >(x); }
	int_t& operator = (int                 x) noexcept { return assign<int                >(x); }
	int_t& operator = (unsigned            x) noexcept { return assign<unsigned           >(x); }
	int_t& operator = (long                x) noexcept { return assign<long               >(x); }
	int_t& operator = (unsigned long       x) noexcept { return assign<unsigned long      >(x); }
	int_t& operator = (long long           x) noexcept { return assign<long long          >(x); }
	int_t& operator = (unsigned long long  x) noexcept { return assign<unsigned long long >(x); }


	int_t& operator += (const fast_builtin_type& other) noexcept { return *this = as_fast_builtin() + other; }
	int_t& operator -= (const fast_builtin_type& other) noexcept { return *this = as_fast_builtin() - other; }
	int_t& operator *= (const fast_builtin_type& other) noexcept { return *this = as_fast_builtin() * other; }
	int_t& operator /= (const fast_builtin_type& other)          { return *this = as_fast_builtin() / other; }
	int_t& operator += (const int_t& other) noexcept { return operator+=(other.as_fast_builtin()); }
	int_t& operator -= (const int_t& other) noexcept { return operator-=(other.as_fast_builtin()); }
	int_t& operator *= (const int_t& other) noexcept { return operator*=(other.as_fast_builtin()); }
	int_t& operator /= (const int_t& other)          { return operator/=(other.as_fast_builtin()); }

	bool operator == (const int_t& other) noexcept { return value == other.value; }
	bool operator != (const int_t& other) noexcept { return value != other.value; }

protected: // conversion operators
	operator fast_builtin_type() const noexcept { return as_integer<fast_builtin_type>(); }

protected: // non-mutator methods
	fast_builtin_type as_fast_builtin() const noexcept  { return as_integer<fast_builtin_type>();  }
	fast_builtin_type as_least_builtin() const noexcept { return as_integer<least_builtin_type>(); }
};

// Additional operators which can make do with public members

template <unsigned NBytes, bool Signed> bool operator >  (const int_t<NBytes, Signed>&x, const int_t<NBytes, Signed>& y) noexcept { return x.as_fast_builtin() >  y.as_fast_builtin(); }
template <unsigned NBytes, bool Signed> bool operator <  (const int_t<NBytes, Signed>&x, const int_t<NBytes, Signed>& y) noexcept { return x.as_fast_builtin() <  y.as_fast_builtin(); }
template <unsigned NBytes, bool Signed> bool operator >= (const int_t<NBytes, Signed>&x, const int_t<NBytes, Signed>& y) noexcept { return x.as_fast_builtin() >= y.as_fast_builtin(); }
template <unsigned NBytes, bool Signed> bool operator <= (const int_t<NBytes, Signed>&x, const int_t<NBytes, Signed>& y) noexcept { return x.as_fast_builtin() <= y.as_fast_builtin(); }

template <unsigned NBytes, bool Signed> int_t<NBytes, Signed>& operator ++ (int_t<NBytes, Signed>& i) noexcept { return (i += 1); }
template <unsigned NBytes, bool Signed> int_t<NBytes, Signed>& operator -- (int_t<NBytes, Signed>& i) noexcept { return (i -= 1); }
template <unsigned NBytes, bool Signed>
int_t<NBytes, Signed> operator++ (int_t<NBytes, Signed>& i, int) noexcept
{
	int_t<NBytes, Signed> result = i;
	i += 1;
	return result;
}
template <unsigned NBytes, bool Signed>
int_t<NBytes, Signed> operator-- (int_t<NBytes, Signed>& i, int) noexcept
{
	int_t<NBytes, Signed> result = i;
	i -= 1;
	return result;
}

template <unsigned NBytes, bool Signed>
std::ostream& operator<<(std::ostream& os, int_t<NBytes, Signed> i) { return os << i.as_least_builtin(); }
template <unsigned NBytes, bool Signed>
std::istream& operator>>(std::istream& is, int_t<NBytes, Signed> i)
{
	typename int_t<NBytes, Signed>::fast_builtin_type fast_builtin;
	is >> fast_builtin;
	i = fast_builtin;
	return is;
}

} // namespace detail

template <unsigned NBytes, bool Signed>
struct int_t {
	using type = detail::int_t<NBytes, Signed>;
};


} // namespace util


#endif /* UTIL_INTEGER_EXTRA_H_ */
