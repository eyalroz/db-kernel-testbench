/**
 * @file Some basic statistics-related utility functions
 *
 * @note no support for complex numbers or other non-arithmetic types
 *
 */
#pragma once
#ifndef UTIL_STATISTICS_HPP_
#define UTIL_STATISTICS_HPP_

#include "util/stl_algorithms.hpp"
#include "util/math.hpp"

#include <algorithm>
#include <numeric>
#include <vector>

namespace util {

template<class RandomIt, class Compare = std::less<typename std::iterator_traits<RandomIt>::value_type>>
typename std::iterator_traits<RandomIt>::value_type
median(RandomIt first, RandomIt last, Compare comp = Compare())
{
	std::vector<typename std::iterator_traits<RandomIt>::value_type> v(first, last);
	auto middle = v.begin() + v.size() / 2;
	std::nth_element(v.begin(), middle, v.end());
	return *middle;
}

template<class Container, class Compare = std::less<typename Container::value_type>>
typename Container::value_type
median(const Container& container, Compare comp = Compare())
{
	return median(std::begin(container), std::end(container), comp);
}

template<class RandomIt, class Compare = std::less<typename std::iterator_traits<RandomIt>::value_type>>
typename std::iterator_traits<RandomIt>::value_type
quantile(RandomIt first, RandomIt last, double which_quantile, Compare comp = Compare() )
{
	std::vector<typename std::iterator_traits<RandomIt>::value_type> v(first, last);
	auto quantile_pos = v.begin() + v.size() * which_quantile;
	std::nth_element(v.begin(), quantile_pos, v.end());
	return *quantile_pos;
}

template<class Container, class Compare = std::less<typename Container::value_type>>
typename Container::value_type quantile(const Container& container, double which_quantile, Compare comp = Compare())
{
	return median(std::begin(container), std::end(container), which_quantile, comp);
}

template<typename RandomIt>
double mean(RandomIt first, RandomIt last)
{
	auto num_elements = std::distance(first, last);
	auto sum = kahan_summation<RandomIt, double>(first, last);
	return sum / num_elements;
}

template< class Container>
double mean(const Container& container)
{
	return mean(std::begin(container), std::end(container));
}

template<typename RandomIt>
double second_moment(RandomIt first, RandomIt last)
{
	auto num_elements = std::distance(first, last);
	auto square = [](const decltype(*first)& x) -> double { return x * x; };
	auto sum_of_squares = kahan_summation<RandomIt, double, decltype(square)>(first, last, square);
	return sum_of_squares / num_elements;
}

template< class Container>
double second_moment(const Container& container)
{
	return second_moment(std::begin(container), std::end(container));
}

template<typename RandomIt>
double third_moment(RandomIt first, RandomIt last)
{
	auto num_elements = std::distance(first, last);
	auto cube = [](const decltype(*first)& x) -> double { return x * x * x; };
	auto sum_of_cubes = kahan_summation<RandomIt, double, decltype(cube)>(first, last, cube);
	return sum_of_cubes / num_elements;
}

template< class Container>
double third_moment(const Container& container)
{
	return second_moment(std::begin(container), std::end(container));
}

template<typename RandomIt>
double moment(RandomIt first, RandomIt last, double which_moment)
{
	auto num_elements = std::distance(first, last);
	auto power = [which_moment](const decltype(*first)& x) -> double { return std::pow(x, which_moment); };
	auto sum_of_powered_elements = kahan_summation<RandomIt, double, decltype(power)>(first, last, power);
	return sum_of_powered_elements / num_elements;
}

template< class Container>
double moment(const Container& container, double which_moment)
{
	return moment(std::begin(container), std::end(container), which_moment);
}

namespace detail {

template<typename RandomIt, bool SampleCorrection = true>
double sum_of_square_differences(RandomIt first, RandomIt last, double mean)
{
	auto square_difference = [mean](const decltype(*first)& x) -> double {
		double delta = x - mean;
		return delta * delta;
	};
	return kahan_summation<RandomIt, double, decltype(square_difference)>(first, last, square_difference);
}

template<typename RandomIt, bool SampleCorrection = true>
double variance(RandomIt first, RandomIt last, double mean)
{
	auto num_elements = std::distance(first, last);
	if (num_elements <= 1) { return 0; }

	auto ssd = sum_of_square_differences<RandomIt, SampleCorrection>(first, last, mean);
	auto corrected_num_elements = (SampleCorrection ? num_elements - 1 : num_elements);
	return ssd / corrected_num_elements;
}

} // namespace detail

template<typename RandomIt, bool SampleCorrection = true>
double variance(RandomIt first, RandomIt last)
{
	// Note: While the two-pass algorithm below has somewhat better
	// stability than just E(X^2) - E(X^2) (I think), it's still
	// pretty bad. Sorting the data and computing things on-line would probably help

	auto mean_ = mean(first, last);
	return detail::variance(first, last, mean_);
}

template<class Container, bool SampleCorrection = true>
double variance(const Container& container)
{
	return variance(std::begin(container), std::end(container));
}

template<typename RandomIt, bool SampleCorrection = true>
double standard_deviation(RandomIt first, RandomIt last)
{
	return std::sqrt(variance(first, last));
}

template<class Container, bool SampleCorrection = true>
double standard_deviation(const Container& container)
{
	return standard_deviation(std::begin(container), std::end(container));
}

template<typename RandomIt, bool SampleCorrection = true>
double relative_standard_deviation(RandomIt first, RandomIt last)
{
	auto distance_ = std::distance(first, last);
	if (distance_ <= 1) { return 0; }

	double mean_ = mean(first, last);
	double variance_ = detail::variance(first, last, mean_);
	return std::sqrt(variance_) / mean_;
}

template<class Container, bool SampleCorrection = true>
double relative_standard_deviation(const Container& container)
{
	return relative_standard_deviation(std::begin(container), std::end(container));
}


} // namespace util

#endif /* UTIL_STATISTICS_HPP_ */
