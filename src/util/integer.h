#pragma once
#ifndef UTIL_INTEGER_H_
#define UTIL_INTEGER_H_

#include "bits.hpp"

// TODO: Consider dropping all of the boost stuff
#include <boost/integer.hpp>
#include <ostream>
#include <istream>
#include <cstring> // for memcpy and memset
#include <type_traits>

namespace util {

template <unsigned NBytes, bool Signed>
struct integer_traits_t {
	static_assert(NBytes <= sizeof(unsigned long long), "larger sizes not supported, for now");

public: // types and constants
	enum { num_bytes = NBytes, num_bits = NBytes * 8 };
	enum : bool { signedness = Signed };
	using byte = unsigned char;
	using value_type = byte[NBytes];
	using fast_builtin_type =
		typename std::conditional<Signed,
			typename boost::int_t<num_bits>::fast,
			typename boost::uint_t<num_bits>::fast
		>::type;
	using least_builtin_type =
	typename std::conditional<Signed,
		typename boost::int_t<num_bits>::least,
		typename boost::uint_t<num_bits>::least
	>::type;

};

namespace detail {

enum : bool { is_signed = true, is_unsigned = false, isnt_signed = false };

} // namespace detail

template <unsigned NBytes, bool Signed = detail::is_signed>
struct int_t;

template <unsigned NBytes>
using uint_t = typename int_t<NBytes, detail::is_unsigned>::type;

template <unsigned NBytes>
using sint_t = typename int_t<NBytes, detail::is_signed>::type;

template<> struct int_t<1, detail::is_signed   > { using type = int8_t;   };
template<> struct int_t<2, detail::is_signed   > { using type = int16_t;  };
template<> struct int_t<4, detail::is_signed   > { using type = int32_t;  };
template<> struct int_t<8, detail::is_signed   > { using type = int64_t;  };
template<> struct int_t<1, detail::is_unsigned > { using type = uint8_t;  };
template<> struct int_t<2, detail::is_unsigned > { using type = uint16_t; };
template<> struct int_t<4, detail::is_unsigned > { using type = uint32_t; };
template<> struct int_t<8, detail::is_unsigned > { using type = uint64_t; };

namespace detail {
namespace {
	template <typename T>
	constexpr int log2_constexpr(T val) { return val ? 1 + log2_constexpr(val >> 1) : -1; }

	template <typename T>
	constexpr int ceil_log2_constexpr(T val) { return val ? 1 + log2_constexpr<T>(val - 1) : -1; }

	template <typename T>
	constexpr inline T round_up_to_power_of_2_constexpr(const T& x_greater_than_one)
	{
		return ((T)1) << ceil_log2_constexpr(x_greater_than_one);
	}

} // namespace (anonymous)
} // namespace detail

/**
 * @brief A type trait for working with sizes of subsets of a type
 *
 * Consider some type, say an unsigned type, T. Its domain of possible
 * values is 0...sizeof(T)*CHAR_BIT - 1 . Now, if we take a subset of
 * this domain, what can its size be? 0...sizeof(T)*CHAR_BIT ; problem
 * is, this larger domain can't be represented by T! That's what this
 * type trait is about; it provides a type for representing this
 * slightly-larger domain, as well as its size.
 *
 * The trait is only defined when there exists a "natural" unsigned
 * integral type which can represent the larger domain, e.g. it
 * isn't defined for integer types of size 8 and higher; for those,
 * see @ref capped_domain_size instead.
 *
 */
template <typename T>
struct domain_size {
	using type = typename std::enable_if<
		sizeof(T) < 8,
		uint_t<detail::round_up_to_power_of_2_constexpr(sizeof(T) + 1)>
	>::type;

	enum : size_t { value = ((size_t)1) << (
		sizeof(typename std::enable_if<
			(sizeof(T) < sizeof(size_t)) and std::is_integral<T>::value, T
		>::type) * bits_per_char) };
};

/**
 * Same as @ref domain_size<T> but pretending the domain size
 * of 8-byte-sized types fits inside a {@code uint64_t} (otherwise a bunch
 * of code won't compile)
 */
template <typename T>
struct capped_domain_size {
	static_assert(std::is_integral<T>::value, "Not supported for non-integral types");
	using type = uint_t<sizeof(T) >= 8 ? 8 :
		detail::round_up_to_power_of_2_constexpr(sizeof(T) + 1)>;
	enum : bool { cap_reached = (sizeof(T) == 8) };
	enum : size_t { value =
		sizeof(T) < sizeof(size_t) ?
			( ((size_t) 1) << sizeof(T) * bits_per_byte) - 1 :
			std::numeric_limits<size_t>::max()
	};
};

namespace detail {

constexpr unsigned get_sized_container_size(
	unsigned element_size_in_bits, unsigned minimum_alignment)
{
#if __cplusplus >= 201402L
	auto full_bytes_in_element = element_size_in_bits / bits_per_byte;
	if (element_size_in_bits % bits_per_byte == 0) {
		return full_bytes_in_element;
	}
	auto bytes_to_cover_element = full_bytes_in_element + 1;
	auto misalignment = bytes_to_cover_element % minimum_alignment;
	return
		bytes_to_cover_element +
		(misalignment == 0) ?
			0 :
			bytes_to_cover_element + minimum_alignment - misalignment;
#else
	// The fugly C++11 version
#define full_bytes_in_element (element_size_in_bits / bits_per_byte)
	return (element_size_in_bits % bits_per_byte == 0) ?
		full_bytes_in_element :
#define bytes_to_cover_element (full_bytes_in_element + 1)
		(
			bytes_to_cover_element +
			(
				(bytes_to_cover_element % minimum_alignment == 0) ?
					0 :
					(minimum_alignment - bytes_to_cover_element % minimum_alignment)
			)
		);
#undef full_bytes_in_element
#undef bytes_to_cover_element
#endif
}


} // namespace detail

/*
 * rewrite this.
 *
 * When we want to place data, whose type is erased (unknown),
 * but whose type _size_ in bits is known to us, into an array,
 * without byte-aligning, i.e. with "packed" bits, we cannot
 * point to this array simply as uint_t<N> - as N is a number
 * of bytes; and if it had not been - pointers are byte-aligned.
 *
 * Instead, we'll such sequences are in aligned containers,
 * so that we only point to elements at the started of a
 * stretch of memory which is fully used up.
 *
 * Also, when N is a power of 2, we use a "regular" point
 * to make for a more palatable interface
 *
 *
 *
 * On the other hand, when the size in bits happens to be
 * byte-aligned,
 *
 * Instead, we make only assume
 *
 * @note move this up.
 */
template <unsigned ElementSizeInBits, unsigned MinimumAlignmentInBytes>
using sized_container_t = uint_t<
	detail::get_sized_container_size(ElementSizeInBits, MinimumAlignmentInBytes)
>;

} // namespace util

#endif /* UTIL_INTEGER_H_ */
