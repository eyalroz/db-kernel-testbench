#pragma once
#ifndef CHRONO_HPP_
#define CHRONO_HPP_

#include <chrono>
#include <ctime>
#include <string>

inline std::string current_datetime()
{
	// This doesn't work with GCC 4.9.x, since it hasn't
	// implemented std::put_time...
	//
	//auto& oss = util::detail::get_ostringstream();
	//auto now = std::chrono::system_clock::now();
	//auto now_c = std::chrono::system_clock::to_time_t(now);
	// oss << std::put_time(std::localtime(&now_c), "%T %F");
	// return oss.str();
	//
    std::time_t now_c = std::time(nullptr);
    std::string str(ctime(&now_c));
	std::string::size_type pos = str.find_last_not_of("\n \t");
	return (pos != std::string::npos) ? str.substr(0, pos + 1) : str;
}

#endif /* CHRONO_HPP_ */
