#include "util/files.h"
#include "util/string.hpp"

#include <pwd.h>
#include <cstdio>
#include <climits>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <stdexcept>
#ifdef DEBUG
#include <iostream>
#endif

namespace util {

util::optional<filesystem::path> get_home_directory()
	{
	struct passwd *pw = getpwuid(getuid());
		// Note: this is not newly-allocated memory and we shouldn't free it
	if (pw == nullptr) { return { }; }
	return filesystem::path(pw->pw_dir);
}


std::string file_contents(const std::string& path)
{
	std::ifstream ifs(path);
	std::stringstream ss;
	ss << ifs.rdbuf();
	return ss.str();
}

bool is_recursable(const filesystem::path& path)
{
	if (not filesystem::is_directory(path)) { return false; }
	try {
		auto di = filesystem::directory_iterator(path);
		return true;
	}
	catch (std::exception& e) {
		return false;
	}
}

bool is_readable(const filesystem::path& path)
{
    std::ifstream infile(path.string());
    return infile.good();
//	return (access(path.string.c_str(), R_OK) == 0);
}

} /* namespace util */
