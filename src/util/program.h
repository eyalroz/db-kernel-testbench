#pragma once
#ifndef PROGRAM_H_
#define PROGRAM_H_

#include <string>
#include <utility> // for pair

namespace util {

namespace command_line {

std::pair<int, char**> get();
std::string get_formatted();
std::string format(int argc, char** argv);
void save(int argc, char** argv);

}

std::string path_to_executing_binary();
std::string current_working_directory();

void disable_stdout_buffering();

} // namespace util

#endif /* PROGRAM_H_ */
