/**
 * @file algorithm_container_adapters.hpp
 *
 * This file has adapters for using either standard library
 * iterator-based 'algorithms', or similar 'algorithms' from
 * @ref stl_algorithms.hpp , on containers rather than iterator
 * pairs. Another section focuses specifically on maps, and
 * applying 'algorithms' to either their keys or their values
 *
 */
#pragma once
#ifndef UTIL_ALGORITHM_CONTAINER_ADAPTERS_HPP_
#define UTIL_ALGORITHM_CONTAINER_ADAPTERS_HPP_

#include <type_traits>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <functional>
#include <unordered_map>
#include "util/stl_algorithms.hpp"

template<typename Container, typename Function>
inline Function for_each(Container& container, Function f)
{
	return std::for_each(std::begin(container), std::end(container), f);
}

template<typename Container>
inline typename Container::const_iterator max_element(const Container& container)
{
	return std::max_element(std::begin(container), std::end(container));
}

template<typename Container>
inline typename Container::const_iterator min_element(const Container& container)
{
	return std::min_element(std::begin(container), std::end(container));
}


template<typename Container, typename T>
inline void fill(Container& container, const T& value) { std::fill(std::begin(container), std::end(container), value); }

template<typename Container, typename Size, typename T>
inline void fill_n(Container& container, Size n, const T& value) { std::fill_n(std::begin(container), n, value); }

template<typename Container, typename T>
inline void iota(Container& container, const T& value) { std::iota(std::begin(container), std::end(container), value); }

template<typename Container>
inline void iota(Container& container)
{
	std::iota(std::begin(container), std::end(container), typename Container::value_type() );
}

template<typename Container, typename OutputIterator>
inline void copy(const Container& container, OutputIterator result)
{
	std::copy(std::begin(container), std::end(container), result);
}

template<typename Container, typename Compare = std::less<typename Container::value_type>>
inline void sort(Container& container, Compare compare = Compare())
{
	std::sort(std::begin(container), std::end(container), compare);
}

template<typename Container, typename Compare = std::less<typename Container::value_type>>
inline void partial_sort(Container& container, size_t num_elements_to_sort, Compare compare = Compare())
{
	auto begin = std::begin(container);
	std::partial_sort(begin, begin + num_elements_to_sort, std::end(container), compare);
}

template<typename InputContainer, typename OutputContainer, typename Compare = std::less<typename InputContainer::value_type>>
inline typename OutputContainer::iterator
partial_sort_copy(const InputContainer& container, OutputContainer& copy_of_sorted, Compare compare = Compare())
{
	return std::partial_sort_copy(
		std::begin(container), std::end(container),
		std::begin(copy_of_sorted), std::end(copy_of_sorted),
		compare);
}

template<typename InputContainer, typename OutputIterator, typename Compare = std::less<typename InputContainer::value_type>>
inline OutputIterator partial_sort_copy(const InputContainer& container, OutputIterator d_start, OutputIterator d_end, Compare compare = Compare())
{
	return std::partial_sort_copy(std::begin(container), std::end(container), d_start, d_end, compare);
}

template <typename Container, typename Compare = std::less<typename Container::value_type> >
typename Container::const_iterator
lower_bound(Container& container, const typename Container::value_type& val, Compare comp = Compare())
{
	return std::lower_bound(std::begin(container), std::end(container), val, comp);
}

template<typename Container, typename Generator>
inline void generate(Container& container, Generator generator)
{
	std::generate(std::begin(container), std::end(container), generator);
}

template<typename Container, typename UniformRandomNumberGenerator>
inline void shuffle(Container& container, UniformRandomNumberGenerator&& generator)
{
	std::shuffle(std::begin(container), std::end(container), generator);
}

template <typename Container>
inline typename Container::const_iterator unique(Container const & container) {
  return std::unique(std::begin(container), std::end(container));
}

template <typename C, typename P>
inline size_t count_if(C const & container, P pred) {
	return std::count_if(std::begin(container), std::end(container), pred);
}

#endif /* UTIL_ALGORITHM_CONTAINER_ADAPTERS_HPP_ */
