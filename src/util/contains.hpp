/**
 * contains.hpp
 *
 * A standard-library-like contains() functions for
 * all standard library (and may other) containers;
 * this requires a tricky implementation since the
 * same code can't be used for all containers.
 *
 * Adapted from:
 * http://stackoverflow.com/a/26888209/1593077
 *
 * by Eyal Rozenberg <E.Rozenberg@cwi.nl>
 *
 */
#ifndef UTIL_CONTAINS_HPP_
#define UTIL_CONTAINS_HPP_

#include <vector>
#include <string>
#include <deque>
#include <algorithm>
#include <utility>
#include <array>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cstddef> // for std::size_t

namespace util {

namespace detail {

template<typename ... Args> struct has_find {};

template<typename T                > struct has_find<std::vector<T>             > { enum : bool { value = false }; };
template<typename T                > struct has_find<std::deque<T>              > { enum : bool { value = false }; };
template<typename T                > struct has_find<std::set<T>                > { enum : bool { value = true  }; };
template<typename T                > struct has_find<std::multiset<T>           > { enum : bool { value = true  }; };
template<typename T                > struct has_find<std::unordered_set<T>      > { enum : bool { value = true  }; };
template<typename T                > struct has_find<std::unordered_multiset<T> > { enum : bool { value = true  }; };
template<typename T, std::size_t I > struct has_find<std::array<T, I>           > { enum : bool { value = false }; };
template<typename T, typename U    > struct has_find<std::map<T, U>             > { enum : bool { value = true  }; };
template<typename T, typename U    > struct has_find<std::unordered_map<T, U>   > { enum : bool { value = true  }; };
template<                          > struct has_find<std::string                > { enum : bool { value = true  }; };
template<                          > struct has_find<std::wstring               > { enum : bool { value = true  }; };


#define USE_CONTAINER_FIND_FOR_CONTAINS_METHOD(_container_type) \
namespace util { \
namespace detail { \
template<> struct has_find<_container_type> { enum : bool { value = true }; }; \
} \
}


//... and so on for the handful remaining containers

template<bool has_find>
struct contains_impl
{
    template <typename C, typename E>
    bool contains(const C& container, E&& element) const
    {
        return container.find(std::forward<E>(element)) != container.end();
    }
};

template<>
struct contains_impl<false>
{
    template <typename C, typename E>
    bool contains(const C& container, E&& element) const
    {
        return std::find(container.cbegin(), container.cend(), std::forward<E>(element)) != container.cend();
    }
};

} // namespace detail

template <typename C, typename E>
bool contains(const C& container, E&& element)
{
	constexpr const auto container_has_find = detail::has_find<C>::value;
    return detail::contains_impl<container_has_find>().contains(container, std::forward<E>(element));
}

} // namespace util

#endif /* UTIL_CONTAINS_HPP_ */
