#include "util/distribution.h"

#include "util/miscellany.hpp"
#include "util/stl_algorithms.hpp"
#include "util/algorithm_container_adapters.hpp"
#include "util/maps.hpp"
#include "util/string.hpp"
#include "util/exception.h"

#include <util/poor_mans_reflection.h>

#include <iterator>
#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>

namespace util {
namespace random {

template <typename N>
void distribution_t<N>::init_distributions()
{
	double probability_of_success;
	closest_unsigned_type num_trials;
	switch(family_) {
	case distribution_family_t::single_value : break;
	case distribution_family_t::uniform :
		// Ignoring the mean here!
		d.uniform = uniform_distribution_t(min_, max_); break;
	case distribution_family_t::bernoulli :
		probability_of_success = mean_;
		// Ignoring min, max here!
		d.bernoulli = std::bernoulli_distribution(probability_of_success); break;
	case distribution_family_t::binomial :
		if (min_ != 0) {
			throw std::invalid_argument(
				"A binomial distribution must be created with minimum value 0 "
				"and maximum value equal to the number of trials");
		}
		num_trials = max_;
		probability_of_success = mean_;
		d.binomial = binomial_distribution_t(num_trials, mean_); break;
	case distribution_family_t::geometric:
		if (mean_ < 1) {
			throw std::invalid_argument(
				"A geometric distribution must have a parameter between 0 and 1 "
				"(i.e. a mean at least 1)");
		}
		d.geometric = geometric_distribution_t(1.0 / mean_); break;
	case distribution_family_t::exponential:
		if (mean_ <= 0) {
			throw std::invalid_argument("An exponential distribution must have a non-negative lambda parameter");
		}
		d.exponential = exponential_distribution_t(1.0 / mean_); break;
	}
}

template <typename N>
distribution_t<N>::distribution_t(std::unordered_map<std::string, std::string> string_params)
: family_(distribution_family_t::uniform), // this is necessary because there's no default ctor for the family
  mean_(0), min_(std::numeric_limits<N>::min()), max_(std::numeric_limits<N>::max())
{
	if (string_params.find("family") == string_params.end()) {
		throw util::invalid_argument("No distribution family specified");
	}
	auto family_name = util::chomp(string_params["family"]);
	if (!distribution_family_t::_is_valid(family_name.c_str())) {
		throw util::invalid_argument("Invalid/unsupported distribution family named: \"" + family_name + "\"");
	}
	family_ = distribution_family_t::_from_string(family_name.c_str());
	bool need_extrema = false;
	bool allow_extrema = false;
	switch(family_) {
	case distribution_family_t::single_value: {
		std::string val = util::get_first_match(string_params, "value", "single_value", "val", "v");
		util::get_from_string(mean_, val);
		min_ = max_ = mean_;
	}; break;
	case distribution_family_t::uniform:
		min_ = std::numeric_limits<N>::min();
		max_ = std::numeric_limits<N>::max();
		need_extrema = false;
		allow_extrema = true; break;
	case distribution_family_t::geometric: {
		std::string p = util::get_first_match(string_params, "p", "parameter");
		auto parsed_p = util::from_string<double>(p);
		if (parsed_p < 0 || parsed_p > 1) {
			throw std::invalid_argument("A geometric distribution must have its p parameter between 0 and 1");
		}
		mean_ = 1.0 / parsed_p;
		allow_extrema = true;
	}; break;
	case distribution_family_t::exponential: {
		std::string lambda = util::get_first_match(string_params, "lambda", "parameter");
		auto parsed_lambda = util::from_string<double>(lambda);
		if (parsed_lambda <= 0) {
			throw std::invalid_argument("An exponential distribution must have a non-negative lambda parameter");
		}
		mean_ = 1.0 / parsed_lambda;
		allow_extrema = true;
	}; break;
	case distribution_family_t::bernoulli: {
		std::string probability_of_success = util::get_first_match(string_params, "p", "parameter", "probability_of_success", "prob_success", "prob", "probability");
		util::get_from_string(mean_, probability_of_success);
	}; break;
	case distribution_family_t::binomial: {
		std::string probability_of_trial_success = util::get_first_match(string_params, "p", "parameter", "probability_of_success", "prob_success", "prob", "probability");
		std::string num_trials = util::get_first_match(string_params, "n", "num_trials", "trials", "number_of_trials");
		double p;
		closest_unsigned_type n;
		util::get_from_string(p, probability_of_trial_success); break;
		util::get_from_string(n, num_trials); break;
		mean_ = p * n;
	}; break;
	}
	if (string_params.find("min") != string_params.end()) {
		util::enforce(allow_extrema || need_extrema, "Superfluous distribution minimum value specified");
		util::get_from_string(min_, string_params["min"]);
	}
	else {
		util::enforce(!need_extrema, "Distribution minimum value not specified");
		if (allow_extrema) { min_ = std::numeric_limits<N>::min(); }
	}
	if (string_params.find("max") != string_params.end()) {
		util::enforce(allow_extrema || need_extrema, "Superfluous distribution max value specified");
		util::get_from_string(max_, string_params["max"]);
	}
	else {
		util::enforce(!need_extrema, "Distribution maximum value not specified");
		if (allow_extrema) { max_ = std::numeric_limits<N>::max(); }
	}

	init_distributions();
}

template <typename N>
std::ostream& operator<<(std::ostream& os, const distribution_t<N>& d)
{
	using promoted = typename util::detail::promoted_for_streaming<N>::type;
	bool print_min = false;
	bool print_max = false;
	bool printed_anything = false;
	os << d.family()._to_string() << " [";
	switch(d.family()) {
	case distribution_family_t::single_value:
		os << " value = " << (promoted) d.min();
		printed_anything = true;
		break;
	case distribution_family_t::uniform:
		print_min = true; print_max = true;
		break;
	case distribution_family_t::geometric:
		os << " p = " << 1.0/d.mean();
		print_min = (d.min() != std::numeric_limits<N>::min());
		print_max = (d.max() != std::numeric_limits<N>::max());
		printed_anything = true;
		break;
	case distribution_family_t::exponential:
		os << " p = " << 1.0/d.mean();
		print_min = (d.min() != std::numeric_limits<N>::min());
		print_max = (d.max() != std::numeric_limits<N>::max());
		printed_anything = true;
		break;
	case distribution_family_t::bernoulli:
		os << " p = " << (promoted) d.mean();
		printed_anything = true;
		break;
	case distribution_family_t::binomial:
		os << (promoted) d.max() << "trials, p = " << (promoted) d.mean();
		printed_anything = true;
		break;
	}
	if (print_min) {
		if (printed_anything) { os << ','; }
		os << " min = " << (promoted) d.min();
		printed_anything = true;
	}
	if (print_max) {
		if (printed_anything) { os << ','; }
		os << " max = " << (promoted) d.max();
	}
	return os <<  " ]";
}

} // namespace random
} // namespace util

template <typename N>
util::random::distribution_t<N> util::istream_traits<util::random::distribution_t<N>>::read(std::istream& is)
{
	using std::string;
	using std::ws;
	is >> ws;
	std::string family_name, all_unparsed_params;
	std::getline(is, family_name, '[');
	util::remove_spaces(family_name);
	std::unordered_map<string, string> params;
	params.emplace("family", family_name);
	is >> ws;
	std::getline(is, all_unparsed_params, ']');
	util::remove_spaces(all_unparsed_params);
	if (not all_unparsed_params.empty()) {
		std::vector<std::string> unparsed_key_values;
		util::tokenize(all_unparsed_params, unparsed_key_values, ',', false);
		for_each(unparsed_key_values, [&](const string& key_value_str) {
			auto pair = util::split_by_first(key_value_str, '=');
			params.emplace(pair.first, pair.second);
		});
	}
	return util::random::distribution_t<N>(params);
}

#define INSTANTIATE_1(_type_name) \
template util::random::distribution_t<_type_name>::distribution_t(std::unordered_map<std::string, std::string> string_params);
#define INSTANTIATE_2(_type_name) \
template std::ostream& util::random::operator<< <_type_name>(std::ostream& os, const util::random::distribution_t<_type_name>& d);
#define INSTANTIATE_3(_type_name) \
template util::random::distribution_t<_type_name> util::istream_traits<util::random::distribution_t<_type_name>>::read(std::istream& is);
#define INSTANTIATE_4(_type_name) \
template void util::random::distribution_t<_type_name>::init_distributions();

MAP(INSTANTIATE_1, ALL_NUMERIC_TYPES_NO_DUPES)
MAP(INSTANTIATE_2, ALL_NUMERIC_TYPES_NO_DUPES)
MAP(INSTANTIATE_3, ALL_NUMERIC_TYPES_NO_DUPES)
MAP(INSTANTIATE_4, ALL_NUMERIC_TYPES_NO_DUPES)

