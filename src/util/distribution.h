/**
 * A quick-and-dirty holder class for any of the C++ standard
 * library's probability distribution classes.
 *
 * Should be coded with an std::variant (or a pre-C++17 variant
 * implementation such as boost::variant, or at least a union)
 * rather tha instantiating each and every distribution. Could
 * also use composability with transforming functors.
 */
#pragma once
#ifndef DISTRIBUTION_H_
#define DISTRIBUTION_H_

#include "util/random.h"
#include "util/string.hpp" // for promotion for printing
#include "util/better_enums.hpp"

#include <random>
#include <iostream>
#include <type_traits>
#include <limits>
#include <unordered_map>

namespace util {
namespace random {

// TODO: More distributions?
BETTER_ENUM(
	distribution_family_t, unsigned,
	single_value,
	uniform,
	geometric,
	exponential,
	bernoulli,
	binomial
)


/**
 * This is an ugly class! Rewrite or use something more standard from another library
 */
template <typename N>
class distribution_t {

public: // types

	using result_type = N;
	using closest_unsigned_type =
		typename std::conditional<std::is_unsigned<N>::value, N, unsigned long long int>::type;

protected: // types
	using uniform_distribution_t = typename std::conditional<
		std::is_integral<N>::value,
		std::uniform_int_distribution<N>,
		std::uniform_real_distribution<N>
	>::type;
	using exponential_distribution_t = std::exponential_distribution<double>;
	using geometric_distribution_t = std::geometric_distribution<closest_unsigned_type>;
	using binomial_distribution_t = std::binomial_distribution<closest_unsigned_type>;

public: // constructors
	// TODO: Perhaps enforce compatible values for unused parameters, e.g.
	// in Bernoulli only accept min = 0 and max = 1 - or allow
	// for optionals to be passed
	distribution_t(distribution_family_t family, double mean, N min, N max) :
	family_(family), mean_(mean), min_(min), max_(max) { init_distributions(); }
	distribution_t(const distribution_t& other) = default;
	distribution_t(N v) : distribution_t( distribution_family_t::single_value, (double) v, v, v) { };
	distribution_t() : distribution_t((N) 0) { };
	distribution_t(std::unordered_map<std::string, std::string> parameters);

public: // constructors from standard library distributions

	distribution_t(const uniform_distribution_t& other) :
		family_(distribution_family_t::uniform),
		mean_((other.b() + other.a()) / 2),
		min_(other.a()),
		max_(other.b())
	{
		d.uniform = other;
	}
	distribution_t(const geometric_distribution_t& other) :
		family_(distribution_family_t::geometric),
		mean_(1.0 / other.p())
	{
		d.geometric = other;
	}

	distribution_t(const exponential_distribution_t& other) :
		family_(distribution_family_t::exponential),
		mean_(1.0 / other.lambda())
	{
		d.exponential = other;
	}

	distribution_t(const binomial_distribution_t& other) :
		family_(distribution_family_t::binomial),
		min_(0),
		max_(other.t()),
		mean_(other.t() * other.p())
	{
		d.binomial = other;
	}

	distribution_t(const std::bernoulli_distribution& other) :
		family_(distribution_family_t::bernoulli),
		min_(0),
		max_(1),
		mean_(other.p())
	{
		d.binomial = other;
	}

public: // named constructor idioms

	static distribution_t single_value(N v)
	{
		return distribution_t(v);
	}
	static distribution_t uniform(N min, N max)
	{
		return distribution_t(distribution_family_t::uniform, (min + max) / 2.0, min, max);
	}
	static distribution_t full_range_uniform()
	{
		return uniform(std::numeric_limits<N>::min(), std::numeric_limits<N>::max());
	}
	static distribution_t uniform()
	{
		return full_range_uniform();
	}
	static distribution_t geometric(
		double mean = 1.0,
		N min = std::numeric_limits<N>::min(),
		N max = std::numeric_limits<N>::max())
	{
		return distribution_t(distribution_family_t::geometric, mean, min, max);
	}
	static distribution_t exponential(
		double mean = 1.0,
		N min = std::numeric_limits<N>::min(),
		N max = std::numeric_limits<N>::max())
	{
		return distribution_t(distribution_family_t::exponential, mean, min, max);
	}
	static distribution_t bernoulli(double probability_of_success) {
		return distribution_t(distribution_family_t::bernoulli, probability_of_success, 0, 1 );
	}
	static distribution_t binomial(closest_unsigned_type num_trials, double trial_success_probability) {
		return distribution_t(distribution_family_t::binomial, trial_success_probability, 0, num_trials);
	}

protected:
	void init_distributions();

public: // const_getters
	distribution_family_t family() const { return family_; }
	double                mean()   const { return mean_;   }
	N                     min()    const { return min_;    }
	N                     max()    const { return max_;    }
	bool                  is_degenerate() const { return min_ == max_; }

protected: // data members
	distribution_family_t family_;
	double       mean_;
//	double       standard_deviation_;
	N            min_ { std::numeric_limits<N>::min() };
	N            max_ { std::numeric_limits<N>::max() };

	union distribution_union {
		uniform_distribution_t       uniform;
		geometric_distribution_t     geometric;
		exponential_distribution_t   exponential;
		std::bernoulli_distribution  bernoulli;
		binomial_distribution_t      binomial;
		// Ugly, but does the job
		distribution_union() { memset( this, 0, sizeof( *this ) ); }
	};
	distribution_union d;

public: // mutators
	// TODO: perhaps make this a member?
	N operator()(std::default_random_engine& engine)
	{
		// TODO: Try using a "visit" pattern to avoid the switch here
		switch(family_) {
		case distribution_family_t::single_value:
			return min_;
		case distribution_family_t::uniform:
			return util::random::sample_from(d.uniform, engine);
		case distribution_family_t::geometric: {
			// TODO: What happens if the sample exceeds the representation capability of
			// the result type?
			result_type sample = util::random::sample_from(d.geometric, engine);
			return
				sample < min_ ? min_ :
				sample > max_ ? max_ :
				sample;
		}; break;
		case distribution_family_t::exponential: {
			auto sample = util::random::sample_from(d.exponential, engine);
			return
				sample < min_ ? min_ :
				sample > max_ ? max_ :
				sample;
		}; break;
		case distribution_family_t::bernoulli:
			return util::random::sample_from(d.bernoulli, engine);
		case distribution_family_t::binomial:
			return util::random::sample_from(d.binomial, engine);
		default: // can't get here
			return N();
		}
	}

	// TODO: Implement the rest of RandomNumberDistribution
};

template <typename N>
std::ostream& operator<<(std::ostream& os, const distribution_t<N>& d);

} // namespace random
} // namespace util


namespace util {

template <typename N>
struct istream_traits<util::random::distribution_t<N>>
{
	static util::random::distribution_t<N> read(std::istream& is);
};


namespace random {

template <typename ForwardIterator, typename Distribution, typename Engine = std::default_random_engine>
inline void fill(ForwardIterator start, ForwardIterator end, Distribution& distribution, Engine& engine)
{
	std::generate(start, end, [&distribution, &engine]() {
		return util::random::sample_from(distribution, engine);
	});
}

template <typename ForwardIterator, typename Size, typename Distribution, typename Engine = std::default_random_engine>
inline void fill_n(ForwardIterator start, Size n, Distribution& distribution, Engine& engine)
{
	std::generate_n(start, n, [&distribution, &engine]() {
		return util::random::sample_from(distribution, engine);
	});
}

template <typename Container, typename Distribution, typename Engine = std::default_random_engine>
inline void fill(Container& container, Distribution& distribution, Engine& engine)
{
	fill(container.begin(), container.end(), distribution, engine);
}

} // namespace random
} // namespace util

#endif /* DISTRIBUTION_H_ */

