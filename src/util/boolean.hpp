#pragma once
#ifndef SRC_UTIL_BOOLEAN_HPP_
#define SRC_UTIL_BOOLEAN_HPP_

#include <string>
#include <algorithm>

template <typename T>
T logical_xor (const T& x, const T& y) { return not x != not y; }

template <typename T>
T logical_xnor(const T& x, const T& y) { return not x = not y; }

template <typename T>
T logical_nxor(const T& x, const T& y) { return logical_xnor(x,y); }

namespace  util {

template <typename T>
constexpr inline T select(bool b, const T& on_true, const T& on_false)
{
	return b ? on_true : on_false;
}

constexpr inline const char* select(bool b, const char* on_true, const char* on_false)
{
	return b ? on_true : on_false;
}

// Quick and dirty boolean conversion without worrying about istringstreams, boolalpha etc.
inline bool to_bool(const std::string& x) {
	auto wsfront = std::find_if_not(x.begin(),x.end(),[](int c){return std::isspace(c);});
	auto wsback = std::find_if_not(x.rbegin(),x.rend(),[](int c){return std::isspace(c);}).base();
	if (wsback <= wsfront) return false;
	std::string trimmed = std::string(wsfront,wsback);
	for(const auto& s : {"1", "true", "True", "TRUE", "yes", "Yes"}) {
		if (trimmed == s) return true;
	}
	return false;
}

inline constexpr bool none_of()        { return true; }
inline constexpr bool all_of()         { return true; }
inline constexpr bool at_most_one_of() { return true; }
inline constexpr bool exactly_one_of() { return false; }

template <typename T, typename... Ts>
inline constexpr bool none_of(T head_value, Ts... tail_values) {
	return !head_value && none_of(tail_values...);
}

template <typename T, typename... Ts>
inline constexpr bool all_of(T head_value, Ts... tail_values) {
	return head_value && all_of(tail_values...);
}

template <typename T, typename... Ts>
inline constexpr bool at_most_one_of(T head_value, Ts... tail_values) {
	return
		( head_value && none_of(tail_values...)       ) or
		(!head_value && at_most_one_of(tail_values...));
}

template <typename T, typename... Ts>
inline constexpr bool excactly_one_of(T head_value, Ts... tail_values) {
	return
		( head_value && none_of(tail_values...)       ) or
		(!head_value && exactly_one_of(tail_values...));
}

} /* namespace util */


#endif /* SRC_UTIL_BOOLEAN_HPP_ */
