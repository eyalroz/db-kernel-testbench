
#include "util/program.h"
#include "util/exception.h"

#include <sstream>
#ifdef WINDOWS
#include <Windows.h>
#else
#include <unistd.h>
#endif
#include <vector>
#include <system_error>
#include <cstdio>

namespace util {

namespace command_line {

static std::pair<int, char**> saved_command_line;

std::pair<int, char**> get()
{
	return saved_command_line;
}

std::string format(int argc, char** argv)
{
	std::ostringstream oss;
	oss << argv[0];
	for(auto i = 1; i < argc; i++) { oss << ' ' << argv[i]; }
	return oss.str();
}

std::string get_formatted()
{
	return format(saved_command_line.first, saved_command_line.second);
}

void save(int argc, char** argv)
{
	saved_command_line = std::make_pair(argc, argv);
}

} // namespace command_line

std::string path_to_executing_binary()
{
	auto max_path_length = 65565;
	std::vector<char> buffer(max_path_length);
#ifdef WINDOWS
	int bytes = GetModuleFileName(NULL, &buffer[0], max_path_length);
	if(bytes == 0) {
		throw std::runtime_error("Failed to obtain the 'module filename' for the tester executable");
	}
#else
	// assuming a Linux-like OS

	auto proc_path = "/proc/" + std::to_string(getpid()) + "/exe";
	auto bytes_read = readlink(proc_path.c_str(), &buffer[0], max_path_length - 1);
	if (bytes_read == (ssize_t)(-1)) {
		throw util::make_system_error("Failed to read the tester executable's path from " + proc_path);
	}
	if (bytes_read == 0) {
		throw std::runtime_error("Empty readlink() result for the executable's path " + proc_path);
	}
	if (bytes_read > max_path_length - 1) {
		throw std::logic_error("Invalid number of bytes reported as read");
	}
	buffer[bytes_read] = '\0';
#endif
	return std::string(&buffer[0]);
}

std::string current_working_directory()
{
	char* malloced_cwd_string = get_current_dir_name();
	if (malloced_cwd_string == nullptr) { throw std::system_error(); }
	std::string cwd(malloced_cwd_string);
	free(malloced_cwd_string);
	return cwd;
}

void disable_stdout_buffering()
{
	auto result = std::setvbuf(stdout, nullptr, _IOFBF, 0);
  	if (result != 0) {
       throw util::make_system_error("Failed to setvbuf() to 0 for the standard output");
    }
}
} // namespace util
