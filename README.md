# A GPU kernel test harness and DBMS-oriented kernel collection #
## ... serving also as a GPU decompression library


### What is this project? What do you want to achieve? ###

- **10,000 meter high view:** I'm interested in designing and implementing an analytic query processor (the core of an analytics-oriented Database Management System) which is inherently aware of the variety of processors and memory spaces on a system. 
- **1,000 meter high view:** Utilizing GPUs is a prime use-case and avenue for this kind of work. Working with them is alien enough to traditional CPU work for require a query processor to have many/most of the capabilities it should have generally.
- **100 meter high view:** A GPU-utilizing query processor requires good implementations of various DBMS-related primitives it would use, i.e. apply to data. (There are caveats to that, e.g. JIT code generation and compilation.)
- **10 meter high view:** Implementing DBMS-related GPU primitives - by individual kernels or as multiple kernels could sure use a kernel test harness, and I haven't found a decent one to use. Also, there do not seem to be publicly available and flexibly-usable GPU kernels for a lot of the computational work of query processing - so I'm writing some.
- **1 meter high view:** Hi! Nice to meet you. This repository has a GPU kernel test harness (currently CUDA-only) and a bunch of kernels. Ain't that neat?

### What kernels and test-harness adapters are already implemented? ###

Here's the list of the pre-instantiated test adapters. Most correspond to a single kernels, some of them involve two kernels in sequence, or a choice between one and two. The instantiations are not just about choosing between, say, int's and double's, but choosing functor objects to use (aggregate-min vs aggregate-sum, scalar-vector binary operation or vector-vector, etc.).

```
#!cuda

decompression::Delta
decompression::Dictionary
decompression::DiscardZeroBytesFixed
decompression::DiscardZeroBytesVariable
decompression::FrameOfReference
decompression::IncidenceBitmaps
decompression::Model
decompression::patched::aposteriori::CompressedIndices
decompression::patched::aposteriori::Naive
decompression::RunLengthEncoding
decompression::RunPositionEncoding
elementwise::BinaryOperation
elementwise::Copy
elementwise::SimpleAdd
elementwise::TernaryOperation
elementwise::UnaryOperation
Gather
GatherBits
Generate
reduction::CountIf
reduction::CountNonZeros
reduction::CountSetBits
reduction::DenseSetMinimum
reduction::Histogram
reduction::PrefixCountSetBits
reduction::Reduce
reduction::ReduceByIndex
reduction::Scan
Scatter
SelectByDenseSubset
set_representation::DenseToSparse
set_representation::SparseToDense
```

### How do I get set up? ###

#### Hardware requirements: ####

* A CUDA-enabled GPU, of Compute Capability 3.0 or better (3.2 or better recommended), i.e. A Kepler-microarchitecture card or later, i.e. a GTX 650 card or newer (here a [list](https://en.wikipedia.org/wiki/List_of_Nvidia_graphics_processing_units) of nVIDIA GPUs, look for codenames starting with GK, GM or GP - Kepler, Maxwell or Pascal). Unfortunately, only CUDA is supported as a backend at the moment, and that is an nVIDIA-specific technology. This should hopefully change (see Issue #8).
* A PC whose case, power supply, and motherboard configuration support your GPU card. This can sometime be tricky, since the cards might be too long, or not get reasonable air flow, or take more power than your PSU provides (this is rare these days but could theoretically happen).
* The Motherboard should support PCI Express 3.0, and have an x16 card slot for your GPU card. This is not absolutely necessary, but recommended.
* The CPU can be mediocre, the only price you'll pay is longer compilation time.
 
#### Software requirements: ####

* A Unix-like operating system supported by the CUDA drivers and toolkit - which probably means Linux
* **CUDA toolkit v7.5** or later (preferably 8.0 or later), available [here](https://developer.nvidia.com/cuda-downloads)
* A recent **CUDA driver** from nvidia.com (this requires root privileges to install)
* A compiler compatible with the CUDA toolkit. If you're using CUDA 8.0, this can be **GCC 5.x** (but not GCC 6.0 or later; and clang support is not there yet); if you're using CUDA 7.5 or earlier, it must be **GCC 4.8.x or 4.9.x** (4.9.3 is the last of those, and is recommended for CUDA 7.5). If your distribution doesn't use one of these compilers, you'll need to download it and its GNU dependencies, and build it yourself (possible but tedious). Note that GCC mande ABI changes with the switch from 4.x to 5.x, so expect to not be able to use anything compiled with 4.x together with code compiled by 5.x/6.x and vice-versa. Even GCC versions differing on a minor number may produce incompatible code.
* The **Boost C++ libraries**, of a recent version. You may not be able to use your Linux distribution's version if they're compiled with a newer GCC, so you should probably [download and build it](http://www.boost.org/doc/libs/1_62_0/more/getting_started/unix-variants.html) yourself.
* Duane Merill's **libCUB**. That's not packaged by (almost) anyone, and it's a header-only library, so there's no building to do. Just download it from its [website](https://nvlabs.github.io/cub/index.html).
* The **CMake** build system, version 3.0 or later (version 2.8 might work, but no promises). This should definitely be packaged by your Linux distribution, no need to build anything yourself.
* **GNU Make** obviously.

#### Configuration: ####

Having cloned the repository, run `cmake` in the directory you have it in. It should give you meaningful error messages if it fails.

* You need to make CMake prefer the appropriate gcc and g++ versions over your distribution's default versions
* You need to make CMake choose the appropriate version of the Boost libraries and header files, again over your distribution's packaged version (which is very often installed)
* CUDA paths need to be in the environment

After `cmake`'ing successfully, run `make`. If all goes well, you will have built `bin/tester`

### Running the tester ###

Running `bin/tester` with no arguments will give you the tester's usage information and various option descriptions. 

* `bin/tester -T` will list all available kernel test adapters you can run
* `bin/tester -P` will list the relevant capabilities of CUDA devices visible on your system (and is a good way to check you can actually use any of them)


### "I'm interested in more information, in working on some of the code or in collaboration"  ###

Currently, there's just one developer - me, Eyal Rozenberg. Do [contact me](mail:E.Rozenberg@cwi.nl) if you know some CUDA / OpenCL, and...

- You've started an M.Sc. / Ph.D. in some field related to data processing, and want to work on optimized GPU implementations.
- You're an SW engineer in some company and are about to write so GPU code to handle data for your application - and there's a chance this could be done in collaboration with an outside party and as free software.
- You are an enthusiast who has some crazy GPU code to release, but it's not a really good fit to suggest it to another project.
- You've just started programming GPUs, and want to try out some interesting coding challenges in exchange for code review/discussion (and public acknowledgement of your contribution of course).

#### "... but I don't have a GPU / the right hardware/software :-(" ####

If you are interested in trying this out and possibly contributing your own code, or improving existing code, it may be still be possible with remote access to the CWI DB architecture group's [SciLens](http://www.scilens.org/) cluster - please contact me (see information below).

### Some context information about myself and where I work ###

This is part of my work with the DB architectures group](https://www.cwi.nl/research-groups/database-architectures) at [CWI](http://www.cwi.nl), Amsterdam, which does several interesting things, including the development and maintenance of the [MonetDB](http://www.monetdb.org/) column-oriented (CPU-only) analytic DBMS. It's a great FOSS product, quite mature and deserves a lot more publicity. It's not directly related to this repository however.

One of the senior group members is my host, of sorts - Prof. Dr. [Peter Boncz](http://homepages.cwi.nl/~boncz/). He has been the driving force, academically, behind [Vectorwise](https://en.wikipedia.org/wiki/Vectorwise) (now "Actian Vector"), a front-runner in TPC-H benchmark scores. It's worthwhile to check out his publications regarding lightweight compression schemes for DBMSes, main-memory Join optimizations, 'vectorized' (read: in-cache chunk based) query processing and so on - despite being all essentially about CPU processing, many of the ideas and approaches are interesting to apply, adapt or even refute when GPUs are involved.

